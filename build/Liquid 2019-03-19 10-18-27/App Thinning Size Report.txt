
App Thinning Size Report for All Variants of Liquid

Variant: Liquid-0625BFAF-5F77-4B9C-A776-04FBDC37FD13.ipa
Supported variant descriptors: [device: iPad7,4, os-version: 11], [device: iPad7,2, os-version: 11], [device: iPad6,4, os-version: 11], [device: iPad6,3, os-version: 11], [device: iPad7,3, os-version: 11], and [device: iPad7,1, os-version: 11]
App + On Demand Resources size: 29.3 MB compressed, 75.3 MB uncompressed
App size: 29.3 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-07074FAC-D41A-4011-917E-F247EC7F4B60.ipa
Supported variant descriptors: [device: iPad4,2, os-version: 11], [device: iPad6,11, os-version: 11], [device: iPad7,6, os-version: 11], [device: iPad4,1, os-version: 11], [device: iPad4,6, os-version: 11], [device: iPad4,8, os-version: 11], [device: iPad5,1, os-version: 11], [device: iPad4,4, os-version: 11], [device: iPad4,9, os-version: 11], [device: iPad6,12, os-version: 11], [device: iPad6,8, os-version: 11], [device: iPad4,3, os-version: 11], [device: iPad4,7, os-version: 11], [device: iPad7,5, os-version: 11], [device: iPad5,3, os-version: 11], [device: iPad5,4, os-version: 11], [device: iPad5,2, os-version: 11], [device: iPad4,5, os-version: 11], and [device: iPad6,7, os-version: 11]
App + On Demand Resources size: 29.2 MB compressed, 75.3 MB uncompressed
App size: 29.2 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-0D2A46D5-8E66-4F1A-AD38-2FE6AC74A66A.ipa
Supported variant descriptors: [device: iPhone7,2, os-version: 11], [device: iPod7,1, os-version: 11], [device: iPhone6,1, os-version: 11], [device: iPhone8,1, os-version: 11], [device: iPhone8,4, os-version: 11], and [device: iPhone6,2, os-version: 11]
App + On Demand Resources size: 29.2 MB compressed, 75.3 MB uncompressed
App size: 29.2 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-154DC13B-0EB6-4064-B240-C2153585AD39.ipa
Supported variant descriptors: [device: iPhone10,1, os-version: 12], [device: iPhone9,3, os-version: 12], [device: iPhone9,1, os-version: 12], and [device: iPhone10,4, os-version: 12]
App + On Demand Resources size: 26.7 MB compressed, 75.3 MB uncompressed
App size: 26.7 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-16C2D2E2-DD62-4B6D-B810-7B9C198D5572.ipa
Supported variant descriptors: [device: iPhone9,1, os-version: 11], [device: iPhone9,3, os-version: 11], [device: iPhone10,1, os-version: 11], and [device: iPhone10,4, os-version: 11]
App + On Demand Resources size: 29.3 MB compressed, 75.3 MB uncompressed
App size: 29.3 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-28AC4434-6A06-4A75-88F8-DE13C36B0B8A.ipa
Supported variant descriptors: [device: iPad4,9, os-version: 12], [device: iPad6,8, os-version: 12], [device: iPad4,4, os-version: 12], [device: iPad4,3, os-version: 12], [device: iPad4,5, os-version: 12], [device: iPad4,8, os-version: 12], [device: iPad5,3, os-version: 12], [device: iPad4,7, os-version: 12], [device: iPad4,1, os-version: 12], [device: iPad5,4, os-version: 12], [device: iPad6,12, os-version: 12], [device: iPad7,6, os-version: 12], [device: iPad7,5, os-version: 12], [device: iPad6,11, os-version: 12], [device: iPad4,6, os-version: 12], [device: iPad5,1, os-version: 12], [device: iPad4,2, os-version: 12], [device: iPad5,2, os-version: 12], and [device: iPad6,7, os-version: 12]
App + On Demand Resources size: 26.7 MB compressed, 75.3 MB uncompressed
App size: 26.7 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-2BE46713-D35A-4741-8572-27604A974C63.ipa
Supported variant descriptors: [device: iPhone8,1, os-version: 12], [device: iPhone6,2, os-version: 12], [device: iPhone6,1, os-version: 12], [device: iPod7,1, os-version: 12], [device: iPhone7,2, os-version: 12], and [device: iPhone8,4, os-version: 12]
App + On Demand Resources size: 26.6 MB compressed, 75.3 MB uncompressed
App size: 26.6 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-415AB27D-53F0-42D5-9FAD-63A2654B0D90.ipa
Supported variant descriptors: [device: iPhone11,2, os-version: 12], [device: iPhone10,6, os-version: 12], [device: iPhone9,4, os-version: 12], [device: iPhone11,4, os-version: 12], [device: iPhone10,2, os-version: 12], [device: iPhone10,5, os-version: 12], [device: iPhone10,3, os-version: 12], [device: iPhone11,6, os-version: 12], and [device: iPhone9,2, os-version: 12]
App + On Demand Resources size: 27.8 MB compressed, 75.3 MB uncompressed
App size: 27.8 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-48C4D0F3-81B7-44C4-AD0E-19EE3362891F.ipa
Supported variant descriptors: [device: iPad7,1, os-version: 12], [device: iPad8,3, os-version: 12], [device: iPad6,4, os-version: 12], [device: iPad6,3, os-version: 12], [device: iPad8,7, os-version: 12], [device: iPad8,4, os-version: 12], [device: iPad8,2, os-version: 12], [device: iPad7,3, os-version: 12], [device: iPad8,1, os-version: 12], [device: iPad8,5, os-version: 12], [device: iPad8,6, os-version: 12], [device: iPad8,8, os-version: 12], [device: iPad7,4, os-version: 12], and [device: iPad7,2, os-version: 12]
App + On Demand Resources size: 26.7 MB compressed, 75.3 MB uncompressed
App size: 26.7 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-7A0D100E-3050-4F36-B560-A73DF3320399.ipa
Supported variant descriptors: [device: iPhone11,8, os-version: 12]
App + On Demand Resources size: 26.7 MB compressed, 75.3 MB uncompressed
App size: 26.7 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-84412A12-8FD0-4F97-88AB-4B3443B6D25D.ipa
Supported variant descriptors: [device: iPhone9,2, os-version: 11], [device: iPhone10,6, os-version: 11], [device: iPhone10,3, os-version: 11], [device: iPhone10,2, os-version: 11], [device: iPhone10,5, os-version: 11], and [device: iPhone9,4, os-version: 11]
App + On Demand Resources size: 30.7 MB compressed, 75.3 MB uncompressed
App size: 30.7 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-9427ADBB-0187-4CA3-A6DB-4EE35ABEED2F.ipa
Supported variant descriptors: [device: iPhone7,1, os-version: 11] and [device: iPhone8,2, os-version: 11]
App + On Demand Resources size: 30.5 MB compressed, 75.3 MB uncompressed
App size: 30.5 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid-CE9B2CD6-F3CC-45DE-AF92-A6525388B890.ipa
Supported variant descriptors: [device: iPhone8,2, os-version: 12]
App + On Demand Resources size: 27.6 MB compressed, 75.3 MB uncompressed
App size: 27.6 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed


Variant: Liquid.ipa
Supported variant descriptors: Universal
App + On Demand Resources size: 42.7 MB compressed, 75.3 MB uncompressed
App size: 42.7 MB compressed, 75.3 MB uncompressed
On Demand Resources size: Zero KB compressed, Zero KB uncompressed
