//
//  AppDelegate.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase
import UserNotifications
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps
import Instabug
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var googleKey = Mics.getGoogleApiKey()
    var keys: NSDictionary?
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //Instabug.start(withToken: "1782d7755e5658c5eb723569a4116bcb", invocationEvents: [.shake, .screenshot])
        Fabric.with([Crashlytics.self])
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        var preferredStatusBarStyle : UIStatusBarStyle {
            return .lightContent
        }

        //application.statusBarStyle = .default
        
        Mics.setMainTheme()
        
        //realm migration
        var config = Realm.Configuration(
            schemaVersion: 27,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                }
        })
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerForFCM(application: application)
                
        Realm.Configuration.defaultConfiguration = config
        config = Realm.Configuration()
        IQKeyboardManager.shared.enable = true
        
        GMSPlacesClient.provideAPIKey(googleKey)
        GMSServices.provideAPIKey(googleKey)
        
        //print realm file
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        //MARK:- calling for session purposes
        Timer.scheduledTimer(timeInterval: 540.0, target: self, selector: #selector(AppDelegate.refreshToken), userInfo: nil, repeats: true).fire()
       
        let user = User.getUser()
            
        if user == nil || user!.accountStatus == UserStatus.NOT_VERIFIED.rawValue || user!.firstName.isEmpty || !user!.hasAddress || !user!.hasIdDocument || !user!.hasAcceptedTermsOfAgreement {
            Mics.setOnboardTheme()
            
            //check app version
            let vc = OnboardViewController()
            vc.modalPresentationStyle = .fullScreen
            self.window?.rootViewController = vc
            
        } else if user!.isLocked || Mics.loginRequired()  {
            Mics.setOnboardTheme()
            
            
            let destination = LockScreenController()
            destination.modalPresentationStyle = .fullScreen
            window?.rootViewController = destination
            
        } else {
            let notification: [AnyHashable: Any]? = (launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable: Any])
            if notification != nil {
                self.startController(userInfo: notification!)
                self.application(application, didReceiveRemoteNotification: notification!)
            }   else {
                window?.rootViewController = HomeTabController()
            }
        }
        
        //window?.rootViewController = UINavigationController(rootViewController: NewPasswordController())
        return true
    }
    
    
    @objc func refreshToken()
    {
       let user = User.getUser()
        if user != nil  {
            ApiService().getReferrals { (status, friends, message) in
                if status == ApiCallStatus.SUCCESS {
                   Mics.updateLoginTime()
                }
            }
        }
    }

    
    func registerForFCM(application: UIApplication)  {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        let notificationCount = UserDefaults.standard.integer(forKey: Key.notificationCount)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount
        Messaging.messaging().shouldEstablishDirectChannel = false
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        NotificationCenter.default.post(name: NSNotification.Name("checkUpdate"), object: self)
        if application.applicationIconBadgeNumber > 0 {
            application.applicationIconBadgeNumber = 0
        }
        //update notification badge
        UserDefaults.standard.set(0, forKey: Key.notificationCount)
        connectToFcm()
    }

    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        self.processNotificationData(userInfo: userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [START refresh_token]
    func tokenRefreshNotification(_ notification: Notification) {
        connectToFcm()
    }
    // [END refresh_token]
    
    
    // [START connect_to_fcm]
    func connectToFcm() {
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        Messaging.messaging().shouldEstablishDirectChannel = false
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    // [END connect_to_fcm]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        if let _ = InstanceID.instanceID().token() {
            if User.getUser() != nil {
                ApiService().updateDeviceGcm { (status, withMessage) in
                }
            }
        }
    }
    
    func startController(userInfo:[AnyHashable:Any]) {
        if let action = userInfo["action"] as? String {
            switch action {
            case "TRANSACTION":
                let vc = UINavigationController(rootViewController: HomeTabController())
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                break
            default:
                break
            }
        }
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        print("CALLED NOTIFICATION \(userInfo)")
        completionHandler([.alert, .badge, .sound])
    }
    
    
    private func updateBadge() {
        let notificationCount = UserDefaults.standard.integer(forKey: Key.notificationCount)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount + 1
        UserDefaults.standard.set(notificationCount + 1, forKey: Key.notificationCount)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        print("ACTION TAKEN \(userInfo)")
        UIApplication.shared.applicationIconBadgeNumber = 0
        startController(userInfo: userInfo)
        completionHandler()
    }
    
    
    func processNotificationData(userInfo: [AnyHashable: Any]) {
        
        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = "IDENTIFIER"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let requestIdentifier = "IDENTIFIER"
        
        if let action = userInfo["click_action"] as? String {
            print("ACTION \(action)")
            switch action {
            case "NOTIFICATION_ASSIGNED":
                let _ = userInfo["requestId"] as! String
                let title = userInfo["title"] as! String
                let message = userInfo["message"] as! String
                
                content.title = title
                content.body = message
                content.userInfo = userInfo
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error) in
                    print(error as Any)
                }
                break
            default:
                break
            }
        }
    }
    
}


// [START ios_10_data_message_handling]
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        if let _ = InstanceID.instanceID().token() {
            if User.getUser() != nil {
                ApiService().updateDeviceGcm { (status, withMessage) in
                    
                }
            }
        }
    }
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    func application(received remoteMessage: MessagingRemoteMessage) {
        self.updateBadge()
        print("DATA MESSAGE IN \(remoteMessage.appData)")
        Messaging.messaging().appDidReceiveMessage(remoteMessage.appData)
        self.processNotificationData(userInfo: remoteMessage.appData)
    }
}


