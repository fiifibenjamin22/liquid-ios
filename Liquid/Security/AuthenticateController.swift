//
//  AuthenticateController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 18/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import SmileLock

protocol handleSecurityUpdate {
    func isUpdated(updated: Bool)
}

class AuthenticateController: ControllerWithBack {
    
    var firstPin = ""
    var pinDelegate : PinDelegate?
    var isAuthOnly = false
    var isDisableAction = false
    let viewController = ViewControllerHelper()
    let apiService = ApiService()
    
    var hasUpdate: Bool = false
    var delegate: handleSecurityUpdate?
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.backgroundColor = UIColor.clear
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.text = "Please enter your passcode to proceed."
        return textView
    }()
    
    lazy var passwordContainerView: PasswordContainerView = {
        let kPasswordDigit = 4
        let passwordContainerView = PasswordContainerView.create(withDigit: kPasswordDigit)
        passwordContainerView.isVibrancyEffect = true
        passwordContainerView.deleteButton.setTitle("Clear", for: .normal)
        passwordContainerView.deleteButton.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        passwordContainerView.delegate = self
        passwordContainerView.touchAuthenticationEnabled = false
        passwordContainerView.backgroundColor = UIColor.clear
        passwordContainerView.touchAuthenticationReason = "Use touch ID to confirm"
        //customize password UI
        passwordContainerView.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        passwordContainerView.highlightedColor = UIColor.hex(hex: Key.secondaryHexCode)
        passwordContainerView.translatesAutoresizingMaskIntoConstraints = false
        return passwordContainerView
    }()
    
    let bgImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: Images.barBg)
        return imageView
    }()
    
//    lazy var forgotBtn: UIButton = {
//        let fBtn = UIButton(type: .system)
//        fBtn.setTitle("Forgot?", for: .normal)
//        fBtn.setTitleColor(.white, for: .normal)
//        fBtn.titleLabel?.font.withSize(24)
//        fBtn.isHidden = true
//        fBtn.addTarget(self, action: #selector(gotoSupport), for: .touchUpInside)
//        fBtn.translatesAutoresizingMaskIntoConstraints = false
//        return fBtn
//    }()
    
    lazy var forgotBtnOntop: UILabel = {
        let fbtn = UILabel()
        fbtn.text = "Forgot?"
        fbtn.textColor = .white
        fbtn.isHidden = true
        fbtn.font = UIFont.systemFont(ofSize: 19)
        fbtn.isUserInteractionEnabled = true
        fbtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoSupport)))
        fbtn.translatesAutoresizingMaskIntoConstraints = false
        return fbtn
    }()
    
    lazy var forgotBtn: UILabel = {
        let fbtn = UILabel()
        fbtn.text = "Forgot?"
        fbtn.textColor = .white
        fbtn.isHidden = true
        fbtn.font = UIFont.systemFont(ofSize: 19)
        fbtn.isUserInteractionEnabled = true
        fbtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoSupport)))
        fbtn.translatesAutoresizingMaskIntoConstraints = false
        return fbtn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        if fingerPrintEnabled && isAuthOnly {
            passwordContainerView.touchAuthenticationEnabled = true
        }
        
        if fingerPrintEnabled == true {
            self.forgotBtnOntop.isHidden = true
            self.forgotBtn.isHidden = false
        }else{
            self.forgotBtnOntop.isHidden = false
            self.forgotBtn.isHidden = true
        }
        
        let passcodeNewEntry = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        //let passcodeStatusValue = UserDefaults.standard.string(forKey: Key.passcodeEnabled)
        if (passcodeNewEntry == false) {
            setUpNavigationBar(title: "Passcode")
            self.messageTextView.text = "Set new passcode"
        }else{
            setUpNavigationBar(title: "Authenticate")
            self.messageTextView.text = "Enter passcode to disable"
            //self.forgotBtn.isHidden = false
            //self.forgotBtnOntop.isHidden = false
        }
    
        self.view.backgroundColor = UIColor.clear
        self.view.addSubview(bgImageView)
        self.view.addSubview(messageTextView)
        self.view.addSubview(passwordContainerView)
        self.view.addSubview(forgotBtnOntop)
        self.passwordContainerView.addSubview(forgotBtn)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
         self.passwordContainerView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.passwordContainerView.widthAnchor.constraint(equalToConstant: 280).isActive = true
        self.passwordContainerView.heightAnchor.constraint(equalToConstant: 400).isActive = true
        self.passwordContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.forgotBtnOntop.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 50).isActive = true
        self.forgotBtnOntop.widthAnchor.constraint(equalToConstant: 70)
        self.forgotBtnOntop.heightAnchor.constraint(equalToConstant: 45).isActive = true
        self.forgotBtnOntop.bottomAnchor.constraint(equalTo: self.passwordContainerView.bottomAnchor, constant: -12).isActive = true
        self.forgotBtnOntop.backgroundColor = .clear
        self.forgotBtnOntop.textAlignment = .left
        
        self.forgotBtn.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 50).isActive = true
        self.forgotBtn.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -45).isActive = true
        self.forgotBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        self.forgotBtn.topAnchor.constraint(equalTo: self.passwordContainerView.bottomAnchor, constant: 12).isActive = true
        self.forgotBtn.backgroundColor = .clear
        self.forgotBtn.textAlignment = .left
        
        self.bgImageView.anchorWithConstantsToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        if self.isAuthOnly {
           self.messageTextView.text = "Enter passcode"
        }
    }
    
    //delete action
    @objc func deleteAction(){
        self.passwordContainerView.clearInput()
    }
    
    @objc func gotoSupport(){
        let alert = UIAlertController(title: "Alert", message: "Please contact our support for assistance with resetting your passcode", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (finished) in
            let vc = SupportController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (cancelled) in
            self.navigationController?.popViewController(animated: true)
        }
        
        alert.addAction(cancel)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}

extension AuthenticateController: PasswordInputCompleteProtocol {
    
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        print("Auth complete \(input)")
        
        let savedPin = Array(UserDefaults.standard.dictionaryRepresentation())
        print("after input: ",self.isAuthOnly,", disabled: ", self.isDisableAction," pin: ",savedPin)

        //MARK: if the action is auth as well as disable action
        if (self.isAuthOnly == true) || (self.isDisableAction == true) {
             guard let pin = UserDefaults.standard.string(forKey: Key.fingerPrintPin) else {
               self.verifyPasscode(pin: input)
               return
             }
            
            //check if passcode is correct
             if  pin == input {
                print("user passcode compered: ","pin: ",pin," input: ",input)
                if self.isAuthOnly {
                    self.showPinActivated(pin: "")
                }else{
                    if self.isDisableAction {
                        self.showDisableAction(pin: input)
                    }
                }
                
             }  else {
                passwordContainerView.clearInput()
                self.messageTextView.text = "Invalid passcode"
            }
            return
        }
        
        //MARK: first pin action
        if firstPin.isEmpty {
           self.firstPin = input
           self.messageTextView.text = "Please confirm passcode"
           passwordContainerView.clearInput()
        }  else if firstPin == input {
            self.showPinActivated(pin: firstPin)
        }  else if firstPin != input {
           self.firstPin = ""
           passwordContainerView.clearInput()
           self.messageTextView.text = "Passcode does not match"
        }
    }
    
    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            self.showPinActivated(pin: "")
        } else {
            self.messageTextView.text = "Failed to set Passcode"
            passwordContainerView.clearInput()
        }
    }
    
    func showPinActivated(pin: String) {
        if let pinDel = self.pinDelegate {
            pinDel.didSetPin()
            self.dismiss(animated: true, completion: nil)
        }  else {
            self.viewController.showActivityIndicator()
            self.apiService.enablePasscode(passcode: Int(pin)!) { (status, withMessage) in
                  self.viewController.hideActivityIndicator()
                  if status == ApiCallStatus.SUCCESS {
                     ViewControllerHelper.showPrompt(vc: self, message: "Passcode activated successfully") { (done) in
                        UserDefaults.standard.set(true, forKey: Key.passcodeEnabled)
                        UserDefaults.standard.set(pin, forKey: Key.fingerPrintPin)
                        NotificationCenter.default.post(name: NSNotification.Name("enablePasscode"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name("switchState"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }
                  } else {
                     ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
        }
    }
    
    func showDisableAction(pin: String) {
        self.viewController.showActivityIndicator()
        print("user entered pin: ",pin)
        self.apiService.disablePasscode(passcode: Int(pin)!) { (status, withMessage) in
            self.viewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage) { (done) in
                    UserDefaults.standard.set(false, forKey: Key.passcodeEnabled)
                    self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage, completion: { (finished) in
                    self.passwordContainerView.clearInput()
                })
            }
        }
    }
    
    func verifyPasscode(pin: String) {
        self.viewController.showActivityIndicator()
        self.apiService.verifyPasscode(passcode: Int(pin)!) { (status, withMessage) in
            self.viewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                
                if self.isAuthOnly {
                    self.pinDelegate?.didSetPin()
                    UserDefaults.standard.set(pin, forKey: Key.fingerPrintPin)
                    self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage) { (done) in
                        self.pinDelegate?.didSetPin()
                        UserDefaults.standard.set(pin, forKey: Key.fingerPrintPin)
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }

}


