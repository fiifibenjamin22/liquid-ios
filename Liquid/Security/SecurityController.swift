//
//  SecurityController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import BiometricAuthentication

class SecurityController: BaseScrollViewController {
    
    let viewController = ViewControllerHelper()
    let apiService = ApiService()
    
    lazy var enableUILabel: UIButton = {
        let button =  self.getButton(title: "Enable Passcode")
        return button
    }()
    
    let enableUISwitch: UISwitch = {
        let uiSwitch = ViewControllerHelper.baseUISwitch()
        uiSwitch.addTarget(self, action: #selector(enablePasscode(_:)), for: UIControl.Event.touchUpInside)
        return uiSwitch
    }()
    
    let enableSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    lazy var fingerPrintUILabel: UIButton = {
        let button =  self.getButton(title: "Enable Finger Print")
        return button
    }()
    
    let fingerPrintUISwitch: UISwitch = {
        let uiSwitch = ViewControllerHelper.baseUISwitch()
        uiSwitch.addTarget(self, action: #selector(enableFingerPrintAction(_:)), for: UIControl.Event.touchUpInside)
        return uiSwitch
    }()
    
    let fingerPrintSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    lazy var setOrChangePasswordUILabel: UIButton = {
        let button = self.getButton(title: "Set Passcode")
        let finerPrintStatus = UserDefaults.standard.string(forKey: Key.fingerPrintPin)
        if (finerPrintStatus != nil) {
            button.setTitle("Change Passcode", for: .normal)
        }
        button.addTarget(self, action: #selector(tapItem(_:)), for: .touchUpInside)
        return button
    }()
    
    let setSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    lazy var changePasscodeUILabel: UIButton = {
        let button = self.getButton(title: "Change Password")
        button.addTarget(self, action: #selector(tapItem(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var moreSecurityButton: UIButton = {
        let button = self.getButton(title: "Want to know more about security?")
        button.addTarget(self, action: #selector(moreSecurity(_:)), for: .touchUpInside)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.contentHorizontalAlignment = .center
        return button
    }()
    
    let changeSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    
    func getButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return button
    }
    
    
    override func setUpView() {
        super.setUpView()
        
        setUpNavigationBarWhite(title: "Security")
        
        let width = self.view.frame.width
       
        self.scrollView.addSubview(enableUILabel)
        self.scrollView.addSubview(enableSplitLine)
        self.scrollView.addSubview(enableUISwitch)
        self.scrollView.addSubview(fingerPrintUILabel)
        self.scrollView.addSubview(fingerPrintSplitLine)
        self.scrollView.addSubview(fingerPrintUISwitch)
        self.scrollView.addSubview(setOrChangePasswordUILabel)
        self.scrollView.addSubview(setSplitLine)
        self.scrollView.addSubview(changePasscodeUILabel)
        self.scrollView.addSubview(changeSplitLine)
        self.scrollView.addSubview(moreSecurityButton)
        
        self.enableUILabel.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: enableUISwitch.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.enableUILabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.enableUILabel.widthAnchor.constraint(equalToConstant: width - 50 - 32 - 16).isActive = true
        
        self.enableUISwitch.anchorWithConstantsToTop(top: scrollView.topAnchor, left: nil, bottom: nil, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.enableUISwitch.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.enableUISwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.enableSplitLine.anchorWithConstantsToTop(top: enableUILabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.enableSplitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        self.fingerPrintUILabel.anchorWithConstantsToTop(top: enableSplitLine.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: enableUISwitch.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.fingerPrintUILabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.fingerPrintUILabel.widthAnchor.constraint(equalToConstant: width - 50 - 32 - 16).isActive = true
        
        self.fingerPrintUISwitch.anchorWithConstantsToTop(top: enableSplitLine.bottomAnchor, left: nil, bottom: nil, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.fingerPrintUISwitch.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.fingerPrintUISwitch.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.fingerPrintSplitLine.anchorWithConstantsToTop(top: fingerPrintUILabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.fingerPrintSplitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        self.setOrChangePasswordUILabel.anchorWithConstantsToTop(top: fingerPrintSplitLine.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.setOrChangePasswordUILabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //MARK
        
        self.setSplitLine.anchorWithConstantsToTop(top: setOrChangePasswordUILabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.setSplitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        self.changePasscodeUILabel.anchorWithConstantsToTop(top: setSplitLine.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.changePasscodeUILabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.changeSplitLine.anchorWithConstantsToTop(top: changePasscodeUILabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.changeSplitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        self.moreSecurityButton.anchorWithConstantsToTop(top: changeSplitLine.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.moreSecurityButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUpdateNotification(_:)), name: NSNotification.Name("switchState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(enablingPasscode(_:)), name: NSNotification.Name("enablePasscode"), object: nil)
    }
    
    @objc func receiveUpdateNotification(_ notification: Notification){
        if self.fingerPrintUISwitch.isOn {
            self.fingerPrintUISwitch.isOn = false
            return
        }else{
            self.fingerPrintUISwitch.isOn = true
            return
        }
    }
    
    @objc func enablingPasscode(_ notification: Notification){
        self.showSwitchStatus()
        let finerPrintStatus = UserDefaults.standard.string(forKey: Key.fingerPrintPin)
        if (finerPrintStatus != nil) {
            self.setOrChangePasswordUILabel.setTitle("Change Passcode", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.showSwitchStatus()
    }
    
    func showSwitchStatus()  {
        let finerPrintStatus = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        print("finerPrintStatus: ",finerPrintStatus)
        if finerPrintStatus == true {
            self.setOrChangePasswordUILabel.setTitle("Change Passcode", for: .normal)
        }
        self.enableUISwitch.isOn = finerPrintStatus
        
        let finerPrintBioStatus = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        print("finerPrintBioStatus: ",finerPrintBioStatus)
        self.fingerPrintUISwitch.isOn = finerPrintBioStatus
    }
    
    @objc func enablePasscodeAction(_ sender: UISwitch){
       self.startAuthenticationSection()
    }
    
    @objc func enableFingerPrintAction(_ sender: UISwitch){
        self.tapBioMetricSection()
    }
    
    //change passcode
    @objc func enablePasscode(_ sender: UISwitch){
        if sender.isOn {
            print("enabled passcode")
            guard let pin = UserDefaults.standard.string(forKey: Key.fingerPrintPin) else {
                self.viewController.hideActivityIndicator()
                let destination = AuthenticateController()
                self.navigationController?.present(destination, animated: true, completion: nil)
                return
            }
            print("the passcode: ",pin)
            self.viewController.showActivityIndicator()
            self.apiService.enablePasscode(passcode: Int(pin)!) { (status, withMessage) in
                self.viewController.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    ViewControllerHelper.showPrompt(vc: self, message: "Passcode activated successfully") { (done) in
                        UserDefaults.standard.set(true, forKey: Key.passcodeEnabled)
                        UserDefaults.standard.set(pin, forKey: Key.fingerPrintPin)
                        //self.navigationController?.popViewController(animated: true)
                        //self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
        }
        
        if !sender.isOn {
            print("disabled passcode")
            let destination = AuthenticateController()
            destination.isDisableAction = true
            destination.pinDelegate = self
            let nav = UINavigationController(rootViewController: destination)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func startAuthenticationSection()  {
        let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        print("passcode switch status: ",passcodeEnabled)
        if !passcodeEnabled {
            //set new pi here
            let destination = AuthenticateController()
            let nav = UINavigationController(rootViewController: destination)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        } else {
            
            //turn on passcode enabled (do not ask for password)
            if (enableUISwitch.isOn){
                UserDefaults.standard.set(true, forKey: Key.passcodeEnabled)
                let switchStatus = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
                print("turn on passcode status: ",switchStatus)
                return
            }else{
                //Disabling/enabling pin
                let destination = AuthenticateController()
                //destination.isDisableAction = true
                destination.pinDelegate = self
                let nav = UINavigationController(rootViewController: destination)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    @objc func tapItem(_ sender: UIButton)  {
        if sender == setOrChangePasswordUILabel {
            let passcodeStatus = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
            let userPasscode = UserDefaults.standard.string(forKey: Key.fingerPrintPin)
            
            print("passcode settings: ",passcodeStatus," ",userPasscode ?? "");
            if (passcodeStatus && userPasscode != nil) || (!passcodeStatus && userPasscode != nil) {
                
                //change passcode (if the user has passcode)
                let destination = ChangePinController()//AuthenticateController()
                let nav = UINavigationController(rootViewController: destination)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            } else {
                
                //Add passcode (if user has no passcode)
                self.startAuthenticationSection()
            }
        } else if sender == changePasscodeUILabel {
           let destination = OldPasswordController()
           self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    @objc func moreSecurity(_ sender: UIButton)  {
        let destination = LQWebViewController()
        destination.url = "\(AppConstants.faqUrl)#security"
        destination.title = "FAQs"
        self.navigationController?.pushViewController(destination, animated: true)
    }
}

extension SecurityController: PinDelegate {
    
    func didSetPin() {
       UserDefaults.standard.set("", forKey: Key.fingerPrintPin)
       UserDefaults.standard.set(false, forKey: Key.passcodeEnabled)
       self.enableUISwitch.isOn = false
    }
    
    func didFailed() {
        
    }
    
    func didCanceled() {
        
    }
}

extension SecurityController {
    
    @objc func tapBioMetricSection(){
        // start authentication
        var fingerPrintMessage = "Scan fingerprint to enable touch ID"
        if !self.fingerPrintUISwitch.isOn {
            fingerPrintMessage = "Scan fingerprint to disable touch ID"
        }
        BioMetricAuthenticator.authenticateWithPasscode(reason: fingerPrintMessage, completion: { result in
            switch result {
            case .success( _):
                self.showPinActivated()
            case .failure(let error):
                // do nothing on canceled
                print("cancel error: ",error)
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "switchState"), object: ApiCallStatus.SUCCESS)

                if error == .canceledByUser {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "switchState"), object: ApiCallStatus.SUCCESS)
                }else if error == .canceledBySystem {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "switchState"), object: ApiCallStatus.SUCCESS)
                }
                    // device does not support biometric (face id or touch id) authentication
                else if error == .biometryNotAvailable {
                    self.fingerPrintUISwitch.isOn = false
                    self.showErrorAlert(message: error.message())
                }
                    // show alternatives on fallback button clicked
                else if error == .fallback {
                    // here we're entering username and password
                }
                    // No biometry enrolled in this device, ask user to register fingerprint or face
                else if error == .biometryNotEnrolled {
                    self.fingerPrintUISwitch.isOn = false
                    self.showGotoSettingsAlert(message: error.message())
                }
                    // Biometry is locked out now, because there were too many failed attempts.
                    // Need to enter device passcode to unlock.
                else if error == .biometryLockedout {
                    self.fingerPrintUISwitch.isOn = false
                    self.showPasscodeAuthentication(message: error.message())
                }
                    // show error on authentication failed
                else {
                    self.showErrorAlert(message: error.message())
                }
            default:
                return
            }
        })
    }
    
    
    // show passcode authentication
    func showPasscodeAuthentication(message: String) {
        BioMetricAuthenticator.authenticateWithPasscode(reason: "Scan fingerprint to enable touch ID", completion: { result in
            // passcode authentication success
            switch result {
            case .success( _):
                self.showPinActivated()
            case .failure(let error):
                print(error.message())
            default:
                return
            }
            
            
        })
    }
    
    func showAlert(title: String, message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showErrorAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showGotoSettingsAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message) { (done) in
            /*let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE")
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
            */
        }
    }
    
    func showPinActivated() {
            let fingerPrintBioStatus = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
            let message = fingerPrintBioStatus ? "Fingerprint disabled successfully" : "Fingerprint enabled successfully"
            ViewControllerHelper.showPrompt(vc: self, message: message) { (done) in
                UserDefaults.standard.set(!fingerPrintBioStatus, forKey: Key.fingerPrintEnabled)
            }
    }
    
    
}
