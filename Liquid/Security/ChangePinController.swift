//
//  ChangePinController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 11/03/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import SmileLock
import SwiftyJSON

class ChangePinController: ControllerWithBack {
    
    var oldPin = ""
    var newPin = ""
    var repeatNewPin = ""
    
    var pinDelegate : PinDelegate?
    let viewController = ViewControllerHelper()
    let apiService = ApiService()
    
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.backgroundColor = UIColor.clear
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.text = "Please enter your passcode to proceed."
        return textView
    }()
    
    lazy var passwordContainerView: PasswordContainerView = {
        let kPasswordDigit = 4
        let passwordContainerView = PasswordContainerView.create(withDigit: kPasswordDigit)
        passwordContainerView.isVibrancyEffect = true
        passwordContainerView.deleteButton.setTitle("Clear", for: .normal)
        passwordContainerView.deleteButtonLocalizedTitle = "Clear"
        passwordContainerView.deleteButton.addTarget(self, action: #selector(self.clearAction), for: .touchUpInside)
        
//        passwordContainerView.clearInput()
        passwordContainerView.delegate = self
        passwordContainerView.touchAuthenticationEnabled = false
        passwordContainerView.backgroundColor = UIColor.clear
        passwordContainerView.touchAuthenticationReason = "Use touch ID to confirm"
        //customize password UI
        passwordContainerView.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        passwordContainerView.highlightedColor = UIColor.hex(hex: Key.secondaryHexCode)
        passwordContainerView.translatesAutoresizingMaskIntoConstraints = false
        return passwordContainerView
    }()
    
//    lazy var forgotBtn: UIButton = {
//        let fBtn = UIButton(type: .system)
//        fBtn.setTitle("Forgot?", for: .normal)
//        fBtn.setTitleColor(.white, for: .normal)
//        fBtn.titleLabel?.font.withSize(19)
//        fBtn.isHidden = true
//        fBtn.addTarget(self, action: #selector(gotoSupport), for: .touchUpInside)
//        fBtn.translatesAutoresizingMaskIntoConstraints = false
//        return fBtn
//    }()
    
    lazy var forgotBtnOntop: UILabel = {
        let fbtn = UILabel()
        fbtn.text = "Forgot?"
        fbtn.textColor = .white
        fbtn.isHidden = true
        fbtn.font = UIFont.systemFont(ofSize: 19)
        fbtn.isUserInteractionEnabled = true
        fbtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoSupport)))
        fbtn.translatesAutoresizingMaskIntoConstraints = false
        return fbtn
    }()
    
    lazy var forgotBtn: UILabel = {
        let fbtn = UILabel()
        fbtn.text = "Forgot?"
        fbtn.textColor = .white
        fbtn.isHidden = true
        fbtn.font = UIFont.systemFont(ofSize: 19)
        fbtn.isUserInteractionEnabled = true
        fbtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoSupport)))
        fbtn.translatesAutoresizingMaskIntoConstraints = false
        return fbtn
    }()
    
    let bgImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: Images.barBg)
        return imageView
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setUpNavigationBar(title: "Authenticate")
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        
        if fingerPrintEnabled == true {
            self.forgotBtnOntop.isHidden = true
            self.forgotBtn.isHidden = false
        }else{
            self.forgotBtnOntop.isHidden = false
            self.forgotBtn.isHidden = true
        }
        
        let passcodeNewEntry = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        print("new passcode entry: ", passcodeNewEntry)
        if (passcodeNewEntry == true) {
            setUpNavigationBar(title: "Passcode")
            self.messageTextView.text = "Set new passcode"
        }else{
            setUpNavigationBar(title: "Passcode")
        }
        
        self.view.backgroundColor = UIColor.clear
        self.view.addSubview(bgImageView)
        self.view.addSubview(messageTextView)
        self.view.addSubview(passwordContainerView)
        self.view.addSubview(forgotBtnOntop)
        self.passwordContainerView.addSubview(forgotBtn)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.passwordContainerView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 30, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.passwordContainerView.widthAnchor.constraint(equalToConstant: 280).isActive = true
        self.passwordContainerView.heightAnchor.constraint(equalToConstant: 400).isActive = true
        self.passwordContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.forgotBtnOntop.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 50).isActive = true
        self.forgotBtnOntop.widthAnchor.constraint(equalToConstant: 70)
        self.forgotBtnOntop.heightAnchor.constraint(equalToConstant: 45).isActive = true
        self.forgotBtnOntop.bottomAnchor.constraint(equalTo: self.passwordContainerView.bottomAnchor, constant: -12).isActive = true
        self.forgotBtnOntop.backgroundColor = .clear
        self.forgotBtnOntop.textAlignment = .left
        
        self.forgotBtn.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 50).isActive = true
        self.forgotBtn.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -45).isActive = true
        self.forgotBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        self.forgotBtn.topAnchor.constraint(equalTo: self.passwordContainerView.bottomAnchor, constant: 12).isActive = true
        self.forgotBtn.backgroundColor = .clear
        self.forgotBtn.textAlignment = .left
        
        self.bgImageView.anchorWithConstantsToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        /*
        if self.isAuthOnly {
            self.messageTextView.text = "Enter PIN"
        }*/
        
        if self.oldPin == "" {
            self.messageTextView.text = "Enter old PASSCODE"
            
            if fingerPrintEnabled == true {
                self.forgotBtnOntop.isHidden = true
                self.forgotBtn.isHidden = false
            }else{
                self.forgotBtnOntop.isHidden = false
                self.forgotBtn.isHidden = true
            }
            
        } else if self.newPin == "" {
            self.messageTextView.text = "Enter new PASSCODE"
        } else if self.repeatNewPin == "" {
            self.messageTextView.text = "Enter new PASSCODE again"
        }
    }
    
    @objc func clearAction(){
        passwordContainerView.clearInput()
    }
    
    @objc func gotoSupport(){
        let alert = UIAlertController(title: "Alert", message: "Please contact our support for assistance with resetting your passcode", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (finished) in
            let vc = SupportController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (cancelled) in
            self.navigationController?.popViewController(animated: true)
        }
        
        alert.addAction(cancel)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ChangePinController: PasswordInputCompleteProtocol {
    
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        //MARK: if the action is auth as well as disable action
        let vc = ChangePinController()
        if self.oldPin == "" {
            let oldPassCode = UserDefaults.standard.string(forKey: Key.fingerPrintPin)
            if oldPassCode == input {
                vc.oldPin = input
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: "The Passcode you have provided is invalid") { (isCompleted) in
                    passwordContainerView.clearInput()
                }
            }
            
        } else if self.newPin == "" {
            vc.oldPin = self.oldPin
            vc.newPin = input
        } else if self.repeatNewPin == "" {
            vc.oldPin = self.oldPin
            vc.newPin = self.newPin
            vc.repeatNewPin = input
            
            if input != self.newPin {
                ViewControllerHelper.showPrompt(vc: self, message: "Your new passcodes do not match") { (done) in
                    self.navigationController?.popViewController(animated: true)
                    return
                }
            }
            
            self.viewController.showActivityIndicator()
            self.apiService.updatePasscode(oldPasscode: Int(self.oldPin)!, newPasscode: Int(self.newPin)!, completion: {(json, status, message) in
                self.viewController.hideActivityIndicator()
                if json == JSON.null {
                    ViewControllerHelper.showPrompt(vc: self, message: "Failed to change your passcode") { (done) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    return
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: "Successfully changed your passcode", completion: { (done) in
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            })
            
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            self.showPinActivated(pin: "")
        } else {
            self.messageTextView.text = "Failed to set Passcode"
            passwordContainerView.clearInput()
        }
    }
    
    func showPinActivated(pin: String) {
        if let pinDel = self.pinDelegate {
            pinDel.didSetPin()
            self.dismiss(animated: true, completion: nil)
        }  else {
            self.viewController.showActivityIndicator()
            self.apiService.enablePasscode(passcode: Int(pin)!) { (status, withMessage) in
                self.viewController.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    ViewControllerHelper.showPrompt(vc: self, message: "Passcode activated successfully") { (done) in
                        UserDefaults.standard.set(true, forKey: Key.passcodeEnabled)
                        UserDefaults.standard.set(pin, forKey: Key.fingerPrintPin)
                        NotificationCenter.default.post(name: NSNotification.Name("enablePasscode"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name("switchState"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
        }
    }
    
    func showDisableAction(pin: String) {
        self.viewController.showActivityIndicator()
        self.apiService.disablePasscode(passcode: Int(pin)!) { (status, withMessage) in
            self.viewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage) { (done) in
                    UserDefaults.standard.set(false, forKey: Key.passcodeEnabled)
                    self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func verifyPasscode(pin: String) {
        self.viewController.showActivityIndicator()
        self.apiService.verifyPasscode(passcode: Int(pin)!) { (status, withMessage) in
            self.viewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage) { (done) in
                    self.pinDelegate?.didSetPin()
                    UserDefaults.standard.set(pin, forKey: Key.fingerPrintPin)
                    self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
}


