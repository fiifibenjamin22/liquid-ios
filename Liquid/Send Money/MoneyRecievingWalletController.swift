//
//  MoneyRecipientController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class MoneyRecievingWalletController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let cellSection = "cellHeading"
    let favouriteSection = "favouriteSection"
    var favourites = [FavouriteContact]()
    var favouriteCellSection: FavouriteView?
    
    var providers = [
        //ReceivingWalletType(name: "LIQUID Wallet", imageName: "WalletIcon",type: .LIQUID_ACCOUNT),
        ReceivingWalletType(name: "MTN Mobile Money",imageName: "MTNmomo",type: .MTN_MOMO),
        ReceivingWalletType(name: "Vodafone Cash",imageName: "VodafoneCash",type: .VODAFONE_CASH),
        ReceivingWalletType(name: "Airtel Money",imageName: "AirtelMoney",type: .AIRTEL_MONEY),
        ReceivingWalletType(name: "Tigo Cash",imageName: "TigoCash",type: .TIGO_CASH),
        //ReceivingWalletType(name: "Bank Account",imageName: "BankTransfer",type: .BANK_ACCOUNT_VIA_ACH),
        //ReceivingWalletType(name: "Direct Bank Transfer",imageName: "BankTransfer",type: .BANK_ACCOUNT_VIA_INSTAPAY)
    ]
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Send Money")
        
        self.showFavourite()
        self.setUpLayout()
    }
    
    func showFavourite()  {
        //MARK:- parse all provides
        self.favourites.removeAll()
        self.favourites.append(contentsOf: FavouriteContact.filterByTypeMoneyTransfer())
        if !favourites.isEmpty {
            let favouriteSection = ReceivingWalletType(name: "My Favourites", imageName: "", type: .ALL)
            self.providers.insert(favouriteSection, at: 0)
        }
        self.collectionView.reloadData()
    }
    
    func setUpLayout()  {
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.collectionView.register(ReceivingWalletCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(ProviderCellSection.self, forCellWithReuseIdentifier: cellSection)
        self.collectionView.register(FavouriteView.self, forCellWithReuseIdentifier: favouriteSection)
    }
    
}

extension MoneyRecievingWalletController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return providers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = providers[indexPath.row]
        
        //MARK: section label
        if item.type == .NONE {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellSection, for: indexPath) as! ProviderCellSection
            cell.item = ""
            return cell
        }
        
        //MARK: favourite section here
        if item.type == .ALL {
            favouriteCellSection =  collectionView.dequeueReusableCell(withReuseIdentifier: favouriteSection, for: indexPath) as? FavouriteView
            favouriteCellSection?.favourites = self.favourites
            favouriteCellSection?.delegate = self
            return favouriteCellSection!
        }
        
        //MARK: wallet section
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! ReceivingWalletCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = providers[indexPath.row]
        let itemWidth = collectionView.frame.width
        if item.type == .ALL {
            let itemHeight = CGFloat(120)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(walletType: self.providers[indexPath.row])
    }
    
    //MARK:- clicked wallet type
    func transactionClicked(walletType: ReceivingWalletType) {
       let destination = RecipientController()
       destination.isSendingMoney = true
       destination.provider = walletType.type
       destination.name = walletType.name
       self.navigationController?.pushFade(destination)
    }
}

extension MoneyRecievingWalletController: FavouriteDelegate {
    
    //selected the favourite contact
    func didSelectContact(contact: FavouriteContact) {
        let destination = TransactionAmountController()
        destination.contact = contact
        destination.name = "\(Mics.targetIssuerToName(target: contact.network))"
        destination.isSendingMoney = true
        destination.provider = TargetIssuer(rawValue: contact.network)
        self.navigationController?.pushFade(destination)
    }
    
    //view all saved
    func didSelectAll() {
        let destination = FavouriteListController()
        destination.delegate = self
        self.navigationController?.pushFade(destination)
    }
    
    //delete the favourite contact
    func removeContact(contact: FavouriteContact) {
        ViewControllerHelper.showRemoveFavouriteCotact(vc: self, item: contact) { (isDone) in
            if isDone {
                self.providers.remove(at: 0)
                self.showFavourite()
                if self.favourites.isEmpty {
                    self.providers.remove(at: 0)
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
}
