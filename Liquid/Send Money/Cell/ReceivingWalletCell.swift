//
//  ReceivingWalletCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class ReceivingWalletCell: BaseCell {
    
    var item: ReceivingWalletType?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = unwrapedItem.name
            self.iconImageView.image = UIImage(named: unwrapedItem.imageName)
        }
    }
    
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 18)!)
        return textView
    }()
    
    let arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "Next")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(iconImageView)
        addSubview(messageTextView)
        addSubview(arrowImageView)
        
        let frameHeight = frame.height - 8
        self.iconImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        self.iconImageView.widthAnchor.constraint(equalToConstant: frameHeight).isActive = true
        self.iconImageView.layer.cornerRadius = frameHeight/2
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: iconImageView.trailingAnchor, bottom: bottomAnchor, right: arrowImageView.leadingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        self.arrowImageView.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.arrowImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
    }
}




