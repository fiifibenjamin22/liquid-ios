//
//  GoalDestination.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/08/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class GoalDestination: Object {
    @objc dynamic var id = 0
    @objc dynamic var issuer = ""
    @objc dynamic var identifier = ""
    @objc dynamic var type = ""
 
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [GoalDestination] {
        let realm = try! Realm()
        return Array(realm.objects(GoalDestination.self))
    }
    
    //save item
    static func save(data: GoalDestination) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(GoalDestination.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
    
    
}

