//
//  RecurringDebit.swift
//  Liquid
//
//  Created by Benjamin Acquah on 26/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class RecurringDebit: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var payment_method = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var status = ""
    @objc dynamic var frequency = ""
    @objc dynamic var next_debit_date = ""
    @objc dynamic var goal_id = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get RecurringDebit saved into the db
    static func getRecurringDebit() -> RecurringDebit?{
        let realm = try! Realm()
        let rd = realm.objects(RecurringDebit.self).last
        return rd
    }
    
    //MARK:- save item
    static func save(data: RecurringDebit) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(RecurringDebit.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}
