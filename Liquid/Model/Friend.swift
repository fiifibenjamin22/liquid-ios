//
//  Friend.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class Friend: Object {
    @objc dynamic var inviteeName = ""
    @objc dynamic var inviteeContact = ""
    @objc dynamic var inviteeSelfieUrl = ""
  
    //MARK:- get all
    static func all() -> [Friend] {
        let realm = try! Realm()
        return Array(realm.objects(Friend.self))
    }

}
