//
//  Bank.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/02/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class Bank: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    static func updateBank(data: Bank) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //MARK:- get all
    static func all() -> [Bank] {
        let realm = try! Realm()
        return Array(realm.objects(Bank.self))
    }
}
