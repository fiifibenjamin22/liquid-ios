//
//  BillDetail.swift
//  Liquid
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class BillDetail: NSObject {
    var value = 0
    var name = ""
    var bundle = ""
}
