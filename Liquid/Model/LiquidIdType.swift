//
//  LiquidIdType.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class LiquidIdType: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
   
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [LiquidIdType] {
        let realm = try! Realm()
        return Array(realm.objects(LiquidIdType.self))
    }
    
    
    //get user saved into the db
    static func getId() -> LiquidIdType?{
        let realm = try! Realm()
        let device = realm.objects(LiquidIdType.self).first
        return device
    }
    
    //save item
    static func save(data: LiquidIdType) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(LiquidIdType.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}

