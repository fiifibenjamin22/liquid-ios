//
//  GoalActivity.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class GoalActivity: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var date = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var currency = ""
    @objc dynamic var issuer = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [GoalActivity] {
        let realm = try! Realm()
        return Array(realm.objects(GoalActivity.self))
    }
    
    //save item
    static func save(data: GoalActivity) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(GoalActivity.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}

