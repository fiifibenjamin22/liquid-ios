//
//  GoalCategory.swift
//  Liquid
//
//  Created by Benjamin Acquah on 11/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

//
//  Goal.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class GoalCategory: Object {
    @objc dynamic var categoryId = 0
    @objc dynamic var name = ""
    @objc dynamic var image = ""

    override class func primaryKey() -> String? {
        return "categoryId"
    }
    
    //MARK:- get all
    static func all() -> [GoalCategory] {
        let realm = try! Realm()
        return Array(realm.objects(GoalCategory.self))
    }
    
    //save item
    static func save(data: GoalCategory) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(GoalCategory.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}


