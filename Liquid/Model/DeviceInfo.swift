//
//  DeviceInfo.swift
//  Liquid
//
//  Created by Benjamin Acquah on 14/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class DeviceInfo: Object {
    @objc dynamic var uuid = ""
    @objc dynamic var deviceId = 0
    @objc dynamic var osVersion = ""
    @objc dynamic var appVersion = ""

    
    override class func primaryKey() -> String? {
        return "uuid"
    }
    
    //get user saved into the db
    static func getDevice() -> DeviceInfo?{
        let realm = try! Realm()
        let device = realm.objects(DeviceInfo.self).first
        return device
    }
    
    //save item
    static func save(data: DeviceInfo) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(DeviceInfo.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}
