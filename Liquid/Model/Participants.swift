//
//  Participants.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class Participants: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var total_contribution = 0.0
    @objc dynamic var status = ""
    @objc dynamic var phone = ""
    @objc dynamic var date_joined = ""
    @objc dynamic var picture = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- save item
    static func save(data: Participants) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //MARK:- get all
    static func all() -> [Participants] {
        let realm = try! Realm()
        return Array(realm.objects(Participants.self))
    }
    
    static func searchParticipants(name: String) -> [Participants] {
        let realm = try! Realm()
        let filtering = realm.objects(Participants.self).filter("name = '\(name)'")
        return Array(filtering)
    }
    
    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(Participants.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
    
    static func deleteParticipant(mobile:String){
        let realm = try! Realm()
        let all = realm.objects(Participants.self).filter("phone = '\(mobile)'")
        try! realm.write() {
            realm.delete(all)
        }
    }
}
