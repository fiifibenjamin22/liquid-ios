//
//  Notification.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//


import RealmSwift
import Foundation

class Notification: Object {
    @objc dynamic var id = 0
    @objc dynamic var type = ""
    @objc dynamic var message = ""
    @objc dynamic var time = ""
    @objc dynamic var dateTime = ""
    @objc dynamic var isRead = false
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [Notification] {
        let realm = try! Realm()
        return Array(realm.objects(Notification.self))
    }
    
    
    //save item
    static func save(data: Notification) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    
}

