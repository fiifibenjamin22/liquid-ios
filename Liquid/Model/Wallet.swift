//
//  Wallet.swift
//  Liquid
//
//  Created by Benjamin Acquah on 18/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class Wallet: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var amount = ""
    @objc dynamic var icon = ""
    @objc dynamic var paymentType = ""
    @objc dynamic var number = ""
    @objc dynamic var type = ""
    @objc dynamic var expiry = ""
    @objc dynamic var bank = ""
    @objc dynamic var payment_verified = false
    @objc dynamic var verification_status = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
        
    //MARK:- default wallet image
    static func getLiquidWalet() -> Wallet {
        let wallet = Wallet()
        wallet.icon = "SplashLogo"
        wallet.paymentType = Liquid.CODE.rawValue
        wallet.type = Liquid.CODE.rawValue
        wallet.name = AppConstants.defaultWalletName
        wallet.amount = "\(User.getUser()!.balance)"
        wallet.number = User.getUser()!.accountNumber
        return wallet
    }
    
    //MARK:- Bank transfer wallets
    static func bankTransfer() -> Wallet {
        let wallet = Wallet()
        wallet.icon = "bankTransfers"
        wallet.paymentType = BankTransfer.CODE.rawValue
        wallet.type = BankTransfer.NAME.rawValue
        wallet.name = AppConstants.bankTransferWalletName
        wallet.amount = "\(User.getUser()!.balance)"
        wallet.number = User.getUser()!.accountNumber
        return wallet
    }
    
    //MARK:- get all
    static func all() -> [Wallet] {
        let realm = try! Realm()
        return Array(realm.objects(Wallet.self))
    }
    
    static func allMomoWallets() -> [Wallet] {
        let realm = try! Realm()
        let filtering = realm.objects(Wallet.self).filter("paymentType = 'momo'")
        return Array(filtering)
    }
    
    //MARK:- save item
    static func save(data: Wallet) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(Wallet.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
    
    //MARK:- delete an item
    static func deleteWallet(walletId:Int){
        let realm = try! Realm()
        let all = realm.objects(Wallet.self).filter("id = %s", walletId)
        try! realm.write() {
            realm.delete(all)
        }
    }
}
