//
//  Occupation.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class Occupation: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [Occupation] {
        let realm = try! Realm()
        return Array(realm.objects(Occupation.self))
    }
    
    
    //get user saved into the db
    static func getId() -> Occupation?{
        let realm = try! Realm()
        let device = realm.objects(Occupation.self).first
        return device
    }
    
    //save item
    static func save(data: Occupation) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(Occupation.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}
