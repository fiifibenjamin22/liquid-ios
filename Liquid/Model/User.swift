//
//  User.swift
//  Liquid
//
//  Created by Benjamin Acquah on 14/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class User: Object {
    @objc dynamic var id = 0
    @objc dynamic var cutomerId = 0
    @objc dynamic var token = ""
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var otherName = ""
    @objc dynamic var email = ""
    @objc dynamic var occupation = ""
    @objc dynamic var citizenship = ""
    @objc dynamic var birthDate = ""
    @objc dynamic var picture = ""
    @objc dynamic var sex = ""
    @objc dynamic var phone = ""
    @objc dynamic var documentName = ""
    @objc dynamic var documentNumber = ""
    @objc dynamic var referralCode = ""
    @objc dynamic var accountId = 0
    @objc dynamic var accountNumber = ""
    @objc dynamic var accountStatus = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var streetAddress = ""
    @objc dynamic var digitalAddress = ""
    @objc dynamic var city = ""
    @objc dynamic var country = ""
    @objc dynamic var currency = "GHS"
    @objc dynamic var balance = 0.0
    
    @objc dynamic var goalCurrency = "GHS"
    @objc dynamic var goalBalance = 0.0
    @objc dynamic var totalGoals = 0
    @objc dynamic var password = ""
    
    //KYC section
    @objc dynamic var hasAcceptedTermsOfAgreement = false
    @objc dynamic var hasSelfie = false
    @objc dynamic var hasAddress = false
    
    @objc dynamic var hasExtraKyc = false
    
    @objc dynamic var hasIdDocument = false
    @objc dynamic var has_full_account = false
    
    //KYC skip section
    @objc dynamic var hasSkippedAcceptedTermsOfAgreement = false
    @objc dynamic var hasSkippedSelfie = false
    @objc dynamic var hasSkippedAddress = false
    
    @objc dynamic var hasSkippedExtraKyc1 = false
    @objc dynamic var hasSkippedExtraKyc2 = false
    @objc dynamic var hasSkippedExtraKyc3 = false
    
    @objc dynamic var hasSkippedIdDocument = false
    
    //
    @objc dynamic var userIsSecuringAccount = false
    //email status
    @objc dynamic var emailStatus = "NOT VERIFIED"
    @objc dynamic var hasNotification = false
  
    @objc dynamic var isLocked = false
    @objc dynamic var has_used_invite_code = false
    
    @objc dynamic var rank = ""
    @objc dynamic var points = 0
    
    @objc dynamic var totaldeposite = 0.0
    @objc dynamic var totalwithdrawals = 0.0
    @objc dynamic var totalbalance = 0.0
    @objc dynamic var totalearnings = 0.0
    
    @objc dynamic var completedGoals = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    static func updateUserTotalAmount(d: Double,w: Double,b: Double,e: Double,c: Int){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.totaldeposite = d
            data?.totalwithdrawals = w
            data?.totalbalance = b
            data?.totalearnings = e
            data?.completedGoals = c
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserCoach(r:String, p: Int){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.rank = r
            data?.points = p
            realm.add(data!,update: true)
        }
    }
    
    //MARK:- get user saved into the db
    static func getUser() -> User?{
        let realm = try! Realm()
        let user = realm.objects(User.self).last
        return user
    }
    
    //MARK:- save item
    static func save(data: User) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //MARK:- save item
    static func updateUser(data: User) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    static func setPassword(password: String) {
        let realm = try! Realm()
        let aes = SHA256()
        let user = User.getUser()!
        try! realm.write() {
            user.password = aes.encryptString(string: password)
        }
    }
    
    static func getPassword() -> String {
        let aes = SHA256()
        let user = User.getUser()!
        return aes.decryptString(string: user.password)
    }
    
    func setSecuringAccount(state: Bool){
        let realm = try! Realm()
        try! realm.write() {
            self.userIsSecuringAccount = state
        }
    }
    
    func setHasFullAccount(state: Bool){
        let realm = try! Realm()
        try! realm.write() {
            self.has_full_account = state
        }
    }
    //MARK: updating KYC information
    static func updateUserKYC(gender:String,citezenShip:String,occupation:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            
            if gender != "" {
                data?.sex = gender
            }
            
            if citezenShip != "" {
                data?.citizenship = citezenShip
            }
            
            if occupation != "" {
                data?.occupation = occupation
            }
            
            realm.add(data!,update: true)
        }
    }
    
    static func updateGender(gender: String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.sex = gender
            realm.add(data!,update: true)
        }
    }
    
    static func updateCitezenShip(citezenShip: String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.citizenship = citezenShip
            realm.add(data!,update: true)
        }
    }
    
    static func updateOccupation(occupation: String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.occupation = occupation
            realm.add(data!,update: true)
        }
    }
    
    //MARK: updating KYC information
    static func updateExtraUserKYC(documentType:String,documentNumber:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.documentName = documentType
            data?.documentNumber = documentNumber
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserTerms(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.hasAcceptedTermsOfAgreement = status
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserExtraInfo(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasExtraKyc = status
            realm.add(data!,update: true)
        }
    }
    
    static func setHasUsedInviteCode(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.has_used_invite_code = status
            realm.add(data!,update: true)
        }
    }
    static func lock(locked: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.isLocked = locked
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserAddress(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasAddress = status
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserSelfie(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasSelfie = status
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserEmailStatus(status:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.emailStatus = status
            realm.add(data!,update: true)
        }
    }
    
    static func updateUserDocument(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasIdDocument = status
            realm.add(data!,update: true)
        }
    }
    //MARK: updating kyc
    
    static func skipUserTerms(){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasSkippedAcceptedTermsOfAgreement = true
            realm.add(data!,update: true)
        }
    }
    
    static func skipUserExtraInfo1(){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasSkippedExtraKyc1 = true
            realm.add(data!,update: true)
        }
    }
    
    static func skipUserExtraInfo2(){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasSkippedExtraKyc2 = true
            realm.add(data!,update: true)
        }
    }
    
    static func skipUserExtraInfo3(){
        let realm = try! Realm()
        try! realm.write() {
            let data = getUser()
            data?.hasSkippedExtraKyc3 = true
            realm.add(data!,update: true)
        }
    }
    
    static func skipUserAddress(){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.hasSkippedAddress = true
            realm.add(data!,update: true)
        }
    }
    
    static func skipUserSelfie(){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.hasSkippedSelfie = true
            realm.add(data!,update: true)
        }
    }
    
    static func setSkipUserSelfieStatus(status: Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.hasSkippedSelfie = status
            realm.add(data!,update: true)
        }
    }
    
    static func skipUserDocument(){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.hasSkippedIdDocument = true
            realm.add(data!,update: true)
        }
    }
    //MARK: skipping kyc
    
    static func updateUserPic(url:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.picture = url
            realm.add(data!,update: true)
        }
    }
    
    static func updateReferralCode(code:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.referralCode = code
            realm.add(data!,update: true)
        }
    }
    
    static func updateUser(firstName:String,lastName:String,otherName:String,email:String,DOB:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.firstName = firstName
            data?.email = email
            data?.lastName = lastName
            data?.otherName = otherName
            data?.birthDate = DOB
            realm.add(data!,update: true)
        }
    }
    
    static func updateUser(customer:Int,firstName:String,lastName:String,otherName:String,email:String,DOB:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.cutomerId = customer
            data?.firstName = firstName
            data?.email = email
            data?.lastName = lastName
            data?.otherName = otherName
            data?.birthDate = DOB
            realm.add(data!,update: true)
        }
    }
    
    static func updateUser(customerId:Int,lat:Double,lon:Double,address:String,digAddress:String,city:String,country:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.cutomerId = customerId
            data?.latitude = lat
            data?.longitude = lon
            data?.streetAddress = address
            data?.digitalAddress = digAddress
            data?.city = city
            data?.country = country
            realm.add(data!,update: true)
        }
    }
    
    //MARK:- update user number
    static func updateUserNumber(number:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.phone = number
            realm.add(data!,update: true)
        }
    }
    
    //MARK:- update user email
    static func updateUserEmail(email: String){
        let realm = try! Realm()
        try! realm.write() {
            let user = User.getUser()!
            user.email = email
            realm.add(user,update: true)
        }
    }


    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(User.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
    
    static func updateExtraInfo(lat:Double,lon:Double,emailStatus:String,address:String){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.emailStatus = emailStatus
            data?.streetAddress = address
            data?.latitude = lat
            data?.longitude = lon
            realm.add(data!,update: true)
        }
    }
    
    static func saveMissingSteps(missingSteps: MissingSteps?) {
        if let steps = missingSteps {
            
            User.updateUserAddress(status: steps.hasAddress)
            User.updateUserSelfie(status: steps.hasSelfie)
            User.updateUserExtraInfo(status: steps.hasExtraKyc)
            User.updateUserTerms(status: steps.hasAcceptedTermsOfAgreement)
            User.updateUserDocument(status: steps.hasIdDocument)
        }
    }
    
    static func updateUserBalance(goalBalance:Double,accountBalance:Double,status:Bool){
        let realm = try! Realm()
        try! realm.write() {
            let data = realm.objects(User.self).first
            data?.goalBalance = goalBalance
            data?.balance = accountBalance
            data?.hasNotification = status
            realm.add(data!,update: true)
        }
    }
    
}

