//
//  GoalNextKin.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/08/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class GoalNextKin: Object {
    @objc dynamic var id = ""
    @objc dynamic var name = ""
   
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [GoalNextKin] {
        let realm = try! Realm()
        return Array(realm.objects(GoalNextKin.self))
    }
    
    //save item
    static func save(data: GoalNextKin) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(GoalNextKin.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
    
    
}
