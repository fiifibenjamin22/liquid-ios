//
//  CoachCategories.swift
//  Liquid
//
//  Created by Benjamin Acquah on 10/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import RealmSwift

class CoachCategories: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    var CoachTask = List<CoachTask>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get all
    static func all() -> [CoachCategories] {
        let realm = try! Realm()
        return Array(realm.objects(CoachCategories.self))
    }
    
    //save item
    static func save(data: CoachCategories) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(CoachCategories.self)
        try! realm.write() {
            realm.delete(all)
        }
    }

}
