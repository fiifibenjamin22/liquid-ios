//
//  FavouriteContact.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class FavouriteContact: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var network = ""
    @objc dynamic var number = ""
   
    override class func primaryKey() -> String? {
        return "id"
    }
    
    //MARK:- get user saved into the db
    static func getContact() -> FavouriteContact?{
        let realm = try! Realm()
        let user = realm.objects(FavouriteContact.self).first
        return user
    }

    //save item
    static func save(data: FavouriteContact) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    static func saveRemote(data: FavouriteContact){
        save(data: data)
        ApiService().saveContact(name: data.name, identifierType: "phone", identifier: data.number, network: data.network) { (status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                
            }
        }
    }
    
    //MARK:- get all
    static func all() -> [FavouriteContact] {
        let realm = try! Realm()
        return Array(realm.objects(FavouriteContact.self))
    }
    
    //MARK:- get all by key
    static func filterByTypeData() -> [FavouriteContact] {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "network == %@ OR network = %@", "\(TargetIssuer.SURFLINE_DATA.rawValue)", "\(TargetIssuer.BUSY4G_DATA.rawValue)")
        return Array(realm.objects(FavouriteContact.self).filter(predicate))
    }
    
    //MARK:- filter by airtime
    static func filterByTypeAirtime() -> [FavouriteContact] {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "network == %@ OR network = %@ OR network = %@ OR network = %@ OR network = %@" , "\(TargetIssuer.VODAFONE_AIRTIME.rawValue)", "\(TargetIssuer.MTN_AIRTIME.rawValue)", "\(TargetIssuer.TIGO_AIRTIME.rawValue)" , "\(TargetIssuer.AIRTEL_AIRTIME.rawValue)" , "\(TargetIssuer.GLO_AIRTIME.rawValue)")
        return Array(realm.objects(FavouriteContact.self).filter(predicate))
    }
    
    //MARK:- filter by money transfer
    static func filterByTypeMoneyTransfer() -> [FavouriteContact] {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "network == %@ OR network = %@ OR network = %@ OR network = %@ OR network = %@" , "\(TargetIssuer.VODAFONE_CASH.rawValue)", "\(TargetIssuer.MTN_MOMO.rawValue)", "\(TargetIssuer.AIRTEL_MONEY.rawValue)" , "\(TargetIssuer.TIGO_CASH.rawValue)" , "\(TargetIssuer.LIQUID_ACCOUNT.rawValue)")
        return Array(realm.objects(FavouriteContact.self).filter(predicate))
    }
    
    //MARK:- filter by utility
    static func filterByTypeUtility() -> [FavouriteContact] {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "network == %@ OR network = %@ OR network = %@ OR network = %@" , "\(TargetIssuer.BOXOFFICE_TV.rawValue)", "\(TargetIssuer.DSTV_TV.rawValue)", "\(TargetIssuer.ECG_POSTPAID_BILLPAYMENT.rawValue)" , "\(TargetIssuer.GOTV_TV.rawValue)")
        return Array(realm.objects(FavouriteContact.self).filter(predicate))
    }
    
    //MARK:- delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(FavouriteContact.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
    
    static func deleteConatct(contact: FavouriteContact){
        let realm = try! Realm()
        let predicate = NSPredicate(format: "number == %@" , "\(contact.number)")
        let item = realm.objects(FavouriteContact.self).filter(predicate).first
        
        let contactId = contact.id
        if let itemIn = item {
            try! realm.write() {
              realm.delete(itemIn)
            }
            //MARK: remove form server
            ApiService().removeContact(contactId: contactId) { (status, withMessage) in
                
            }
        }
        
    }
}

