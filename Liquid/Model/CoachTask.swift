//
//  CoachTask.swift
//  Liquid
//
//  Created by Benjamin Acquah on 10/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import RealmSwift

class CoachTask: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var taskDescription = ""
    @objc dynamic var taskValue = 0
    @objc dynamic var taskCompleted = false
    
    //MARK:- get all
    static func all() -> [CoachTask] {
        let realm = try! Realm()
        return Array(realm.objects(CoachTask.self))
    }
    
    //save item
    static func save(data: CoachTask) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(CoachTask.self)
        try! realm.write() {
            realm.delete(all)
        }
    }

}
