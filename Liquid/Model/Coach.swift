//
//  Coach.swift
//  Liquid
//
//  Created by Benjamin Acquah on 10/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import RealmSwift

class Coach: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var points = 0
    @objc dynamic var points_to_next_rank = 0
    @objc dynamic var rank = ""
    @objc dynamic var categoryItems = 0
    
    var CoachCategories = List<CoachCategories>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    static func getCoach() -> Coach?{
        let realm = try! Realm()
        let user = realm.objects(Coach.self).first
        return user
    }
    
    //save item
    static func save(data: Coach) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(Coach.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}
