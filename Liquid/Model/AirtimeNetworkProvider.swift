//
//  AirtimeProvider.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift

class AirtimeNetworkProvider: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var type = ""
   
    
    override class func primaryKey() -> String? {
        return "name"
    }
}
