//
//  LiquidStructs.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

struct Page {
    let imageName:String
    let title:String
    let message:String
    let bottomOffset: Float
    let topOffset: Float
    let number: Int
}

struct Helper {
    let imageName:String
    let action:String
    let message:String
}

struct Transaction {
    var title: String
    var imageName: String
    var type: TransactionType
}

struct AirtimeProvider {
    var name: String
    var imageName: String
    var type: TargetIssuer
}

struct ReceivingWalletType {
    var name: String
    var imageName: String
    var type: TargetIssuer
}

struct TokenType {
    var name: String
    var imageName: String
    var code: String
    var date: String
    var type: TokenProvider
}

struct QuickPrices {
    var amount: String
}

struct SettingItem {
    var title: String
    var type: SettingType
}

struct BalanceItem {
    var title: String
    var currency: String
    var amount: String
    var action: String
    var type: BalanceType
}

struct RulePeriodItem {
    var title: String
    var duration: Int
    var type: PaymentRule
}

struct UpgradeSteps {
    var title: String
    var message: String
    var imageName: String
}

struct SecurityItem {
    var title: String
    var isSwitch: Bool
}

struct ActionItem {
    var name: String
    var action: String
}

struct FAQItem {
    var title: String
    var content: String
    var isOpened: Bool
}

struct ConfirmationItem {
    var reference: String
    var amount: Double
    var fees: Double
    var total: Double
    var currency: String
    var transferVoucher: String
}

struct TempLocation {
    var number: String
    var subLocality: String
    var street: String
    var country: String
    var name: String
    var lat: Double
    var lon: Double
    var city: String
}

struct MissingSteps {
    var hasIdDocument: Bool
    var hasAcceptedTermsOfAgreement: Bool
    var hasAddress: Bool
    var hasSelfie: Bool
    var hasExtraKyc: Bool
}

struct WithdrawalDestination {
    var customerId: Int
    var issuer: String
    var identifier: String
    var type: String
    var name: String
}

struct GoalAssist {
    var goalId: Int
    var amount: Double
    var period: String
    var currency: String
}



