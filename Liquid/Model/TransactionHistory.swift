//
//  TransactionHistory.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//
import RealmSwift
import Foundation

class TransactionHistory: Object {
    @objc dynamic var id = 0
    @objc dynamic var type = ""
    @objc dynamic var recipientName = ""
    @objc dynamic var recipientNetwork = ""
    @objc dynamic var recipientProfile = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var status = ""
    @objc dynamic var datePerformed = ""
    @objc dynamic var senderId = ""
    @objc dynamic var senderName = ""
    @objc dynamic var senderProfile = ""
    @objc dynamic var currency = "GHS"
    @objc dynamic var sourceAccount = ""
    @objc dynamic var direction = "D"
    @objc dynamic var fees = 0.0
    @objc dynamic var totalAmount = 0.0
    @objc dynamic var message = ""
    
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    
    
    //MARK:- get all
    static func all() -> [TransactionHistory] {
        let realm = try! Realm()
        return Array(realm.objects(TransactionHistory.self))
    }
    
    //get saved into the db
    static func getDevice() -> TransactionHistory? {
        let realm = try! Realm()
        let device = realm.objects(TransactionHistory.self).first
        return device
    }
    
    //save item
    static func save(data: TransactionHistory) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
    }
    
    //delete all
    static func delete(){
        let realm = try! Realm()
        let all = realm.objects(TransactionHistory.self)
        try! realm.write() {
            realm.delete(all)
        }
    }
}

