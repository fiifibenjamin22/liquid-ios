//
//  Goal.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import RealmSwift
import Foundation

class Goal: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var detail = ""
    @objc dynamic var image = ""
    @objc dynamic var dateCreated = ""
    @objc dynamic var inProgress = true
    @objc dynamic var assistModeEnabled = true
    @objc dynamic var initialAmountDeposited = 0.0
    @objc dynamic var currentAmount = 0.0
    @objc dynamic var targetAmount = 0.0
    @objc dynamic var investmentKnowledge = "low"
    @objc dynamic var riskLevel = "low"
    @objc dynamic var currency = "GHS"
    @objc dynamic var balance = 0.0
    @objc dynamic var dueDate = ""
    @objc dynamic var status = ""
    @objc dynamic var categoryId = 0
    @objc dynamic var totalDebit = 0.0
    @objc dynamic var totalCredit = 0.0
    @objc dynamic var awaiting_invesment = 0.0
    @objc dynamic var invested = 0.0
    @objc dynamic var total_deposits = 0.0
    @objc dynamic var total_withdrawals = 0.0
    
    @objc dynamic var nextOfKinName = ""
    @objc dynamic var nextOfKinrelationship = ""
    @objc dynamic var nextKinContact = ""
    @objc dynamic var nextKinEmail = ""
    
    @objc dynamic var goalAssistPeriod = "Weekly"
    @objc dynamic var goalAssistAmount = 0.0
    @objc dynamic var goalAssistCurrency = "GHS"
    
    
    @objc dynamic var withdrawalDestinationId = 0
    @objc dynamic var withdrawalDestinationIdentifier = ""
    @objc dynamic var withdrawalDestinationIssuer = ""
    
    @objc dynamic var hasAssistant = false
    
    @objc dynamic var is_goal_owner = false
    @objc dynamic var total_participants = 0
    @objc dynamic var is_shared = false
    
    @objc dynamic var owner_picture = ""
    @objc dynamic var owner_name = ""
    @objc dynamic var image1 = ""
    @objc dynamic var image2 = ""
    @objc dynamic var image3 = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func getGoalDestination() -> GoalDestination {
        return try! Realm().object(ofType: GoalDestination.self, forPrimaryKey: self.withdrawalDestinationId)!
    }
    
    //MARK:- get all
    static func all() -> [Goal] {
        let realm = try! Realm()
        return Array(realm.objects(Goal.self))
    }
    
    
    //get user saved into the db
    static func getId() -> Goal?{
        let realm = try! Realm()
        let device = realm.objects(Goal.self).first
        return device
    }
    
    //save item
    static func save(data: Goal) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data, update: true)
        }
    }
    
    //delete all
    static func delete(){
        /*let realm = try! Realm()
        let all = realm.objects(Goal.self)
        try! realm.write() {
            realm.delete(all)
        }*/
    }
    
}

