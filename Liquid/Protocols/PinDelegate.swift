//
//  PinDelegate.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/06/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

protocol PinDelegate: class {
    func didSetPin()
    func didFailed()
    func didCanceled()
}

