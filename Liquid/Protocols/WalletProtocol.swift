//
//  WalletProtocol.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation


protocol FavouriteDelegate: class {
    func removeContact(contact:FavouriteContact)
    func didSelectContact(contact:FavouriteContact)
    func didSelectAll()
}
