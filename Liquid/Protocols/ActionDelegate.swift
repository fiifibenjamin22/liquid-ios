//
//  ActionDelegate.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/07/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

protocol ActionDelegate: class {
    func didTake()
    func didFailed()
    func didCanceled()
}

