//
//  OnboardProtocol.swift
//  Liquid
//
//  Created by Benjamin Acquah on 02/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

protocol LoginDelegate: class {
    func clickCountryPicker()
    func clickContinue()
}

protocol TextChangeDelegate: class {
    func didChange(text:String)
}

protocol CountryPickerDelegate: class {
    func clickCountryPicker()
}

/**
 Terms delegate.
 
 ## Important Notes ##
 1. Return an action of the clicked agreement status.
 */
protocol TermsAgreementDelegate: class {
    func didAgreed()
}

