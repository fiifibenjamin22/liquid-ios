//
//  GoalDateProtocol.swift
//  Liquid
//
//  Created by Benjamin Acquah on 07/08/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

protocol GoalRangeProtocol: class {
    func rangeChanges(startDate: String,endDate: String,goal: Goal)
}



