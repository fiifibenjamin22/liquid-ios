//
//  AirtimeProtocol.swift
//  Liquid
//
//  Created by Benjamin Acquah on 18/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation


protocol SelectWalletDelegate: class {
    func didSelectAddNew()
    func didSelectPicker()
    func didSelectWallet(item: Wallet)
}

protocol QuickSelectDelegate: class {
    func didSelect(item: QuickPrices)
}

protocol ShareDelegate: class {
    func didSelect(message:String)
}

protocol SelectNetworkDelegate: class {
    func didSelectNetwork(item: AirtimeProvider)
}


