//
//  HomeProtocol.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

protocol HomeDelegate: class {
    func didSelectGoal(item:Goal)
    func didGoalCategory(item:GoalCategory)
    func didStartInvite()
    func clickedBuyAirtime()
    func clickedManageGoal()
    func didSelectContact(contact:FavouriteContact)
    func clickedNotification()
    func clickedAddFund()
    func startTransactions()
    
    func addNewGoal()
    func ic()
    func changeAmount(amountFigure: Double)
}

protocol CoachDelegate: class {
    func didOpen(Indexes: IndexPath)
}
