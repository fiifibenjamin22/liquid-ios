//
//  SupportDelegate.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

protocol SupportDelegate: class {
    func clickedCall()
    func clickedChat()
    func clickedEmail()
}

