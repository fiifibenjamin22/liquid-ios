//
//  StatementWalletCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class StatementWalletCell: BaseCell {
    
    var actionDelagate: ActionDelegate?
    
    
    let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.alpha = 0.5
        return view
    }()
    
    let cardView: CardView = {
        let card = CardView()
        card.backgroundColor = UIColor.white
        card.cornerRadius = 5
        card.shadowOffsetHeight = 2
        card.translatesAutoresizingMaskIntoConstraints = false
        return card
    }()
    
    let nameTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        textView.text = "Transaction History"
        return textView
    }()

    lazy var actionButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("View All ", for: .normal)
        button.setImage(UIImage(named: "Next"), for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 2)
        button.addTarget(self, action: #selector(viewAllClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    
    //MARK: click the view all
    @objc func viewAllClicked(_ sender: UIButton)  {
       self.actionDelagate?.didTake()
    }
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(lineView)
        addSubview(cardView)
        cardView.addSubview(actionButton)
        cardView.addSubview(nameTextView)
        
       
        self.lineView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: -16, rightConstant: 0)
        self.lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        self.cardView.anchorWithConstantsToTop(top: lineView.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
    
        self.actionButton.anchorWithConstantsToTop(top: cardView.topAnchor, left: nil, bottom: cardView.bottomAnchor, right: cardView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: -8, rightConstant: -8)
        self.actionButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.nameTextView.anchorWithConstantsToTop(top: cardView.topAnchor, left: cardView.leadingAnchor, bottom: cardView.bottomAnchor, right: actionButton.leadingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: -8, rightConstant: -8)
        
    }
    
    
}
