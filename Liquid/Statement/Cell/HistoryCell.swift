//
//  HistoryCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

//
//  UpgradeCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
class HistoryCell: BaseCell {
    
    let iconImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        //imageView.image = UIImage(named: Images.Onboard.logoQ)
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        return textView
    }()
    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        return textView
    }()
    
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    var item: TransactionHistory? {
        didSet {
            guard let unwrapedItem = item else {return}
            
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 12)!)
            
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.darkText, NSAttributedString.Key.font: boldFont]
            let andAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: normalFont]
            
            let header = NSMutableAttributedString(string: "\(unwrapedItem.message)", attributes: attributes)
            let and = NSMutableAttributedString(string: "\n\(Mics.setDateForGoalActivity(dateString: unwrapedItem.datePerformed))", attributes: andAttributes)
            
            let combinedText = NSMutableAttributedString()
            combinedText.append(header)
            combinedText.append(and)
            
            self.messageTextView.attributedText = combinedText
            self.amountTextView.text = "GHS \(unwrapedItem.amount)"
            self.amountTextView.font = boldFont
            //self.iconImageView.image = UIImage(named: unwrapedItem.)
            print(unwrapedItem)
        }
    }
    
    override func setUpView() {
        super.setUpView()
        
        
        backgroundColor = UIColor.white
        
        addSubview(iconImageView)
        addSubview(messageTextView)
        addSubview(amountTextView)
        addSubview(splitLine)
        
        
        let height = frame.height
        self.iconImageView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.iconImageView.widthAnchor.constraint(equalToConstant: height/2).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: height/2).isActive = true
        self.iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        self.amountTextView.anchorWithConstantsToTop(top: topAnchor, left: nil, bottom: splitLine.topAnchor, right: trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        self.amountTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: iconImageView.trailingAnchor, bottom: splitLine.topAnchor, right: amountTextView.leadingAnchor, topConstant: 4, leftConstant: 8, bottomConstant: -4, rightConstant: -8)
        
        self.splitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        self.splitLine.anchorWithConstantsToTop(top: nil, left: iconImageView.trailingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        
    }
    
}

