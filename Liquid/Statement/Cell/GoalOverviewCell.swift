//
//  GoalOverviewCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 07/08/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalOverviewCell: BaseCell {
    
    var item: Goal? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.nameTextView.text = "\(unwrapedItem.name)"
            self.amountTextView.text = "GHS \(unwrapedItem.balance)"
        }
    }
    
    let nameTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        return textView
    }()
    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(nameTextView)
        addSubview(amountTextView)
      
        self.amountTextView.anchorWithConstantsToTop(top: nameTextView.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
      
        self.nameTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.nameTextView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
    }
    
    
}
