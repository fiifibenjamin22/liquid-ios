//
//  StatementGoalCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class StatementGoalCell: BaseCell {
    
    var item: Goal?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.nameTextView.text = "\(unwrapedItem.name)"
            self.amountTextView.text = "\(Mics.convertDoubleToCurrency(amount: unwrapedItem.balance))"
           
            if  !(unwrapedItem.image.isEmpty) {
                self.iconImageView.af_setImage(
                    withURL: URL(string: (unwrapedItem.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.iconImageView.image = Mics.placeHolder()
            }
            
        }
    }
    
    let cardView: CardView = {
        let card = CardView()
        card.backgroundColor = UIColor.white
        card.cornerRadius = 10
        card.shadowOffsetHeight = 2
        card.translatesAutoresizingMaskIntoConstraints = false
        return card
    }()
    
    let nameTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        return textView
    }()
    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.textAlignment = .right
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let coverImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        return imageView
    }()
    
    lazy var actionButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("View Statement ", for: .normal)
        button.setImage(UIImage(named: "Next"), for: .normal)
        button.isHidden = true
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 2,left: 5,bottom: 2,right: 2)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(cardView)
        cardView.addSubview(iconImageView)
        cardView.addSubview(coverImageView)
        coverImageView.addSubview(amountTextView)
        coverImageView.addSubview(actionButton)
        coverImageView.addSubview(nameTextView)
        
        //let height = frame.height - 60 - 40
        
        self.cardView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -8, rightConstant: -8)
        
        self.amountTextView.anchorWithConstantsToTop(top: nil, left: nil, bottom: cardView.bottomAnchor, right: cardView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.amountTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.amountTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.actionButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: cardView.bottomAnchor, right: cardView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -8)
        self.actionButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.actionButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.iconImageView.topAnchor.constraint(equalTo: cardView.topAnchor).isActive = true
        self.iconImageView.leftAnchor.constraint(equalTo: cardView.leftAnchor).isActive = true
        self.iconImageView.rightAnchor.constraint(equalTo: cardView.rightAnchor).isActive = true
        self.iconImageView.bottomAnchor.constraint(equalTo: cardView.bottomAnchor).isActive = true
        
        self.coverImageView.anchor(iconImageView.topAnchor, left: iconImageView.leftAnchor, bottom: iconImageView.bottomAnchor, right: iconImageView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.nameTextView.anchorWithConstantsToTop(top: nil, left: cardView.leadingAnchor, bottom: cardView.bottomAnchor, right: amountTextView.leadingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: -8)
        self.nameTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
}
