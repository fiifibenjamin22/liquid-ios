//
//  StatementSummaryController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class StatementSummaryController: ControllerWithBack {
  
    let collectionGoalId = "collectionGoalId"
    let cellSection = "cellHeading"
    let cellAll = "cellAll"
    let cellHeader = "cellHeader"
    let apiService = ApiService()
    let utilViewController = ViewControllerHelper()
    var headerView: StatementSummaryHeader?
    var actionDelagate: ActionDelegate?
    
    var accounts = [Any]()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Statement Overview")
        
        self.setUpViews()
    }
    
    func setUpViews()  {
        self.view.addSubview(collectionView)
       
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.collectionView.register(StatementGoalCell.self, forCellWithReuseIdentifier: collectionGoalId)
        self.collectionView.register(SettingSectionCell.self, forCellWithReuseIdentifier: cellSection)
        self.collectionView.register(StatementWalletCell.self, forCellWithReuseIdentifier: cellAll)
        self.collectionView.register(StatementSummaryHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellHeader)
        
        self.getStatementOverview()
    }
    
    func getStatementOverview() {
        self.utilViewController.showActivityIndicator()
        ApiService().goals { (status, goals, withMessage) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if let goalsIn = goals {
                    if !goalsIn.isEmpty {
                        self.accounts.append("YOUR GOALS")
                    }
                    var totalDepositArray = [Double]()
                    var totalWithdrawalsArray = [Double]()
                    var totalBalanceArray = [Double]()
                    
                    for goal in goalsIn {
                        if goal.is_goal_owner == true {
                            self.accounts.append(goal)
                            totalDepositArray.append(goal.total_deposits)
                            totalWithdrawalsArray.append(goal.total_withdrawals)
                            totalBalanceArray.append(goal.balance)
                        }
                    }
                    
                    let totalDeposte = gettotals(totals: totalDepositArray)
                    let totalWithdrawals = gettotals(totals: totalWithdrawalsArray)
                    let totalBalance = gettotals(totals: totalBalanceArray)
                    let totalEarnings: Double = (totalBalance + totalWithdrawals) - totalDeposte
                                                            
                    self.headerView?.depositeAmount.text = "\(Mics.convertDoubleToCurrency(amount: totalDeposte))"
                    self.headerView?.withdrawalAmount.text = "\(Mics.convertDoubleToCurrency(amount: totalWithdrawals))"
                    self.headerView?.earningsAmount.text = "\(Mics.convertDoubleToCurrency(amount: totalEarnings))"
                    self.headerView?.totalBalanceAmount.text = "\(Mics.convertDoubleToCurrency(amount: totalBalance))"
                    
                    self.headerView?.earnings.text = "Earnings:"
                    self.headerView?.withdrawal.text = "Withdrawals:"
                    self.headerView?.deposite.text = "Deposits:"
                    self.headerView?.totalBalance.text = "Total Balance:"
                    
                    self.accounts.append("ALL")
                    self.collectionView.reloadData()
                }
            }else{
                print("FAILED")
            }
        }
        
        func gettotals(totals: Array<Double>) -> Double {
            var counter = 0
            var amount: Double = 0
            while counter < totals.count {
                let newValue = totals[counter]
                amount += newValue
                  counter += 1
            }
            return amount
        }
    }
}

extension StatementSummaryController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return accounts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = accounts[indexPath.row]
        
        if indexPath.row == accounts.count - 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellAll, for: indexPath) as! StatementWalletCell
            cell.actionDelagate = self
            return cell
        }
        
        if item is String {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellSection, for: indexPath) as! SettingSectionCell
            cell.labelItem = item as? String
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionGoalId, for: indexPath) as! StatementGoalCell
        cell.item = item as? Goal
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth = collectionView.frame.width
        var itemHeight = CGFloat(50)
        
        //MARK: the last item clicked
        if indexPath.row == accounts.count - 1 {
            itemHeight = 100
            return CGSize(width: itemWidth, height: itemHeight)
        }
        //MARK: the section
        let item = accounts[indexPath.row]
        if item is String {
             return CGSize(width: itemWidth, height: itemHeight)
        }
        //MARK: the goals section
        itemHeight = 150
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(140)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView  = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellHeader, for: indexPath) as? StatementSummaryHeader
        return headerView!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.settingClicked(position: indexPath.row)
    }
    
}

extension StatementSummaryController: ActionDelegate {
    
    func didTake() {
        self.startTransactionHisory()
    }
    
    func didFailed() {
        
    }
    
    func didCanceled() {
        
    }
    
    //MARK:- clicked here
    func settingClicked(position:Int) {
        if position == accounts.count - 1 {
            self.startTransactionHisory()
            return
        }
        let itemSelected = self.accounts[position]
        if itemSelected is Goal {
            let destination = StatementOverviewController()
            destination.goalIn = itemSelected as? Goal
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    func startTransactionHisory()  {
        let destination = TransactionHistoryController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
