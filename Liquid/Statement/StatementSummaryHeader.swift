//
//  StatementSummaryHeader.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


//MARK: the haeder section of the account overview
class StatementSummaryHeader: BaseCellHeader {
    
    let backGroundView: UIImageView = {
        let view = ViewControllerHelper.baseImageView()
        view.image = UIImage(named: Images.barBg)
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.textAlignment = .center
        return textView
    }()
    
    let totalBalance: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    let totalBalanceAmount: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .left
        lbl.backgroundColor = .clear
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    let earnings: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .right
        lbl.backgroundColor = .clear
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    let earningsAmount: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .left
        lbl.backgroundColor = .clear
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    let deposite: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .right
        lbl.backgroundColor = .clear
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    let depositeAmount: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .left
        lbl.backgroundColor = .clear
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    let withdrawal: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        lbl.backgroundColor = .clear
        return lbl
    }()
    
    let withdrawalAmount: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textAlignment = .left
        lbl.backgroundColor = .clear
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.textColor = .white
        return lbl
    }()
    
    var totalDeposit = ""
    var totalWithdrawal = ""
    
    var item: String? {
        didSet {
            guard let unwrappedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 24)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 24)!)
            let currencyFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
            
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: boldFont]
            let andAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: normalFont]
            let currencyAttribute = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: currencyFont]
            
            
            let totalText = NSMutableAttributedString(string: "Total Balance\n", attributes: attributes)
            let amountText = NSMutableAttributedString(string: " \(unwrappedItem.split(separator: " ")[1]) ", attributes: andAttributes)
            let currencyText = NSMutableAttributedString(string: " \(unwrappedItem.split(separator: " ")[0]) ", attributes: currencyAttribute)
            
            let combinedText = NSMutableAttributedString()
            combinedText.append(totalText)
            combinedText.append(currencyText)
            combinedText.append(amountText)
            
            let currency = unwrappedItem.split(separator: " ")[0]
            let amount = unwrappedItem.split(separator: " ")[1]
            
            self.totalBalanceAmount.text = "\(currency) \(amount)"
            
            self.messageTextView.attributedText = combinedText
            self.messageTextView.textAlignment = .center
            
        }
    }
    
    
    
    override func setUpViews() {
        super.setUpViews()
    
        self.addSubview(backGroundView)
        //self.addSubview(messageTextView)
        self.backGroundView.addSubview(totalBalance)
        self.backGroundView.addSubview(totalBalanceAmount)
        
        self.backGroundView.addSubview(earnings)
        self.backGroundView.addSubview(earningsAmount)
        
        self.backGroundView.addSubview(deposite)
        self.backGroundView.addSubview(depositeAmount)
        self.backGroundView.addSubview(withdrawal)
        self.backGroundView.addSubview(withdrawalAmount)
        
        self.backGroundView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.totalBalance.topAnchor.constraint(equalTo: self.backGroundView.topAnchor, constant: 30.0).isActive = true
        self.totalBalance.leftAnchor.constraint(equalTo: self.backGroundView.leftAnchor, constant: 8.0).isActive = true
        self.totalBalance.widthAnchor.constraint(equalToConstant: (self.frame.size.width / 2) - 8).isActive = true
        self.totalBalance.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        
        self.totalBalanceAmount.leftAnchor.constraint(equalTo: self.totalBalance.rightAnchor, constant: 8).isActive = true
        self.totalBalanceAmount.topAnchor.constraint(equalTo: self.totalBalance.topAnchor).isActive = true
        self.totalBalanceAmount.rightAnchor.constraint(equalTo: self.backGroundView.rightAnchor, constant: -4.0).isActive = true
        self.totalBalanceAmount.bottomAnchor.constraint(equalTo: self.totalBalance.bottomAnchor).isActive = true
                
        self.earnings.topAnchor.constraint(equalTo: self.totalBalance.bottomAnchor, constant: 5.0).isActive = true
        self.earnings.leftAnchor.constraint(equalTo: self.totalBalance.leftAnchor, constant: 0.0).isActive = true
        self.earnings.widthAnchor.constraint(equalToConstant: (self.frame.size.width / 2) - 8).isActive = true
        self.earnings.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        
        self.earningsAmount.leftAnchor.constraint(equalTo: self.earnings.rightAnchor, constant: 8).isActive = true
        self.earningsAmount.topAnchor.constraint(equalTo: self.earnings.topAnchor).isActive = true
        self.earningsAmount.rightAnchor.constraint(equalTo: self.totalBalanceAmount.rightAnchor, constant: 0.0).isActive = true
        self.earningsAmount.bottomAnchor.constraint(equalTo: self.earnings.bottomAnchor).isActive = true

        self.deposite.topAnchor.constraint(equalTo: self.earnings.bottomAnchor, constant: 5.0).isActive = true
        self.deposite.leftAnchor.constraint(equalTo: self.earnings.leftAnchor, constant: 0.0).isActive = true
        self.deposite.widthAnchor.constraint(equalToConstant: (self.frame.size.width / 2) - 8).isActive = true
        self.deposite.heightAnchor.constraint(equalToConstant: 20.0).isActive = true

        self.depositeAmount.leftAnchor.constraint(equalTo: self.deposite.rightAnchor, constant: 8).isActive = true
        self.depositeAmount.topAnchor.constraint(equalTo: self.deposite.topAnchor).isActive = true
        self.depositeAmount.rightAnchor.constraint(equalTo: self.totalBalanceAmount.rightAnchor, constant: 0.0).isActive = true
        self.depositeAmount.bottomAnchor.constraint(equalTo: self.deposite.bottomAnchor).isActive = true

        self.withdrawal.topAnchor.constraint(equalTo: self.deposite.bottomAnchor, constant: 5.0).isActive = true
        self.withdrawal.leftAnchor.constraint(equalTo: self.deposite.leftAnchor, constant: 0.0).isActive = true
        self.withdrawal.widthAnchor.constraint(equalToConstant: (self.frame.size.width / 2) - 8).isActive = true
        self.withdrawal.heightAnchor.constraint(equalToConstant: 20.0).isActive = true

        self.withdrawalAmount.leftAnchor.constraint(equalTo: self.withdrawal.rightAnchor, constant: 8).isActive = true
        self.withdrawalAmount.topAnchor.constraint(equalTo: self.withdrawal.topAnchor).isActive = true
        self.withdrawalAmount.rightAnchor.constraint(equalTo: self.totalBalanceAmount.rightAnchor, constant: 0.0).isActive = true
        self.withdrawalAmount.bottomAnchor.constraint(equalTo: self.withdrawal.bottomAnchor).isActive = true

        //self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
    }
}

