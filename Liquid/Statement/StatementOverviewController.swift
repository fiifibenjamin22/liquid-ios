//
//  StatementOverviewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 27/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import DZNEmptyDataSet


class StatementOverviewController: ControllerWithBack, UIDocumentInteractionControllerDelegate {

    let collectionId = "transactionCellId"
    let cellHeaderId = "cellHeaderId"
    let apiService = ApiService()
    let utilViewController = ViewControllerHelper()
    var headerView: StatementHederView?
    var pageNumber = 1
    var histories = [TransactionHistory]()
    var goalIn: Goal?
    
    var startDate = ""
    var endDate = ""
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.emptyDataSetSource = self
        
        return collectionIn
    }()
    
    
    lazy var exportButton: UIButton = {
        let button = ViewControllerHelper.baseRoudImageButton()
        button.setTitle(" Export", for: .normal)
        button.addTarget(self, action: #selector(generateReport), for: .touchUpInside)
        
        button.semanticContentAttribute = .forceLeftToRight
        button.imageEdgeInsets = UIEdgeInsets(top: 15, left: -10, bottom: 15, right: -20)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.setImage(UIImage(named: "printer"), for: .normal)
        
        return button
    }()
    
    
    override func viewDidLoad() {
        setUpNavigationBar(title: "Statement Overview")
        
        self.setUpLayout()
    }
    
    func setUpLayout()  {
        self.view.addSubview(collectionView)
        self.view.addSubview(exportButton)
        
        self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
        self.collectionView.register(HistoryCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(StatementHederView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellHeaderId)
        
        self.exportButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -100, rightConstant: -16)
        self.exportButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.exportButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let startDateRaw = Calendar.current.date(byAdding: .month, value: -1, to: Date())
        self.startDate = dateFormatterGet.string(from: startDateRaw!)
        self.endDate = dateFormatterGet.string(from: Date())
        
        //MARK: get first goal information
        let goals = Goal.all()
        print(goals)
        if !goals.isEmpty {
           //self.goalIn = goals[0]
            self.resetData(goalId: self.goalIn!.id, startDate: self.startDate, endDate: self.endDate)
        }
    }
    
    func resetData(goalId:Int, startDate:String, endDate:String)  {
       self.histories.removeAll()
       self.collectionView.reloadData()
       self.getAccountStatement(accountId: goalId, startDate: startDate, endDate: endDate)
    }
    
    
    func getAccountStatement(accountId: Int, startDate: String, endDate: String) {
        self.utilViewController.showActivityIndicator()
        self.apiService.getAccountStatement(accountId: accountId, startDate: startDate, endDate: endDate, page: self.pageNumber) { (status, message,goal, histories, withMessage) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if let historiesIn = histories {
                    for history in historiesIn {
                        self.histories.append(history)
                    }
                    
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    @objc func generateReport(){
        if let goalIn = self.goalIn {
        self.utilViewController.showActivityIndicator()
        self.apiService.exportStatement(accountId: goalIn.id, startDate: self.startDate, endDate: self.endDate) { (status, withMessage) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let vc = UIDocumentInteractionController(url: URL(string: withMessage)!)
                vc.delegate = self
                vc.presentPreview(animated: true)
                //ViewControllerHelper.showPrompt(vc: self, message: "Report successfully generated.")
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
        }
    }
}

extension StatementOverviewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate, DZNEmptyDataSetSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return histories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = histories[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! HistoryCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = CGFloat(100)
        return CGSize(width: collectionView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(250)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView  = (collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellHeaderId, for: indexPath) as! StatementHederView)
        headerView?.goalRangeProtocol = self
        headerView?.goal = self.goalIn
        return headerView!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.clicked(item: self.histories[indexPath.row])
    }
    
    //MARK:- clicked here
    func clicked(item:TransactionHistory) {
        print("ITEM \(item)")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let regularFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 16)!)
        return "There are no transactions between the selected dates".formatAsAttributed(font: regularFont, color: UIColor.darkText)
    }

    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self.navigationController!
    }
}

extension StatementOverviewController: GoalRangeProtocol {
    
    func rangeChanges(startDate: String, endDate: String, goal: Goal) {
        self.startDate = startDate
        self.endDate = endDate
        self.goalIn = goal
        self.resetData(goalId: goal.id, startDate: startDate, endDate: endDate)
    }

}
