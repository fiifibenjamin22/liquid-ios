//
//  StatementHederView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class StatementHederView: BaseCellHeader {
    
    let color = UIColor.hex(hex: Key.primaryHexCode)
    let goals = Goal.all()
    let collectionId = "collectionId"
    var goalRangeProtocol: GoalRangeProtocol?
    let startDateRaw = Calendar.current.date(byAdding: .month, value: -1, to: Date())
    var goal: Goal? {
        didSet {
            guard let unwrapedItem = goal else { return }
            pageControl.currentPage = goals.firstIndex(where: { goal!.id == $0.id })!
            let indexPath = IndexPath(row: Int(goals.firstIndex(where: { goal!.id == $0.id })!), section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    let backGroundView: UIImageView = {
        let view = ViewControllerHelper.baseImageView()
        view.image = UIImage(named: Images.barBg)
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPageIndicatorTintColor = UIColor.gray
        pc.pageIndicatorTintColor = UIColor.white
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.numberOfPages = self.goals.count
        return pc
    }()
    
    let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let periodTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.lightGray
        textView.text = "PERIOD"
        textView.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 0, right: 16)
        textView.textAlignment = .left
        return textView
    }()
    
    let startDateLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "Start Date"
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.textAlignment = .left
        return textView
    }()
    
    lazy var startDateTextField: UIButton = {
        let button = baseButton()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatterGet.string(from: self.startDateRaw!)
        button.setTitle(dateString, for: .normal)
        return button
    }()
    
    lazy var endDateLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = color
        textView.text = "End Date"
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.textAlignment = .left
        return textView
    }()
    
    lazy var endDateTextField: UIButton = {
        let button = baseButton()
        return button
    }()
    
    
    func baseButton() -> UIButton {
        let button = ViewControllerHelper.plainButton()
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.addBottomBorder(color, height: 0.5)
        button.addTarget(self, action: #selector(pickDateOfBirth(_:)), for: .touchUpInside)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatterGet.string(from: Date())
        
        button.setTitle(dateString, for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return button
    }


    override func setUpViews() {
        super.setUpViews()
        
        backgroundColor = UIColor.white
        
        addSubview(backGroundView)
        addSubview(collectionView)
        addSubview(pageControl)
        addSubview(contentView)
        
    
        self.backGroundView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.backGroundView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        //MARK:- collection section
        self.collectionView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.collectionView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.collectionView.register(GoalOverviewCell.self, forCellWithReuseIdentifier: collectionId)
        
        self.pageControl.anchorWithConstantsToTop(top: collectionView.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        //MARK:- sub section here
        self.contentView.anchorWithConstantsToTop(top: backGroundView.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.contentView.addSubview(periodTextView)
        self.contentView.addSubview(startDateTextField)
        self.contentView.addSubview(endDateTextField)
        self.contentView.addSubview(startDateLabel)
        self.contentView.addSubview(endDateLabel)
     
    
        self.periodTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.periodTextView.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        let width = frame.width - 64
        self.startDateLabel.anchorWithConstantsToTop(top: periodTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        self.startDateLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.startDateLabel.widthAnchor.constraint(equalToConstant: width/2).isActive = true
        
        self.startDateTextField.anchorWithConstantsToTop(top: startDateLabel.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.startDateTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.startDateTextField.widthAnchor.constraint(equalToConstant: width/2).isActive = true
        
        self.endDateLabel.anchorWithConstantsToTop(top: periodTextView.bottomAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 4, leftConstant: 8, bottomConstant: 0, rightConstant: -16)
        self.endDateLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.endDateLabel.widthAnchor.constraint(equalToConstant: width/2).isActive = true
        
        self.endDateTextField.anchorWithConstantsToTop(top: endDateLabel.bottomAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.endDateTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.endDateTextField.widthAnchor.constraint(equalToConstant: width/2).isActive = true
    }
    
    //MARK:- pick date of birth instead
    @objc func pickDateOfBirth(_ sender: UIButton){
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let datePicker = ActionSheetDatePicker(title: "PICK DATE", datePickerMode: UIDatePicker.Mode.date, selectedDate: dateFormatterGet.date(from: sender.title(for: .normal)!), doneBlock: {
            picker, value, index in
            
            let startDate = dateFormatterGet.string(from: value! as! Date)
            if sender == self.startDateTextField {
                self.startDateTextField.setTitle(startDate, for: .normal)
            }  else {
                self.endDateTextField.setTitle(startDate, for: .normal)
            }
            self.updateDateChange()
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        if sender == self.startDateTextField {
            datePicker?.maximumDate = dateFormatterGet.date(from: self.endDateTextField.title(for: .normal)!)
        }
        
        datePicker?.show()
    }
}

extension StatementHederView : UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.goals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let goal = goals[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! GoalOverviewCell
        cell.item = goal
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let cellWidth : CGFloat = collectionView.frame.width - 50
        let numberOfCells = floor(self.collectionView.frame.size.width / cellWidth)
        let edgeInsets = (self.collectionView.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        return UIEdgeInsets(top: 15, left: edgeInsets, bottom: 0, right: edgeInsets)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width - 50
        let itemHeight = collectionView.frame.height
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    
    func snapToCenter() {
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        let pageNumber = indexPath.item
        self.pageControl.currentPage = pageNumber
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        self.updateDateChange()
    }
    
    func updateDateChange() {
        let startDate = self.startDateTextField.currentTitle!
        let endDate = self.endDateTextField.currentTitle!
        if self.goals.count > 0 {
            self.goalRangeProtocol?.rangeChanges(startDate: startDate, endDate: endDate, goal: self.goals[self.pageControl.currentPage])
        }
        
    }
}

extension StatementHederView: UIScrollViewDelegate {
    
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        snapToCenter()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            snapToCenter()
        }
    }
    
}
