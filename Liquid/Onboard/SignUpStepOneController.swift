//
//  SignUpStepOneController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import IHKeyboardAvoiding

class SignUpStepOneController: BaseScrollViewController {
    
    var number = ""
    var firstName = ""
    var lastName = ""
    var otherName = ""
    var invitaionCode = ""
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
  

    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkGray
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var firstTextField: UITextField = {
        let textField = baseUITextField(placeholder: "First Name")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    let firstNameErrorLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.red
        textView.backgroundColor = UIColor.clear
        textView.text = "Provide a valid name."
        textView.font = UIFont(name: Font.Roboto.Light, size: 16)!
        return textView
    }()
    
    lazy var lastTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Last Name")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    let lastNameErrorLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.red
        textView.backgroundColor = UIColor.clear
        textView.text = "Provide a valid name."
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    lazy var otherTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Other Names")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkText
        let textField = ViewControllerHelper.baseField()
        let font = UIFont(name: Font.Roboto.Light, size: 16)
        textField.font = UIFontMetrics.default.scaledFont(for: font!)
        textField.textColor = color
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        
        return textField
    }
    
    //invitation code
    let inviteNotice: UITextView = {
        let txt = UITextView()
        txt.isScrollEnabled = false
        txt.textColor = .lightGray
        txt.text = "Thank you for your interest in LIQUID.Access to LIQUID is currently by invitaion only.Please enter your invitaion code below"
        txt.font = UIFont.boldSystemFont(ofSize: 14)
        txt.textAlignment = .left
        txt.translatesAutoresizingMaskIntoConstraints = false
        return txt
    }()
    
    lazy var invitaionCodeTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Invitation Code")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.newBaseButton()
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = UIColor.hex(hex: "e9e8e9")
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.black.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = button.bounds
        button.layer.insertSublayer(gradientLayer, at: 0)
        
        return button
    }()
    
    override func setUpView() {
        setUpNavigationBar(title: "")
        
        self.scrollView.addSubview(messageTextView)
        self.scrollView.addSubview(firstTextField)
        self.scrollView.addSubview(firstNameErrorLabel)
        self.scrollView.addSubview(lastTextField)
        self.scrollView.addSubview(lastNameErrorLabel)
        self.scrollView.addSubview(otherTextField)
        
        self.scrollView.addSubview(inviteNotice)
        self.scrollView.addSubview(invitaionCodeTextField)
        
        self.view.addSubview(nextButton)
        
        let width = self.view.frame.width - 32
        self.messageTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.firstTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.firstTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.firstTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.firstNameErrorLabel.anchorWithConstantsToTop(top: firstTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
        self.firstNameErrorLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        self.firstNameErrorLabel.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.lastTextField.anchorWithConstantsToTop(top: firstNameErrorLabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.lastTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.lastTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.lastNameErrorLabel.anchorWithConstantsToTop(top: lastTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
        self.lastNameErrorLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        self.lastNameErrorLabel.widthAnchor.constraint(equalToConstant: width).isActive = true

        self.otherTextField.anchorWithConstantsToTop(top: lastNameErrorLabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.otherTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.otherTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.inviteNotice.topAnchor.constraint(equalTo: otherTextField.bottomAnchor, constant: 24).isActive = true
        self.inviteNotice.leftAnchor.constraint(equalTo: otherTextField.leftAnchor, constant: 0).isActive = true
        self.inviteNotice.rightAnchor.constraint(equalTo: otherTextField.rightAnchor, constant: -50).isActive = true
        self.inviteNotice.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        self.invitaionCodeTextField.anchorWithConstantsToTop(top: inviteNotice.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.invitaionCodeTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.invitaionCodeTextField.widthAnchor.constraint(equalToConstant: width).isActive = true

        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 22)!)
        let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        
        let messageHeader = "Hi, what's your name?\n".formatAsAttributed(font: boldFont, color: UIColor.darkGray)
        let messageBody = "We need your legal name to verify your identity".formatAsAttributed(font: normalFont, color: UIColor.lightGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        self.messageTextView.attributedText = result
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, leftConstant: 15, rightConstant: -15, bottomConstant: -15)
        
        self.firstNameErrorLabel.isHidden = true
        self.lastNameErrorLabel.isHidden = true
 
        //KeyboardAvoiding.avoidingView = self.otherTextField
        
        self.view.bindToKeyboard()
    }
    
    //MARK:- pick date of birth instead
    @objc func pickDateOfBirth(_ sender: UIButton){
        //let sixteenInterval: TimeInterval = 30 * 12 * 4 * 7 * 24 * 60 * 60
        let startingDate = Calendar.current.date(byAdding: .year, value: -28, to: Date())!
        let datePicker = ActionSheetDatePicker(title: "SELECT BIRTHDAY", datePickerMode: UIDatePicker.Mode.date, selectedDate: startingDate, doneBlock: {
            picker, value, index in
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd/MM/yyyy"
            let dateTaken =  value! as! Date
            let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())!
            if dateTaken >= date  {
               ViewControllerHelper.showPrompt(vc: self, message: "You must be 18 years or older.")
               return
            }
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        datePicker?.show()
    }
    
    //MARK:- proceed to change number
   @objc func changeNumber()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- proceed to the next page
    @objc func nextAction()  {
        self.viewControllerHelper.showActivityIndicator()
        //verify invitation code before moving to the next page
        ApiService().verifyReferral(code: self.invitaionCodeTextField.text!) { (response, status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                
                print("invitation response: \(response["referrer_name"])")
                print("invitation response: \(status)")
                print("invitation response: \(withMessage)")
                
                //let theInviter = response["referrer_name"]
                let part2_vc = SignUpStepOnePart2ViewController()
                part2_vc.firstName = self.firstName
                part2_vc.lastName = self.lastName
                part2_vc.otherName = self.otherName
                part2_vc.number = self.number
                self.navigationController?.pushViewController(part2_vc, animated: true)

                
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func goNextToLocation()  {
        let destination = CurrentLocationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.firstName = self.firstTextField.text!
        self.lastName = self.lastTextField.text!
        self.otherName = self.otherTextField.text!
        self.invitaionCode = self.invitaionCodeTextField.text!
        self.changeButtonStatus()
        
        if self.firstTextField == textField {
            self.firstNameErrorLabel.isHidden = !self.firstName.isEmpty
        }
        
        if self.lastTextField == textField {
            self.lastNameErrorLabel.isHidden = !self.lastName.isEmpty
        }
    }
    
    func changeButtonStatus()  {
        if !firstName.isEmpty && !lastName.isEmpty && !invitaionCode.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.hex(hex: "e9e8e9")
            self.nextButton.backgroundColor = color
        }
    }
    
}
