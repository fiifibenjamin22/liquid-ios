//
//  LoginPageCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class LoginView: BaseUIView {
    
    let bottomView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let messageLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        let font = UIFont(name: Font.Roboto.Bold, size: 20)
        textView.font = UIFontMetrics.default.scaledFont(for: font!)
        textView.text = "Create an account or Log in."
        return textView
    }()
    
   var countryButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        let font = UIFont(name: Font.Roboto.Regular, size: 18)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: font!)
        let text = Mics.flag(country: "GH")
        button.setTitle("\(text) +233", for: .normal)
        button.layer.cornerRadius = 0
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.addTarget(self, action: #selector(pickCountry), for: .touchUpInside)
    
        let frameWidth = CGFloat(110)
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
    button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
    
        return button
    }()
    
     lazy var numberTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.numberPad
        textField.delegate = self
        textField.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter mobile number",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    lazy var loginButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Continue", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(clickContinue), for: .touchUpInside)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
        return button
    }()
    
    
    weak var delegate: LoginDelegate?
    
    
    override func setUpLayout() {
        super.setUpLayout()
        backgroundColor = UIColor.white
        addSubview(bottomView)
        bottomView.addSubview(loginButton)
        bottomView.addSubview(countryButton)
        bottomView.addSubview(numberTextField)
        bottomView.addSubview(messageLabel)
        
        
        self.bottomView.anchorToTop(top: nil, left: leadingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: trailingAnchor)
        self.bottomView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        self.loginButton.anchorToTop(top: nil, left: bottomView.leadingAnchor, bottom: bottomView.bottomAnchor, right: bottomView.trailingAnchor)
        self.loginButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        self.messageLabel.anchorWithConstantsToTop(top: bottomView.topAnchor, left: bottomView.leadingAnchor, bottom: countryButton.topAnchor, right:bottomView.trailingAnchor,topConstant:16 , leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.countryButton.anchorWithConstantsToTop(top: nil, left: bottomView.leadingAnchor, bottom: loginButton.topAnchor, right:nil,topConstant:0 , leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.countryButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.countryButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        self.numberTextField.anchorWithConstantsToTop(top: nil, left: countryButton.trailingAnchor, bottom: loginButton.topAnchor, right: bottomView.trailingAnchor,topConstant:0 , leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.numberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    }
    
    @objc private func pickCountry() {
        self.delegate?.clickCountryPicker()
    }
    
    @objc private func clickContinue() {
        self.delegate?.clickContinue()
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        let numberReal = "\(self.countryButton.currentTitle!.split(separator: " ")[1])\(textField.text!)"
        if Mics.validNumber(number: numberReal) {
           self.loginButton.isEnabled = true
           let color = UIColor.hex(hex: Key.primaryHexCode)
           self.loginButton.backgroundColor = color
        }  else {
           self.loginButton.isEnabled = false
           let color = UIColor.gray
           self.loginButton.backgroundColor = color
        }
    }
    
}

extension LoginView : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
}

