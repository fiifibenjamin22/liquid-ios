//
//  SwipingPageCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class SwipingPageCell: BaseCell {
    
    var page: Page? {
        didSet {
            guard let unwrapedPage = page else {return}
            self.mainImageView.image = UIImage(named: unwrapedPage.imageName)
            
            let font = UIFont(name: Font.Roboto.Bold, size: 24)
            let boldFont = UIFontMetrics.default.scaledFont(for: font!)
            
            let fontNormal = UIFont(name: Font.Roboto.Regular, size: 16)
            let normalFont = UIFontMetrics.default.scaledFont(for: fontNormal!)
            
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: boldFont]
            let andAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: normalFont]
            
            let header = NSMutableAttributedString(string: "\(unwrapedPage.title)", attributes: attributes)
            let and = NSMutableAttributedString(string: "\n\(unwrapedPage.message)", attributes: andAttributes)
            
            let combinedText = NSMutableAttributedString()
            combinedText.append(header)
            combinedText.append(and)
            
            messageTextView.attributedText = combinedText
            messageTextView.textAlignment = .center
            
            self.setUpView()
        }
    }
    
    let mainImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let youtubeView: YTPlayerView = {
        let playerVars = [
            "playsinline": NSNumber(value: 1)
        ]
        let v = YTPlayerView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        v.load(withVideoId: "AXHraxorLZE", playerVars: playerVars)
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        v.layer.borderWidth = 2.0
        v.layer.borderColor = UIColor.white.cgColor
        return v
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        
        textView.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    override func setUpView(){
        backgroundColor = UIColor.clear
        addSubview(mainImageView)
        addSubview(messageTextView)
        addSubview(youtubeView)
        
        if self.page == nil {
            return
        }
                        
        if self.page?.number == 1 {
            
            self.messageTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: CGFloat((self.page?.bottomOffset)!), rightConstant: -16)

            self.mainImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 16, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
        } else if self.page?.number == 2 {
            
            self.messageTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: CGFloat((self.page?.bottomOffset)! - 30), rightConstant: -16)

            self.mainImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 16, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
        } else if self.page?.number == 3 {
                        
            self.messageTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: CGFloat((self.page?.bottomOffset)! - 20), rightConstant: -16)
            
            self.mainImageView.image = UIImage(named: "gradient_bg")
            self.mainImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 16, rightConstant: 0, widthConstant: 0, heightConstant: 0)

            self.youtubeView.anchor(nil, left: leftAnchor, bottom: messageTextView.topAnchor, right: rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 16, rightConstant: 16, widthConstant: 0, heightConstant: 200)
        }
        
        self.messageTextView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.mainImageView.image = nil
    }
}

