//
//  ConfirmNumberViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 15/11/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
protocol XEconfirmDelegate: AnyObject {
    func proceed(numberReal: String)
    func dismiss()
}

class ConfirmNumberViewController: UIViewController {
    var delegate: XEconfirmDelegate?
    var phone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let textLbl = self.view.viewWithTag(1) as! UILabel
        textLbl.text = "Please confirm your phone number: \(self.phone!)"
    }
    
    @IBAction func confirm(_ sender: Any) {
        self.delegate?.proceed(numberReal: self.phone!)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.delegate?.dismiss()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
