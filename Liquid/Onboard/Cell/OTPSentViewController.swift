//
//  OTPSentViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 15/11/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class OTPSentViewController: UIViewController {

    var delegate: XEconfirmDelegate?
    var phone: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let textLbl = self.view.viewWithTag(1) as! UILabel
        textLbl.text = "A verification code has been sent to \(self.phone!)\n\nPlease check your device and enter code"
    }
    
    @IBAction func confirm(_ sender: Any) {
        self.delegate?.dismiss()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
