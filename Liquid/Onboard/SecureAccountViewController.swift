//
//  SecureAccountViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/11/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class SecureAccountViewController: ControllerWithBack {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar(title: "")
        // Do any additional setup after loading the view.
        let protectBtn = self.view.viewWithTag(2) as! UIButton
        protectBtn.layer.borderColor = UIColor.white.cgColor
        protectBtn.layer.borderWidth = 1
        
        let titleLbl = self.view.viewWithTag(1) as! UILabel
        let user = User.getUser()
        titleLbl.text = "Excellent, \(user!.firstName)!\n Now let’s protect your account"
    }
    
    override func viewDidLayoutSubviews() {
        let bgLayer = self.view.viewWithTag(100)!
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.hex(hex: "00F6FF").cgColor, UIColor.hex(hex: "02A9E8").cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = bgLayer.bounds
        bgLayer.layer.addSublayer(gradientLayer)
    }
    
    @IBAction func protect(_ sender: Any) {
        User.getUser()?.setSecuringAccount(state: true)
        self.navigationController?.pushViewController(ProfilePhotoMesageController(), animated: true)
    }
    
    @IBAction func skip(_ sender: Any) {
        let user = User.getUser()!
        if user.hasAcceptedTermsOfAgreement {
            Mics.setMainTheme()
            let destination = HomeTabController()
            destination.modalPresentationStyle = .fullScreen
            self.present(destination, animated: true, completion: nil)
        }  else {
            let destination = TermsController()
            destination.isJustTerms = true
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
