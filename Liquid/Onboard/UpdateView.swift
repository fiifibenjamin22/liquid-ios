//
//  UpdateView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 21/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol UpdateDelegate: AnyObject {
    func proceedToUpdate()
    func dismissUpdate()
}

class UpdateViewController: UIViewController {
    
    var versiondelegate: UpdateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func updateNow(_ sender: Any) {
        self.versiondelegate?.proceedToUpdate()
    }
    
    @IBAction func dismissUpdate(_ sender: Any) {
        self.versiondelegate?.dismissUpdate()
    }
    
}
