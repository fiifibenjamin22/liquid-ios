//
//  AboutSelfPart2ViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ADCountryPicker

class AboutSelfPart2ViewController: BaseScrollViewController {

    let apiService = ApiService()
    var occupation: Occupation?
    var country = ""
    var gender = ""
    
    let viewControllerHelper = ViewControllerHelper()
    var occupationPickerTopConstraint: NSLayoutConstraint?
    
    let actionButtonStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        return container
    }()
    
    lazy var residenceTextView: UITextView = {
        let textView = self.baseLocalTextView(label: "What is your country of citizenship?")
        return textView
    }()
    
    let residenceStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.distribution = .fillProportionally
        return container
    }()
    
    func baseLocalTextView(label: String) -> UITextView {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.attributedText = label.formatAsAttributed(font: boldFont, color: UIColor.darkText)
        
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }
    
    lazy var countryButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
        button.setTitle("Country of Citizenship", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 8,bottom: 0,right: 0)
        button.addBottomBorder(UIColor.darkGray, height: 0.5)
        button.addTarget(self, action: #selector(pickCountry), for: .touchUpInside)
        return button
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Tell Us About Yourself")
        
        scrollView.addSubview(residenceTextView)
        scrollView.addSubview(residenceStackView)
        scrollView.addSubview(countryButton)
        view.addSubview(actionButtonStackView)
        
        let width = view.frame.width - 32
        
        self.residenceTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.residenceTextView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        self.residenceTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- contry section
        self.countryButton.anchorWithConstantsToTop(top: residenceTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.countryButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.countryButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- action section
        self.actionButtonStackView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.actionButtonStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        //self.actionButtonStackView.addArrangedSubview(skipButton)
        self.actionButtonStackView.addArrangedSubview(nextButton)
        
        self.updateUtils()
        
    }
    
    //MARK:- get all utils
    func updateUtils()  {
        print("UPDATING...")
        self.apiService.getIdTypes { (status, idTypes, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("IDS")
            }
        }
        self.apiService.occupations { (status, occupations, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("OCCUPATIONS")
            }
        }
    }
    /**
     Triggers the selection of the country.
     
     ## Important Notes ##
     1. Returns the country **code** if the delegate is provided and implemented.
     */
    @objc func pickCountry() {
        ViewControllerHelper.presentCountryPicker(vc: self, delegate: self)
    }
    
    @objc func nextAction() {
        if self.country.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Choose your country")
            return
        }
        
        
        let nextVc = AboutSelfPart3ViewController()
        nextVc.country = self.country
        nextVc.gender = self.gender
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
}

extension AboutSelfPart2ViewController: ADCountryPickerDelegate {
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        let text = Mics.flag(country: code) + " " + name
        self.countryButton.setTitle(text, for: .normal)
        self.country = name
        picker.dismiss(animated: true, completion: nil)
    }
}
