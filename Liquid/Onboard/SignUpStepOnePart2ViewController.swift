//
//  SignUpStepOnePart2ViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import IHKeyboardAvoiding

class SignUpStepOnePart2ViewController: BaseScrollViewController {

    var number = ""
    var firstName = ""
    var lastName = ""
    var otherName = ""
    var dateOfBirth = ""
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkGray
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var dateTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        let font = UIFont(name: Font.Roboto.Light, size: 16)
        button.setTitle("Date of Birth", for: .normal)
        button.titleLabel?.font = font
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addBottomBorder(UIColor.darkGray, height: 0.5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(pickDateOfBirth(_:)), for: .touchUpInside)
        return button
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        
        textField.textColor = color
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }
    
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.newBaseButton()
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = UIColor.hex(hex: "e9e8e9")
        
        return button
    }()
    
    override func setUpView() {
        setUpNavigationBar(title: "")
        
        self.scrollView.addSubview(messageTextView)
        self.scrollView.addSubview(dateTextField)
        
        self.view.addSubview(nextButton)
        
        let width = self.view.frame.width - 32
        self.messageTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.dateTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.dateTextField.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.dateTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        
        let messageHeader = "Nice to meet you, \(self.firstName).\nWhen were you born?".formatAsAttributed(font: boldFont, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        self.messageTextView.attributedText = result
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, leftConstant: 15, rightConstant: -15, bottomConstant: -15)
        
        KeyboardAvoiding.avoidingView = self.dateTextField
    }
    
    //MARK:- pick date of birth instead
    @objc func pickDateOfBirth(_ sender: UIButton){
        //let sixteenInterval: TimeInterval = 30 * 12 * 4 * 7 * 24 * 60 * 60
        let startingDate = Calendar.current.date(byAdding: .year, value: -28, to: Date())!
        let datePicker = ActionSheetDatePicker(title: "SELECT BIRTHDAY", datePickerMode: UIDatePicker.Mode.date, selectedDate: startingDate, doneBlock: {
            picker, value, index in
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy"
            let dateTaken =  value! as! Date
            let date = Calendar.current.date(byAdding: .year, value: -18, to: Date())!
            if dateTaken >= date  {
                ViewControllerHelper.showPrompt(vc: self, message: "You must be 18 years or older.")
                return
            }
            self.dateOfBirth = dateFormatterGet.string(from: value! as! Date)
            self.dateTextField.setTitle(self.dateOfBirth, for: .normal)
            self.changeButtonStatus()
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        datePicker?.show()
    }
    
    //MARK:- proceed to change number
    @objc func changeNumber()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- proceed to the next page
    @objc func nextAction()  {
        let part3_vc = SignUpStepOnePart3ViewController()
        part3_vc.firstName = self.firstName
        part3_vc.lastName = self.lastName
        part3_vc.otherName = self.otherName
        part3_vc.dateOfBirth = self.dateOfBirth
        part3_vc.number = self.number
        self.navigationController?.pushViewController(part3_vc, animated: true)
    }
    
    
    
    func goNextToLocation()  {
        let destination = CurrentLocationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        if !dateOfBirth.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.hex(hex: "e9e8e9")
            self.nextButton.backgroundColor = color
        }
    }

}
