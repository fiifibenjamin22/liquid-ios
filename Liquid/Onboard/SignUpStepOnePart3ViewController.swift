//
//  SignUpStepOnePart3ViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import IHKeyboardAvoiding

class SignUpStepOnePart3ViewController: BaseScrollViewController {

    var number = ""
    var firstName = ""
    var lastName = ""
    var otherName = ""
    var email = ""
    var dateOfBirth = ""
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var emailTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Email Address")
        textField.keyboardType = UIKeyboardType.emailAddress
        return textField
    }()
    
    let emailErrorLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.red
        textView.backgroundColor = UIColor.clear
        textView.text = "Provide a valid email address."
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        
        textField.textColor = color
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }
    
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.newBaseButton()
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = UIColor.hex(hex: "e9e8e9")
        return button
    }()
    
    override func setUpView() {
        setUpNavigationBar(title: "")
        self.scrollView.addSubview(messageTextView)
        self.scrollView.addSubview(emailTextField)
        self.scrollView.addSubview(emailErrorLabel)
        self.view.addSubview(nextButton)
        
        let width = self.view.frame.width - 32
        self.messageTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.emailTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.emailTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.emailTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.emailErrorLabel.anchorWithConstantsToTop(top: emailTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
        self.emailErrorLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.emailErrorLabel.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        
        let messageHeader = "How can we reach you?".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        self.messageTextView.attributedText = result
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, leftConstant: 15, rightConstant: -15, bottomConstant: -15)

        self.emailErrorLabel.isHidden = true
        
        //KeyboardAvoiding.avoidingView = self.emailTextField
    }
    
    //MARK:- proceed to change number
    @objc func changeNumber()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- proceed to the next page
    @objc func nextAction()  {
        self.validateEmail(emailText: emailTextField.text!)
    }
    
    func goNextToLocation()  {
        let destination = CurrentLocationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        if textField.text!.containsEmoji{
            let theImoji = textField.text?.emojis[0]
            let alertController = UIAlertController(title: "Email Error", message: "your email must not contain emojis \(theImoji ?? ""), kindly correct and proceed", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Correct it", style: .default) { (finished) in
                textField.text?.removeLast()
            }
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        self.email = self.emailTextField.text!
        self.changeButtonStatus()
        self.emailErrorLabel.isHidden = Validator.isValidEmail(email: self.email)
    }
    
    func changeButtonStatus()  {
        if Validator.isValidEmail(email: self.email) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.hex(hex: "e9e8e9")
            self.nextButton.backgroundColor = color
        }
    }
    
    //make api call to check if the user e-mail is valid
    func validateEmail(emailText: String){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().checkEmail(email: emailText) { (status, errorMessage) in
            if status == ApiCallStatus.SUCCESS {
                self.viewControllerHelper.hideActivityIndicator()
                let part4_vc = SignUpStepOnePart4ViewController()
                part4_vc.firstName = self.firstName
                part4_vc.lastName = self.lastName
                part4_vc.otherName = self.otherName
                part4_vc.dateOfBirth = self.dateOfBirth
                part4_vc.email = self.email
                part4_vc.number = self.number
                self.navigationController?.pushViewController(part4_vc, animated: true)
            }else{
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: errorMessage)
            }
        }
    }
}
