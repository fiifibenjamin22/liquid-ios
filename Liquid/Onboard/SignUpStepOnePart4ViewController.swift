//
//  SignUpStepOnePart4ViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import IHKeyboardAvoiding

class SignUpStepOnePart4ViewController: BaseScrollViewController {

    var number = ""
    var firstName = ""
    var lastName = ""
    var otherName = ""
    var password = ""
    var email = ""
    var dateOfBirth = ""
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var passwordTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Password")
        textField.isSecureTextEntry = true
        textField.rightViewMode = UITextField.ViewMode.always
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: Images.Onboard.eyeClosed)
        imageView.isUserInteractionEnabled = true
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.rightView = imageView
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    let passwordHintLable: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.text = AppConstants.passwordHintMessage
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        textView.backgroundColor = UIColor.hex(hex: "e6f8ff")
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
        
        return textView
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        
        textField.textColor = color
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.newBaseButton()
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        button.isEnabled = false
        button.backgroundColor = UIColor.hex(hex: "e9e8e9")
        return button
    }()
    
    override func setUpView() {
        setUpNavigationBar(title: "")
        
        self.scrollView.addSubview(messageTextView)
        
        self.scrollView.addSubview(passwordTextField)
        self.scrollView.addSubview(passwordHintLable)
        self.view.addSubview(nextButton)
        
        
        let width = self.view.frame.width - 32
        self.messageTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.passwordTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.passwordTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.passwordHintLable.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordHintLable.heightAnchor.constraint(equalToConstant: 120).isActive = true
        self.passwordHintLable.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        
        let messageHeader = "Please set a secure password.".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        self.messageTextView.attributedText = result
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, leftConstant: 15, rightConstant: -15, bottomConstant: -15)
        
        //KeyboardAvoiding.avoidingView = self.passwordTextField
        
    }
    
    //MARK:- proceed to change number
    @objc func changeNumber()  {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- proceed to the next page
    @objc func nextAction()  {
        self.viewControllerHelper.showActivityIndicator()
        print("Creating: FN: \(self.firstName) LN: \(self.lastName) ON: \(self.otherName) EM: \(self.email) DOB: \(self.dateOfBirth) PASS: \(self.password)")
        self.apiService.createCustomer(firstName: self.firstName, lastName: self.lastName, otherNames: self.otherName, email: self.email, DOB: self.dateOfBirth, password: self.password) { (status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                self.loginUser()
            } else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    //MARK: needed to activate token
    private func loginUser()  {
        print("Logging in with PN: \(self.number) PS: \(self.password)")
        self.apiService.loginUser(number: self.number, password: self.password, isFingerprint: false) { (status, user, withMessage) in
            if status == ApiCallStatus.SUCCESS, let userIn = user {
                self.getProfile(customerId: userIn.cutomerId)
            } else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    //MARK:- get user profile
    func getProfile(customerId:Int)  {
        self.apiService.getAccountOverview(customerNumber: customerId) { (status, user, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.goNextToLocation()
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func goNextToLocation()  {
        self.updateUtils()
        let destination = CurrentLocationController()
        self.navigationController?.setViewControllers([destination], animated: true)
    }
    
    func updateUtils()  {
        print("UPDATING...")
        self.apiService.getIdTypes { (status, idTypes, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("IDS")
            }
        }
        self.apiService.occupations { (status, occupations, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("OCCUPATIONS")
            }
        }
    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        self.passwordTextField.resignFirstResponder()
        let isPassworMode = self.passwordTextField.isSecureTextEntry
        self.passwordTextField.isSecureTextEntry = !isPassworMode
        let image = isPassworMode == true ? UIImage(named: Images.Onboard.eyeClosed) : UIImage(named: Images.Onboard.eye)
        self.imageView.image = image
        passwordTextField.becomeFirstResponder()
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.password = self.passwordTextField.text!
        self.changeButtonStatus()
        
        //self.passwordHintLable.isHidden = true
        if textField == self.passwordTextField && !Validator.isValidPassword(password: self.password) {
            self.passwordHintLable.isHidden = false
        }
        
        self.passwordHintLable.text = Validator.getPasswordMessage(password: self.password)
    }
    
    func changeButtonStatus()  {
        if !firstName.isEmpty && !lastName.isEmpty && Validator.isValidEmail(email: self.email) && !dateOfBirth.isEmpty && Validator.isValidPassword(password: self.password) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.hex(hex: "e9e8e9")
            self.nextButton.backgroundColor = color
        }
    }
}
