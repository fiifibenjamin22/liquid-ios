//
//  PasswordResetFinalController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 08/08/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//
import UIKit
import ActionSheetPicker_3_0
import IHKeyboardAvoiding
import PopupDialog

class PasswordResetFinalController: ControllerWithBack {
    var number = ""
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var code =  ""
    var password = ""
    var passwordConfirm = ""
    var customerId = 0
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    var popup: PopupDialog?
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "Kindly check your email inbox. We have sent a code to the email associated with your account. Please enter the 6-digit code below to confirm ownership of the account."
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var codeTextField: UITextField = {
        let color = UIColor.darkGray
        let textField =  ViewControllerHelper.baseField()
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.textColor = UIColor.darkGray
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter code here",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }()
    
   /*
    lazy var passwordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = self.basePassword()
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter new password",
                                                              attributes: [NSAttributedStringKey.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    let passwordHintLable: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.red
        textView.text = AppConstants.passwordHintMessage
        textView.isHidden = true
        return textView
    }()
    
    lazy var confirmPasswordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.attributedPlaceholder =  NSAttributedString(string: "Confirm password",
                                                              attributes: [NSAttributedStringKey.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textAlignment = .left
        textField.textColor = UIColor.darkText
      
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        return textField
    }()
    */
    
    func basePassword() -> UITextField {
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textColor = UIColor.darkText
        
        textField.rightViewMode = UITextField.ViewMode.always
        //imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: Images.Onboard.eyeClosed)
        imageView.isUserInteractionEnabled = true
        
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.rightView = imageView
        
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        return textField
    }
    
    lazy var confirmButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Continue", for: .normal)
        let color = UIColor.gray
        button.backgroundColor = color
        button.isEnabled = false
        button.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        return button
    }()
    
    lazy var forgottenBtn: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "RESEND CODE"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.underline()
        lbl.textAlignment = .left
        lbl.backgroundColor = .clear
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(resendCode)))
        lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        return lbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarDark(title: "Confirm Reset")
       
        self.view.addSubview(messageTextView)
        self.view.addSubview(codeTextField)
        /*
        self.view.addSubview(passwordTextField)
        self.view.addSubview(passwordHintLable)
        self.view.addSubview(confirmPasswordTextField)*/
        self.view.addSubview(confirmButton)
        self.view.addSubview(forgottenBtn)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.messageTextView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        
        //MARK:
        self.codeTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.codeTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true

        self.confirmButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.confirmButton.anchorWithConstantsToTop(top: codeTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor,topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        
        self.forgottenBtn.topAnchor.constraint(equalTo: self.confirmButton.bottomAnchor, constant: 12.0).isActive = true
        self.forgottenBtn.leftAnchor.constraint(equalTo: self.confirmButton.leftAnchor).isActive = true
        self.forgottenBtn.rightAnchor.constraint(equalTo: self.confirmButton.rightAnchor).isActive = true
        self.forgottenBtn.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
    }


    //complete reset action
    @objc func doneAction(){
        if self.code.isEmpty {
            let message = "Please provide code to proceed."
            ViewControllerHelper.showPrompt(vc: self, message: message)
            return
        }
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.confirmForgottenCode( code: self.code) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let setPassword_vc = SetNewPasswordViewController(nibName: "SetNewPasswordView", bundle: nil)
                setPassword_vc.delegate = self
                self.popup = PopupDialog(viewController: setPassword_vc)
                self.present(self.popup!, animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    //MARK: - resend code
    @objc func resendCode(){
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.initForgotPassword(number: self.number) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.customerId = Int(withMessage)!
                let message = "Password reset initiated. A verification code has been sent to: \(self.number)"
                ViewControllerHelper.showPrompt(vc: self, message: message, completion: { (isDone) in
                    self.codeTextField.becomeFirstResponder()
                })
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.code = self.codeTextField.text!
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        if !self.code.isEmpty {
            self.confirmButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.confirmButton.backgroundColor = color
        } else {
            self.confirmButton.isEnabled = false
            let color = UIColor.gray
            self.confirmButton.backgroundColor = color
        }
    }
    /*
    @objc func tap(sender: UITapGestureRecognizer){
        var field = passwordTextField
        if sender.view! == self.confirmPasswordTextField  {
            field = confirmPasswordTextField
        }
        let isPassworMode = field.isSecureTextEntry
        self.setStatus(isPassworMode: isPassworMode, textFiled: field)
    }
    
    func setStatus(isPassworMode:Bool,textFiled:UITextField)  {
        if isPassworMode {
            imageView.image = UIImage(named: Images.Onboard.eyeClosed)
            textFiled.isSecureTextEntry = false
        }  else {
            imageView.image = UIImage(named: Images.Onboard.eye)
            textFiled.isSecureTextEntry = true
        }
    }*/

}

extension PasswordResetFinalController: SetPasswordDelegate {
    func proceed(password: String) {
        self.popup?.dismiss()
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.resetPassword(password: password) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: {(Alert:UIAlertAction!) -> Void in
                    self.dismiss(animated: true, completion: nil)
                })
                let alertView = UIAlertController(title: "Liquid", message: "Your password has been successfully reset", preferredStyle: .alert)
                alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
                alertView.addAction(alertAction)
                self.present(alertView, animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
}
