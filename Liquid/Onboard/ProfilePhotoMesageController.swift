//
//  ProfilePhotoMesageController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import CropViewController

class ProfilePhotoMesageController: BaseScrollViewController {
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var isInApp = false
    var documentName = ""
    
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 50
        imageView.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        imageView.image = UIImage(named: "Circle")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let titleTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.darkText
        textView.text = "Selfie"
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        return textView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 30, bottom: 0, right: 30)
        textView.backgroundColor = UIColor.hex(hex: "e6f8ff")
        
        textView.text = "Follow these instructions to get the best results:\n\n• Do not wear any headgear or eyewear\n\n• Make sure you have enough light\n\n• Snap a photo\n\n• Confirm that the photo is not blurry\n\n"
        
        return textView
    }()
    
    let actionButtonStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.addBackground(color: UIColor.white)
        return container
    }()
    
    lazy var skipButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("Skip", for: .normal)
        button.addTarget(self, action: #selector(skipAction), for: .touchUpInside)
        return button
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.layer.cornerRadius = 5
        button.setTitle("Take a selfie", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Take a selfie")
        
        scrollView.addSubview(messageTextView)
        scrollView.addSubview(profileImageView)
        scrollView.addSubview(titleTextView)
        view.addSubview(actionButtonStackView)
        
        let width = view.frame.width - 32
        self.profileImageView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.profileImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.titleTextView.anchorWithConstantsToTop(top: profileImageView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.titleTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: titleTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: -50, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        //MARK:- action section
        self.actionButtonStackView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -15, rightConstant: -16)
        self.actionButtonStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        self.actionButtonStackView.addArrangedSubview(skipButton)
        self.actionButtonStackView.addArrangedSubview(nextButton)
        
    }
    
    @objc func nextAction() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            picker.allowsEditing = false
            picker.cameraCaptureMode = .photo
            self.present(picker, animated: true, completion: nil)
        }  else {
           ViewControllerHelper.showPrompt(vc: self, message: "Your device does not support camera options.")
        }
    }
    
    @objc func skipAction() {
        self.skip()
    }
 
}

extension ProfilePhotoMesageController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[.originalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: .circular, image: image)
        cropController.delegate = self
        picker.pushViewController(cropController, animated: true)
    }
    
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
     func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        cropViewController.dismiss(animated: true, completion: nil)
  
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.saveSelfieProfile(image: image) { (status,url,withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                User.updateUserSelfie(status: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadCoach"), object: self)
                self.goNext()
            }  else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func goNext()  {
        Mics.startNextControllerForUpgrade(controller: self)
        //Mics.startController(controller: self)
    }
    
    func skip()  {
        User.skipUserSelfie()
        self.goNext()
    }
}
