//
//  CurrentLocationController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 10/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//


import UIKit
import GoogleMaps
import GooglePlaces
import LinearProgressBarMaterial
import Alamofire
import SwiftyJSON

class CurrentLocationController: UIViewController {
    
    var locationManager = CLLocationManager()
    var currentLocation: GMSPlace?
    var tempLocation: TempLocation?
    var selectedLocation:CLLocation?
    var placesClient = GMSPlacesClient.shared()
    var zoomLevel: Float = 15.0
    var previewBottomConstraint: NSLayoutConstraint?
    var marker: GMSMarker?
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var isUpdate = false
    var locations = [TempLocation]()
    let cellId = "cellId"
    var formattedPlace = ""
    
    lazy var mapView: GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: 2.6676,
                                              longitude: 9.000,
                                              zoom: 15.0)
        let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        //mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //mapView.isMyLocationEnabled = true
        mapView.delegate = self
        //mapView.isHidden = true
        return mapView
    }()
    
    let locSelectedTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Selected Location"
        lbl.backgroundColor = .lightGray
        lbl.textColor = .darkGray
        lbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let addressPreviewTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.white
        textView.dropShadow()
        return textView
    }()
    
    lazy var pickCurrentLocationButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setImage(UIImage(named: "CurrentLocation"), for: .normal)
        button.addTarget(self, action: #selector(currentPlace(_:)), for: .touchUpInside)
        button.dropShadow()
        return button
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Continue", for: .normal)
        button.addTarget(self, action: #selector(acceptLocation), for: .touchUpInside)
        return button
    }()
    
    
    private lazy var progressBar: LinearProgressBar = {
        let progress = ViewControllerHelper.progressBar()
        return progress
    }()
    
    lazy var bottomUIView: CardView = {
        let view = CardView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = ViewControllerHelper.baseVerticalCollectionView()
        collectionView.isHidden = true
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.clear
        return collectionView
    }()
    
    private lazy var searchView: UISearchBar = {
        let mySearchBar = UISearchBar()
        mySearchBar.delegate = self
        mySearchBar.layer.position = CGPoint(x: self.view.bounds.width/2, y: 100)
        mySearchBar.layer.shadowColor = UIColor.black.cgColor
        mySearchBar.layer.shadowOpacity = 0.5
        mySearchBar.layer.masksToBounds = false
        mySearchBar.showsCancelButton = false
        mySearchBar.showsBookmarkButton = false
        
        // set Default bar status.
        mySearchBar.searchBarStyle = UISearchBar.Style.default
        mySearchBar.placeholder = "Enter your address or tap on the map"
        mySearchBar.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        mySearchBar.showsSearchResultsButton = false
        mySearchBar.translatesAutoresizingMaskIntoConstraints = false
        return mySearchBar
    }()
    
    override func viewDidLoad() {
        setUpNavigationBar(title: "Where do you live?")
        // Initialize the location manager.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        
        self.view.addSubview(mapView)
        self.view.addSubview(pickCurrentLocationButton)
        self.view.addSubview(progressBar)
        self.view.addSubview(collectionView)
        self.view.addSubview(searchView)
        
        self.progressBar.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.progressBar.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        self.searchView.anchorWithConstantsToTop(top: progressBar.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 30, leftConstant: 16, bottomConstant: -200, rightConstant: -16)
        self.searchView.heightAnchor.constraint(equalToConstant: 44).isActive = true
      
        
        self.pickCurrentLocationButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: -265, rightConstant: 0)
        self.pickCurrentLocationButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.pickCurrentLocationButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        self.pickCurrentLocationButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        
        self.view.addSubview(bottomUIView)
        self.bottomUIView.addSubview(locSelectedTitle)
        self.bottomUIView.addSubview(addressPreviewTextView)
        self.bottomUIView.addSubview(nextButton)
        
        self.bottomUIView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: -10)
        self.bottomUIView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        self.previewBottomConstraint = bottomUIView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        self.previewBottomConstraint?.isActive = true
        self.bottomUIView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
        self.locSelectedTitle.topAnchor.constraint(equalTo: bottomUIView.topAnchor).isActive = true
        self.locSelectedTitle.leftAnchor.constraint(equalTo: bottomUIView.leftAnchor).isActive = true
        self.locSelectedTitle.rightAnchor.constraint(equalTo: bottomUIView.rightAnchor).isActive = true
        self.locSelectedTitle.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: bottomUIView.leadingAnchor, bottom: bottomUIView.safeAreaLayoutGuide.bottomAnchor, right: bottomUIView.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -10, rightConstant: -8)
        self.nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.addressPreviewTextView.anchorWithConstantsToTop(top: locSelectedTitle.bottomAnchor, left: bottomUIView.leadingAnchor, bottom: nextButton.topAnchor, right: bottomUIView.trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: -8, rightConstant: -8)
       
        self.collectionView.anchorWithConstantsToTop(top: searchView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: bottomUIView.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.collectionView.register(GenericSectionCell.self, forCellWithReuseIdentifier: cellId)
        
        
        self.getCurrentPlace()
        
    }
    
    
    @objc func acceptLocation(){
        
        if let currentPlace = self.currentLocation {
            var name = "\(String(describing: currentPlace.name))"
            if let namePreview = currentPlace.formattedAddress {
                name = namePreview
            }
            let lat = currentPlace.coordinate.latitude
            let lon = currentPlace.coordinate.longitude
            let city = "\(currentPlace.formattedAddress?.split(separator: ",")[1] ?? "")"
            let country = "\(currentPlace.formattedAddress?.split(separator: ",")[2] ?? "")"
            print("current places: ",city)
            print("current places: ",currentPlace.formattedAddress?.split(separator: ",")[2] ?? "")
            
            //guard let currentPlaces = currentPlace.addressComponents else {return}
            //print("current places: ",currentPlaces)
            
//            for component in currentPlaces {
//
//                print("current place type: ",component.name)
//                print("current place type: ",component.shortName ?? "")
//
//                if component.type == "locality" {
//                    city = component.name
//                }
//                if component.type == "country" {
//                    country = component.name
//                }
//            }
            
            self.updateLocationApi(lon: lon, lat: lat, name: name, country: country, city: city)

        } else if let currentPlace = self.tempLocation {
            self.updateLocationApi(lon: currentPlace.lon, lat: currentPlace.lat, name: currentPlace.name, country: currentPlace.country, city: currentPlace.city)
        }
        
    }
    
    func updateLocationApi(lon:Double,lat:Double,name:String,country:String,city:String)  {
        
        print("country: ",country," city: ",city)
        if country.isEmpty || city.isEmpty {
            let destination = SignUpStepTwoController()
            destination.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(destination, animated: true)
            return
        }
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.saveResidence(latitude: lat, longitude: lon, streetAddress: name, city: city, country: country) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.goToNext()
            }   else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }

    
}

// Delegates to handle events for the location manager.
extension CurrentLocationController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if  mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        self.getCurrentPlace()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Restricted")
        case .denied:
            ViewControllerHelper.showPrompt(vc: self, message: "Liquid works great when you grant location permission.")
            let camera = GMSCameraPosition.camera(withLatitude: 5.6004282, longitude: -0.2009291, zoom: zoomLevel)
            mapView.animate(to: camera)
            
            mapView.isHidden = false
            self.addMarkerWithoutLocation()
            self.pickCurrentLocationButton.isHidden = true
        case .notDetermined:
            print("Not determined")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("When in use")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
    }
}


extension CurrentLocationController: UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.removeDataAndHide()
    }
    
    override func pressesCancelled(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        self.removeDataAndHide()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.removeDataAndHide()
    }
    
    func removeDataAndHide()  {
        self.locations.removeAll()
        self.collectionView.reloadData()
        self.collectionView.isHidden = true
        self.view.endEditing(true)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            
            self.locations.removeAll()
            self.collectionView.isHidden = false
            self.collectionView.reloadData()
            
            if let results = results {
                for result in results {
                    let tempLocation = TempLocation(number: result.placeID, subLocality: "", street: "", country: "", name: result.attributedFullText.string, lat: 0.0, lon: 0.0, city: "")
                    self.locations.append(tempLocation)
                    
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
            
            let cantFind = TempLocation(number: "NO_LOC", subLocality: "", street: "", country: "", name: "Can't find my location", lat: 0.0, lon: 0.0, city: "")
            let returnMap = TempLocation(number: "PICK", subLocality: "", street: "", country: "", name: "Set Location on map", lat: 0.0, lon: 0.0, city: "")
            
            self.locations.append(returnMap)
            self.locations.append(cantFind)
            
            self.collectionView.reloadData()
        })

        
        return
        print("Searching: ", searchText)
        let apiKey = Mics.getGooglePlaceApiKey()
        let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchText)&key=\(apiKey)"
        print(url)
        self.progressBar.startAnimation()
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: nil)
            .responseJSON { response in
                self.progressBar.stopAnimation()
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        let item = try! JSON(data: response.data!)
                        self.locations.removeAll()
                        self.collectionView.isHidden = false
                        self.collectionView.reloadData()
                        
                        for (_, locationItem) in item["predictions"]{
                            print(locationItem)
                            
                            //let locationItem = itemIn.element.1
                            let formatedAddress = locationItem["formatted_address"].stringValue.split(separator: ",")
                            var street = ""
                            var country = ""
                            var city = ""
                            var locality = ""
                            if formatedAddress.count >= 4 {
                                street = String(formatedAddress[0]).trimmingCharacters(in: .whitespacesAndNewlines)
                                locality = String(formatedAddress[1]).trimmingCharacters(in: .whitespacesAndNewlines)
                                city = String(formatedAddress[2]).trimmingCharacters(in: .whitespacesAndNewlines)
                                country = String(formatedAddress[3]).trimmingCharacters(in: .whitespacesAndNewlines)
                            }
                            if formatedAddress.count >= 3 {
                                locality = String(formatedAddress[0]).trimmingCharacters(in: .whitespacesAndNewlines)
                                city = String(formatedAddress[1]).trimmingCharacters(in: .whitespacesAndNewlines)
                                country = String(formatedAddress[2]).trimmingCharacters(in: .whitespacesAndNewlines)
                            }
                            if formatedAddress.count >= 2 {
                                city = String(formatedAddress[0]).trimmingCharacters(in: .whitespacesAndNewlines)
                                country = String(formatedAddress[1]).trimmingCharacters(in: .whitespacesAndNewlines)
                            }
                            if formatedAddress.count >= 1 {
                                country = String(formatedAddress[0]).trimmingCharacters(in: .whitespacesAndNewlines)
                            }
                            let tempLocation = TempLocation(number: locationItem["place_id"].stringValue, subLocality: locality, street: street, country: country, name: locationItem["description"].stringValue, lat: locationItem["geometry"]["location"]["lat"].doubleValue, lon: locationItem["geometry"]["location"]["lng"].doubleValue, city: city)
                            self.locations.append(tempLocation)
                        }
                        
                        let cantFind = TempLocation(number: "NO_LOC", subLocality: "", street: "", country: "", name: "Can't find my location", lat: 0.0, lon: 0.0, city: "")
                        let returnMap = TempLocation(number: "PICK", subLocality: "", street: "", country: "", name: "Set Location on map", lat: 0.0, lon: 0.0, city: "")
                        
                        self.locations.append(returnMap)
                        self.locations.append(cantFind)
                        
                        self.collectionView.reloadData()
                    case 301...499:
                        print("FAILED")
                    default:
                        print("Field")
                    }
                }
        }
    }
    
    func getLocationFromPrediction(record: TempLocation, success: @escaping (_ location: TempLocation) -> Void) {
        let placeClient = GMSPlacesClient()
        self.progressBar.startAnimation()
        
        placeClient.lookUpPlaceID(record.number) { (place, error) -> Void in
            self.progressBar.stopAnimation()
            if error != nil {
                //show error
                return
            }
            
            if let place = place {
                self.currentLocation = place
                success(TempLocation(number: place.placeID!, subLocality: place.formattedAddress!, street: place.formattedAddress!, country: place.formattedAddress!, name: place.name!, lat: place.coordinate.latitude, lon: place.coordinate.longitude, city: ""))
                
            } else {
                //show error
            }
        }
    }
}

//MARK:- swiping collection view delagates implementation
extension CurrentLocationController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.locations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! GenericSectionCell
        let location = self.locations[indexPath.row]
        
        if location.number == "NO_LOC" || location.number == "PICK" {
            let font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
            let attributes: [NSAttributedString.Key: Any] = [.font: font]
            
            let imageAttachment = NSTextAttachment()
            let image_name = location.number == "NO_LOC" ? "cant_find" : "set_location"
            let image = UIImage(named: image_name)
            
            imageAttachment.image = image
            
            let imageSize = imageAttachment.image!.size
            imageAttachment.bounds = CGRect(x: CGFloat(0), y: (font.capHeight - imageSize.height) / 2, width: imageSize.width, height: imageSize.height)

            let imageString = NSAttributedString(attachment: imageAttachment)
            
            let fullString = NSMutableAttributedString(string: "", attributes: attributes)
            fullString.append(imageString)
            fullString.append(NSAttributedString(string: "   " + location.name, attributes: attributes))

            cell.attributedItem = fullString
        } else {
            cell.item = location.name
        }
        
        cell.messageTextView.isUserInteractionEnabled = true
        cell.messageTextView.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedItem(_:)))
        cell.messageTextView.addGestureRecognizer(tapGestureRecognizer)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = view.frame.width
        return CGSize(width: itemWidth, height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    @objc func tappedItem(_ sender: UIGestureRecognizer)  {
        
        let item = self.locations[(sender.view?.tag)!]
        
        if item.number == "NO_LOC" {
            self.removeDataAndHide()
            
            let destination = SignUpStepTwoController()
            destination.isUpdate = self.isUpdate
            self.navigationController?.pushViewController(destination, animated: true)
            
            return
        }
        
        if item.number == "PICK" {
            self.removeDataAndHide()
            return
        }
        
        // Check if item from auto complete
        self.getLocationFromPrediction(record: item, success: { item in
            self.tempLocation = item
            self.showNamedPreview(name: item.name, address: item.subLocality)
            self.removeDataAndHide()
            
            // Zoom map and add marker
            let camera = GMSCameraPosition.camera(withLatitude: item.lat,
                                                  longitude: item.lon,
                                                  zoom: self.zoomLevel)
            self.mapView.animate(to: camera)
            self.marker?.position = CLLocationCoordinate2D(latitude: item.lat, longitude: item.lon)
        })
        
    }
}
extension CurrentLocationController: GMSMapViewDelegate {
    
//    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
//        self.marker?.position = coordinate
//        self.getLocationName(coordinate: coordinate)
//    }
}

extension CurrentLocationController {
    
    func goToNext()  {
        User.updateUserAddress(status: true)
        if self.isUpdate {
            UserDefaults.standard.set(true, forKey: Key.profileUpdated)
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "secure_account_vc") as! SecureAccountViewController
        self.navigationController?.pushViewController(destination, animated: true)
        //Mics.startController(controller: self)
    }
    
    func showPreview()  {
        if self.currentLocation != nil {
            //self.getLocationName(coordinate: currentPlace.coordinate)
            self.updateMarker()
        }
    }
    
    func showNamedPreview(name:String?,address:String?)  {
        
        print("print current name: ",name ?? "","\nprint current address: ",address ?? "")
        self.updateMarker()
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 18)!)
        let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 12)!)
        var addressPreviewable = "No associated address."
        var namedLocation = "Unnamed location."
        if let addressPreview = address {
            addressPreviewable = addressPreview
        }
        if let namePreview = name {
            namedLocation = namePreview
        }
        let messageHeader = "\(namedLocation).\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let messageBody = "\(addressPreviewable)".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        
        self.addressPreviewTextView.attributedText = result
        self.previewBottomConstraint?.constant = -200
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    func addMarker()  {
        if  (self.currentLocation != nil) && self.marker == nil {
            let place = self.currentLocation!
            marker = GMSMarker()
            marker?.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            marker?.title = "\(place.name ?? "")"
            var address = "No associated address."
            if let namePreview = place.formattedAddress {
                address = namePreview
            }
            marker?.snippet = "\(address)"
            marker?.isDraggable = true
            marker?.map = mapView
        }
    }
    
    func addMarkerWithoutLocation(){
        marker = GMSMarker()
        marker?.position = CLLocationCoordinate2D(latitude: 5.6004282, longitude: -0.2009291)
        marker?.title = "Accra, Ghana"
        marker?.snippet = "Accra, Ghana"
        marker?.isDraggable = true
        marker?.map = mapView
    }
    
    func updateMarker()   {
        if let place = self.currentLocation {
            //marker = GMSMarker()
            marker?.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
            let camera = GMSCameraPosition.camera(withTarget: (marker?.position)!, zoom: zoomLevel)
            mapView.camera = camera
        }
    }
    
    // Populate the array with the list of likely places.
    func getCurrentPlace(addMarker: Bool = false) {
        placesClient.currentPlace(callback: { (placeLikelihoods, error)  in
            if let place =  placeLikelihoods?.likelihoods.last {
                let camera = GMSCameraPosition.camera(withLatitude: place.place.coordinate.latitude,
                                                      longitude: place.place.coordinate.longitude,
                                                      zoom: self.zoomLevel)
                self.mapView.animate(to: camera)
                
                print("print current place: ",place.place.formattedAddress ?? "")
                self.formattedPlace = place.place.formattedAddress ?? ""
                
                self.currentLocation = place.place
                self.getLocationName(coordinate: place.place.coordinate, formattedAddress: place.place.formattedAddress!)
                
                if !addMarker {
                    self.addMarker()
                }   else {
                    self.updateMarker()
                    self.showPreview()
                }
            }
            
            if let error = error {
                print("\(error.localizedDescription)")
                return
            }
        })
    }
    
    // Populate the array with the list of likely places.
    @objc func currentPlace(_ sender: UIButton) {
        self.getCurrentPlace(addMarker: true)
    }
    
    func setUpNavigationBar(title:String = "")  {
        self.view.backgroundColor = UIColor.white
        navigationItem.title = title
        
        var image = UIImage(named: "BackArrow")?.withRenderingMode(.alwaysOriginal)
        if self.isUpdate {
           image = UIImage(named: "BackWhite")?.withRenderingMode(.alwaysOriginal)
        }
        _ = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleCancel))
        //navigationItem.leftBarButtonItem = menuBack
    }
    
    @objc func handleCancel()  {
        dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func getLocationName(coordinate: CLLocationCoordinate2D,formattedAddress: String)  {
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        geocoder.reverseGeocodeLocation(location) { (placemarksArray, error) in
            
            if (placemarksArray?.count)! > 0 {
                
                let placemark = placemarksArray?.first
                
                let country = placemark!.country!
                let numberReal = placemark!.subThoroughfare == nil ? "" : placemark!.subThoroughfare!
                let street = placemark!.thoroughfare == nil ? "" : placemark!.thoroughfare!
                let subLocal = placemark!.subLocality == nil ? "" : placemark!.subLocality!
                let name = placemark!.name == nil ? "" : placemark!.name!
                
                //self.currentLocation = nil
                self.tempLocation = TempLocation(number: numberReal, subLocality: subLocal, street: street, country: country, name: name, lat: coordinate.latitude, lon: coordinate.longitude, city: subLocal)
                let address = formattedAddress
                                
                self.showNamedPreview(name: name, address: address)
            }
        }
    }
  
}

