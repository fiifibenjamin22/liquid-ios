//
//  ResetPasswordController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import IHKeyboardAvoiding

class ResetPasswordController: ControllerWithBack {
    var number = ""
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var dateOfBirth = ""
    var customerId = 0
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    var has_full_account = false
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "We will send a 6-digit code to the email address associated with your LIQUID account."
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
   /*
    lazy var dateTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, frameWidth  - image.size.width*1.5, 0, 0);
        
        let font = UIFont(name: Font.Roboto.Light, size: 16)
        button.setTitle("Date of Birth", for: .normal)
        button.titleLabel?.font = font
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addBottomBorder(UIColor.darkGray, height: 1)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(pickDateOfBirth(_:)), for: .touchUpInside)
        return button
    }()
    
    
    lazy var dateofBirthTextField: UITextField = {
        let color = UIColor.darkGray
        let textField =  ViewControllerHelper.baseField()
        textField.isHidden = true
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textField.textColor = UIColor.darkGray
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter date of birth",
                                                              attributes: [NSAttributedStringKey.foregroundColor: color])
        return textField
    }()
    
    
    lazy var idNumberTextfield: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.setBottomBorder(color: color.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter ID number",
                                                              attributes: [NSAttributedStringKey.foregroundColor: color])
        //textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        return textField
    }()
    */
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Send Reset Code", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarDark(title: "Reset Password")
        
        self.view.addSubview(messageTextView)
        self.view.addSubview(nextButton)
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.messageTextView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.nextButton.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor,topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
    }
    
    //init forgot password
    @objc func nextAction(){
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.initForgotPassword(number: self.number) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.customerId = Int(withMessage)!
                let message = "Password reset initiated. A verification code has been sent to: \(self.number)"
                let destination = PasswordResetFinalController()
                destination.number = self.number
                destination.modalPresentationStyle = .fullScreen
                destination.customerId = self.customerId
                self.navigationController?.pushViewController(destination, animated: true)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
}
