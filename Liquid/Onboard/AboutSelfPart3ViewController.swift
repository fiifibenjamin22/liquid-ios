//
//  AboutSelfPart3ViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class AboutSelfPart3ViewController: BaseScrollViewController {

    let apiService = ApiService()
    var occupation: Occupation?
    var country: String?
    var gender: String?
    
    let viewControllerHelper = ViewControllerHelper()
    var occupationPickerTopConstraint: NSLayoutConstraint?
    
    lazy var occupationTextView: UITextView = {
        let textView = self.baseLocalTextView(label: "What is your occupation?")
        return textView
    }()
    
    let actionButtonStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        return container
    }()
    
    func baseLocalTextView(label: String) -> UITextView {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.backgroundColor = UIColor.clear
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.attributedText = label.formatAsAttributed(font: boldFont, color: UIColor.darkText)
        
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }
    
    lazy var occupationButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
        button.setTitle("Occupation", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 8,bottom: 0,right: 0)
        button.addBottomBorder(UIColor.darkGray, height: 0.5)
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrow")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        button.addTarget(self, action: #selector(pickOccupation(_:)), for: .touchUpInside)
        return button
    }()
    
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Tell Us About Yourself")
        
        scrollView.addSubview(occupationTextView)
        scrollView.addSubview(occupationButton)
        view.addSubview(actionButtonStackView)
        
        let width = view.frame.width - 32
        
        self.occupationTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.occupationTextView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        self.occupationTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- occupation section
        self.occupationPickerTopConstraint = self.occupationButton.topAnchor.constraint(equalTo: occupationTextView.bottomAnchor, constant: 16)
        self.occupationPickerTopConstraint?.isActive = true
        self.occupationButton.anchorWithConstantsToTop(top: nil, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.occupationButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.occupationButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- action section
        self.actionButtonStackView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.actionButtonStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        //self.actionButtonStackView.addArrangedSubview(skipButton)
        self.actionButtonStackView.addArrangedSubview(nextButton)
        
        self.updateUtils()
        
    }
    
    //MARK:- get all utils
    func updateUtils()  {
        print("UPDATING...")
        self.apiService.getIdTypes { (status, idTypes, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("IDS")
            }
        }
        /*
        self.apiService.occupations { (status, occupations, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("OCCUPATIONS")
            }
        }*/
    }
    
    @objc func nextAction() {
        var occupationName = ""
        
        if let occu = self.occupation {
            occupationName = occu.name
        }
        
        if occupationName == "" {
            ViewControllerHelper.showPrompt(vc: self, message: "Please select an occupation")
            return
        }
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.saveKYCinformation(gender: self.gender!, citizenship: self.country!, occupation: occupationName, completion: { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadCoach"), object: self)
                self.goNext()
            }  else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        })
    }
    
    func goNext() {
        User.updateUserExtraInfo(status: true)
        Mics.startNextControllerForUpgrade(controller: self)
    }
    
    @objc func pickOccupation(_ sender: UIButton)  {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        let occupations = Occupation.all()
        for occupation in occupations {
            let defaultAction = UIAlertAction(title: "\(occupation.name)", style: .default) { (action) in
                self.occupation = occupation
                self.occupationButton.setTitle(occupation.name, for: .normal)
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }

}
