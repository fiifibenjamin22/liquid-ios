//
//  ChangeNumberController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ADCountryPicker

class ChangeNumberController: ControllerWithBack {
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
   
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "Please confirm phone number"
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.isEnabled = false
        let color = UIColor.gray
        button.backgroundColor = color
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    lazy var phoneNumberView: PhoneNumberView = {
        let view = PhoneNumberView()
        view.selectPicker = self
        view.delegate =  self
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarDark(title: "Change Number")
    
        self.setUpUI()
    }
    
    func setUpUI()  {
       self.view.addSubview(messageTextView)
       self.view.addSubview(phoneNumberView)
       self.view.addSubview(nextButton)
        
       self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
       self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
       self.phoneNumberView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
       self.phoneNumberView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
    }
    
    @objc func nextAction(){
        let number = self.phoneNumberView.phoneNumber
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.registerPhone(number: number) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let destination = VerificationCodeController()
                destination.number = number
                let navVC = UINavigationController(rootViewController: destination)
                self.present(navVC, animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    //MARK - auto advance input fields
    func textFieldDidChange(text: String){
        if text.utf16.count == 10 || text.utf16.count == 9 {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }  else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
}

extension ChangeNumberController: TextChangeDelegate {
    func didChange(text: String) {
       self.textFieldDidChange(text: text)
    }
}

extension ChangeNumberController: CountryPickerDelegate,ADCountryPickerDelegate {
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        let text = Mics.flag(country: code) + " " + dialCode
        self.phoneNumberView.countryButton.setTitle(text, for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func clickCountryPicker() {
        ViewControllerHelper.presentCountryPicker(vc: self, delegate: self)
    }
}
