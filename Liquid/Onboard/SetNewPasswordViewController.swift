//
//  SetNewPasswordViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 27/03/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
protocol SetPasswordDelegate: AnyObject {
    func proceed(password: String)
    func dismiss()
}

class SetNewPasswordViewController: UIViewController {
    var delegate: SetPasswordDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.delegate?.dismiss()
    }
    
    @IBAction func submit(_ sender: Any) {
        let passwordTxt = self.view.viewWithTag(1) as! UITextField
        
        if !Validator.isValidPassword(password: passwordTxt.text){
            ViewControllerHelper.showPrompt(vc: self, message: AppConstants.passwordHintMessage)
            return
        }

        self.delegate?.proceed(password: passwordTxt.text!)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
