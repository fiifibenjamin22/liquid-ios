//
//  LoginController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 02/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import SwiftyJSON
import BiometricAuthentication

class LoginController: ControllerWithBack {
    var number = ""
    var customer_id = ""
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var passwordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        textField.rightViewMode = UITextField.ViewMode.always
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: "EyeClosed")
        imageView.isUserInteractionEnabled = true
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.rightView = imageView
        
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    
    lazy var loginButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Sign In", for: .normal)
        button.setImage(UIImage(named: "ArrowRight"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        return button
    }()
    
    lazy var forgotPasswordButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("I forgot my password", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(forgotPassword), for: .touchUpInside)
        return button
    }()
    
    lazy var fingerPrintLogin: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Fingerprint", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(fingerPrintLoginAction), for: .touchUpInside)
        button.setImage(UIImage(named: "Fingerprint"), for: .normal)
        button.imageView?.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        return button
    }()
    
    lazy var signUpButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Don't have an account", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(signUp), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.white
        setUpNavigationBarDark(title: "Log In")
        
        //MARK:- settting up the UI elements
        self.setUpUI()
    }
    
    func setUpUI()  {
        
        print("phone: ",self.number)
        print("customer_id: ",self.customer_id)
        
        let savedPhone = UserDefaults.standard.string(forKey: "phone")
        let savedPassword = UserDefaults.standard.string(forKey: "password")
        
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        print("Finger print status \(fingerPrintEnabled)")
        print("Saaved Phone \(String(describing: savedPhone))")
        print("Finger print status \(String(describing: savedPassword))")
        self.view.addSubview(messageTextView)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(forgotPasswordButton)
        self.view.addSubview(signUpButton)
        self.view.addSubview(loginButton)
        
        
        //MARK:- anchoring elements
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.passwordTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.loginButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.loginButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        if fingerPrintEnabled || ((savedPassword != nil) && (savedPhone != nil)) {
            self.view.addSubview(fingerPrintLogin)
            self.fingerPrintLogin.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
            self.fingerPrintLogin.heightAnchor.constraint(equalToConstant: 20).isActive = true
            self.forgotPasswordButton.anchorWithConstantsToTop(top: fingerPrintLogin.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
        } else {
            self.forgotPasswordButton.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
        }
        
        self.forgotPasswordButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.signUpButton.anchorWithConstantsToTop(top: forgotPasswordButton.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 24, bottomConstant: 0, rightConstant: -16)
        self.signUpButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let messageHeader = "Welcome back.\n".formatAsAttributed(fontSize: 30, color: UIColor.darkText)
        let messageBody = "Please sign in to continue".formatAsAttributed(fontSize: 16, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        
        self.messageTextView.attributedText = result
        KeyboardAvoiding.avoidingView = self.loginButton
        
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text!
        if Validator.isValidPassword(password: text) {
            self.loginButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.loginButton.backgroundColor = color
        }   else {
            self.loginButton.isEnabled = false
            let color = UIColor.gray
            self.loginButton.backgroundColor = color
        }
    }
    
    //MARK:- start login section
    @objc func loginButtonPressed(){
        login(withFingerPrint: false)
    }
    
    func login(withFingerPrint: Bool){
        self.view.endEditing(true)
        var password = self.passwordTextField.text!
        
        let savedPhone = UserDefaults.standard.string(forKey: "phone")
        let savedPassword = UserDefaults.standard.string(forKey: "password")
        let aes = SHA256()
        
        if ((savedPassword != nil) && (savedPhone != nil)) && savedPhone == self.number {
            password = aes.decryptString(string: savedPassword!)
            UserDefaults.standard.set(true, forKey: Key.fingerPrintEnabled)
        } else if withFingerPrint {
            password = User.getPassword()
        }
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.loginUser(number: self.number, password: password, isFingerprint: withFingerPrint ) { (status, user, withMessage) in
        if status == ApiCallStatus.SUCCESS, let userIn = user {
            User.setPassword(password: password)
            self.getProfile(customerId: userIn.cutomerId)
        } else {
            self.viewControllerHelper.hideActivityIndicator()
            ViewControllerHelper.showPrompt(vc: self, message: withMessage)
        }
    }
    }
    
    @objc func fingerPrintLoginAction(){
        BioMetricAuthenticator.authenticateWithPasscode(reason: "Use your fingerprint to unlock", completion: { result in
            switch result {
                case .success( _):
                    self.login(withFingerPrint: true)
                case .failure(let error):
                    // do nothing on canceled
                    if error == .canceledByUser || error == .canceledBySystem {
                        return
                    }
                        // device does not support biometric (face id or touch id) authentication
                    else if error == .biometryNotAvailable {
                        self.showErrorAlert(message: error.message())
                    }
                        // show alternatives on fallback button clicked
                    else if error == .fallback {
                        // here we're entering username and password
                    }
                        // Biometry is locked out now, because there were too many failed attempts.
                        // Need to enter device passcode to unlock.
                    else if error == .biometryLockedout {
                        self.showErrorAlert(message: error.message())
                    }
                        // show error on authentication failed
                    else {
                        self.showErrorAlert(message: error.message())
                    }
                default:
                    return
            }
        })
    }
    
    func showAlert(title: String, message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showErrorAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    //0277016940
    
    //MARK:- get user profile
    func getProfile(customerId:Int)  {
        self.apiService.getAccountOverview(customerNumber: customerId) { (status, user, withMessage) in
            if status == ApiCallStatus.SUCCESS, let userIn = user {
               self.directToController(user: userIn)
            } else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    
    func directToController(user: User) {
        self.apiService.getMissingSteps { (missingSteps, status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                GoalDestination.delete()
                Goal.delete()
                
                Mics.startController(controller: self)
            } else {
               ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }

    @objc func signUp(){
       self.dismiss(animated: true, completion: nil)
    }
    
    @objc func forgotPassword(){
        let user = User.getUser()!
        print("Customer ID: ", user.cutomerId)
        self.apiService.checkAccountType(user_id: "\(user.cutomerId)") { (status, withMessage, item) in
            if status == ApiCallStatus.SUCCESS {
                self.viewControllerHelper.hideActivityIndicator()
                
                let destination = ResetPasswordController()
                destination.number = self.number
                destination.modalPresentationStyle = .fullScreen
                destination.has_full_account = item["has_full_account"].boolValue
                let nav = UINavigationController(rootViewController: destination)
                nav.modalPresentationStyle = .fullScreen
                self.present(nav, animated: true, completion: nil)
            } else if status == ApiCallStatus.WARNING {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }  else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        let isPassworMode = self.passwordTextField.isSecureTextEntry
        if isPassworMode {
           imageView.image = UIImage(named: Images.Onboard.eyeClosed)
           self.passwordTextField.isSecureTextEntry = false
        }  else {
           imageView.image = UIImage(named: Images.Onboard.eye)
           self.passwordTextField.isSecureTextEntry = true
        }
    }
  
}
