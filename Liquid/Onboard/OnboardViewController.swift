//
//  ViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ADCountryPicker
import IHKeyboardAvoiding
import SwiftyJSON
import PopupDialog
import RealmSwift

class OnboardViewController: UIViewController {
    
    //MARK:- Variables section
    let pageCellId = "pageCellId"
    var pageControlBottomConstraint: NSLayoutConstraint?
    var loginButtonBottomConstraint: NSLayoutConstraint?
    var loginViewBottomConstraint: NSLayoutConstraint?
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let user = User.getUser()
    var oldNumber = ""
    let color = UIColor.hex(hex: Key.primaryHexCode)
    var isSwitchingAccount = false
    var popup: PopupDialog?
    var isUpdated = false
   
    let pages = [
        Page(imageName: "Slide1", title: "Set smart GOALS", message: "Let's help you on your way to achieving your dreams.", bottomOffset: -50, topOffset: 0, number: 1),
        Page(imageName: "Slide2", title: "Boost your GOALS.", message: "Our goal assist feature will send you periodic reminders to save towards your goal.", bottomOffset: 0, topOffset: 90, number: 2),
        Page(imageName: "slide3", title: "Watch our demo video", message: "Take a peek at what we can offer, you'll love us after watching this.", bottomOffset: -50, topOffset: 0, number: 3),
    ]
    
    //MARK:- UI elements setup
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = self.pages.count
        pc.currentPageIndicatorTintColor = color
        pc.pageIndicatorTintColor = UIColor.white
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }()
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "SplashLogo")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        let font = UIFont(name: Font.Roboto.Bold, size: 20)
        textView.font = UIFontMetrics.default.scaledFont(for: font!)
        textView.text = "Create an account or Log in."
        return textView
    }()
    
    var mycountryButton: UIButton = {
         let button = ViewControllerHelper.plainButton()
         let font = UIFont(name: Font.Roboto.Regular, size: 18)
         button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: font!)
         let text = Mics.flag(country: "GH")
         button.setTitle("\(text) +233", for: .normal)
         button.layer.cornerRadius = 0
         button.setTitleColor(UIColor.darkText, for: .normal)
         button.addTarget(self, action: #selector(pickCountry), for: .touchUpInside)
     
         let frameWidth = CGFloat(110)
         let image = UIImage(named: "DownArrowSmall")!
         button.imageView?.contentMode = .scaleAspectFit
         button.setImage(image, for: .normal)
         button.setImage(image, for: .highlighted)
     button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
     
         return button
     }()
     
      lazy var mynumberTextField: UITextField = {
         let color = UIColor.darkGray
         let textField = ViewControllerHelper.baseField()
         textField.keyboardType = UIKeyboardType.numberPad
         textField.delegate = self
         textField.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
         textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
         textField.textColor = UIColor.darkText
         textField.attributedPlaceholder =  NSAttributedString(string: "Enter mobile number",
                                                               attributes: [NSAttributedString.Key.foregroundColor: color])
         textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
         return textField
     }()
    
    lazy var loginButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Continue", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(clickContinue), for: .touchUpInside)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    lazy var loginView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .white
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(collectionView)
        self.view.addSubview(pageControl)
        self.view.addSubview(logoImageView)
        self.view.addSubview(loginView)
        self.loginView.addSubview(messageLabel)
        self.loginView.addSubview(mycountryButton)
        self.loginView.addSubview(mynumberTextField)
        self.loginView.addSubview(loginButton)
        
        self.forceUpdate()
        NotificationCenter.default.addObserver(self, selector: #selector(forceUpdate), name: NSNotification.Name("checkUpdate"), object: nil)
        self.view.backgroundColor = UIColor.clear
        self.collectionView.backgroundColor = .white
        
        //MARK:- Setup UI elements and configuration
       self.logoImageView.anchorWithConstantsToTop(top: view.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
       self.logoImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
       self.logoImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
       self.logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
       self.pageControl.anchorWithConstantsToTop(top: nil, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        
        pageControlBottomConstraint = self.pageControl.bottomAnchor.constraint(equalTo: self.collectionView.bottomAnchor, constant: -20)
        pageControlBottomConstraint?.isActive = true
        self.pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.collectionView.anchor(view.topAnchor, left: view.leftAnchor, bottom: loginView.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.collectionView.register(SwipingPageCell.self, forCellWithReuseIdentifier: pageCellId)
        
        self.loginView.anchorToTop(top: nil, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        self.loginViewBottomConstraint = self.loginView.heightAnchor.constraint(equalToConstant: 120)
        self.loginViewBottomConstraint?.isActive = true
        
        self.loginButton.anchorToTop(top: nil, left: loginView.leadingAnchor, bottom: loginView.bottomAnchor, right: loginView.trailingAnchor)
        self.loginButtonBottomConstraint = self.loginButton.heightAnchor.constraint(equalToConstant: 0)
        self.loginButtonBottomConstraint?.isActive = true
        
        self.messageLabel.anchorWithConstantsToTop(top: loginView.topAnchor, left: loginView.leadingAnchor, bottom: mycountryButton.topAnchor, right:loginView.trailingAnchor,topConstant:16 , leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.mycountryButton.anchorWithConstantsToTop(top: nil, left: loginView.leadingAnchor, bottom: loginButton.topAnchor, right:nil,topConstant:0 , leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.mycountryButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.mycountryButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        self.mynumberTextField.anchorWithConstantsToTop(top: nil, left: mycountryButton.trailingAnchor, bottom: loginButton.topAnchor, right: loginView.trailingAnchor,topConstant:0 , leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.mynumberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.updateUtils()
        
        KeyboardAvoiding.avoidingView = self.loginView
        
        if let userIn = self.user {
           let code = "\(self.mycountryButton.currentTitle!.split(separator: " ")[1])"
           self.oldNumber = userIn.phone
           let numberPreviewable = self.oldNumber.replacingOccurrences(of: code, with: "")
           self.mynumberTextField.text = numberPreviewable
        }
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        //self.loginClicked()
        self.updateConstraint()
        let numberReal = "\(self.mycountryButton.currentTitle!.split(separator: " ")[1])\(textField.text!)"
        if Mics.validNumber(number: numberReal) {
           self.loginButton.isEnabled = true
           let color = UIColor.hex(hex: Key.primaryHexCode)
           self.loginButton.backgroundColor = color
        }  else {
           self.loginButton.isEnabled = false
           let color = UIColor.gray
           self.loginButton.backgroundColor = color
        }
    }
    
    @objc func watchVideo(){
        if let url = URL(string: "https://www.youtube.com/watch?v=AXHraxorLZE&feature=youtu.be") {
            UIApplication.shared.open(url)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         //self.forceUpdate()
    }
    
    //MARK:- get all utils
    func updateUtils()  {
        self.apiService.getIdTypes { (status, idTypes, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("IDS")
            }
        }
    }

    //login showup to reveal login
    func loginClicked(){
        self.updateConstraint()
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }

    fileprivate func updateConstraint(){
        //pageControlBottomConstraint?.constant = 50
        loginButtonBottomConstraint?.constant = 50
        loginViewBottomConstraint?.constant = 170
    }
}

//MARK:- swiping collection view delagates implementation
extension OnboardViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: pageCellId, for: indexPath) as! SwipingPageCell
        cell.page = self.pages[indexPath.row]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = view.frame.width
        let itemHeight = view.frame.height
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (_) in
            self.collectionView.invalidateIntrinsicContentSize()
            if (self.pageControl.currentPage == 0){
                self.collectionView.contentOffset = .zero
            } else{
                DispatchQueue.main.async {
                    let indexPath = IndexPath(item: self.pageControl.currentPage, section: 0)
                    self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                }
            }
        }, completion: {(bear_) in
            
        })
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = Int(targetContentOffset.pointee.x/view.frame.width)
        self.pageControl.currentPage = pageNumber
        
        //MARK:- on the last swiping page
        if pageNumber == self.pages.count {
           self.updateConstraint()
        }  else {
            //MARK:- return to regular pages
           //pageControlBottomConstraint?.constant = -20
           loginButtonBottomConstraint?.constant = 0
           loginViewBottomConstraint?.constant = 130
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
}

extension OnboardViewController: ADCountryPickerDelegate,LoginDelegate, XEconfirmDelegate, UpdateDelegate {
    
    func proceedToUpdate() {
        
    }
    
    func dismissUpdate() {
        
    }
    
    //check app version
    @objc func forceUpdate(){
        self.viewControllerHelper.showActivityIndicator()
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        self.apiService.forceupdate(versionCode: appVersion!) { (response, status, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if (response["return_value"].boolValue == true){
                    print("appversion is up to date")
                }else{
                    print("appversion is out of date")
                    
                    let messages = "Your LIQUID App is outdated. Dowload the lastest version to continue"

                    let alert = UIAlertController(title: "TIME TO UPDATE", message: messages, preferredStyle: .alert)
                    let updateNow = UIAlertAction(title: "Update Now", style: .default, handler: { (finished) in
                        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1368025893"),
                            UIApplication.shared.canOpenURL(url)
                        {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url)
                            }
                        }
                    })
                    
                    let exitApp = UIAlertAction(title: "Exit", style: .destructive, handler: { (finished) in
                        UIControl().sendAction(#selector(NSXPCConnection.suspend),
                                               to: UIApplication.shared, for: nil)
                    })
                    
                    alert.addAction(exitApp)
                    alert.addAction(updateNow)
                    
                    let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
                    subview.backgroundColor = UIColor.white
                    alert.view.tintColor = UIColor.black
                    let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
                    
                    imageView.image = UIImage(named: "WalletIcon")
                    
                    alert.view.addSubview(imageView)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        let text = Mics.flag(country: code) + " " + dialCode
        self.mycountryButton.setTitle(text, for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- start with the registration process
    @objc func clickContinue() {
        if var number = self.mynumberTextField.text {
            if number.count == 10 {
              number.remove(at: number.startIndex)
            }
            self.view.endEditing(true)
            let numberReal = "\(self.mycountryButton.currentTitle!.split(separator: " ")[1])\(number)"
            //MARK: user account exist so check params
            if self.user != nil && self.oldNumber  == numberReal {
                self.existingAccountDirection()
                return
            } else {
                User.delete()
                Goal.delete()
                TransactionHistory.delete()
                
                let realm = try! Realm()
                try! realm.write {
                    realm.deleteAll()
                }
                
                // Clear user defaults
                let savedPhone = UserDefaults.standard.string(forKey: "phone")
                let savedPassword = UserDefaults.standard.string(forKey: "password")
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                if savedPhone != nil && savedPassword != nil {
                    UserDefaults.standard.set(savedPhone, forKey: "phone")
                    UserDefaults.standard.set( savedPassword, forKey: "password")
                    UserDefaults.standard.set(true, forKey: Key.fingerPrintEnabled)
                }
            }
            
            // Check API if number is new
            self.apiService.checkNumber(number: numberReal, justCheck: false) { (status, withMessage, response) in
                if status == ApiCallStatus.SUCCESS {
                    self.viewControllerHelper.hideActivityIndicator()
                    self.proceed(numberReal: numberReal)
                } else if status == ApiCallStatus.WARNING {
                    if withMessage == UserStatus.NO_ACCOUNT.rawValue || withMessage == UserStatus.NOT_VERIFIED.rawValue {
                        // No number
                        //MARK: non existing account for prompt
                        let confirm_vc = ConfirmNumberViewController(nibName: "ConfirmPhoneView", bundle: nil)
                        confirm_vc.delegate = self
                        confirm_vc.phone = numberReal
                        self.popup = PopupDialog(viewController: confirm_vc)
                        self.present(self.popup!, animated: true, completion: nil)
                        
                    } else if withMessage == "no_password" {
                        print(numberReal)
                        let destination = SignUpStepOneController()
                        destination.number = numberReal
                        let nav = UINavigationController(rootViewController: destination)
                        nav.modalPresentationStyle = .fullScreen
                        self.present(nav, animated: true, completion: nil)
                    } else {
                        self.viewControllerHelper.hideActivityIndicator()
                        ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                    }
                }  else {
                    self.viewControllerHelper.hideActivityIndicator()
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
            
            
        }
    }
    
    func proceed(numberReal: String) {
        self.popup?.dismiss()
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.registerDevice { (status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                self.checkNumber(numberReal: numberReal)
            }   else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
    
    func checkNumber(numberReal: String) {
        self.apiService.checkNumber(number: numberReal, justCheck: false) { (status, withMessage, response) in
            if status == ApiCallStatus.SUCCESS {
                self.viewControllerHelper.hideActivityIndicator()
                let destination = LoginController()
                destination.number = numberReal
                destination.modalPresentationStyle = .fullScreen
                let item = try! JSON(data: response!.data!)
                destination.customer_id = item["customer_id"].stringValue
                
                let navVC = UINavigationController(rootViewController: destination)
                navVC.modalPresentationStyle = .fullScreen
                self.present(navVC, animated: true, completion: nil)
            } else if status == ApiCallStatus.WARNING {
                if withMessage == UserStatus.NO_ACCOUNT.rawValue || withMessage == UserStatus.NOT_VERIFIED.rawValue {
                  self.createDeviceAccount(number: numberReal)
                } else {
                    self.viewControllerHelper.hideActivityIndicator()
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }  else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func createDeviceAccount(number:String)  {
        self.apiService.registerPhone(number: number) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let destination = VerificationCodeController()
                destination.number = number
                let navVC = UINavigationController(rootViewController: destination)
                navVC.modalPresentationStyle = .fullScreen
                self.present(navVC, animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    @objc func pickCountry(){
        self.clickCountryPicker()
    }
    
    func clickCountryPicker() {
       ViewControllerHelper.presentCountryPicker(vc: self, delegate: self)
    }
    
    func existingAccountDirection()  {
        if user!.accountStatus == UserStatus.NOT_VERIFIED.rawValue {
            let destination = VerificationCodeController()
            destination.isSentCodeAlready = true
            destination.number = user!.phone
            let nav = UINavigationController(rootViewController: destination)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
            //NOT PROVIDED THE FIRST NAME
        } else if user!.firstName.isEmpty {
            //request for password first
            let destination = SignUpStepOneController()
            destination.number = user!.phone
            let nav = UINavigationController(rootViewController: destination)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        } else {
            //NO ABOUT INFORMATION
            
            let auth = LoginController()
            auth.customer_id = "\(user!.cutomerId)"
            auth.number = user!.phone
            let nav = UINavigationController(rootViewController: auth)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
            //Mics.startController(controller: self)
        }
    }
}

extension OnboardViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
}

