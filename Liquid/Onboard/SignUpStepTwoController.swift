//
//  SignUpAddressController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ADCountryPicker
import GooglePlacePicker
import McPicker

class SignUpStepTwoController: BaseScrollViewController {
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var country = "Ghana"
    var isUpdate = false
    
    var cities = ["Accra", "Bolgatanga", "Cape-coast", "Ho", "Koforidua", "Kumasi", "Obuasi", "Sekondi-Takoradi"]
    
    lazy var countryButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Ghana", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 8,bottom: 0,right: 0)
        button.addBottomBorder(UIColor.darkGray, height: 0.5)
        //button.addTarget(self, action: #selector(pickCountry), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    lazy var citytField: UITextField = {
        let textField = baseUITextField(placeholder: "City")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.delegate = self
        return textField
    }()
    
    lazy var addressField: UITextField = {
        let textField = baseUITextField(placeholder: "Residential Address")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    lazy var mapPickerButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("or Locate on map", for: .normal)
        button.contentHorizontalAlignment = .right
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(startMapPicker), for: .touchUpInside)
        return button
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var signUpButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Continue", for: .normal)
        button.addTarget(self, action: #selector(signUpDone), for: .touchUpInside)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Residential Address")
        
        self.scrollView.addSubview(countryButton)
        self.scrollView.addSubview(citytField)
        self.scrollView.addSubview(addressField)
        self.scrollView.addSubview(mapPickerButton)
        self.view.addSubview(signUpButton)
        
        
        let width = self.view.frame.width - 32
        self.countryButton.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.countryButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.countryButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.citytField.anchorWithConstantsToTop(top: countryButton.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.citytField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.citytField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.addressField.anchorWithConstantsToTop(top: citytField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.addressField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.addressField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.mapPickerButton.anchorWithConstantsToTop(top: addressField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 1, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.mapPickerButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.mapPickerButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.mapPickerButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -50).isActive = true
        
    
        self.signUpButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.signUpButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
    }
    
    @objc func pickCountry() {
       ViewControllerHelper.presentCountryPicker(vc: self, delegate: self)
    }
    
    //MARK:- ready to sign up
    @objc func signUpDone() {
        let lat = Double("0.0")!
        let country = self.country
        let city = self.citytField.text!
        let name = self.addressField.text!
        
        if  country.isEmpty || city.isEmpty || name.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Provide fields and continue.")
            return
        }
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.saveResidence(latitude: lat, longitude: lat, streetAddress: name, city: city, country: country) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.goToNext()
            }   else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func goToNext()  {
        User.updateUserAddress(status: true)
        if self.isUpdate {
            UserDefaults.standard.set(true, forKey: Key.profileUpdated)
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        //Mics.startController(controller: self)
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //let destination = storyboard.instantiateViewController(withIdentifier: "secure_account_vc") as! SecureAccountViewController
        //self.navigationController?.pushViewController(DocumentController(), animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "secure_account_vc") as! SecureAccountViewController
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    //MARK:- pick on map
    @objc func startMapPicker() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension SignUpStepTwoController: ADCountryPickerDelegate, GMSPlacePickerViewControllerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.country = name
        self.countryButton.setTitle(name, for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        let name = place.name
        if let address = place.formattedAddress {
           self.addressField.text = address
        } else {
           self.addressField.text = name
        }
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        self.addressField.text = ""
    }
    
}

extension SignUpStepTwoController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let data: [[String]] = [self.cities]
        McPicker.show(data: data) {  [weak self] (selections: [Int : String]) -> Void in
            if let name = selections[0] {
                self?.citytField.text = name
            }
        }
        return false
    }
}
