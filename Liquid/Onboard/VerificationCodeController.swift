//
//  VerificationCodeController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import RealmSwift

class VerificationCodeController: ControllerWithBack, XEconfirmDelegate {
    var number = ""
    var email = ""
    var password = ""
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var isChangingNumber = false
    var isFromEditCV = false
    var isChangingEmail = false
    var isSentCodeAlready = false
    var isVerifyEmail = false
    var seconds = 60
    var timer = Timer()
    var popup: PopupDialog?
    let realm = try! Realm()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    let codeStackView: UIStackView = {
        let codeStack = UIStackView()
        codeStack.translatesAutoresizingMaskIntoConstraints = false
        codeStack.distribution = .fillEqually
        codeStack.axis = .horizontal
        codeStack.spacing = 10
        return codeStack
    }()
    
    
    lazy var firstCodeTextField: UITextField = {
        return baseInnerField()
    }()
    
    lazy var secondCodeTextField: UITextField = {
        return baseInnerField()
    }()
    
    lazy var thirdCodeTextField: UITextField = {
        return baseInnerField()
    }()
    
    lazy var fourthCodeTextField: UITextField = {
        return baseInnerField()
    }()
    
    func baseInnerField() -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.mainBaseField(placeHolder: "")
        textField.delegate = self
        textField.textColor = color
        textField.keyboardType = UIKeyboardType.numberPad
        textField.setBottomBorder(color: color.cgColor)
        textField.textAlignment = .center
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        return textField
    }
    
    lazy var resentButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Resend code in", for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 16)!)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.isEnabled = false
        button.addTarget(self, action: #selector(resendCode), for: .touchUpInside)
        return button
    }()
    
    lazy var changeNumberButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Change Phone Number", for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 16)!)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(changeNumber), for: .touchUpInside)
        return button
    }()
    
    let timeLabel: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        label.textColor = UIColor.darkText
        label.isHidden = true
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBarDark(title: "")
        
        print("mobile Number: \(self.number)  \(email)")
        
        self.setUpUI()
    }
    
    func setUpUI()  {
        self.view.addSubview(messageTextView)
        self.view.addSubview(codeStackView)
        self.view.addSubview(resentButton)
        self.view.addSubview(timeLabel)
        self.view.addSubview(changeNumberButton)
        
        self.codeStackView.addArrangedSubview(firstCodeTextField)
        self.codeStackView.addArrangedSubview(secondCodeTextField)
        self.codeStackView.addArrangedSubview(thirdCodeTextField)
        self.codeStackView.addArrangedSubview(fourthCodeTextField)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
        self.codeStackView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -100)
        self.codeStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.resentButton.anchorWithConstantsToTop(top: codeStackView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.resentButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.resentButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        self.timeLabel.anchorWithConstantsToTop(top: codeStackView.bottomAnchor, left: resentButton.trailingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.timeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        
        self.changeNumberButton.anchorWithConstantsToTop(top: resentButton.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.changeNumberButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let font = UIFont(name: Font.Roboto.Bold, size: 18)
        _ = UIFontMetrics.default.scaledFont(for: font!)
        
        let fontNormal = UIFont(name: Font.Roboto.Light, size: 18)
        let normalFont = UIFontMetrics.default.scaledFont(for: fontNormal!)
        
    
        var display = self.number
        if  display.isEmpty {
            display = self.email
        }
        
        let title = "Liquid is committed to security. Let's ensure only you can log into your account by verifying your number\n\n".formatAsAttributed(font: fontNormal!, color: UIColor.darkText)
        let messageHeader = "Enter the 4-Digit code sent to you at: ".formatAsAttributed(font: normalFont, color: UIColor.darkText)
        let messageBody = "\(display)".formatAsAttributed(font: fontNormal!, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        
        if !isVerifyEmail {
            result.append(title)
        }
        
        result.append(messageHeader)
        result.append(messageBody)
        self.messageTextView.attributedText = result
        
        if (self.isChangingNumber && !self.isChangingEmail){
            self.changeNumberButton.isHidden = true
            self.enableButton()
        }
        
        if (!self.isChangingNumber && self.isChangingEmail){
            self.changeNumberButton.isHidden = true
            self.resendEmailCode()
        }
        
        if (!self.isChangingNumber && !self.isChangingEmail){
            if !isSentCodeAlready {
               self.runTimer()
            }  else {
               self.enableButton()
            }
        }
        
        //MARK: changing email
        if isVerifyEmail {
           self.changeNumberButton.isHidden = true
           self.resendEmailCode()
           self.runTimer()
        }
        
        //MARK: changing number
        if isChangingNumber {
            self.changeNumberButton.isHidden = true
            self.enableButton()
            self.runTimer()
        }
        
    }
    
    func runTimer() {
        self.disableButton()
        self.seconds = 60
        self.timeLabel.isHidden = false
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.seconds -= 1
        self.timeLabel.text = "00:\(seconds)"
        if self.seconds <= 0 {
           self.enableButton()
        }
    }
    
    func enableButton()  {
        self.resentButton.isEnabled = true
        self.resentButton.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        resentButton.setTitle("Resend code", for: .normal)
        self.timer.invalidate()
        self.timeLabel.isHidden = true
    }
    
    func disableButton(){
        self.resentButton.setTitleColor(UIColor.lightGray, for: .normal)
        resentButton.setTitle("Resend code in", for: .normal)
        self.resentButton.isEnabled = false
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if text?.utf16.count == 1{
            switch textField{
            case firstCodeTextField:
                secondCodeTextField.becomeFirstResponder()
            case secondCodeTextField:
                thirdCodeTextField.becomeFirstResponder()
            case thirdCodeTextField:
                fourthCodeTextField.becomeFirstResponder()
            case fourthCodeTextField:
                self.startVerification()
            default:
                break
            }
        } else {
            
        }
    }
    
    @objc private func resendCode() {
        print(self.isFromEditCV)
        
        if isFromEditCV == true {
            if self.isChangingNumber && !self.isChangingEmail && !self.isVerifyEmail {
               self.runTimer()
               self.resendVerificationCode()
            }  else {
                self.runTimer()
               self.resendEmailCode()
            }
        }else{
            self.runTimer()
            self.resendVerificationCode()
        }
    }
    
    func resendVerificationCode()  {
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.registerPhone(number: number) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: "A verification code has been sent to your number. A verification code has been sent to \(self.number)\n\nPlease check your device and enter code")
                self.showVerificationPrompt()
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func showVerificationPrompt(){
        print("Showing prompt")
        let confirm_vc = OTPSentViewController(nibName: "OTPSentView", bundle: nil)
        confirm_vc.delegate = self
        confirm_vc.phone = self.number
        self.popup = PopupDialog(viewController: confirm_vc)
        self.present(popup!, animated: true, completion: nil)
    }
    
    func proceed(numberReal: String) {
        
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
    func resendEmailCode() {
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.triggerEmailChange(email: self.email) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: "Verification code sent to your email.")
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    @objc private func changeNumber() {
       self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- starting the profile page
    func startProfileUpdate() {
        let destination = SignUpStepOneController()
        destination.number = self.number
        self.navigationController?.setViewControllers([OnboardViewController(), destination], animated: true)
    }
    
}

extension VerificationCodeController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 1
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //self.startVerification()
    }
    
    func startVerification()  {
        let code = "\(firstCodeTextField.text!)\(secondCodeTextField.text!)\(thirdCodeTextField.text!)\(fourthCodeTextField.text!)"
        //MARK: during the onboarding process
        if (code.count == 4){
            self.view.endEditing(true)
            self.viewControllerHelper.showActivityIndicator()
            
            if (self.isVerifyEmail) {
                self.verifyChangingEmail(code:code)
                return
            }
            
            if (!self.isChangingNumber && !self.isChangingEmail){
                self.verifyRegistrationNumber(code:code)
                return
            }
            
            if (self.isChangingNumber && !self.isChangingEmail){
                self.verifyChangingNumber(code:code)
                return
            }
            
            if (!self.isChangingNumber && self.isChangingEmail){
                self.verifyChangingEmail(code:code)
                return
            }
            
            
        }
        
    }
    
    func verifyRegistrationNumber(code:String) {
        print("No: \(self.number) Code: \(code)")
        self.apiService.verifyPhone(number: self.number, code: code) { (status, user, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let user = User.getUser()!
                try! self.realm.write() {
                    user.accountStatus = UserStatus.ACTIVE.rawValue
                }
                
                self.startProfileUpdate()
            } else {
                let alertAction = UIAlertAction(title: "TRY AGAIN", style: .default, handler: nil)
                let alertView = UIAlertController(title: "Liquid", message: withMessage, preferredStyle: .alert)
                alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
                alertView.addAction(alertAction)
                self.present(alertView, animated: true, completion: nil)
            }
        }
    }
    
    func verifyChangingNumber(code:String)  {
        self.apiService.changeNumberStepTwo(newNumber: self.number, confirmationCode: code) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                //MARK: update user number
                User.updateUserNumber(number: self.number)
                self.dismiss(animated: true, completion: nil)
                
                let alertController = UIAlertController(title: "Success", message: "Your liquid mobile number has been changed successfully to \(self.number)", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (finished) in
                    //send notification accross app for profile number update
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.updatedMobile), object: ApiCallStatus.SUCCESS)
                    self.navigationController?.backToViewController(vc: EditProfileController.self)
                    
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func verifyAlert(){
        
        self.apiService.changeNumberStepOne(newNumber: self.number, password: self.password) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            
            let response = String(withMessage)
            
            let responseJson = response.convertToDictionary()
            let errorCheck = responseJson?["error"]
            
            let errorToString = errorCheck as? String ?? ""
            //                    let splitString = errorToString.split(separator: ".")
            //                    guard let err = splitString[1] else {return}
            //                    print(err)
            
            if (errorToString == "err.PasswordInvalid"){
                ViewControllerHelper.showPrompt(vc: self, message: "Your password is invalid, kindly check and enter again")
                
            }else{
                if status == ApiCallStatus.SUCCESS {
                    self.number = "\(self.number)"
                    self.isChangingNumber = true
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: "Something went wrong changing your phone number. Try again  later.", completion: { (isFinished) in
                        self.verifyAlert()
                    })
                }
            }
        }
    }
    
    func verifyChangingEmail(code:String)  {
        self.apiService.verifyEmailChange(pin: code, completion: { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                //MARK: update user number
                print("updated email: ",self.email)
                print("updated email: ",EmailStatus.verified.rawValue)
                User.updateUserEmail(email: self.email)
                User.updateUserEmailStatus(status: EmailStatus.verified.rawValue)
                
                //User.updateUserEmail(email: EmailStatus.verified.rawValue)
                UserDefaults.standard.set(true, forKey: Key.profileUpdated)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.updatedMobile), object: ApiCallStatus.SUCCESS)
                self.navigationController?.popViewController(animated: true)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: "Something went wrong changing your email.")
            }
        })
    }
    
}


