//
//  OnbaordSuccessController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 05/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class OnboardSuccessController: UIViewController {
    
    var code = ""
    var isSkip = true
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    
    let topView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let topMiddleView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.clear
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let bottomView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let profileImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 50
        imageView.image = UIImage(named: "ProfileWave")
        return imageView
    }()
    
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkGray
        textView.text = ""
        textView.font = UIFont.systemFont(ofSize: 20)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    let titleTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.text = "Do you have an invite code ?"
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        return textView
    }()
    
    lazy var inviteCodeTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.setBottomBorder(color: color.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter Code Here",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    let actionButtonStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.addBackground(color: UIColor.white)
        return container
    }()
    
    lazy var skipButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("Skip", for: .normal)
        button.addTarget(self, action: #selector(skipAction), for: .touchUpInside)
        return button
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.layer.cornerRadius = 5
        button.setTitle("Continue", for: .normal)
        button.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.white
        super.viewDidLoad()
        
        let height = view.frame.height/2
        self.view.addSubview(topView)
        self.view.addSubview(bottomView)
        self.topView.addSubview(profileImageView)
        self.topView.addSubview(messageTextView)
        self.topView.addSubview(topMiddleView)
        self.bottomView.addSubview(titleTextView)
        self.bottomView.addSubview(inviteCodeTextField)
        self.bottomView.addSubview(actionButtonStackView)
        
        self.topView.anchorToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor)
        self.topView.heightAnchor.constraint(equalToConstant: height).isActive = true
        
       
        self.topMiddleView.anchorWithConstantsToTop(top: nil, left: topView.leadingAnchor, bottom: nil, right: topView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.topMiddleView.centerYAnchor.constraint(equalTo: topView.centerYAnchor, constant: 0).isActive = true
        self.topMiddleView.centerXAnchor.constraint(equalTo: topView.centerXAnchor, constant: 0).isActive = true
        self.topMiddleView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        self.profileImageView.anchorWithConstantsToTop(top: nil, left: nil, bottom: topMiddleView.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: 0)
        self.profileImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.centerXAnchor.constraint(equalTo: topView.centerXAnchor, constant: 0).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: topMiddleView.bottomAnchor, left: topView.leadingAnchor, bottom: topView.bottomAnchor, right: topView.trailingAnchor, topConstant: 2, leftConstant: 16, bottomConstant: 16, rightConstant: -16)
        
        
        self.bottomView.anchorToTop(top: topView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
        self.titleTextView.anchorWithConstantsToTop(top: bottomView.topAnchor, left: bottomView.leadingAnchor, bottom: nil, right: bottomView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.titleTextView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.inviteCodeTextField.anchorWithConstantsToTop(top: titleTextView.bottomAnchor, left: bottomView.leadingAnchor, bottom: nil, right: bottomView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.inviteCodeTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.actionButtonStackView.anchorWithConstantsToTop(top: nil, left: bottomView.leadingAnchor, bottom: bottomView.bottomAnchor, right: bottomView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.actionButtonStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.actionButtonStackView.addArrangedSubview(skipButton)
        self.actionButtonStackView.addArrangedSubview(nextButton)
        
        
        let fontSize = 20
        let user = User.getUser()!
        
        let messageHeader = "Congratulations \(user.firstName)!\n".formatAsAttributed(font: UIFont.boldSystemFont(ofSize: CGFloat(fontSize)), color: UIColor.white)
        let messageBody = "Your ".formatAsAttributed(fontSize: fontSize, color: UIColor.white)
        let messageComapanyBrand = "LIQUID".formatAsAttributed(font: UIFont.boldSystemFont(ofSize: CGFloat(fontSize)), color: UIColor.white)
        let messageMore = " account has just been created.".formatAsAttributed(fontSize: fontSize, color: UIColor.white)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        result.append(messageComapanyBrand)
        result.append(messageMore)
        self.messageTextView.attributedText = result
        self.messageTextView.textAlignment = .center
        
    }
    
    @objc func nextAction(_ sender: UIButton) {
        self.sendInviteCode()
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.code = self.inviteCodeTextField.text!
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        if !self.inviteCodeTextField.text!.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func skipAction() {
        let user = User.getUser()!
        if user.hasAcceptedTermsOfAgreement {
            self.goNext()
        }  else {
            let destination = TermsController()
            destination.isJustTerms = true
            destination.modalPresentationStyle = .fullScreen
            self.present(destination, animated: true, completion: nil)
        }
    }
    
    func goNext()  {
        Mics.setMainTheme()
        let destination = HomeTabController()
        destination.modalPresentationStyle = .fullScreen
        self.present(destination, animated: true, completion: nil)
    }
    
    func sendInviteCode() {
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.sendInviteCode(inviteCode: self.code) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.skipAction()
            }   else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
}
