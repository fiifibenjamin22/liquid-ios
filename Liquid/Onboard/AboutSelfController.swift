//
//  AboutSelfController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import DLRadioButton
import ADCountryPicker


class AboutSelfController: BaseScrollViewController {
    
    let apiService = ApiService()
    var occupation: Occupation?
    var country: String?
    
    let viewControllerHelper = ViewControllerHelper()
    var occupationPickerTopConstraint: NSLayoutConstraint?
    
    let actionButtonStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        return container
    }()
    
    lazy var genderTitleTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.attributedText = "What is your gender?".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var genderTextView: UITextView = {
        let textView = self.baseLocalTextView(label: "Select gender")
        return textView
    }()
    
    let genderStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.distribution = .fillProportionally
        container.axis = .vertical
        return container
    }()
    
    let maleButton: DLRadioButton = {
        let button = ViewControllerHelper.baseRadioButton(title: "Male", color: UIColor.darkGray)
        button.isSelected = true
        button.isIconOnRight = true
        button.addBottomBorder(UIColor.lightGray, height: 1.0)
        
        let font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.titleLabel!.font = font
        button.iconSize = 20
        
        return button
    }()
    
    let feMaleButton: DLRadioButton = {
        let button = ViewControllerHelper.baseRadioButton(title: "Female", color: UIColor.darkGray)
        button.isIconOnRight = true
        button.addBottomBorder(UIColor.lightGray, height: 1.0)
        let font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.titleLabel!.font = font
        button.iconSize = 20
        
        return button
    }()
    
    func baseLocalTextView(label: String) -> UITextView {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.backgroundColor = UIColor.clear
        textView.text = label
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Tell Us About Yourself")
        
        scrollView.addSubview(genderTitleTextView)
        scrollView.addSubview(genderTextView)
        scrollView.addSubview(genderStackView)
        
        view.addSubview(actionButtonStackView)
        
        let width = view.frame.width - 32
        
        self.genderTitleTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.genderTitleTextView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        self.genderTitleTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.genderTextView.anchorWithConstantsToTop(top: genderTitleTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.genderTextView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        self.genderTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.genderStackView.anchorWithConstantsToTop(top: genderTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 24, bottomConstant: 0, rightConstant: 24)
        self.genderStackView.heightAnchor.constraint(equalToConstant: 88).isActive = true
        self.genderStackView.widthAnchor.constraint(equalToConstant: width - 16).isActive = true
        
        self.genderStackView.addArrangedSubview(maleButton)
        self.genderStackView.addArrangedSubview(feMaleButton)
        self.maleButton.otherButtons = [feMaleButton]
        
        //MARK:- action section
        self.actionButtonStackView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.actionButtonStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
       
        //self.actionButtonStackView.addArrangedSubview(skipButton)
        self.actionButtonStackView.addArrangedSubview(nextButton)
        
        self.updateUtils()
    }
    
    //MARK:- get all utils
    func updateUtils()  {
        print("UPDATING...")
        self.apiService.getIdTypes { (status, idTypes, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("IDS")
            }
        }
        self.apiService.occupations { (status, occupations, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("OCCUPATIONS")
            }
        }
    }
    
    @objc func nextAction() {
        var gender = "female"
      
        if self.maleButton.isSelected {
            gender = "male"
        }
        
        if !self.maleButton.isSelected && !self.feMaleButton.isSelected {
            ViewControllerHelper.showPrompt(vc: self, message: "Choose your gender")
            return
        }
        
        let nextVc = AboutSelfPart2ViewController()
        nextVc.gender = gender
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
}


