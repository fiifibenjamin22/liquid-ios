//
//  DocumentController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import CropViewController


class DocumentController: BaseScrollViewController {
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var imagePicker : UIImagePickerController!
    var idType: LiquidIdType?
    var idNumber = ""
    
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 50
        imageView.backgroundColor = UIColor.hex(hex: "#FFE8E8E8")
        imageView.image = UIImage(named: "Passport")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var titleTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.darkText
        textView.text = "SELECT ID TYPE"
        textView.font = UIFont.boldSystemFont(ofSize: 14)
        return textView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.text = "Follow these instructions to get the best results:\n\n• Fit the document inside the frame\n\n• Make sure you have enough light\n\n• Take a picture of the document"
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        textView.backgroundColor = UIColor.hex(hex: "e6f8ff")
        textView.layer.cornerRadius = 2.0
        return textView
    }()
    
    lazy var idPickerButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
        button.setTitle("Choose ID Type", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 8,bottom: 0,right: 0)
        button.addBottomBorder(UIColor.darkGray, height: 0.5)
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrow")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        button.addTarget(self, action: #selector(pickIdTypes(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var idNumberTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "ID Number",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    let actionButtonStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.addBackground(color: UIColor.white)
        return container
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.layer.cornerRadius = 5
        button.setTitle("Upload", for: .normal)
        button.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Upload ID Document")
        
        scrollView.addSubview(messageTextView)
        scrollView.addSubview(profileImageView)
        scrollView.addSubview(titleTextView)
        scrollView.addSubview(idPickerButton)
        scrollView.addSubview(idNumberTextField)
        view.addSubview(actionButtonStackView)
        
        let width = view.frame.width - 32
        self.profileImageView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.profileImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.titleTextView.anchorWithConstantsToTop(top: profileImageView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.titleTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.idPickerButton.anchorWithConstantsToTop(top: titleTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.idPickerButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.idPickerButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        self.idNumberTextField.anchorWithConstantsToTop(top: idPickerButton.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.idNumberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.idNumberTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: idNumberTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: -50, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        

        //MARK:- action section
        self.actionButtonStackView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.actionButtonStackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        //self.actionButtonStackView.addArrangedSubview(skipButton)
        self.actionButtonStackView.addArrangedSubview(nextButton)
        
    }
    
    @objc func nextAction(_ sender: UIButton) {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: "Take photo", style: .default) { (action) in
            self.takePhoto()
        }
        
        let profileAction = UIAlertAction(title: "Pick from libary", style: .default) { (action) in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(profileAction)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func pickIdTypes(_ sender: UIButton)  {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        let ids = LiquidIdType.all()
        for id in ids {
            let defaultAction = UIAlertAction(title: "\(id.name)", style: .default) { (action) in
                self.idType = id
                self.idPickerButton.setTitle(id.name, for: .normal)
                self.titleTextView.text = id.name
                if id.name.contains("Driver") {
                    self.idNumberTextField.attributedPlaceholder = NSAttributedString(string: "Certificate of competence",
                                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                } else {
                    self.idNumberTextField.attributedPlaceholder = NSAttributedString(string: "ID Number",
                                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                }
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func goNext()  {
        Mics.setMainTheme()
        User.updateUserDocument(status: true)
        Mics.startNextControllerForUpgrade(controller: self)
    }
    
    func takePhoto()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }  else {
            ViewControllerHelper.showPrompt(vc: self, message: "Your device does not support camera options.")
        }
    }
}

extension DocumentController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[.originalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: .default, image: image)
        cropController.delegate = self
        picker.dismiss(animated: true, completion: {
            self.present(cropController, animated: true, completion: nil)
        })

    }
    
    //MARK:- the cropper delegate
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        cropViewController.dismiss(animated: true, completion: nil)
        
        self.uploadDoc(image: image)
        
    }
    
    func uploadDoc(image: UIImage) {
        
        let idNumber = self.idNumberTextField.text!
        if idNumber.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Enter ID number.")
            return
        }
        
        guard let idType = self.idType?.name else {
            ViewControllerHelper.showPrompt(vc: self, message: "Select ID type.")
            return
        }
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.saveDocumant(image: image, idType: idType, idNumber: idNumber) { (status, url, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadCoach"), object: self)
                self.goNext()
            }  else {
                ViewControllerHelper.showPrompt(vc: self, message: "An error occured with the upload. Please try again later")
            }
        }
    }
    
}

