//
//  LegalInfo.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import DLRadioButton

class LegalInfoController: BaseScrollViewController {
    
    let tcImage: UIImageView = {
        let tc = ViewControllerHelper.baseImageView()
        tc.image = UIImage(named: "tc")
        tc.contentMode = .scaleAspectFit
        return tc
    }()
    
    let contentTitle: UILabel = {
       let tx = ViewControllerHelper.baseLabel()
        tx.text = "I understand :"
        tx.font = UIFont.boldSystemFont(ofSize: 17)
        tx.textAlignment = .left
        return tx
    }()
    
    let briefContent: UITextView = {
        let tx = ViewControllerHelper.baseTextView()
        tx.text = "That Liquid trades on the secondary market so the Tbills rates might differ slightly from the BOG displayed rates\n\n" + "That withdrawals before the maturity date are sold at a discounted rate\n\n" + "Matured Tbills will be paid to the payout account on the maturity date unless you decide to rollover"
        tx.textAlignment = .left
        tx.textColor = .darkGray
        tx.font = UIFont.systemFont(ofSize: 16)
        tx.adjustsFontForContentSizeCategory = true
        return tx
    }()
    
    let fullTanC: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Read full terms and conditions"
        lbl.textAlignment = .center
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        return lbl
    }()
    
    let seperatorView: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    lazy var tickBox: DLRadioButton = {
        let button = ViewControllerHelper.baseRadioButton(title: "", color: UIColor.darkGray)
        button.isIconSquare = true
        button.isMultipleSelectionEnabled = true
        button.iconSize = 20
        button.addTarget(self, action: #selector(radioToggled), for: .touchUpInside)
        return button
    }()
    
    let tickInstruction: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "I have read and agree to all the terms above."
        lbl.underline()
        return lbl
    }()
    
    lazy var nextbtn: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "next-btn")
        img.isUserInteractionEnabled = true
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotNext)))
        return img
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setupView()
    }
    
    func setupView(){
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(tcImage)
        self.scrollView.addSubview(contentTitle)
        self.scrollView.addSubview(briefContent)
        self.scrollView.addSubview(fullTanC)
        self.scrollView.addSubview(seperatorView)
        self.scrollView.addSubview(tickBox)
        self.scrollView.addSubview(tickInstruction)
        self.scrollView.addSubview(nextbtn)
        
        tcImage.anchor(self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 70.0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        contentTitle.anchor(tcImage.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 12, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 30)
     
        nextbtn.anchor(nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 8, rightConstant: 16, widthConstant: 0, heightConstant: 50)
        
        tickBox.anchor(nil, left: self.nextbtn.leftAnchor, bottom: self.nextbtn.topAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 40, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        tickInstruction.anchor(tickBox.topAnchor, left: tickBox.rightAnchor, bottom: tickBox.bottomAnchor, right: nextbtn.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        seperatorView.anchor(nil, left: self.view.leftAnchor, bottom: self.tickBox.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: 0, heightConstant: 1)
        
        fullTanC.anchor(nil, left: self.view.leftAnchor, bottom: seperatorView.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        briefContent.anchor(contentTitle.bottomAnchor, left: contentTitle.leftAnchor, bottom: fullTanC.topAnchor, right: contentTitle.rightAnchor, topConstant: 12, leftConstant: 0, bottomConstant: 12, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    @objc func radioToggled(){
        
    }
    
    @objc func gotNext(){
        let layout = UICollectionViewFlowLayout()
        let vc = TBillCategoriesController(collectionViewLayout: layout)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
