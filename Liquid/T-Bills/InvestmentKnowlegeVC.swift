//
//  InvestmentKnowlegeVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 7/22/19.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import ContactsUI
import PhoneNumberKit

class InvestmentKnowlegeVC: BaseScrollViewController {
    
    var riskLevel = ""
    var knowledgeLevel = ""
    
    let headingTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "Let’s personalise your investment"
        textView.textAlignment = .center
        textView.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy)
        return textView
    }()
    
    let knowledgeTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "How deep is your investment knowledge?"
        textView.textAlignment = .center
        return textView
    }()
    
    let knowledgeUIStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.spacing = 15
        return container
    }()
    
    lazy var lowKnowledeButton: UIButton = {
        let button = baseButton(title: "LOW")
        button.addTarget(self, action: #selector(knowledgeButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var mediumKnowledeButton: UIButton = {
        let button = baseButton(title: "MEDIUM")
        button.addTarget(self, action: #selector(knowledgeButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var highKnowledeButton: UIButton = {
        let button = baseButton(title: "HIGH")
        button.addTarget(self, action: #selector(knowledgeButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    let riskTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "How much risk can you tolerate?"
        textView.textAlignment = .center
        return textView
    }()
    
    let riskUIStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.spacing = 15
        return container
    }()
    
    lazy var lowRiskButton: UIButton = {
        let button = baseButton(title: "LOW")
        button.addTarget(self, action: #selector(riskButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var mediumRiskButton: UIButton = {
        let button = baseButton(title: "MEDIUM")
        button.addTarget(self, action: #selector(riskButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var highRiskButton: UIButton = {
        let button = baseButton(title: "HIGH")
        button.addTarget(self, action: #selector(riskButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(color, for: .normal)
        return button
    }
    
    lazy var nextbtn: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "next-btn")
        img.isUserInteractionEnabled = true
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotNext)))
        return img
    }()
    
    override func setUpView() {
        super.setUpView()
        
        self.scrollView.addSubview(headingTextView)
        self.scrollView.addSubview(knowledgeTextView)
        self.scrollView.addSubview(knowledgeUIStackView)
        self.scrollView.addSubview(riskTextView)
        self.scrollView.addSubview(riskUIStackView)
        self.scrollView.addSubview(nextbtn)
        
        let width = view.frame.width
        self.headingTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.headingTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.headingTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        
        //MARK:- investment knowledge section
        self.knowledgeTextView.anchorWithConstantsToTop(top: headingTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.knowledgeTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.knowledgeTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.knowledgeUIStackView.anchorWithConstantsToTop(top: knowledgeTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.knowledgeUIStackView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.knowledgeUIStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.knowledgeUIStackView.addArrangedSubview(lowKnowledeButton)
        self.knowledgeUIStackView.addArrangedSubview(mediumKnowledeButton)
        self.knowledgeUIStackView.addArrangedSubview(highKnowledeButton)
        
        //MARK:- risk section
        self.riskTextView.anchorWithConstantsToTop(top: knowledgeUIStackView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.riskTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.riskTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.riskUIStackView.anchorWithConstantsToTop(top: riskTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.riskUIStackView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.riskUIStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.riskUIStackView.addArrangedSubview(lowRiskButton)
        self.riskUIStackView.addArrangedSubview(mediumRiskButton)
        self.riskUIStackView.addArrangedSubview(highRiskButton)
        
        nextbtn.anchor(nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    }
    
    //MARK - risk button clicked here
    @objc func knowledgeButtonClicked(_ sender: UIButton){
        self.knowledgeLevel = sender.currentTitle!
        
        if sender == lowKnowledeButton {
            self.activateButton(button: lowKnowledeButton)
            self.resetButton(button: mediumKnowledeButton)
            self.resetButton(button: highKnowledeButton)
        }
        if sender == mediumKnowledeButton {
            self.activateButton(button: mediumKnowledeButton)
            self.resetButton(button: lowKnowledeButton)
            self.resetButton(button: highKnowledeButton)
        }
        if sender == highKnowledeButton {
            self.activateButton(button: highKnowledeButton)
            self.resetButton(button: lowKnowledeButton)
            self.resetButton(button: mediumKnowledeButton)
        }
        
        self.changeButtonStatus()
    }
    
    //MARK - risk button clicked here
    @objc func riskButtonClicked(_ sender: UIButton){
        self.riskLevel = sender.currentTitle!
        if sender == lowRiskButton {
            self.activateButton(button: lowRiskButton)
            self.resetButton(button: mediumRiskButton)
            self.resetButton(button: highRiskButton)
        }
        if sender == mediumRiskButton {
            self.activateButton(button: mediumRiskButton)
            self.resetButton(button: lowRiskButton)
            self.resetButton(button: highRiskButton)
        }
        if sender == highRiskButton {
            self.activateButton(button: highRiskButton)
            self.resetButton(button: lowRiskButton)
            self.resetButton(button: mediumRiskButton)
        }
        
        self.changeButtonStatus()
    }
    
    //MARK:- reset the buttons
    func resetButton(button:UIButton) {
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
    }
    
    //MARK:- activate buttons
    func activateButton(button:UIButton)  {
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
    }
    
    func changeButtonStatus()  {

    }
    
    @objc func gotNext(){
        let vc = ChooseNextOfKinVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
