//
//  ChooseNextOfKinVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 7/22/19.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import ContactsUI
import PhoneNumberKit

class ChooseNextOfKinVC: BaseScrollViewController {
    
    let cellId = "cellId"
    var nextOfKinRelationShip = "SPOUSE"
    
    let recentLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "  RECENTLY USED"
        lbl.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        lbl.textColor = .lightGray
        return lbl
    }()
    
    //recently used
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView(direction: .horizontal)
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    let newnextOfKinLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "  NEW NEXT OF KIN"
        lbl.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        lbl.textColor = .lightGray
        return lbl
    }()
    
    let phoneNumberTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Phone Number"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .left
        return lbl
    }()
    
    lazy var numberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Phone number")
        textField.keyboardType = UIKeyboardType.alphabet
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPhoneFromContact)))
        imageView.image = UIImage(named: "AddContact")
        imageView.isUserInteractionEnabled = true
        
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        
        textField.rightView = imageView
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    let nameTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Enter Description (eg: AMA)"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .left
        return lbl
    }()
    
    lazy var nameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    let relationshipTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Relationship"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .left
        return lbl
    }()
    
    lazy var relationShipTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("SPOUSE", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addBottomBorder(UIColor.lightGray, height: 1)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickType(_:)), for: .touchUpInside)
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        return button
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(color, for: .normal)
        return button
    }
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        
        button.isEnabled = true
        button.backgroundColor = UIColor.gray
        
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 0
        button.addTarget(self, action: #selector(addPayment), for: .touchUpInside)
        
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        self.collectionView.register(RecentNextOfKinCell.self, forCellWithReuseIdentifier: cellId)
        self.navigationItem.title = "Choose Next Of Kin"
        
        self.scrollView.addSubview(recentLabel)
        self.scrollView.addSubview(collectionView)
        self.scrollView.addSubview(newnextOfKinLabel)
        
        self.scrollView.addSubview(nameTitle)
        self.scrollView.addSubview(nameTextField)
        
        self.scrollView.addSubview(phoneNumberTitle)
        self.scrollView.addSubview(numberTextField)
        
        self.scrollView.addSubview(relationshipTitle)
        self.scrollView.addSubview(relationShipTextField)
        
        self.scrollView.addSubview(nextButton)
        
        recentLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        recentLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        recentLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        recentLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        collectionView.anchor(recentLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        
        newnextOfKinLabel.anchor(collectionView.bottomAnchor, left: collectionView.leftAnchor, bottom: nil, right: collectionView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
        
        let width = view.frame.width
        //MARK:- name section
        
        phoneNumberTitle.anchor(newnextOfKinLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 30)
        
        self.numberTextField.anchor(phoneNumberTitle.bottomAnchor, left: phoneNumberTitle.leftAnchor, bottom: nil, right: phoneNumberTitle.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 44)
        
        //MARK:- number section
        nameTitle.anchor(numberTextField.bottomAnchor, left: numberTextField.leftAnchor, bottom: nil, right: numberTextField.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        self.nameTextField.anchor(nameTitle.bottomAnchor, left: phoneNumberTitle.leftAnchor, bottom: nil, right: phoneNumberTitle.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 44)
        
        //MARK:- realtionship section
        relationshipTitle.anchor(nameTextField.bottomAnchor, left: numberTextField.leftAnchor, bottom: nil, right: numberTextField.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        self.relationShipTextField.anchor(relationshipTitle.bottomAnchor, left: relationshipTitle.leftAnchor, bottom: nil, right: relationshipTitle.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 44)
        
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: scrollView.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: -50, rightConstant: 0)
        self.nextButton.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
}

extension ChooseNextOfKinVC: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! RecentNextOfKinCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150.0, height: 150.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}

extension ChooseNextOfKinVC : CNContactPickerDelegate {
    
    @objc func pickType(_ sender: UIButton)  {
        let relationships = GoalNextKin.all()
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        for item in relationships {
            let defaultAction = UIAlertAction(title: "\(item.name)", style: .default) { (action) in
                sender.setTitle(item.name, for: .normal)
                self.relationShipTextField.setTitle(item.name, for: .normal)
                self.nextOfKinRelationShip = item.name
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
     @objc func textFieldDidChange(textField: UITextField){
        
    }
    
    @objc func selectPhoneFromContact(sender: UITapGestureRecognizer){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.predicateForEnablingContact = NSPredicate(format:"phoneNumbers.@count > 0",argumentArray: nil)
        contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'", argumentArray: nil)
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let contactName = CNContactFormatter.string(from: contact, style: .fullName) ?? ""
        for number in contact.phoneNumbers {
            let number = (number.value).value(forKey: "digits") as? String
            if let numberReal = number {
                self.numberTextField.text = numberReal
                self.nameTextField.text = contactName
            }
        }
        picker.dismiss(animated: true, completion: nil)
        self.textFieldDidChange(textField: self.numberTextField)
    }
    
    @objc func addPayment(){
        let vc = SelectPayOutOptionVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
