//
//  OrderTbillsPageOneVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 7/21/19.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import GradientView

class OrderTbillsPageOneVC: BaseScrollViewController {
    
    var colorArray = [[UIColor]]()
    
    let itemCardView: GradientView = {
        let v = GradientView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 12
        v.layer.masksToBounds = true
        v.layer.shadowColor = UIColor.gray.cgColor
        v.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        v.layer.shadowRadius = 5.0
        v.layer.shadowOpacity = 0.24
        v.locations = [0.3, 1.0]
        v.direction = .vertical
        return v
    }()
    
    let numberOfDays: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "0 Days"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    let maturityDateLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Maturity Date"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    let maturityDate: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Jan 21, 2019"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    let interestRateLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Interest Rate"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    let interestRate: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "16.1% pa"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    let arrowRightImg: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "NextActionBlack")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = .white
        img.isHidden = true
        return img
    }()
    
    lazy var nextbtn: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "next-btn")
        img.isUserInteractionEnabled = true
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotNext)))
        return img
    }()
    
    //contents
    let daysTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Title"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let days: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "91 Days"
        lbl.textColor = .gray
        lbl.textAlignment = .left
        return lbl
    }()
    
    let amountTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Amount (GHS)"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let amount: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "1,000"
        lbl.textColor = .gray
        lbl.textAlignment = .left
        return lbl
    }()
    
    let maturityView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        return v
    }()
    
    let maturityTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Calculated Maturity Amount(GHS)"
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .left
        return lbl
    }()
    
    let maturity: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "1,140"
        lbl.textColor = .gray
        lbl.textAlignment = .left
        return lbl
    }()
    
    override func setUpView() {
        super.setUpView()
        self.view.backgroundColor = .white
        self.itemCardView.colors = colorArray[0]
        
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(itemCardView)
        self.itemCardView.addSubview(numberOfDays)
        self.itemCardView.addSubview(maturityDateLbl)
        self.itemCardView.addSubview(maturityDate)
        self.itemCardView.addSubview(interestRateLbl)
        self.itemCardView.addSubview(interestRate)
        self.itemCardView.addSubview(arrowRightImg)
        
        self.scrollView.addSubview(daysTitle)
        self.scrollView.addSubview(days)
        
        self.scrollView.addSubview(amountTitle)
        self.scrollView.addSubview(amount)
        
        self.scrollView.addSubview(maturityView)
        self.maturityView.addSubview(maturityTitle)
        self.maturityView.addSubview(maturity)
        
        self.scrollView.addSubview(nextbtn)
        
        itemCardView.anchor(self.scrollView.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 30, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 150)
        
        numberOfDays.anchor(itemCardView.topAnchor, left: itemCardView.leftAnchor, bottom: nil, right: itemCardView.rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        maturityDateLbl.anchor(interestRateLbl.topAnchor, left: self.itemCardView.leftAnchor, bottom: interestRateLbl.bottomAnchor, right: interestRateLbl.leftAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        maturityDate.anchor(interestRate.topAnchor, left: maturityDateLbl.leftAnchor, bottom: interestRate.bottomAnchor, right: interestRate.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        interestRateLbl.anchor(nil, left: interestRate.leftAnchor, bottom: interestRate.topAnchor, right: interestRate.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 3, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        interestRate.anchor(nil, left: nil, bottom: arrowRightImg.topAnchor, right: arrowRightImg.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 3, rightConstant: 0, widthConstant: self.itemCardView.frame.width / 2, heightConstant: 20)
        
        arrowRightImg.anchor(nil, left: nil, bottom: self.itemCardView.bottomAnchor, right: self.itemCardView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 8, widthConstant: 20, heightConstant: 40)
        
        //contents
        daysTitle.anchor(itemCardView.bottomAnchor, left: itemCardView.leftAnchor, bottom: nil, right: itemCardView.rightAnchor, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        days.anchor(daysTitle.bottomAnchor, left: daysTitle.leftAnchor, bottom: nil, right: daysTitle.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        amountTitle.anchor(days.bottomAnchor, left: days.leftAnchor, bottom: nil, right: days.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        amount.anchor(amountTitle.bottomAnchor, left: amountTitle.leftAnchor, bottom: nil, right: amountTitle.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)

        maturityView.anchor(amount.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 12, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 100)
        
        maturityTitle.anchor(maturityView.topAnchor, left: maturityView.leftAnchor, bottom: nil, right: maturityView.rightAnchor, topConstant: 25, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        maturity.anchor(maturityTitle.bottomAnchor, left: maturityTitle.leftAnchor, bottom: nil, right: maturityTitle.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        nextbtn.anchor(nil, left: self.itemCardView.leftAnchor, bottom: self.scrollView.safeAreaLayoutGuide.bottomAnchor, right: self.itemCardView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    }
    
    @objc func gotNext(){
        let vc = InvestmentKnowlegeVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
