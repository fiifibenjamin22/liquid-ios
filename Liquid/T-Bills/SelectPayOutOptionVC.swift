//
//  SelectPayOutOptionVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class SelectPayOutOptionVC: BaseScrollViewController {
    
    let cellId = "cellId"
    
    let bankImages = ["BankStanbic","BankZenith"]
    
    let selectLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Please select or create a payout account."
        lbl.textColor = .darkGray
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView(direction: .vertical)
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    lazy var nextButton: UIButton = {
        let button = self.baseButton(title: "Add a new payout account")
        button.addTarget(self, action: #selector(selectPaymentAction), for: .touchUpInside)
        return button
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(color, for: .normal)
        return button
    }
    
    override func setUpView() {
        super.setUpView()
        self.collectionView.register(PayoutOptionCell.self, forCellWithReuseIdentifier: cellId)
        
        self.scrollView.addSubview(selectLabel)
        self.scrollView.addSubview(collectionView)
        self.scrollView.addSubview(nextButton)
        
        selectLabel.anchor(scrollView.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 30)
        
        collectionView.anchor(selectLabel.bottomAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        nextButton.anchor(nil, left: selectLabel.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: selectLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    }
    
    @objc func selectPaymentAction(){
        let vc = NewPaymentOptionController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SelectPayOutOptionVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PayoutOptionCell
        cell.bankImage.image = UIImage(named: bankImages[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 60.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
