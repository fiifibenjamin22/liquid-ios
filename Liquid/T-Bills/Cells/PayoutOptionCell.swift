//
//  PayoutOptionCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class PayoutOptionCell: BaseCell {
    
    let bankImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let bankAccount: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Zenith - 8105"
        lbl.textColor = .gray
        lbl.textAlignment = .left
        return lbl
    }()
    
    let rightArrow: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "NextActionBlack")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        return img
    }()
    
    let seperator: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func setUpView() {
        super.setUpView()
        
        self.addSubview(bankImage)
        self.addSubview(bankAccount)
        self.addSubview(rightArrow)
        self.addSubview(seperator)
        
        bankImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        bankImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        bankImage.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        bankImage.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        bankImage.layer.cornerRadius = 20
        bankImage.layer.masksToBounds = true
        
        bankAccount.leftAnchor.constraint(equalTo: bankImage.rightAnchor, constant: 8).isActive = true
        bankAccount.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        bankAccount.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -8).isActive = true
        bankAccount.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        rightArrow.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        rightArrow.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        rightArrow.widthAnchor.constraint(equalToConstant: 30).isActive = true
        rightArrow.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        seperator.anchor(nil, left: bankImage.leftAnchor, bottom: bottomAnchor, right: rightArrow.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 1)
    }
}
