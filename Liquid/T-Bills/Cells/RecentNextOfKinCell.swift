//
//  RecentNextOfKinCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 7/22/19.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class RecentNextOfKinCell: BaseCell {
    
    let cardView: CardView = {
        let v = CardView()
        v.backgroundColor = .white
        return v
    }()
    
    let firstInitBackView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.hex(hex: "CC0000")
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let firstInitial: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textColor = .white
        lbl.text = "BA"
        lbl.textAlignment = .center
        lbl.backgroundColor = .clear
        return lbl
    }()
    
    let nextOfKinName: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.textColor = .gray
        lbl.text = "Benjamin Acquah"
        return lbl
    }()
    
    override func setUpView() {
        super.setUpView()
        
        self.addSubview(cardView)
        self.cardView.addSubview(firstInitBackView)
        self.firstInitBackView.addSubview(firstInitial)
        self.cardView.addSubview(nextOfKinName)
        
        cardView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        firstInitBackView.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        firstInitBackView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 16).isActive = true
        firstInitBackView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        firstInitBackView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        firstInitBackView.layer.cornerRadius = 35.0
        firstInitBackView.layer.masksToBounds = true
        
        firstInitial.anchor(firstInitBackView.topAnchor, left: firstInitBackView.leftAnchor, bottom: firstInitBackView.bottomAnchor, right: firstInitBackView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        nextOfKinName.anchor(nil, left: cardView.leftAnchor, bottom: cardView.bottomAnchor, right: cardView.rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 8, rightConstant: 8, widthConstant: 0, heightConstant: 30)
    }
}
