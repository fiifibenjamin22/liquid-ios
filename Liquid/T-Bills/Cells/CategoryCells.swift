//
//  CategoryCells.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import GradientView

class CategoryCells: BaseCell {
    
    let itemCardView: GradientView = {
        let v = GradientView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 12
        v.layer.masksToBounds = true
        v.layer.shadowColor = UIColor.gray.cgColor
        v.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        v.layer.shadowRadius = 5.0
        v.layer.shadowOpacity = 0.24
        v.locations = [0.3, 1.0]
        v.direction = .vertical
        return v
    }()
        
    let numberOfDays: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "0 Days"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    let maturityDateLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Maturity Date"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    let maturityDate: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Jan 21, 2019"
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    let interestRateLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Interest Rate"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    let interestRate: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "16.1% pa"
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy)
        return lbl
    }()
    
    let arrowRightImg: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "NextActionBlack")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = .white
        return img
    }()
    
    override func setUpView() {
        super.setUpView()
        
        self.addSubview(itemCardView)
        
        self.itemCardView.addSubview(numberOfDays)
        self.itemCardView.addSubview(maturityDateLbl)
        self.itemCardView.addSubview(maturityDate)
        self.itemCardView.addSubview(interestRateLbl)
        self.itemCardView.addSubview(interestRate)
        self.itemCardView.addSubview(arrowRightImg)
        
        itemCardView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        
        numberOfDays.anchor(itemCardView.topAnchor, left: itemCardView.leftAnchor, bottom: nil, right: itemCardView.rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        maturityDateLbl.anchor(interestRateLbl.topAnchor, left: self.itemCardView.leftAnchor, bottom: interestRateLbl.bottomAnchor, right: interestRateLbl.leftAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        maturityDate.anchor(interestRate.topAnchor, left: maturityDateLbl.leftAnchor, bottom: interestRate.bottomAnchor, right: interestRate.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        interestRateLbl.anchor(nil, left: interestRate.leftAnchor, bottom: interestRate.topAnchor, right: interestRate.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 3, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        interestRate.anchor(nil, left: nil, bottom: arrowRightImg.topAnchor, right: arrowRightImg.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 3, rightConstant: 0, widthConstant: self.itemCardView.frame.width / 2, heightConstant: 20)
        
        arrowRightImg.anchor(nil, left: nil, bottom: self.itemCardView.bottomAnchor, right: self.itemCardView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 8, widthConstant: 20, heightConstant: 40)
    }
}
