//
//  TBillsCategoriesController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

let cellId = "CellId"

class TBillCategoriesController: UICollectionViewController {
    
    let gradArrays = [[UIColor.hex(hex: "02A9E8"), UIColor.hex(hex: "00F6FF")],[UIColor.hex(hex: "FF2E00"), UIColor.hex(hex: "FF8A00")],[UIColor.hex(hex: "0500FF"), UIColor.hex(hex: "CC00ED")]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = .white
        self.collectionView.register(CategoryCells.self, forCellWithReuseIdentifier: cellId)
        self.navigationItem.title = "Purchase T-Bills"
        self.collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
}

extension TBillCategoriesController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CategoryCells
        cell.itemCardView.colors = gradArrays[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = OrderTbillsPageOneVC()
        vc.colorArray.append(gradArrays[indexPath.row])
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
