//
//  ShareGoalVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 15/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class ShareGoalVC: UIViewController {
    
    var goals: Goal?
    
    var gradientLayer = CAGradientLayer()
    
    var messageTxt: UITextView = {
        let txt = ViewControllerHelper.baseTextView()
        txt.text = "Let others know about this goal, choose any of the options below to share."
        txt.textColor = .white
        txt.font = UIFont.boldSystemFont(ofSize: 19)
        return txt
    }()
    
    lazy var oneTimeLink: UIButton = {
        let btn = UIButton()
        btn.setTitle("One-Time Link", for: .normal)
        btn.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        btn.backgroundColor = .white
        btn.layer.cornerRadius = 10
        btn.addTarget(self, action: #selector(handleOneTimeLink), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var addParticipants: UIButton = {
        let btn = UIButton()
        btn.setTitle("Add Participant", for: .normal)
        btn.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        btn.backgroundColor = .white
        btn.layer.cornerRadius = 10
        btn.addTarget(self, action: #selector(handleAddParticipant), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Share Goal"
        self.setupViews()
    }
    
    func setupViews(){
        
        gradientLayer.frame = self.view.frame
        gradientLayer.colors = [UIColor.magenta.cgColor, UIColor.cyan.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.95)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
        self.view.layer.addSublayer(gradientLayer)
        
        self.view.addSubview(messageTxt)
        self.view.addSubview(oneTimeLink)
        self.view.addSubview(addParticipants)
        
        self.messageTxt.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 50.0).isActive = true
        self.messageTxt.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16.0).isActive = true
        self.messageTxt.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
        self.messageTxt.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        
        self.oneTimeLink.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -70.0).isActive = true
        self.oneTimeLink.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.oneTimeLink.widthAnchor.constraint(equalToConstant: self.view.frame.width / 1.5).isActive = true
        self.oneTimeLink.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        self.addParticipants.topAnchor.constraint(equalTo: self.oneTimeLink.bottomAnchor, constant: 12.0).isActive = true
        self.addParticipants.leftAnchor.constraint(equalTo: self.oneTimeLink.leftAnchor).isActive = true
        self.addParticipants.rightAnchor.constraint(equalTo: self.oneTimeLink.rightAnchor).isActive = true
        self.addParticipants.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
    }
    
    @objc func handleOneTimeLink() {
        ViewControllerHelper.presentSharer(targetVC: self, message: " ")
    }
    
    @objc func handleAddParticipant() {
        let vc = AddParticipantVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
