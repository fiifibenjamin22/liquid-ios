//
//  NewGoalNameController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CropViewController

class NewGoalNameController: BaseScrollViewController {
    
    var goal: Goal?
    var goalName = ""
    var goalAmount = "0"
    var goalInitialAmount = "0"
    var goalDueDate = ""
    var dateHint = "Due Date"
    var isStrictMode = true
    var imagePicker : UIImagePickerController!
    var goalImage: UIImage?
    

    let headingTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var goalNameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Goal name")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    lazy var amountNeededTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Amount needed (GHS)")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.delegate = self
        return textField
    }()
    /*
    lazy var initialAmountTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Initial amount to contribute (GHS)")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.delegate = self
        return textField
    }()*/
    
    lazy var dateTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle(self.dateHint, for: .normal)
        button.contentHorizontalAlignment = .left
        let color = UIColor.darkGray
        button.setTitleColor(color, for: .normal)
        button.addBottomBorder(UIColor.lightGray, height: 1)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.addTarget(self, action: #selector(pickDueDate(_:)), for: .touchUpInside)
        return button
    }()
    
    
    let pickImageUIButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle(" Change cover photo", for: .normal)
        button.titleLabel?.textAlignment = .left
        button.setImage(UIImage(named: "GroupCamera"), for: .normal)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickImage(_:)), for: .touchUpInside)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 20)!)
        return button
    }()
    
    
    lazy var createGoalButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(createGoal), for: .touchUpInside)
        return button
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    
    override func setUpView() {
        super.setUpView()
        
        if let goalIn = self.goal {
           self.setUpNavigationBarWhite(title: "\(goalIn.name)")
        }  else {
           self.setUpNavigationBarWhite(title: "New Goal")
        }
       
        self.addViews()
    }
    
    func addViews()  {
       self.scrollView.addSubview(headingTextView)
       self.scrollView.addSubview(imageView)
       self.scrollView.addSubview(pickImageUIButton)
       self.scrollView.addSubview(goalNameTextField)
       self.scrollView.addSubview(amountNeededTextField)
       //self.scrollView.addSubview(initialAmountTextField)
       self.scrollView.addSubview(dateTextField)
       self.scrollView.addSubview(createGoalButton)
        
       let width = view.frame.width
       
       self.headingTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
       self.headingTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
       self.headingTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
       
        self.imageView.anchorWithConstantsToTop(top: headingTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.imageView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.imageView.layer.cornerRadius = 15
        
        self.pickImageUIButton.anchorWithConstantsToTop(top: nil, left: imageView.leadingAnchor, bottom: imageView.bottomAnchor, right: imageView.trailingAnchor, topConstant: 0, leftConstant: 4, bottomConstant: -4, rightConstant: -100)
        self.pickImageUIButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.goalNameTextField.anchorWithConstantsToTop(top: imageView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.goalNameTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.goalNameTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.amountNeededTextField.anchorWithConstantsToTop(top: goalNameTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.amountNeededTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.amountNeededTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        /*
        self.initialAmountTextField.anchorWithConstantsToTop(top: amountNeededTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.initialAmountTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.initialAmountTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        */
        self.dateTextField.anchorWithConstantsToTop(top: amountNeededTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.dateTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.dateTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        self.createGoalButton.anchorWithConstantsToTop(top: dateTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: -50, rightConstant: 0)
        self.createGoalButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.createGoalButton.widthAnchor.constraint(equalToConstant: width - 32).isActive = true

        self.updateUI()
    }
    
    func updateUI()  {
     
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        
        let messageHeader = "Step 1 out of 4.\n".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
        let messageBody = "Provide your goal details".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        
        self.headingTextView.attributedText = result
        self.headingTextView.textAlignment = .center
        
        if let goalIn = self.goal {
            if  !(goalIn.image.isEmpty) {
                self.imageView.af_setImage(
                    withURL: URL(string: (goalIn.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.imageView.image = Mics.placeHolder()
            }
        }
       
    }
    
    
    //MARK - create goals here
    @objc func createGoal(){
       self.goal?.name = self.goalName
       self.goal?.targetAmount = Double(self.goalAmount)!
       self.goal?.initialAmountDeposited = self.goalInitialAmount != "" ? Double(self.goalInitialAmount)! : 0.0
       self.goal?.dueDate = self.goalDueDate
        
        if self.goalDueDate.isEmpty || self.goalDueDate == self.dateHint {
            ViewControllerHelper.showPrompt(vc: self, message: "Due date is required for the goal.")
            return
        }
        
        if Double(self.goalInitialAmount)! > Double(self.goalAmount)! {
            ViewControllerHelper.showPrompt(vc: self, message: "Initial deposit appears to be more than the target amount.")
            return
        }
        
        let destination = NewGoalRuleController()
        destination.goal = self.goal
        destination.image = self.goalImage
        
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.goalName = self.goalNameTextField.text!
        self.goalAmount = self.amountNeededTextField.text! == "" ? "0" : self.amountNeededTextField.text!
        //self.goalInitialAmount = self.initialAmountTextField.text! == "" ? "0" : self.initialAmountTextField.text!
        self.goalDueDate = self.dateTextField.currentTitle!
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        var dueDateValid = false
        if dateFormatterGet.date(from: self.goalDueDate) != nil{
            dueDateValid = true
        }
        if !self.goalName.isEmpty && Int(self.goalAmount)! > 0 && dueDateValid {
            self.createGoalButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.createGoalButton.backgroundColor = color
        } else {
            self.createGoalButton.isEnabled = false
            let color = UIColor.gray
            self.createGoalButton.backgroundColor = color
        }
    }
    
    //MARK:- pick date of birth instead
    @objc func pickDueDate(_ sender: UIButton){
        let aDayAhead: TimeInterval =  7 * 24 * 60 * 60
        let startingDate = Date(timeInterval: aDayAhead, since: Date())
        let datePicker = ActionSheetDatePicker(title: "SELECT DATE", datePickerMode: UIDatePicker.Mode.date, selectedDate: startingDate, doneBlock: {
            picker, value, index in
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy"
            let dateTaken =  value! as! Date
            if dateTaken <= Date()  {
                ViewControllerHelper.showPrompt(vc: self, message: "Due date must be a future date.")
                return
            }
            self.goalDueDate = dateFormatterGet.string(from: dateTaken)
            self.dateTextField.setTitle(self.goalDueDate, for: .normal)
            self.dateTextField.setTitleColor(UIColor.darkText, for: .normal)
            self.textFieldDidChange(textField: self.goalNameTextField)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
        datePicker?.show()
    }
    
    @objc func pickImage(_ sender: UIButton) {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: "Take photo", style: .default) { (action) in
            self.takePhoto()
        }
        
        let profileAction = UIAlertAction(title: "Pick from libary", style: .default) { (action) in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(profileAction)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func takePhoto()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }  else {
            ViewControllerHelper.showPrompt(vc: self, message: "Your device does not support camera options.")
        }
    }
    
}


extension NewGoalNameController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: ".0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
    
}

extension NewGoalNameController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[.originalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: .default, image: image)
        cropController.delegate = self
        picker.dismiss(animated: true, completion: {
            self.present(cropController, animated: true, completion: nil)
        })
        
    }
    
    //MARK:- the cropper delegate
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        cropViewController.dismiss(animated: true, completion: nil)
        
        self.goalImage = image
        self.imageView.image = image
        
    }
    
}

