//
//  NewGoalController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class NewGoalController: UIViewController {
    
    let collectionId = "collectionId"
    let collectionHeader = "collectionHeader"
    var goals = [GoalCategory]()
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    

    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Create A Goal"
        self.view.addSubview(collectionView)
        
        
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: -8)
        self.collectionView.register(NewGoalCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(GenericHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionHeader)
        
        self.setGoalsCategory()
        
        //MARK:- ended notification
        NotificationCenter.default.addObserver(self, selector: #selector(receivedEndedNotification(notification:)), name: NSNotification.Name(Key.createGoal), object: nil)
    }
    
    @objc func receivedEndedNotification(notification: Notification){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- fetch data from storage
    func setGoalsCategory()  {
        self.loadFromDb()
        if self.goals.isEmpty {
            self.viewControllerHelper.showActivityIndicator()
            self.apiService.goalCategories { (status, categories, withMessage) in
               self.viewControllerHelper.hideActivityIndicator()
               self.loadFromDb()
            }
        }
    }
    
    func loadFromDb()  {
        self.goals.append(contentsOf: GoalCategory.all())
        self.collectionView.reloadData()
    }
}

extension NewGoalController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.goals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! NewGoalCell
        cell.item = self.goals[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemHeight = 150
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(2 - 1)) + 10
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(2))
        
        return CGSize(width: size, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView  = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionHeader, for: indexPath) as! GenericHeader
        reusableView.item = "Awesome, start by choosing what you want to save for"
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(80)
        return CGSize(width: itemWidth, height: itemHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         self.goNext(position: indexPath.row)
    }
    
    func goNext(position:Int)  {
        let goalCategory = self.goals[position]
        let destination = NewGoalNameController()
        let goal = Goal()
        goal.name = goalCategory.name.htmlToString
        goal.image = goalCategory.image
        goal.categoryId = goalCategory.categoryId
        destination.goal = goal
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
