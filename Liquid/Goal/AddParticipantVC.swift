//
//  AddParticipantVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import ContactsUI
import PhoneNumberKit
import SCLAlertView

class AddParticipantVC: UIViewController, UITextFieldDelegate {
    
    var goal: Goal?
    var goalId = 0
    var goalName = ""
    let user = User.getUser()!
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var popup: PopupDialog?
    var isButtonReady = false
    
    var referal_codes = ""
    var ref_message = ""
    
    var participantNumbers = [String]()
    
    var addNumberLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Add a participant’s phone number"
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textColor = .black
        return lbl
    }()
    
    lazy var numberTextField: UITextField = {
        let tf = ViewControllerHelper.baseField()
        tf.placeholder = "Enter Phone Number"
        tf.textColor = .lightGray
        tf.font = UIFont.boldSystemFont(ofSize: 16)
        tf.textAlignment = .left
        tf.keyboardType = UIKeyboardType.phonePad
        tf.setBottomBorder(color: .lightGray)
        tf.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)

        return tf
    }()
    
    lazy var iconImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AddContact")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = UIColor.lightGray
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        img.isUserInteractionEnabled = true
        return img
    }()
    
    var underlineView: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
//    let passwordHintLable: UILabel = {
//        let textView = ViewControllerHelper.baseLabel()
//        textView.textAlignment = .left
//        textView.textColor = UIColor.red
//        textView.text = AppConstants.passwordHintMessage
//        textView.isHidden = true
//        return textView
//    }()
    
    var infoLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Invite others to contribute to and track progress toward this goal!  Anyone you add as a goal participant will be able to see transaction history and fund the goal, but cannot trigger withdrawals."
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textColor = .black
        return lbl
    }()
    
    lazy var nextButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.addGradient()
        btn.layer.cornerRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Next", for: .normal)
        btn.isEnabled = false
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 19)
        btn.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        btn.backgroundColor = UIColor.gray
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add Participants"
                
        self.setupViews()
    }
    
    func setupViews(){
        
        self.view.addSubview(addNumberLabel)
        self.view.addSubview(numberTextField)
        self.numberTextField.addSubview(iconImage)
        self.view.addSubview(underlineView)
        //self.view.addSubview(passwordHintLable)
        self.view.addSubview(infoLabel)
        self.view.addSubview(nextButton)
        
        self.addNumberLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 70.0).isActive = true
        self.addNumberLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16.0).isActive = true
        self.addNumberLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16.0).isActive = true
        self.addNumberLabel.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
        
        self.numberTextField.topAnchor.constraint(equalTo: self.addNumberLabel.bottomAnchor, constant: 16).isActive = true
        self.numberTextField.leftAnchor.constraint(equalTo: addNumberLabel.leftAnchor).isActive = true
        self.numberTextField.rightAnchor.constraint(equalTo: addNumberLabel.rightAnchor).isActive = true
        self.numberTextField.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        self.iconImage.rightAnchor.constraint(equalTo: numberTextField.rightAnchor).isActive = true
        self.iconImage.centerYAnchor.constraint(equalTo: numberTextField.centerYAnchor).isActive = true
        self.iconImage.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        self.iconImage.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        self.underlineView.topAnchor.constraint(equalTo: numberTextField.bottomAnchor).isActive = true
        self.underlineView.leftAnchor.constraint(equalTo: numberTextField.leftAnchor).isActive = true
        self.underlineView.rightAnchor.constraint(equalTo: numberTextField.rightAnchor).isActive = true
        self.underlineView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        //self.passwordHintLable.anchor(underlineView.bottomAnchor, left: underlineView.leftAnchor, bottom: nil, right: underlineView.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        self.infoLabel.bottomAnchor.constraint(equalTo: nextButton.topAnchor, constant: -16).isActive = true
        self.infoLabel.leftAnchor.constraint(equalTo: nextButton.leftAnchor).isActive = true
        self.infoLabel.rightAnchor.constraint(equalTo: nextButton.rightAnchor).isActive = true
        self.infoLabel.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        
        self.nextButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -12).isActive = true
        self.nextButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 16).isActive = true
        self.nextButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -16).isActive = true
        self.nextButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true

    }
    
    @objc func textFieldDidChange(textField: UITextField){
        let phoneNumber = "\(textField.text!)"
        textFieldDidBeginEditing(phone: phoneNumber)
    }
    
    private func textFieldDidBeginEditing(phone: String) {
        let number = phone
        if Validator.validNumber(number: number) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }   else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    
    @objc func toggleButtonStatus(){
        let number = self.numberTextField.text!
        if Validator.validNumber(number: number) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }   else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.predicateForEnablingContact = NSPredicate(format:"phoneNumbers.@count > 0",argumentArray: nil)
        contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'", argumentArray: nil)
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    //MARK - auto advance input fields
    func textFieldDidChange(text: String){
        if text.utf16.count == 10 || text.utf16.count == 9 {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }  else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func handleNext(){
        
        if self.numberTextField.text == "" {
            self.nextButton.isEnabled = false
            return
        }
        
        self.nextButton.isEnabled = true
        
        //MARK:- find participant by mobile number
        self.viewControllerHelper.showActivityIndicator()
        
        
        if participantNumbers.contains(self.numberTextField.text!){
            ViewControllerHelper.showPrompt(vc: self, message: "This user is already a participant of this goal") { (isFinished) in
                self.numberTextField.becomeFirstResponder()
            }
            self.viewControllerHelper.hideActivityIndicator()
            return
        }else{
            self.apiService.findParticipant(mobileNumber: self.numberTextField.text!, goalId: self.goalId) { (status, response, message) in
                self.viewControllerHelper.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    
                    let name = response["name"].stringValue
                    let customer_id = response["customer_id"].intValue
                    
                    if customer_id == -1 {
                        
                        let referal_code = response["referral_code"].stringValue
                        
                        let confirm_vc = ParticipantViewDeclined(nibName: "ParticipantViewDeclined", bundle: nil)
                        confirm_vc.delegate = self
                        self.referal_codes = referal_code
                        confirm_vc.shareCode = referal_code
                        confirm_vc.goalName = self.goalName
                        confirm_vc.partNumber = self.numberTextField.text!
                        self.popup = PopupDialog(viewController: confirm_vc)
                        self.present(self.popup!, animated: true, completion: nil)

                        
                        return
                    }else{
                        
                        let confirm_vc = AddLiquidParticipantVC(nibName: "AddLiquidParticipantVC", bundle: nil)
                        confirm_vc.delegate = self
                        confirm_vc.participantName = name
                        confirm_vc.ownerName = "\(self.user.firstName) \(self.user.otherName) \(self.user.lastName)"
                        confirm_vc.customerId = customer_id
                        self.popup = PopupDialog(viewController: confirm_vc)
                        self.present(self.popup!, animated: true, completion: nil)
            
                    }
                }
                
                if status == ApiCallStatus.DETAIL {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
                
                if status == ApiCallStatus.FAILED {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            }
        }
    }
    
    func shareCode(codeTextView: String){
        let code = codeTextView
        let inviteMessage = AppConstants.inviteToGoalMessage.replacingOccurrences(of: "-referralcode-", with: code)
        ViewControllerHelper.presentSharer(targetVC: self, message: "\(inviteMessage)")
    }
}

extension AddParticipantVC: CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        //let contactName = CNContactFormatter.string(from: contact, style: .fullName) ?? ""
        for number in contact.phoneNumbers {
            let number = (number.value).value(forKey: "digits") as? String
            if let numberReal = number {
                self.numberTextField.text = numberReal
                //self.recipientNameTextField.text = contactName
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension AddParticipantVC: TextChangeDelegate {
    
    func didChange(text: String) {
        self.toggleButtonStatus()
        self.ref_message = text
        self.textFieldDidChange(text: text)
    }
    
    func proceed(custom: String, cust_id: Int) {
        viewControllerHelper.showActivityIndicator()
        self.apiService.addParticipant(goalId: self.goalId, customerId: cust_id,custom_message: custom, phone: self.numberTextField.text!) { (response, status, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                
                if response["error"].stringValue != "" {
                    ViewControllerHelper.showPrompt(vc: self, message: response["error"].stringValue)
                    return
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name("addParticipantListen"), object: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            if status == ApiCallStatus.DETAIL {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
            
            if status == ApiCallStatus.FAILED {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
}

extension AddParticipantVC: ConfirmParticipantDelegate,ConfirmLiquidParticipantDelegate {
    func proceedToShare(custom: String, custId: Int) {
        print("cust message: ",custom)
        print("cust id: ",custId)
        
        self.proceed(custom: custom, cust_id: custId)
        self.popup?.dismiss()
    }
    
    
    func dismissShare() {
        self.popup?.dismiss()
    }
        
    func proceed() {
        self.popup?.dismiss()
        self.shareCode(codeTextView: referal_codes)
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
}
