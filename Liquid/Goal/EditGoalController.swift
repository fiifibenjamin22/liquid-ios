//
//  EditGoalController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CropViewController
import RealmSwift
class EditGoalController: BaseScrollViewController {
    
    var goal: Goal?
    var goalName = ""
    var goalAmount = "0"
    var goalDueDate = ""
    var isStrictMode = true
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var goalImage: UIImage?
    var imagePicker : UIImagePickerController!
    let actionItemDelate = ActionItem(name: "Delete Goal", action: "DELETED")
    
    let imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        let overlay: UIView = UIView(frame: CGRect(x: 0, y: 0, width: imageView.frame.size.width, height: imageView.frame.size.height))
        overlay.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        imageView.addSubview(overlay)
        
        overlay.anchorWithConstantsToTop(top: imageView.topAnchor, left: imageView.leadingAnchor, bottom: imageView.bottomAnchor, right: imageView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        return imageView
    }()
    
    let pickImageUIButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle(" Change cover photo", for: .normal)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickImage(_:)), for: .touchUpInside)
        button.setImage(UIImage(named: "GroupCamera"), for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 20)!)
        return button
    }()
    
    lazy var goalNameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Goal name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    lazy var amountNeededTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Amount needed (GHS)")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.delegate = self
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
 
    lazy var dateTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.contentHorizontalAlignment = .left
        let color = UIColor.darkText
        button.setTitleColor(color, for: .normal)
        button.addBottomBorder(UIColor.lightGray, height: 1)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.addTarget(self, action: #selector(pickGoalDate(_:)), for: .touchUpInside)
        return button
    }()
    
    let nextOfKinTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "Next of kin"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        textView.textAlignment = .left
        return textView
    }()
    
    lazy var nameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        
        return textField
    }()
    
    lazy var numberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Phone number")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    lazy var relationShipTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Relationship", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addBottomBorder(UIColor.lightGray, height: 1)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickType(_:)), for: .touchUpInside)
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        return button
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(color, for: .normal)
        return button
    }
    
    let goalAssistTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let goalAssistUISwitch: UISwitch = {
        let baseSwitch = ViewControllerHelper.baseUISwitch()
        baseSwitch.addTarget(self, action: #selector(agreeAction(_:)), for: UIControl.Event.touchUpInside)
        return baseSwitch
    }()
    
    lazy var editAssistSettings: UIButton = {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.setTitle("Edit Goal Assist Settings", for: .normal)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.titleLabel?.textAlignment = .left
        button.semanticContentAttribute = .forceLeftToRight
        button.isHidden = true
        
        button.addTarget(self, action: #selector(editGoalAssist), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    
    lazy var deleteButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.red.cgColor
        button.setTitle("Delete", for: .normal)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.titleLabel?.textAlignment = .left
        button.addTarget(self, action: #selector(deleteGoal), for: UIControl.Event.touchUpInside)
        
        //let image = UIImage(named: "Delete")
        //button.setImage(image, for: .normal)
        return button
    }()
    
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    override func setUpView() {
        super.setUpView()
        self.setUpNavigationBarWhite(title: "Edit Goal")
    
        self.addViews()
    }
    
    func addViews()  {
        
        self.scrollView.addSubview(imageView)
        self.scrollView.addSubview(pickImageUIButton)
        self.scrollView.addSubview(goalNameTextField)
        self.scrollView.addSubview(amountNeededTextField)
        self.scrollView.addSubview(dateTextField)
        self.scrollView.addSubview(nextOfKinTextView)
        self.scrollView.addSubview(nameTextField)
        //self.scrollView.addSubview(emailTextField)
        self.scrollView.addSubview(numberTextField)
        self.scrollView.addSubview(relationShipTextField)
        
        self.scrollView.addSubview(goalAssistTextView)
        self.scrollView.addSubview(goalAssistUISwitch)
        self.scrollView.addSubview(editAssistSettings)
        
        
        if goal!.balance == 0 {
            self.scrollView.addSubview(deleteButton)
        }
        
        let width = view.frame.width
        
        self.imageView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.imageView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.imageView.layer.cornerRadius = 15
        
        self.pickImageUIButton.anchorWithConstantsToTop(top: nil, left: imageView.leadingAnchor, bottom: imageView.bottomAnchor, right: imageView.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -8, rightConstant: -100)
        self.pickImageUIButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.goalNameTextField.anchorWithConstantsToTop(top: imageView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.goalNameTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.goalNameTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.amountNeededTextField.anchorWithConstantsToTop(top: goalNameTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.amountNeededTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.amountNeededTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.dateTextField.anchorWithConstantsToTop(top: amountNeededTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.dateTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.dateTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //MARK:- next of kin section
        self.nextOfKinTextView.anchorWithConstantsToTop(top: dateTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 24, bottomConstant: 0, rightConstant: 0)
        self.nextOfKinTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nextOfKinTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //MARK:- name section
        self.nameTextField.anchorWithConstantsToTop(top: nextOfKinTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.nameTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nameTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        /*
        //MARK:- email section
        self.emailTextField.anchorWithConstantsToTop(top: nameTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.emailTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.emailTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        */
        //MARK:- number section
        self.numberTextField.anchorWithConstantsToTop(top: nameTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.numberTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.numberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //MARK:- realtionship section
        self.relationShipTextField.anchorWithConstantsToTop(top: numberTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.relationShipTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.relationShipTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        //Assist section
        self.goalAssistTextView.anchorWithConstantsToTop(top: relationShipTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 24, bottomConstant: -50, rightConstant: 0)
        self.goalAssistTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.goalAssistTextView.widthAnchor.constraint(equalToConstant: width - 120).isActive = true
        
        
        self.goalAssistUISwitch.anchorWithConstantsToTop(top: relationShipTextField.bottomAnchor, left: goalAssistTextView.trailingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 40, leftConstant: 16, bottomConstant: 0, rightConstant: -24)
        self.goalAssistUISwitch.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        if goal!.balance == 0 {
            self.editAssistSettings.anchorWithConstantsToTop(top: goalAssistUISwitch.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
            self.editAssistSettings.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            self.deleteButton.anchorWithConstantsToTop(top: editAssistSettings.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: scrollView.trailingAnchor, topConstant: 30, leftConstant: 16, bottomConstant: -10, rightConstant: -24)
            self.deleteButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        } else {
            self.editAssistSettings.anchorWithConstantsToTop(top: goalAssistUISwitch.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: -10, rightConstant: 0)
            self.editAssistSettings.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
        //KeyboardAvoiding.avoidingView = self.goalAssistTextView
        self.getGoal()
    }
    
    
    //MARK - pick the period type
    @objc func pickType(_ sender: UIButton)  {
        let relationships = GoalNextKin.all()
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        for item in relationships {
            let defaultAction = UIAlertAction(title: "\(item.name)", style: .default) { (action) in
                sender.setTitle(item.name, for: .normal)
                self.relationShipTextField.setTitle(item.name, for: .normal)
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func agreeAction(_ sender: UISwitch){
        self.goal?.assistModeEnabled = sender.isOn
        self.editAssistSettings.isHidden = !sender.isOn
    }
    
    func setNavItem()  {
        let doneAction = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(handleDoneAction))
        navigationItem.rightBarButtonItem = doneAction
    }
    
    @objc func handleDoneAction()  {
        self.goal?.name = self.goalNameTextField.text!
        self.goal?.dueDate = self.goalDueDate
        self.goal?.targetAmount = Double(self.amountNeededTextField.text!)!
        self.goal?.nextOfKinrelationship = self.relationShipTextField.currentTitle!
        //self.goal?.nextKinEmail = self.emailTextField.text!
        self.goal?.nextKinContact = self.numberTextField.text!
        self.goal?.nextOfKinName = self.nameTextField.text!
        
        if self.goalDueDate.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Choose a due date for your goal.")
            return
        }
        
        if  Double(self.goalAmount)! <= 0 {
            ViewControllerHelper.showPrompt(vc: self, message: "Target amount must be more than GHS 0.0.")
            return
        }
        
        
        //print("Has assistant \(self.goal!.hasAssistant) \(self.goalAssistUISwitch.isOn)")
        //if !self.goal!.hasAssistant && self.goalAssistUISwitch.isOn {
        //    self.editGoalAssist()
        //    return
        //}
        
        //MARK: update the next of kin information as well as the assist settings
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.editGoal(goal: self.goal!, image: self.goalImage) { (status,goal,withMessage) in
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showAlert(vc: self, message: "Goal updated successfully", type: MessageType.info)
                UserDefaults.standard.set(true, forKey: Key.createGoal)
                self.updateNextOfKinParams()
            } else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    //MARK: updating next of kin params
    func updateNextOfKinParams()  {
        self.apiService.updateGoalNextOfKin(name: self.nameTextField.text!,mobile:self.numberTextField.text!,relationship:self.relationShipTextField.currentTitle!,goal: self.goal!) { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showAlert(vc: self, message: "Goal updated successfully", type: MessageType.info)
                let vcs = self.navigationController!.viewControllers.dropLast(2)
                self.navigationController?.setViewControllers(Array(vcs), animated: true)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func getGoal() {
        if let goalIn = self.goal {
            viewControllerHelper.showActivityIndicator()
            self.apiService.getGoal(goal: goalIn) { (status, goal, withMessage) in
                self.viewControllerHelper.hideActivityIndicator()
                if let goalRetrievd = goal {
                    goalRetrievd.hasAssistant = self.goal!.hasAssistant
                    self.goal = goalRetrievd
                    self.setNavItem()
                    self.updateUI(goal: goalRetrievd)
                }
            }
        }
    }
    

    func updateUI(goal: Goal)  {
        self.setUpNavigationBarWhite(title: "\(goal.name)")
       
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        
        let messageHeaderGoalAssit = "Goal Assist\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let messageBodyGoalAssist = "Based on your target date, we will calculate a periodic amount to contribute toward  your goal.".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
        
        let result = NSMutableAttributedString()
        result.append(messageHeaderGoalAssit)
        result.append(messageBodyGoalAssist)
        
        self.goalAssistTextView.attributedText = result
        
        self.goalName = goal.name
        self.goalDueDate = goal.dueDate
        self.goalAmount = "\(goal.targetAmount)"
        
        self.amountNeededTextField.text = "\(goal.targetAmount)"
        self.goalNameTextField.text = goal.name
        self.dateTextField.setTitle(goal.dueDate, for: .normal)
        
        self.goalAssistUISwitch.isOn = goal.assistModeEnabled
        self.editAssistSettings.isHidden = !goal.assistModeEnabled
        
        //MARK: next of kin section
        self.nameTextField.text = goal.nextOfKinName
        //self.emailTextField.text = goal.nextKinEmail
        self.relationShipTextField.setTitle(goal.nextOfKinrelationship, for: .normal)
        self.numberTextField.text = goal.nextKinContact
        
        
        if  !(goal.image.isEmpty) {
            self.imageView.af_setImage(
                    withURL: URL(string: (goal.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
            )}else {
            self.imageView.image = Mics.placeHolder()
        }
    }
    
    
    

    //MARK:- pick date of birth instead
    @objc func pickGoalDate(_ sender: UIButton){
        //let sixteenInterval: TimeInterval = 17 * 12 * 4 * 7 * 24 * 60 * 60
        let startingDate = Date()
        
        let datePicker = ActionSheetDatePicker(title: "SELECT DATE", datePickerMode: UIDatePicker.Mode.date, selectedDate: startingDate, doneBlock: {
            picker, value, index in
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy"
            self.goalDueDate = dateFormatterGet.string(from: value! as! Date)
            self.dateTextField.setTitle(self.goalDueDate, for: .normal)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @objc func pickImage(_ sender: UIButton) {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: "Take photo", style: .default) { (action) in
            self.takePhoto()
        }
        
        let profileAction = UIAlertAction(title: "Pick from libary", style: .default) { (action) in
            self.imagePicker = UIImagePickerController()
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = false
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(profileAction)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func takePhoto()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }  else {
            ViewControllerHelper.showPrompt(vc: self, message: "Your device does not support camera options.")
        }
    }
    
    @objc func deleteGoal()  {
        
        let alert = UIAlertController(title: "Delete this goal?", message: "", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Delete", style: .default) { (isDone) in
            self.viewControllerHelper.showActivityIndicator()
            self.apiService.pauseGoal(goal: self.goal!) { (status, goal, withMessage) in
                self.viewControllerHelper.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    UserDefaults.standard.set(true, forKey: Key.createGoal)
                    
                    let realm = try! Realm()
                    try! realm.write() {
                        realm.delete(realm.objects(Goal.self).filter("id=%@",self.goal!.id))
                    }
                    
                    let vcs = self.navigationController!.viewControllers.dropLast(2)
                    self.navigationController?.setViewControllers(Array(vcs), animated: true)
                } else {
                    self.viewControllerHelper.hideActivityIndicator()
                }
            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (isCanceled) in
            
        }
        
        alert.addAction(cancel)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func editGoalAssist()  {
        let destination = NewGoalRuleController()
        destination.goal = self.goal
        destination.goalId = self.goal!.id
        destination.isEditScreen = true
        destination.isEditingAssistant = true
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}

extension EditGoalController: GoalAssistPeriodDelegate {
    func setGoalRule(rule: RulePeriodItem) {
        self.goal?.goalAssistPeriod = rule.title
    }
    
    func setAssistant(hasAssistant: Bool) {
        self.goal?.hasAssistant = hasAssistant
    }
}

extension EditGoalController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[.originalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: .default, image: image)
        cropController.delegate = self
        picker.dismiss(animated: true, completion: {
            self.present(cropController, animated: true, completion: nil)
        })
        
    }
    
    //MARK:- the cropper delegate
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        cropViewController.dismiss(animated: true, completion: nil)
        
        self.goalImage = image
        self.imageView.image = image
        
    }
    
}


extension EditGoalController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
    
}
