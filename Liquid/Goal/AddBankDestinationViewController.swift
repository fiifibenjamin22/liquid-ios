//
//  AddBankDestinationViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/01/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class AddBankDestinationViewController: ControllerWithBack {

    var goal: Goal?
    var image: UIImage?
    
    var bankName = ""
    var accountNumber = ""
    var accountName = ""

    let apiService = ApiService()
    let viewControllerUtil = ViewControllerHelper()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 0
        button.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        return button
    }()
    
    lazy var accountNameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Account Name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        
        let user = User.getUser()
        textField.text = "\(user!.firstName) \(user!.lastName)"
        
        return textField
    }()
    
    lazy var accountNumberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Account Number")
        textField.keyboardType = UIKeyboardType.alphabet
        
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))

        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    lazy var bankTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Select Bank", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addBottomBorder(UIColor.hex(hex: Key.primaryHexCode), height: 1)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickType(_:)), for: .touchUpInside)
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        return button
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(color, for: .normal)
        return button
    }
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        setUpNavigationBar(title: "Add Bank")
        self.addViews()
        
        self.apiService.getGoalWithdrawalDestinationBanks(completion: {(status, id, message) in
            print("Message")
        })
    }
    
    func addViews()  {
        self.view.addSubview(accountNameTextField)
        self.view.addSubview(accountNumberTextField)
        self.view.addSubview(bankTextField)
        self.view.addSubview(nextButton)
        
        
        //MARK:- next of kin section
        self.bankTextField.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.bankTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //MARK:- name section
        self.accountNameTextField.anchorWithConstantsToTop(top: bankTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.accountNameTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //MARK:- number section
        self.accountNumberTextField.anchorWithConstantsToTop(top: accountNameTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.accountNumberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //MARK:- next button
        self.nextButton.anchorWithConstantsToTop(top: nil, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -32, rightConstant: -16)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.accountName = self.accountNameTextField.text!
        self.accountNumber = self.accountNumberTextField.text!
        //self.nextOfKinEmail = self.emailTextField.text!
        
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        if !self.accountName.isEmpty && !self.accountNumber.isEmpty && !self.bankName.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    //MARK - pick the period type
    @objc func pickType(_ sender: UIButton)  {
        let banks = Bank.all()
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT Bank", preferredStyle: .actionSheet)
        for item in banks {
            let defaultAction = UIAlertAction(title: "\(item.name)", style: .default) { (action) in
                sender.setTitle(item.name, for: .normal)
                self.bankTextField.setTitle(item.name, for: .normal)
                self.bankName = item.name
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)

    }
    
    
    //MARK - create goals here
    @objc func save(){
        let withdrawalDestination = WithdrawalDestination(customerId: User.getUser()!.cutomerId, issuer: self.bankName, identifier: self.accountNumber, type: "BANK", name: self.accountName)
        self.viewControllerUtil.showActivityIndicator()
        self.apiService.newGoalDestinationCall(item: withdrawalDestination) { (status, id, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                self.viewControllerUtil.hideActivityIndicator()
                NotificationCenter.default.post(name: NSNotification.Name("loadCoach"), object: self)
                let vcs = self.navigationController!.viewControllers.dropLast(2)
                self.navigationController?.setViewControllers(Array(vcs), animated: true)
            } else {
                self.viewControllerUtil.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    
    //MARK:- reset the buttons
    func resetButton(button:UIButton) {
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
    }
    
    //MARK:- activate buttons
    func activateButton(button:UIButton)  {
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
    }
}
