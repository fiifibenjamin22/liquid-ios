//
//  RiskLevelController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 08/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ContactsUI
import PhoneNumberKit

class RiskLevelController: BaseScrollViewController {
    
    var goal: Goal?
    var riskLevel = ""
    var knowledgeLevel = ""
    var image: UIImage?
    
    var frequency = ""
    var amountToDebit = 0.0
    var paymentMethodId = 0
    var paymentMethodType = ""
    var isAutoDeductionState = false
    
    var nextOfKinName = ""
    var nextOfKinRelationShip = "SPOUSE"
    var nextOfKinEmail = ""
    var nextOfKinContact = ""
    
    let headingTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 0
        button.addTarget(self, action: #selector(createGoal), for: .touchUpInside)
        
        return button
    }()
    
    let knowledgeTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "How deep is your investment knowledge?"
        textView.textAlignment = .center
        return textView
    }()
    
    let knowledgeUIStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.spacing = 15
        return container
    }()
    
    lazy var lowKnowledeButton: UIButton = {
        let button = baseButton(title: "LOW")
        button.addTarget(self, action: #selector(knowledgeButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var mediumKnowledeButton: UIButton = {
        let button = baseButton(title: "MEDIUM")
        button.addTarget(self, action: #selector(knowledgeButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var highKnowledeButton: UIButton = {
        let button = baseButton(title: "HIGH")
        button.addTarget(self, action: #selector(knowledgeButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    let riskTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "How much risk can you tolerate?"
        textView.textAlignment = .center
        return textView
    }()
    
    let riskUIStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.spacing = 15
        return container
    }()
    
    lazy var lowRiskButton: UIButton = {
        let button = baseButton(title: "LOW")
        button.addTarget(self, action: #selector(riskButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var mediumRiskButton: UIButton = {
        let button = baseButton(title: "MEDIUM")
        button.addTarget(self, action: #selector(riskButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var highRiskButton: UIButton = {
        let button = baseButton(title: "HIGH")
        button.addTarget(self, action: #selector(riskButtonClicked(_:)), for: .touchUpInside)
        return button
    }()
    
    let nextOfKinTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "Please provide details of your next of kin"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.textAlignment = .center
        return textView
    }()
    
    lazy var nameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    lazy var numberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Phone number")
        textField.keyboardType = UIKeyboardType.alphabet
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPhoneFromContact)))
        imageView.image = UIImage(named: "AddContact")
        imageView.isUserInteractionEnabled = true
        
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        
        textField.rightView = imageView
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        return textField
    }()
    
    lazy var relationShipTextField: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("SPOUSE", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addBottomBorder(UIColor.lightGray, height: 1)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickType(_:)), for: .touchUpInside)
        
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        return button
    }()
    
    func baseButton(title:String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.darkText
        button.setTitle(title, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(color, for: .normal)
        return button
    }
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let goalIn = self.goal {
            self.setUpNavigationBarWhite(title: "\(goalIn.name)")
        }   else {
            self.setUpNavigationBarWhite(title: "New Goal")
        }
        
        self.addViews()
    }
    
    func addViews()  {
        self.scrollView.addSubview(headingTextView)
        self.scrollView.addSubview(knowledgeTextView)
        self.scrollView.addSubview(knowledgeUIStackView)
        self.scrollView.addSubview(riskTextView)
        self.scrollView.addSubview(riskUIStackView)
        self.scrollView.addSubview(nextOfKinTextView)
        self.scrollView.addSubview(nameTextField)
        //self.scrollView.addSubview(emailTextField)
        self.scrollView.addSubview(numberTextField)
        self.scrollView.addSubview(relationShipTextField)
        self.scrollView.addSubview(nextButton)
        
        let width = view.frame.width
        self.headingTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.headingTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.headingTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        

        //MARK:- investment knowledge section
        self.knowledgeTextView.anchorWithConstantsToTop(top: headingTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.knowledgeTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.knowledgeTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.knowledgeUIStackView.anchorWithConstantsToTop(top: knowledgeTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.knowledgeUIStackView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.knowledgeUIStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.knowledgeUIStackView.addArrangedSubview(lowKnowledeButton)
        self.knowledgeUIStackView.addArrangedSubview(mediumKnowledeButton)
        self.knowledgeUIStackView.addArrangedSubview(highKnowledeButton)
        
        //MARK:- risk section
        self.riskTextView.anchorWithConstantsToTop(top: knowledgeUIStackView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.riskTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.riskTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: 0).isActive = true
        
        self.riskUIStackView.anchorWithConstantsToTop(top: riskTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.riskUIStackView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.riskUIStackView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.riskUIStackView.addArrangedSubview(lowRiskButton)
        self.riskUIStackView.addArrangedSubview(mediumRiskButton)
        self.riskUIStackView.addArrangedSubview(highRiskButton)
        
        //MARK:- next of kin section
        self.nextOfKinTextView.anchorWithConstantsToTop(top: riskUIStackView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.nextOfKinTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nextOfKinTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //MARK:- name section
        self.numberTextField.anchorWithConstantsToTop(top: nextOfKinTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.numberTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.numberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
                
        //MARK:- number section
        self.nameTextField.anchorWithConstantsToTop(top: numberTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.nameTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nameTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //MARK:- realtionship section
        self.relationShipTextField.anchorWithConstantsToTop(top: nameTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.relationShipTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.relationShipTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        
        self.nextButton.anchorWithConstantsToTop(top: relationShipTextField.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: -50, rightConstant: 0)
        self.nextButton.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.updateUI()
    }
    
    func updateUI()  {
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        
        let messageHeader = "Step 2 out of 4.\n".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
        let messageBody = "Let’s personalise your investment".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        
        self.headingTextView.attributedText = result
        self.headingTextView.textAlignment = .center
        
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.nextOfKinName = self.nameTextField.text!
        self.nextOfKinContact = self.numberTextField.text!
        //self.nextOfKinEmail = self.emailTextField.text!
        
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        if !self.nextOfKinName.isEmpty && !self.nextOfKinContact.isEmpty && !self.nextOfKinRelationShip.isEmpty && !self.knowledgeLevel.isEmpty && !self.riskLevel.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    //MARK - pick the period type
    @objc func pickType(_ sender: UIButton)  {
        let relationships = GoalNextKin.all()
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        for item in relationships {
            let defaultAction = UIAlertAction(title: "\(item.name)", style: .default) { (action) in
                sender.setTitle(item.name, for: .normal)
                self.relationShipTextField.setTitle(item.name, for: .normal)
                self.nextOfKinRelationShip = item.name
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    
    //MARK - create goals here
    @objc func createGoal(){
        if self.knowledgeLevel.isEmpty || self.riskLevel.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Indicate you risk and knowledge level first")
            return
        }
        
        if self.nextOfKinName.isEmpty || self.nextOfKinRelationShip.isEmpty || self.nextOfKinContact.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Provide details of your next of kin")
            return
        }
        
        do {
            let phoneNumberKit = PhoneNumberKit()
            _ = try phoneNumberKit.parse(self.nextOfKinContact, withRegion: "GH", ignoreType: true)
        }
        catch {
            ViewControllerHelper.showPrompt(vc: self, message: "Please enter a valid phone number")
            return
        }

        
        let destination = CreateGoalController()
        self.goal?.investmentKnowledge = self.knowledgeLevel
        self.goal?.riskLevel = self.riskLevel
        self.goal?.nextOfKinName = self.nextOfKinName
        self.goal?.nextKinContact = self.nextOfKinContact
        self.goal?.nextKinEmail = self.nextOfKinEmail
        self.goal?.nextOfKinrelationship = self.nextOfKinRelationShip
        destination.goal = self.goal
        destination.image = self.image
        destination.amountToDebit = self.amountToDebit
        destination.frequency = self.frequency
        destination.paymentMethodId = self.paymentMethodId
        destination.paymentMethodType = self.paymentMethodType
        destination.isAutoDeductionState = self.isAutoDeductionState

        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
    //MARK - risk button clicked here
    @objc func knowledgeButtonClicked(_ sender: UIButton){
        self.knowledgeLevel = sender.currentTitle!
        
        if sender == lowKnowledeButton {
            self.activateButton(button: lowKnowledeButton)
            self.resetButton(button: mediumKnowledeButton)
            self.resetButton(button: highKnowledeButton)
        }
        if sender == mediumKnowledeButton {
            self.activateButton(button: mediumKnowledeButton)
            self.resetButton(button: lowKnowledeButton)
            self.resetButton(button: highKnowledeButton)
        }
        if sender == highKnowledeButton {
            self.activateButton(button: highKnowledeButton)
            self.resetButton(button: lowKnowledeButton)
            self.resetButton(button: mediumKnowledeButton)
        }
        
        self.changeButtonStatus()
    }
    
    //MARK - risk button clicked here
    @objc func riskButtonClicked(_ sender: UIButton){
        self.riskLevel = sender.currentTitle!
        if sender == lowRiskButton {
            self.activateButton(button: lowRiskButton)
            self.resetButton(button: mediumRiskButton)
            self.resetButton(button: highRiskButton)
        }
        if sender == mediumRiskButton {
            self.activateButton(button: mediumRiskButton)
            self.resetButton(button: lowRiskButton)
            self.resetButton(button: highRiskButton)
        }
        if sender == highRiskButton {
            self.activateButton(button: highRiskButton)
            self.resetButton(button: lowRiskButton)
            self.resetButton(button: mediumRiskButton)
        }
        
        self.changeButtonStatus()
    }
    
    //MARK:- reset the buttons
    func resetButton(button:UIButton) {
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
    }
    
    //MARK:- activate buttons
    func activateButton(button:UIButton)  {
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        button.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
    }
    
    @objc func selectPhoneFromContact(sender: UITapGestureRecognizer){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.predicateForEnablingContact = NSPredicate(format:"phoneNumbers.@count > 0",argumentArray: nil)
        contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'", argumentArray: nil)
        self.present(contactPicker, animated: true, completion: nil)
    }
}

extension RiskLevelController : CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let contactName = CNContactFormatter.string(from: contact, style: .fullName) ?? ""
        for number in contact.phoneNumbers {
            let number = (number.value).value(forKey: "digits") as? String
            if let numberReal = number {
                self.numberTextField.text = numberReal
                self.nameTextField.text = contactName
            }
        }
        picker.dismiss(animated: true, completion: nil)
        self.textFieldDidChange(textField: self.numberTextField)
    }
    
}

