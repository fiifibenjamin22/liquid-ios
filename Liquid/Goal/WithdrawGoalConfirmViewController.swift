//
//  WithdrawGoalConfirmViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 21/02/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import SwiftyJSON

class WithdrawGoalConfirmViewController: BaseScrollViewController {
    
    var amount = "0"
    let walletOptions = WalletSelectOptionView()
    let apiService = ApiService()
    let utilViewController = ViewControllerHelper()
    var code = ""
    var cvv = ""
    var transactionRef = ""
    
    var goal: Goal?
    var isGoalAddFunds = false
    var isGoalWithdrawFunds = false
    
    let user = User.getUser()!
    var paymentMethod = PaymentMethodTypes.FUND_ACCOUNT.rawValue
    var targetAccountType = TargetIssuer.LIQUID_ACCOUNT.rawValue
    var targetAccount = User.getUser()!.phone
    
    var popup: PopupDialog?
    
    let recipientView: RecipientView = {
        let recipientUIView = RecipientView()
        recipientUIView.translatesAutoresizingMaskIntoConstraints = false
        return recipientUIView
    }()
    
    lazy var amountCardView: AmountCardView = {
        let amountView = AmountCardView()
        amountView.amount = self.amount
        amountView.translatesAutoresizingMaskIntoConstraints = false
        amountView.payWithLable.text = "Withdraw From"
        return amountView
    }()
    
    let tipTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "TRANSACTION DETAILS"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(completeLoading), for: .touchUpInside)
        return button
    }()
    
    lazy var backButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "BackArrow"), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        //MARK:- withd
        setUpNavigationBarWhite(title: "Confirm Withdrawal")
        self.setUpWithDrawalStuffs()
        
        scrollView.addSubview(tipTextView)
        scrollView.addSubview(amountCardView)
        scrollView.addSubview(recipientView)
        view.addSubview(nextButton)
        view.addSubview(backButton)
        
        let width = self.view.frame.width
        
        self.recipientView.anchorToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil)
        self.recipientView.heightAnchor.constraint(equalToConstant: CGFloat(ConstantHeight.RECIPIENT_CARD_HEIGHT.rawValue)).isActive = true
        self.recipientView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- set the recipient information
        self.recipientView.goal = self.goal
        
        self.tipTextView.anchorToTop(top: recipientView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor)
        self.tipTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.tipTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.amountCardView.anchorWithConstantsToTop(top: tipTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right:nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.amountCardView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.amountCardView.heightAnchor.constraint(equalToConstant: CGFloat(ConstantHeight.WALLET_CARD_HEIGHT.rawValue)).isActive = true
        
        self.backButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: backButton.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.amountCardView.updateAmount(amount: self.amount)
    }
    
    func setUpWithDrawalStuffs()  {
        self.amountCardView.walletPickerButton.isHidden = true
        self.amountCardView.updateGoalInfo(goal: self.goal!)
        
        self.nextButton.isEnabled = true
        self.nextButton.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        
        self.initTransaction()
    }
    
    func initTransaction(){
        //MARK: go to the amount section
        
        let goalWallet = Wallet()
        if self.isGoalWithdrawFunds {
            paymentMethod = PaymentMethodTypes.WITHDRAW_CASH.rawValue
            targetAccountType = TargetIssuer.CASH_WITHDRAWAL.rawValue
            targetAccount = "\(user.accountId)"
            
            goalWallet.paymentType = "WITHDRAW_CASH"
            goalWallet.number = "\(self.goal!.id)"
        }
        
        self.utilViewController.showActivityIndicator()
        print("Wallet type: \(goalWallet.type) \(goalWallet.paymentType)")
        self.apiService.initTransaction(amount: Double(self.amount)!, methodType: paymentMethod, reference: "", sourceWallet: goalWallet, issuerToken: "", targetType: targetAccountType, targetAccount: "-1", theGoal: goal!) { (status, confirmation, message) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if let confirmationIn = confirmation {
                    let amountString = String(format: "%.2f", confirmationIn.amount)
                    let feeString = String(format: "%.2f", confirmationIn.fees)
                    let totalString = String(format: "%.2f", Double(self.amount)!.rounded(toPlaces: 2))
                    let displayMessage =  "\(confirmationIn.currency) \(amountString)\n\(confirmationIn.currency) \(feeString)"
                    self.amountCardView.amountFee.text = displayMessage
                    self.amountCardView.totalamountFee.text = "\(confirmationIn.currency) \(totalString)"
                    
                    self.transactionRef = confirmationIn.reference
                    
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    @objc func completeLoading(){
        self.confirmTransaction()
    }
    
    func confirmTransaction(){
        //MARK: go to the amount section
        
        self.utilViewController.showActivityIndicator()
        self.apiService.withdrawFromGoal(amount: Double(self.amount)!, goal: self.goal!, reference: self.transactionRef) { (response, status, message) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.navigationController?.setViewControllers(Array((self.navigationController?.viewControllers.dropLast(2))!), animated: true)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    @objc func backAction(){
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
