//
//  ConfirmDepositWalletViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/01/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
protocol ConfirmDepositWalletDelegate: AnyObject {
    func proceed(destination: GoalDestination)
    func dismiss()
}

class ConfirmDepositWalletViewController: UIViewController {
    var delegate: ConfirmDepositWalletDelegate?
    var destination: GoalDestination?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let view = self.view.viewWithTag(100)
        
        let iv = view?.viewWithTag(1) as! UIImageView
        let issuerParts = self.destination!.issuer.split(separator: " ")
        iv.image = Mics.walletIconFromIssuer(name: String(issuerParts[0]))
        let destinationDetails = view?.viewWithTag(2) as! UILabel
        destinationDetails.text = self.destination!.issuer
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.delegate?.dismiss()
    }
    
    @IBAction func `continue`(_ sender: Any) {
        self.delegate?.proceed(destination: self.destination!)
    }

}
