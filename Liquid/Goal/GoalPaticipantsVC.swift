//
//  GoalPaticipantsVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 02/08/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalPaticipantsVC: UITableViewController,UISearchBarDelegate {
    
    let CellId = "CellId"
    var participant = [Participants]()
    var selectedArr = [Participants]()
    var goalId = 0
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    var participantNumbers = [String]()
    
    var indexPaths :IndexPath?
    
    lazy var searchBar = UISearchBar(frame: .zero)

    lazy var addParticipantBtn: UIButton = {
        let button = ViewControllerHelper.baseRoudImageButton()
        button.setTitle("+ Add Participant", for: .normal)
        button.addTarget(self, action: #selector(gotoAddParticipant), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.backgroundColor = .white
        self.tableView.register(ParticipantsCell.self, forCellReuseIdentifier: CellId)
        self.tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0, bottom: -70, right: 0)
        self.tableView.separatorStyle = .none
        //self.tableView.allowsSelection = true
        //self.tableView.isEditing = true
        self.tableView.allowsMultipleSelection = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
        
        searchBar.tintColor = UIColor.white
        UITextField.appearance(whenContainedInInstancesOf: [type(of: searchBar)]).tintColor = .black
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for:UIBarMetrics.default)
        
        searchBar.placeholder = "Search"
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        
        self.allParticipants()
                
        self.view.addSubview(addParticipantBtn)
        self.addParticipantBtn.anchorWithConstantsToTop(top: nil, left: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.addParticipantBtn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.addParticipantBtn.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData(_:)), name: NSNotification.Name(rawValue: "addParticipantListen"), object: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchBar.becomeFirstResponder()

        let contents = self.participant.filter({
            $0.name.lowercased().contains(searchText.lowercased())
        })

        print("print search found: ",contents)

        if searchText.count < 2 {
            self.allParticipants()
        }else{
            if contents.isEmpty {
                ViewControllerHelper.showPrompt(vc: self, message: "no participants matched the search term") { (isNon) in
                    self.allParticipants()
                }
            }else{
                self.participant = contents
            }
        }

        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //search api call
        searchBar.resignFirstResponder()
    }
}

extension GoalPaticipantsVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.participant.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellId, for: indexPath) as! ParticipantsCell
        cell.selectionStyle = .default
        cell.item = self.participant[indexPath.row]
        self.participantNumbers.append(cell.item!.phone)
        cell.toggleDelegate = self
        cell.index = indexPath
        self.indexPaths = indexPath
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        selectDeselectCell(tableView: tableView, indexPath: indexPath)
        print("select")
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        selectDeselectCell(tableView: tableView, indexPath: indexPath)
        print("deselect")
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let alert = UIAlertController(title: "Action", message: "Are you sure you want to remove \(self.participant[indexPath.row].name) from this goal?'", preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default) { (isFinished) in
                self.viewControllerHelper.showActivityIndicator()
                let participantId = self.participant[indexPath.row].id
                self.apiService.removeParticipant(goal_id: self.goalId, customer_id: participantId) { (status, item, message) in
                    self.viewControllerHelper.hideActivityIndicator()
                    if status == ApiCallStatus.SUCCESS {
                        self.participant.remove(at: indexPath.row)
                        tableView.deleteRows(at: [indexPath], with: .fade)
                        if self.participant.count < 1 {
                            NotificationCenter.default.post(name: NSNotification.Name("addParticipantListen"), object: self)
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        ViewControllerHelper.showPrompt(vc: self, message: message)
                    }
                }

            }
            let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(yes)
            alert.addAction(cancel)
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @objc func gotoAddParticipant(){
        let vc = AddParticipantVC()
        vc.goalId = self.goalId
        vc.participantNumbers = self.participantNumbers
        self.navigationController?.pushFade(vc)
    }
    
    @objc func reloadData(_ notification: Notification){
        self.allParticipants()
    }
    
    func allParticipants(){
        viewControllerHelper.showActivityIndicator()
        self.apiService.getAllParticipants(goalId: self.goalId) { (status, participants, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.participant.removeAll()
                self.participant.append(contentsOf: participants!.reversed())
                //self.localData()
                self.tableView.reloadData()
            }
        }
    }
    
    func localData() {
        self.participant.removeAll()
        let parts = Participants.all()
        
        parts.forEach { (person) in
            print("all parts: ",person)
            self.participant.append(person)
        }
    }
}

extension GoalPaticipantsVC: toggleBtnDelegate {
    func onClick(index: Int, state: Bool) {
        print("print delete participant")
        print(self.participant[index])
        print(state)
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.toggleSharing(goal_id: self.goalId, customer_id: self.participant[index].id, sharing_enabled: state) { (status, response, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                print(response)
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    func selectDeselectCell(tableView: UITableView, indexPath: IndexPath){
        self.selectedArr.removeAll()
        if let arr = tableView.indexPathsForSelectedRows {
            print(arr)
            for index in arr {
                selectedArr.append(participant[index.row])
            }
        }
        print(selectedArr)
        if selectedArr.count > 0 {
            let deleteImg = UIImage(named: "Close")?.withRenderingMode(.alwaysTemplate)
            //deleteImg?.withTintColor(.white)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: deleteImg, style: .plain, target: self, action: #selector(handleMultipleDelete))
        }else{
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func handleMultipleDelete(){
        
        if let selectedRows = tableView.indexPathsForSelectedRows {
            
            print(selectedRows.count)
            print(self.participant.count)
            
            for item in selectedArr {
                if let index = selectedArr.firstIndex(of: item){
                    
                    self.apiService.removeParticipant(goal_id: self.goalId, customer_id: self.participant[index].id) { (status, item, message) in
                        self.viewControllerHelper.hideActivityIndicator()
                        if status == ApiCallStatus.SUCCESS {

                            self.participant.remove(at: index)
                            self.tableView.beginUpdates()
                            self.tableView.deleteRows(at: selectedRows, with: .automatic)
                            self.tableView.endUpdates()
                            
                            //self.participant.remove(at: indexes)
                            //if self.participant.count < 1 {
                            //    NotificationCenter.default.post(name: NSNotification.Name("addParticipantListen"), object: self)
                            //    self.navigationController?.popViewController(animated: true)
                            //}
                            
                        }else{
                            ViewControllerHelper.showPrompt(vc: self, message: message)
                        }
                    }
                    
                }
            }
        }
    }
    
    func deleteParticipant(customerid: Int, indexes: Int) throws {
        self.apiService.removeParticipant(goal_id: self.goalId, customer_id: customerid) { (status, item, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                //self.participant.remove(at: indexes)
                //if self.participant.count < 1 {
                //    NotificationCenter.default.post(name: NSNotification.Name("addParticipantListen"), object: self)
                //    self.navigationController?.popViewController(animated: true)
                //}
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
}
