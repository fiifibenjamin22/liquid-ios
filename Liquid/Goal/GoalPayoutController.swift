//
//  GoalPayoutController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 26/07/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
class GoalPayoutController: ControllerWithBack {
    
    var goal: Goal?
    var image: UIImage?
    let cellHeight = 50
    let collectionId = "collectionId"
    var walletes = GoalDestination.all()
    let user = User.getUser()!
    var popup: PopupDialog?
    
    var frequency = ""
    var amountToDebit = 0.0
    var paymentMethodId = 0
    var paymentMethodType = ""
    var isAutoDeductionState = false
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 20)!)
        textView.text = "Add an Investment Payout account"
        return textView
    }()
    
    lazy var newPaymentButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("New Investment payout account", for: .normal)
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.setTitleColor(color, for: .normal)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleNewWallet), for: .touchUpInside)
        button.layer.borderColor = color.cgColor
        return button
    }()
    
    let payoutCautionTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.gray
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 15)!)
        textView.text = "Please note that this acccount will be used when withdrawing your funds and can't be changed"
        return textView
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        return collectionIn
    }()
    
    
    override func viewDidLoad() {
        
        if let goalIn = self.goal {
            self.setUpNavigationBar(title: "\(goalIn.name)")
        }   else {
            self.setUpNavigationBar(title: "New Goal")
        }
    
        self.view.addSubview(messageTextView)
        self.view.addSubview(newPaymentButton)
        self.view.addSubview(payoutCautionTextView)
        self.view.addSubview(collectionView)
     
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.newPaymentButton.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.newPaymentButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.payoutCautionTextView.anchorWithConstantsToTop(top: newPaymentButton.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.payoutCautionTextView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        self.collectionView.anchorWithConstantsToTop(top: payoutCautionTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(WalletCell.self, forCellWithReuseIdentifier: collectionId)
        
        self.loadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadData()
    }
    
    func loadData()  {
        self.walletes.removeAll()
        self.walletes.append(contentsOf: GoalDestination.all())
        self.collectionView.reloadData()
    }

    
    @objc func handleNewWallet() {
       let destination = NewPaymentOptionController()
       destination.isGoalDestination = true
       self.navigationController?.pushViewController(destination, animated: true)
    }
    
}

extension GoalPayoutController:  UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return walletes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = walletes[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! WalletCell
        cell.goalDestination = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: CGFloat(cellHeight))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let confirm_vc = ConfirmDepositWalletViewController(nibName: "ConfirmWithdrawWalletView", bundle: nil)
        confirm_vc.delegate = self
        confirm_vc.destination = self.walletes[indexPath.row]
        self.popup = PopupDialog(viewController: confirm_vc)
        self.present(self.popup!, animated: true, completion: nil)
    }
}


extension GoalPayoutController: ConfirmDepositWalletDelegate {
    func proceed(destination: GoalDestination) {
        self.popup?.dismiss()
        let nextVc = RiskLevelController()
        self.goal?.withdrawalDestinationId = destination.id
        nextVc.goal = self.goal
        nextVc.image = self.image
        nextVc.amountToDebit = self.amountToDebit
        nextVc.frequency = self.frequency
        nextVc.paymentMethodId = self.paymentMethodId
        nextVc.paymentMethodType = self.paymentMethodType
        nextVc.isAutoDeductionState = self.isAutoDeductionState
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
}
