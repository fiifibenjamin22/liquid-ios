//
//  ParticipantsCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 02/08/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol toggleBtnDelegate {
    func onClick(index: Int, state: Bool)
    //func toggleState(stateChange: String)
}

class ParticipantsCell: BaseTableCell {
    
    var toggleDelegate: toggleBtnDelegate?
    var index: IndexPath?
    var state: String?
    
    var item: Participants? {
        didSet {
            
            if  !((item?.picture.isEmpty)!) {
                self.userImage.af_setImage(
                    withURL: URL(string: (item!.picture))!,
                    placeholderImage: Mics.userPlaceHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.userImage.image = Mics.userPlaceHolder()
            }
            
            self.name.text = item?.name
            self.dateJoined.text = "GHS \(item?.total_contribution ?? 0.0)"
            
            if item?.status == "INVITED" {
                self.amountContributed.text = item?.status
                self.switchKnob.isHidden = true
                self.switchState.isHidden = true
            }else{
                if item?.status == "ACTIVE" {
                    self.amountContributed.isHidden = true
                    self.switchKnob.isHidden = false
                    self.switchKnob.isOn = true
                    self.switchState.text = "Enabled"
                    self.switchState.isHidden = false
                }else{
                    self.amountContributed.isHidden = true
                    self.switchKnob.isHidden = false
                    self.switchKnob.isOn = false
                    self.switchState.text = "Disabled"
                    self.switchState.isHidden = false
                }
            }
            
        }
    }
    
    lazy var switchKnob: UISwitch = {
        let img = ViewControllerHelper.baseUISwitch()
        img.isOn = false
        img.addTarget(self, action: #selector(handleSwitch), for: .allTouchEvents)
        return img
    }()
    
    let switchState: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Enabled"
        lbl.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        return lbl
    }()
    
    let userImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.layer.cornerRadius = 20.0
        img.backgroundColor = .black
        img.layer.masksToBounds = true
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let name: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Name"
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        return lbl
    }()
    
    let dateJoined: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "GH¢ 0.00"
        lbl.textColor = .lightGray
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
        return lbl
    }()
    
    let amountContributed: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Invited"
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        return lbl
    }()
    
    let seperator: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func setUpView() {
        super.setUpView()
        self.selectionStyle = .default
        
        self.addSubview(switchKnob)
        //self.addSubview(switchState)
        self.addSubview(userImage)
        self.addSubview(name)
        self.addSubview(dateJoined)
        self.addSubview(amountContributed)
        self.addSubview(seperator)
        
        userImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        userImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        userImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
        userImage.heightAnchor.constraint(equalToConstant: 40).isActive = true
                
        //userImage.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        name.anchor(amountContributed.topAnchor, left: userImage.rightAnchor, bottom: amountContributed.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 70, widthConstant: 0, heightConstant: 0)
        
        dateJoined.anchor(name.bottomAnchor, left: name.leftAnchor, bottom: nil, right: name.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        amountContributed.anchor(userImage.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 16.0, widthConstant: 70.0, heightConstant: 30.0)
        
        switchKnob.anchor(amountContributed.topAnchor, left: name.rightAnchor, bottom: amountContributed.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        
        //switchState.anchor(switchKnob.topAnchor, left: switchKnob.rightAnchor, bottom: switchKnob.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 3, bottomConstant: 0, rightConstant: 4, widthConstant: 0, heightConstant: 0)
        
        seperator.anchor(nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 1.0)
    }
    
    @objc func handleSwitch(_ sender: UISwitch){
        toggleDelegate?.onClick(index: (index?.row)!, state: sender.isOn)
        //toggleDelegate?.toggleState()
        if sender.isOn {
            self.switchState.text = "Enabled"
        }else{
            self.switchState.text = "Disabled"
        }
    }
}
