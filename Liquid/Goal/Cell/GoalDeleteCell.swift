//
//  GoalDeleteCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/07/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalDeleteCell: BaseCell {
    
    var item: ActionItem? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = "\(unwrapedItem.name)"
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let deleteImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "Delete")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let topSplitView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bottomSplitView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(topSplitView)
        addSubview(messageTextView)
        addSubview(deleteImageView)
        addSubview(bottomSplitView)
        
        self.topSplitView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.topSplitView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        self.deleteImageView.anchorWithConstantsToTop(top: topSplitView.bottomAnchor, left: leadingAnchor, bottom: bottomSplitView.topAnchor, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: -16, rightConstant: 0)
        self.deleteImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: topSplitView.bottomAnchor, left: self.deleteImageView.trailingAnchor, bottom: bottomSplitView.topAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
       
        self.bottomSplitView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.bottomSplitView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
    }
}
