//
//  GoalDetailCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/01/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol detailHeaderDelegate {
    func onClick(index: Int)
}

class GoalDetailCell: BaseCell {
    let regularFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 16)!)
    let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
    
    var earningAmountheightAnchor: NSLayoutConstraint?
    var earningTextheightAnchor: NSLayoutConstraint?
    var cellDelegate: detailHeaderDelegate?
    var index: IndexPath?
    var isGoalOwner = false
    
    var item: Goal? {
        didSet {
            guard let unwrapedItem = item else { return }
            
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            currencyFormatter.locale = Locale(identifier: "ha_Latn_GH")
            let thebalance = currencyFormatter.string(from: NSNumber(value: unwrapedItem.balance.rounded(toPlaces: 2)))!.dropFirst(3)
            let theTargetAmount = currencyFormatter.string(from: NSNumber(value: unwrapedItem.targetAmount))!.dropFirst(3)
            let theInvestedAmount = currencyFormatter.string(from: NSNumber(value: unwrapedItem.invested.rounded(toPlaces: 2)))!.dropFirst(3)
            let theDepositeAmount = currencyFormatter.string(from: NSNumber(value: unwrapedItem.total_deposits))!.dropFirst(3)
            let theWithdrwalAmount = currencyFormatter.string(from: NSNumber(value: unwrapedItem.total_withdrawals))!.dropFirst(3)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let dueDate = formatter.date(from: unwrapedItem.dueDate)
            formatter.dateFormat = "MMM dd, yyyy"
            
            self.contentTextView.attributedText = "Due Date: \(formatter.string(from: dueDate!))".formatAsAttributed(font: regularFont, color: UIColor.white)
            
            let ofText = " of ".formatAsAttributed(font: regularFont, color: UIColor.darkText)
            let currency = "\(unwrapedItem.currency) ".formatAsAttributed(font: regularFont, color: UIColor.darkText)
            let balance = "\(thebalance)".formatAsAttributed(font: regularFont, color: UIColor.darkText)
            let targetAmount = "\(theTargetAmount)".formatAsAttributed(font: regularFont, color: UIColor.darkText)
            
            let targetString = NSMutableAttributedString()
            targetString.append(currency)
            targetString.append(balance)
            targetString.append(ofText)
            targetString.append(targetAmount)
            self.totalTextView.attributedText = targetString
            
            let percent = unwrapedItem.balance/unwrapedItem.targetAmount
            
            //Earning = Invested + Withdrawals - Deposits
            
            self.amountTextView.attributedText = "\((percent * 100).rounded(toPlaces: 2))% complete".formatAsAttributed(font: regularFont, color: UIColor.darkText)
            self.progressUIView.setProgress(Float(percent), animated: true)
            
            let totalInvested = NSMutableAttributedString()
            //totalInvested.append("\(unwrapedItem.currency) ".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "333333")))
            investedCurrenceyLbl.attributedText = "\(unwrapedItem.currency)".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "333333"))
            totalInvested.append("\(theInvestedAmount)".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "333333")))
            
            self.totalInvestedTextView.attributedText = totalInvested
            
            self.investedTextView.attributedText = "Invested".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "333333"))
            
            self.totalDepositsTextView.attributedText = "Total Deposits".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: Key.primaryHexCode))
            
            self.totalDepositsAmountTextView.attributedText = "\(theDepositeAmount)".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: Key.primaryHexCode))
            
            self.depositeCurrenceyLbl.attributedText = "\(unwrapedItem.currency)".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: Key.primaryHexCode))
            
            let totalEarnings = unwrapedItem.invested + unwrapedItem.total_withdrawals - unwrapedItem.total_deposits
            let theEarningsAmount = currencyFormatter.string(from: NSNumber(value: totalEarnings.rounded(toPlaces: 2)))!.dropFirst(3)
            
            if totalEarnings < 0 {
                self.earningAmountheightAnchor?.constant = 0
                self.earningTextheightAnchor?.constant = 0
                self.earningAmountheightAnchor?.isActive = true
                self.earningTextheightAnchor?.isActive = true
            }else{
                self.earningAmountheightAnchor?.isActive = true
                self.earningTextheightAnchor?.isActive = true
                self.earningsTextView.attributedText = "Earnings".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "#4b721f"))
                self.earningsAmountTextView.attributedText = "\(theEarningsAmount)".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "#4b721f"))
                self.earningsCurrenceyLbl.attributedText = "\(unwrapedItem.currency)".formatAsAttributed(font: regularFont, color: UIColor.hex(hex: "#4b721f"))
            }
            
            self.totalWithdrawalsTextView.attributedText = "Total Withdrawals".formatAsAttributed(font: regularFont, color: UIColor.red)
            self.totalWithdrawalsAmountTextView.attributedText = "\(theWithdrwalAmount)".formatAsAttributed(font: regularFont, color: UIColor.red)
            self.withdrawalCurrenceyLbl.attributedText = "\(unwrapedItem.currency)".formatAsAttributed(font: regularFont, color: UIColor.red)
            
            if  !(unwrapedItem.image.isEmpty) {
                self.bgView.af_setImage(
                    withURL: URL(string: (unwrapedItem.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )
                
            } else {
                self.bgView.image = Mics.placeHolder()
            }
            
            //show participants logic
            if  !(unwrapedItem.owner_picture.isEmpty) {
                self.ownerImage.af_setImage(
                    withURL: URL(string: (unwrapedItem.owner_picture))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )
                
            } else {
                self.ownerImage.image = Mics.placeHolder()
            }
            self.ownerName.text = unwrapedItem.owner_name
            
            //part one
            if  !(unwrapedItem.image1.isEmpty) {
                self.participantsOne.af_setImage(
                    withURL: URL(string: (unwrapedItem.image1))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )
                
            } else {
                self.participantsOne.image = Mics.placeHolder()
            }
            
            //Part two
            if  !(unwrapedItem.image2.isEmpty) {
                self.participantsTwo.af_setImage(
                    withURL: URL(string: (unwrapedItem.image2))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )
                
            } else {
                self.participantsTwo.image = Mics.placeHolder()
            }
            
            //part three
            if  !(unwrapedItem.image3.isEmpty) {
                self.participantsThree.af_setImage(
                    withURL: URL(string: (unwrapedItem.image3))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )
                
            } else {
                self.participantsThree.image = Mics.placeHolder()
            }
            
            if unwrapedItem.is_shared {
                self.participantParentView.isHidden = false
                
                if isGoalOwner {
                    self.ownerName.isHidden = true
                    self.ownerStatus.isHidden = true
                    self.ownerImage.isHidden = false
                    
                    if unwrapedItem.total_participants == 1 {
                        self.parentViewWidthContraint?.constant = 55
                        self.ownerImage.isHidden = false
                        self.participantsOne.isHidden = false
                        
                        self.participantsTwo.isHidden = true
                        self.participantsThree.isHidden = true
                        self.plusOne.isHidden = true
                    }else if unwrapedItem.total_participants == 2 {
                        self.parentViewWidthContraint?.constant = 73
                        self.ownerImage.isHidden = false
                        self.participantsThree.isHidden = true
                        self.plusOne.isHidden = true
                    }else if unwrapedItem.total_participants == 3 {
                        self.parentViewWidthContraint?.constant = 80
                        self.ownerImage.isHidden = false
                        self.plusOne.isHidden = true
                    }else if unwrapedItem.total_participants > 3 {
                        self.ownerImage.isHidden = false
                        self.plusOne.isHidden = false
                    }
                }else{
                    self.ownerName.isHidden = false
                    self.ownerStatus.isHidden = false
                    
                    self.plusOne.isHidden = true
                    self.participantsOne.isHidden = true
                    self.participantsTwo.isHidden = true
                    self.participantsThree.isHidden = true
                }
            }else{
                self.participantParentView.isHidden = true
            }

        }
    }
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
        textView.textAlignment = .right
        return textView
    }()
    
    let progressUIView: UIProgressView = {
        let progressBar = ViewControllerHelper.baseProgressBar()
        progressBar.tintColor = UIColor.hex(hex: "6DDCF1")
        progressBar.layer.borderColor = UIColor.hex(hex: "6DDCF1").cgColor
        progressBar.layer.borderWidth = 2.0
        progressBar.progress = Float(1)
        progressBar.layer.cornerRadius = 5
        progressBar.backgroundColor = .white
        return progressBar
    }()
    
    let bgView: ImageCardView = {
        let view = ImageCardView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.shadowOffsetHeight = 2
        view.translatesAutoresizingMaskIntoConstraints = true
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        
        let overlay: UIView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        overlay.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        view.addSubview(overlay)
        
        overlay.anchorWithConstantsToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        return view
    }()
    
    let participantParentView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    //owner Data
    let ownerImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let ownerName: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Mensah"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        return lbl
    }()
    
    let ownerStatus: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Owner"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        return lbl
    }()
    
    //Particitpants info
    let participantsOne: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let participantsTwo: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let participantsThree: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let plusOne: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "+1"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        return lbl
    }()
    //currency label
    let depositeCurrenceyLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()

    let earningsCurrenceyLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()

    let withdrawalCurrenceyLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()

    let investedCurrenceyLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()

    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.textAlignment = .left
        return textView
    }()
    
    let totalTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        return textView
    }()
    
    //investment label
    let investedTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .left
        return textView
    }()
    
    //investment amount
    let totalInvestedTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()
    
    //earnings label
    let earningsTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .left
        return textView
    }()
    
    //investment amount
    let earningsAmountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()
    
    //deposit label
    let totalDepositsTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .left
        return textView
    }()
    
    //deposit amount
    let totalDepositsAmountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()
    
    //withdrawal label
    let totalWithdrawalsTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .left
        return textView
    }()
    
    //withdrawal amount
    let totalWithdrawalsAmountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: "4b721f")
        textView.textAlignment = .right
        return textView
    }()
    
    var parentViewWidthContraint: NSLayoutConstraint?
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(bgView)
        
        self.bgView.addSubview(participantParentView)
        self.participantParentView.addSubview(ownerImage)
        self.participantParentView.addSubview(ownerName)
        self.participantParentView.addSubview(ownerStatus)
        
        self.participantParentView.addSubview(participantsOne)
        self.participantParentView.addSubview(participantsTwo)
        self.participantParentView.addSubview(participantsThree)
        self.participantParentView.addSubview(plusOne)
        
        self.addSubview(progressUIView)
        self.addSubview(amountTextView)
        self.addSubview(totalTextView)
        self.addSubview(investedTextView)
        
        self.addSubview(earningsTextView)
        self.addSubview(earningsAmountTextView)
        
        self.addSubview(depositeCurrenceyLbl)
        self.addSubview(earningsCurrenceyLbl)
        self.addSubview(withdrawalCurrenceyLbl)
        self.addSubview(investedCurrenceyLbl)
        
        self.addSubview(totalInvestedTextView)
        self.addSubview(totalDepositsTextView)
        self.addSubview(totalDepositsAmountTextView)
        self.addSubview(totalWithdrawalsTextView)
        self.addSubview(totalWithdrawalsAmountTextView)
        
        self.bgView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.bgView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.participantParentView.layer.cornerRadius = 20
        self.participantParentView.layer.masksToBounds = true
        self.participantParentView.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 16).isActive = true
        self.participantParentView.rightAnchor.constraint(equalTo: bgView.rightAnchor, constant: -16).isActive = true
        self.participantParentView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.parentViewWidthContraint = participantParentView.widthAnchor.constraint(equalToConstant: 110)
        self.parentViewWidthContraint?.isActive = true
        
        self.ownerImage.layer.cornerRadius = 15
        self.ownerImage.layer.masksToBounds = true
        self.ownerImage.anchor(participantParentView.topAnchor, left: participantParentView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 6, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        self.ownerName.anchor(ownerImage.topAnchor, left: ownerImage.rightAnchor, bottom: nil, right: participantParentView.rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        self.ownerStatus.anchor(ownerName.bottomAnchor, left: ownerName.leftAnchor, bottom: ownerImage.bottomAnchor, right: ownerName.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.participantsOne.layer.cornerRadius = 15
        self.participantsOne.layer.masksToBounds = true
        self.participantsOne.anchor(ownerImage.topAnchor, left: ownerImage.leftAnchor, bottom: ownerImage.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 0)
        
        self.participantsTwo.layer.cornerRadius = 15
        self.participantsTwo.layer.masksToBounds = true
        self.participantsTwo.anchor(participantsOne.topAnchor, left: participantsOne.leftAnchor, bottom: participantsOne.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 0)
        
        self.participantsThree.layer.cornerRadius = 15
        self.participantsThree.layer.masksToBounds = true
        self.participantsThree.anchor(participantsTwo.topAnchor, left: participantsTwo.leftAnchor, bottom: participantsTwo.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 0)
        
        self.plusOne.anchor(participantsThree.topAnchor, left: participantsThree.rightAnchor, bottom: participantsThree.bottomAnchor, right: nil, topConstant: 0, leftConstant: 3, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        //MARK:- progress section
        self.progressUIView.anchorWithConstantsToTop(top: bgView.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.progressUIView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        //MARK:- amount section
        self.amountTextView.anchorWithConstantsToTop(top: progressUIView.bottomAnchor, left: progressUIView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.amountTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.amountTextView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        self.totalTextView.anchorWithConstantsToTop(top: progressUIView.bottomAnchor, left: nil, bottom: nil, right: progressUIView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.totalTextView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        //deposit
        self.totalDepositsTextView.anchorWithConstantsToTop(top: amountTextView.bottomAnchor, left: amountTextView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalDepositsTextView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.totalDepositsTextView.widthAnchor.constraint(equalToConstant: 220).isActive = true
        
        //deposite currency
        self.depositeCurrenceyLbl.leftAnchor.constraint(equalTo: totalDepositsTextView.rightAnchor, constant: 0).isActive = true
        self.depositeCurrenceyLbl.rightAnchor.constraint(equalTo: totalDepositsAmountTextView.leftAnchor, constant: 0).isActive = true
        self.depositeCurrenceyLbl.topAnchor.constraint(equalTo: totalDepositsAmountTextView.topAnchor, constant: 0).isActive = true
        self.depositeCurrenceyLbl.bottomAnchor.constraint(equalTo: totalDepositsAmountTextView.bottomAnchor, constant: 0).isActive = true
        
        //deposite amount
        self.totalDepositsAmountTextView.anchorWithConstantsToTop(top: totalTextView.bottomAnchor, left: nil, bottom: nil, right: totalTextView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalDepositsAmountTextView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.totalDepositsAmountTextView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        //earnings
        self.earningsTextView.anchorWithConstantsToTop(top: totalDepositsTextView.bottomAnchor, left: totalDepositsTextView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.earningTextheightAnchor = self.earningsTextView.heightAnchor.constraint(equalToConstant: 25)
        self.earningsTextView.widthAnchor.constraint(equalToConstant: 220).isActive = true
        
        //earnings currency
        self.earningsCurrenceyLbl.leftAnchor.constraint(equalTo: earningsTextView.rightAnchor, constant: 0).isActive = true
        self.earningsCurrenceyLbl.rightAnchor.constraint(equalTo: earningsAmountTextView.leftAnchor, constant: 0).isActive = true
        self.earningsCurrenceyLbl.topAnchor.constraint(equalTo: earningsAmountTextView.topAnchor, constant: 0).isActive = true
        self.earningsCurrenceyLbl.bottomAnchor.constraint(equalTo: earningsAmountTextView.bottomAnchor, constant: 0).isActive = true
        
        //earnings amount
        self.earningsAmountTextView.anchorWithConstantsToTop(top: totalDepositsAmountTextView.bottomAnchor, left: nil, bottom: nil, right: totalDepositsAmountTextView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
         self.earningAmountheightAnchor = self.earningsAmountTextView.heightAnchor.constraint(equalToConstant: 25)
        self.earningsAmountTextView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        //withdrawal
        self.totalWithdrawalsTextView.anchorWithConstantsToTop(top: earningsTextView.bottomAnchor, left: earningsTextView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalWithdrawalsTextView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.totalWithdrawalsTextView.widthAnchor.constraint(equalToConstant: 220).isActive = true
        
        //withdrawal currency
        self.withdrawalCurrenceyLbl.leftAnchor.constraint(equalTo: totalWithdrawalsTextView.rightAnchor, constant: 0).isActive = true
        self.withdrawalCurrenceyLbl.rightAnchor.constraint(equalTo: totalWithdrawalsAmountTextView.leftAnchor, constant: 0).isActive = true
        self.withdrawalCurrenceyLbl.topAnchor.constraint(equalTo: totalWithdrawalsAmountTextView.topAnchor, constant: 0).isActive = true
        self.withdrawalCurrenceyLbl.bottomAnchor.constraint(equalTo: totalWithdrawalsAmountTextView.bottomAnchor, constant: 0).isActive = true
        
        //withdrawal amount
        self.totalWithdrawalsAmountTextView.anchorWithConstantsToTop(top: earningsAmountTextView.bottomAnchor, left: nil, bottom: nil, right: earningsAmountTextView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalWithdrawalsAmountTextView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.totalWithdrawalsAmountTextView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        //investment
        self.investedTextView.anchorWithConstantsToTop(top: totalWithdrawalsTextView.bottomAnchor, left: totalWithdrawalsTextView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.investedTextView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.investedTextView.widthAnchor.constraint(equalToConstant: 220).isActive = true
        
        //investment currency
        self.investedCurrenceyLbl.leftAnchor.constraint(equalTo: investedTextView.rightAnchor, constant: 0).isActive = true
        self.investedCurrenceyLbl.rightAnchor.constraint(equalTo: totalInvestedTextView.leftAnchor, constant: 0).isActive = true
        self.investedCurrenceyLbl.topAnchor.constraint(equalTo: totalInvestedTextView.topAnchor, constant: 0).isActive = true
        self.investedCurrenceyLbl.bottomAnchor.constraint(equalTo: totalInvestedTextView.bottomAnchor, constant: 0).isActive = true
        
        //investment amount
        self.totalInvestedTextView.anchorWithConstantsToTop(top: totalWithdrawalsAmountTextView.bottomAnchor, left: nil, bottom: nil, right: totalWithdrawalsAmountTextView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalInvestedTextView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.totalInvestedTextView.widthAnchor.constraint(equalToConstant: 80).isActive = true
    }
    
    @objc func handleAddParticipants(_ sender: UIButton){
        cellDelegate?.onClick(index: (index?.row)!)
    }
}
