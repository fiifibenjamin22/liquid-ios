//
//  DataCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/01/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class DataCell: BaseCell {

    var item: [String: String]? {
        didSet {
            guard let unwrapedItem = item else { return }
            
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
            
            let messageHeader = unwrapedItem["title"]!.formatAsAttributed(font: boldFont, color: UIColor.darkText)
            let messageBody = unwrapedItem["details"]!.formatAsAttributed(font: normalFont, color: UIColor.darkGray)
            
            self.messageTextView.attributedText = messageHeader
            self.iconImageView.image = UIImage(named: unwrapedItem["image"]!)
            self.detailTextView.attributedText = messageBody
        }
    }
    
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Regular", size: 18)!)
        return textView
    }()
    
    let detailTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Regular", size: 15)!)
        return textView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(iconImageView)
        addSubview(messageTextView)
        addSubview(detailTextView)
        
        let frameHeight = frame.height - 50
        self.iconImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 25, leftConstant: 16, bottomConstant: -25, rightConstant: -16)
        self.iconImageView.widthAnchor.constraint(equalToConstant: frameHeight).isActive = true
        self.iconImageView.layer.cornerRadius = frameHeight/2
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: iconImageView.trailingAnchor, bottom: nil, right: trailingAnchor, topConstant: 25, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        
        self.detailTextView.anchorWithConstantsToTop(top: self.messageTextView.bottomAnchor, left: iconImageView.trailingAnchor, bottom: nil, right: trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        
    }

}
