//
//  GoalAssistCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/08/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalAssistCell: BaseCell {
    
    var item: GoalAssist? {
        didSet {
            guard let unwrapedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
            
            let messageHeader = "Goal Assist\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
            let messageBody = "\(unwrapedItem.currency) \(unwrapedItem.amount) \(unwrapedItem.period)".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
            
            let result = NSMutableAttributedString()
            result.append(messageHeader)
            result.append(messageBody)
            
            self.contentTextView.attributedText = result
            self.imageView.image = UIImage(named: "GoalAssist")
        }
    }
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        return textView
    }()
    
    let imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(imageView)
        addSubview(contentTextView)
       
        let height = frame.height - 16
        self.imageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: 0)
        self.imageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.imageView.layer.cornerRadius = height/2
        
        self.contentTextView.anchorWithConstantsToTop(top: topAnchor, left: imageView.trailingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        
    }
}
