//
//  NoActivityCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/01/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
class NoActivity: NSObject {
    
}
class NoActivityCell: BaseCell {
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "bell_noactivity")
        
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.lightGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Regular", size: 17)!)
        textView.text = "No recent activities to show"
        textView.textAlignment = .center
        return textView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(iconImageView)
        addSubview(messageTextView)
        
        let frameHeight = CGFloat(50)
        self.iconImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 25, leftConstant: 16, bottomConstant: -25, rightConstant: -16)
        self.iconImageView.widthAnchor.constraint(equalToConstant: frameHeight).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: frameHeight).isActive = true
        self.iconImageView.layer.cornerRadius = frameHeight/2
        
        self.messageTextView.anchorWithConstantsToTop(top: self.iconImageView.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 5, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }
}
