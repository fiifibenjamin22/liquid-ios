//
//  NewGoalCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class NewGoalCell: BaseCell {
    
    var item: GoalCategory? {
        didSet {
            guard let unwrapedItem = item else {return}
            //self.contentTextView.text = unwrapedItem.name
            self.contentTextView.text = unwrapedItem.name.htmlToString
            if  !(unwrapedItem.image.isEmpty) {
                self.imageView.af_setImage(
                    withURL: URL(string: (unwrapedItem.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.imageView.image = Mics.placeHolder()
            }
        }
    }
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        return textView
    }()
    
    let imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    let coverImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        return imageView
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(imageView)
        addSubview(coverImageView)
        addSubview(contentTextView)
        
        let textHeight  = frame.height
        self.imageView.anchorToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor)
        self.coverImageView.anchorToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor)
        self.contentTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -8)
        self.contentTextView.heightAnchor.constraint(equalToConstant: textHeight/3).isActive = true
        
        
    }
}
