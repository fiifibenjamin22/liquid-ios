//
//  GoalActivityCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalActivityCell: BaseCell {
    
    
    var item: GoalActivity? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.contentTextView.text = unwrapedItem.name
            self.amountTextView.text = "\(Mics.convertDoubleToCurrency(amount: unwrapedItem.amount))"
            
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
            
            var messageHeader = "\(unwrapedItem.name)\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
            if unwrapedItem.issuer == "LIQUID_GOAL_ACCOUNT" {
                messageHeader = "WITHDRAWAL OF FUNDS\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
                self.amountTextView.textColor = UIColor.hex(hex: Key.colorTransactionfailed)
            }else{
                messageHeader = "GOAL ACCOUNT FUNDED\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
                self.amountTextView.textColor = UIColor.hex(hex: Key.primaryHexCode)
            }
            
            let messageBody = "\(Mics.setDateForGoalActivity(dateString: unwrapedItem.date))".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
            
            let result = NSMutableAttributedString()
            result.append(messageHeader)
            result.append(messageBody)
            
            self.contentTextView.attributedText = result
            //print("activity icon:::: ",unwrapedItem.issuer)
            self.imageView.image = Mics.networkIcon(name: unwrapedItem.issuer)
            
        }
    }
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        return textView
    }()
    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .right
        return textView
    }()
    
    let imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(imageView)
        addSubview(contentTextView)
        addSubview(amountTextView)
        
        let height = frame.height - 16
        
        self.imageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: 0)
        self.imageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.imageView.layer.cornerRadius = height/2
    
        self.contentTextView.anchorWithConstantsToTop(top: topAnchor, left: imageView.trailingAnchor, bottom: bottomAnchor, right: amountTextView.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
       
        self.amountTextView.anchorWithConstantsToTop(top: topAnchor, left: nil, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: -8, rightConstant: -16)
        self.amountTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
    }
}
