//
//  InvestmentOptionController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 08/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import DLRadioButton

class CreateGoalController: BaseScrollViewController {
    
    var goal: Goal?
    var image: UIImage?
    let fontLight = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 15)!)
    let fontBold = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 15)!)
    let utilViewHolder = ViewControllerHelper()
    let apiService = ApiService()
    
    var frequency = ""
    var amountToDebit = 0.0
    var paymentMethodId = 0
    var paymentMethodType = ""
    var isAutoDeductionState = false
    
    let topImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.image = UIImage(named: "tc")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var contentlabel: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.darkText
        let font = UIFont.init(name: Font.Roboto.Bold, size: 20)!
        let oneMore = "I understand:\n\n".formatAsAttributed(font: font, color: .darkText)
        let message = "• My money will be invested in a fund managed by IC Asset Management\n\n• Investments have risks. The yield estimates provided are annual rates, and may go up or down.\n\n• Withdrawals from this account will take up to 2 business days.\n\n".formatAsAttributed(font: fontLight, color: .darkText)
        let result = NSMutableAttributedString()
        result.append(oneMore)
        result.append(message)
        label.attributedText = result
        label.textAlignment = .left
        return label
    }()
    
    lazy var fullTerms: UIButton = {
        let button = UIButton()
        button.setAttributedTitle("Read full terms and conditions".underlineAttributedText(defaultColor: UIColor.hex(hex: Key.primaryHexCode), font: fontLight), for: .normal)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(showTerms), for: .touchUpInside)
        
        return button
    }()
    
    lazy var horizontalRule: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    lazy var termsButton: DLRadioButton = {
        let button = ViewControllerHelper.baseRadioButton(title: "I have read and I agree to all terms above", color: UIColor.darkGray)
        
        button.isIconSquare = true
        button.isMultipleSelectionEnabled = true
        let termsText = NSMutableAttributedString()
        button.iconSize = 20
        
        button.addTarget(self, action: #selector(radioToggled), for: .touchUpInside)
        return button
    }()
   
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Create Goal", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 0
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(createGoal), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let goalIn = self.goal {
            self.setUpNavigationBarWhite(title: "\(goalIn.name)")
        }   else {
            self.setUpNavigationBarWhite(title: "New Goal")
        }
        
        self.addViews()
    }
    
    func addViews()  {
        self.view.addSubview(topImageView)
        self.view.addSubview(contentlabel)
        self.view.addSubview(fullTerms)
        self.view.addSubview(horizontalRule)
        self.view.addSubview(termsButton)
        self.view.addSubview(nextButton)
        
        self.topImageView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.topImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.topImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.topImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.contentlabel.anchorWithConstantsToTop(top: topImageView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: fullTerms.safeAreaLayoutGuide.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 16, rightConstant: -16)
        //self.contentlabel.heightAnchor.constraint(equalToConstant: 200).isActive = true
        self.contentlabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.fullTerms.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: horizontalRule.safeAreaLayoutGuide.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: -24, rightConstant: 0)
        self.fullTerms.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.horizontalRule.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: termsButton.safeAreaLayoutGuide.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 5, rightConstant: 0)
        self.horizontalRule.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        self.termsButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nextButton.safeAreaLayoutGuide.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 5, rightConstant: -16)
        self.termsButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, leftConstant: 15, rightConstant: -15, bottomConstant: -15)
        self.nextButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func tapTerms(sender: UITapGestureRecognizer){
        self.navigationController?.pushViewController(TermsAndConditionController(), animated: true)
    }
    
    @objc func radioToggled(){
        self.nextButton.isEnabled = self.termsButton.isSelected
        let color = self.termsButton.isSelected ?  UIColor.hex(hex: Key.primaryHexCode) : UIColor.gray
        self.nextButton.backgroundColor = color
    }
    @objc func showTerms(){
        let destination = LQWebViewController()
        destination.url = AppConstants.goalTermsUrl
        destination.title = "Terms and Conditions"
        self.navigationController?.pushViewController(destination, animated: true)
        
        //self.navigationController?.pushViewController(TermsAndConditionController(), animated: true)
    }
    //MARK - create goals here
    @objc func createGoal(){
        if !self.termsButton.isSelected {
            ViewControllerHelper.showPrompt(vc: self, message: "Agree with the terms and conditions to proceed.")
            return
        }
        self.utilViewHolder.showActivityIndicator()
        if let goalIn = self.goal {
                        
            self.apiService.newGoal(goal: goalIn,image: self.image) { (status, goalId, withMessage) in
                self.utilViewHolder.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    UserDefaults.standard.set(true, forKey: Key.createGoal)
                    guard let realGoalId = goalId else { return }
                    
                    if self.paymentMethodType != ""{
                        self.setupRecurringDebit(id: Int(realGoalId)!, frequency: self.frequency, amount: self.amountToDebit, paymentMethodId: self.paymentMethodId, paymentType: self.paymentMethodType)
                    }
                    self.finaliseGoal(goalId: realGoalId)

                }  else {
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
        }
    }
    
    //MARK:- setup recurring debit
    func setupRecurringDebit(id: Int,frequency: String,amount: Double,paymentMethodId: Int,paymentType: String){
        self.utilViewHolder.showActivityIndicator()
        self.apiService.setupRecurringDebit(goalId: id, frequency: frequency, amount: amount, paymentMethodId: paymentMethodId, paymentType: paymentMethodType) { (response, status, message) in
            self.utilViewHolder.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: "Account added successfully", completion: { (ifFinished) in
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.createGoal), object: ApiCallStatus.SUCCESS)
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    func getTheGoal(){
        //self.apiService.get
    }
    
    func finaliseGoal(goalId: String)  {
        self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.createGoal), object: ApiCallStatus.SUCCESS)
    }
    
    func fundGoal(goal: Goal)  {
        let wallets = Wallet.all()
        if wallets.isEmpty {
            self.navigationController?.pushViewController(NewPaymentOptionController(), animated: true)
            return
        }
        
        let destination = ConfirmFundController()
        destination.amount = "\(goal.initialAmountDeposited)"
        destination.isGoalAddFunds = true
        destination.goal = goal
        self.navigationController?.pushViewController(destination, animated: true)
        
    }
    
}
