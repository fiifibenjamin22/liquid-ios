//
//  NewGoalRuleController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

protocol GoalAssistPeriodDelegate: AnyObject {
    func setGoalRule(rule: RulePeriodItem)
    func setAssistant(hasAssistant: Bool)
}

class NewGoalRuleController: BaseScrollViewController {
    
    var isEditingAssistant = false
    var canUpdateRecurring = false
    //var isNewGoal = false
    var goalId = 0
    
    var goal: Goal?
    var image: UIImage?
    var rulePeriods = [RulePeriodItem]()

    var menuDelagate:SelectWalletDelegate?
    let walletOptions = WalletSelectOptionView()
    let viewControllerHelper = ViewControllerHelper()
    
    var delegate: GoalAssistPeriodDelegate?
    let apiService = ApiService()
    let utilViewHolder = ViewControllerHelper()
    var paymentMethodId = 0
    var paymentMethodType = ""
    
    var isAutoDebit = false
    var isLinked = false
    var isFromGoalDetail = false
    
    var isEditScreen = false
    let headingTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let periodLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "Period"
        return textView
    }()
    
    lazy var periodButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        //button.setTitle("Daily", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.addBottomBorder(UIColor.lightGray, height: 1)
        button.semanticContentAttribute = .forceLeftToRight
        button.addTarget(self, action: #selector(pickType(_:)), for: .touchUpInside)
       
        let frameWidth = self.view.frame.width - 32
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        return button
    }()
    
    
    let amountPayablePreviewTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let linkAccountTitle: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Let’s take the stress off you, link a mobile money account to this reminder"
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        lbl.textColor = UIColor.hex(hex: "00000")
        lbl.textAlignment = .left
        return lbl
    }()
    
    let learnMoreLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Learn more"
        lbl.isHidden = true
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        lbl.textColor = UIColor.hex(hex: "33333")
        lbl.underline()
        lbl.textAlignment = .left
        return lbl
    }()
    
    lazy var walletSectionView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.hex(hex: "33333").withAlphaComponent(0.5)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.addDashedBorder(borderColor: .black)
        v.isUserInteractionEnabled = true
        v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showmenu)))
        v.layer.cornerRadius = 10
        return v
    }()
    
    let walletTypeIcon: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "Bank")?.withRenderingMode(.alwaysOriginal)
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let linkAccountInstruction: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Link a mobile money account"
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        lbl.textColor = UIColor.black
        lbl.underline()
        lbl.textAlignment = .left
        return lbl
    }()
    
    lazy var linkOptionLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Link an option"
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        lbl.textColor = UIColor.hex(hex: "33333")
        lbl.underline()
        lbl.isHidden = true
        lbl.isUserInteractionEnabled = true
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(linkUnlink)))
        lbl.textAlignment = .right
        return lbl
    }()
 
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Yes! Activate", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 0
        button.addTarget(self, action: #selector(createGoal), for: .touchUpInside)
        return button
    }()
    
    lazy var backButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("Skip", for: .normal)
        button.addTarget(self, action: #selector(skipGoal), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        if let goalIn = self.goal {
            self.setUpNavigationBarWhite(title: "\(goalIn.name)")
        }   else {
            self.setUpNavigationBarWhite(title: "New Goal")
        }
        
        self.addViews()
    }
    
    func addViews()  {
        
        if isEditScreen == true {
            //self.li
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dueDate = formatter.date(from: self.goal!.dueDate)
        let daysToEnd = Mics.daysBetween(start: Date(), end: dueDate!)
        
        let lowerBoundRate = 5.0
        if self.goal!.goalAssistAmount < lowerBoundRate {
            
            if daysToEnd > 7 {
                self.periodButton.setTitle("Weekly", for: .normal)
                self.rulePeriods.append(RulePeriodItem(title: "Weekly", duration: 5, type: PaymentRule.weekly))
            }
            
            if daysToEnd > 29 {
                self.periodButton.setTitle("Monthly", for: .normal)
                self.rulePeriods.append(RulePeriodItem(title: "Monthly", duration: 20,type: PaymentRule.monthly))
            }
            
        }else{
            self.periodButton.setTitle("Daily", for: .normal)
            self.rulePeriods.append(RulePeriodItem(title: "Daily", duration: 1, type: PaymentRule.daily))
            
            if daysToEnd > 7 {
                self.periodButton.setTitle("Weekly", for: .normal)
                self.rulePeriods.append(RulePeriodItem(title: "Weekly", duration: 5, type: PaymentRule.weekly))
            }
            
            if daysToEnd > 29 {
                self.periodButton.setTitle("Monthly", for: .normal)
                self.rulePeriods.append(RulePeriodItem(title: "Monthly", duration: 20,type: PaymentRule.monthly))
            }
        }
        
        self.scrollView.addSubview(headingTextView)
        //self.scrollView.addSubview(tipUIView)
        self.scrollView.addSubview(periodLabel)
        self.scrollView.addSubview(periodButton)
        self.scrollView.addSubview(nextButton)
        
        if !self.isEditScreen {
            self.scrollView.addSubview(backButton)
        }
        self.scrollView.addSubview(amountPayablePreviewTextView)
        
        //automated debit
        self.scrollView.addSubview(linkAccountTitle)
        self.scrollView.addSubview(learnMoreLbl)
        self.scrollView.addSubview(walletSectionView)
        self.walletSectionView.addSubview(walletTypeIcon)
        self.walletSectionView.addSubview(linkAccountInstruction)
        self.scrollView.addSubview(linkOptionLbl)
        
        let width = view.frame.width
        
        self.headingTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.headingTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.headingTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        self.periodLabel.anchorWithConstantsToTop(top: headingTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.periodLabel.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.periodLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        self.periodButton.anchorWithConstantsToTop(top: periodLabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.periodButton.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.periodButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.amountPayablePreviewTextView.anchorWithConstantsToTop(top: periodButton.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: -50, rightConstant: 0)
        self.amountPayablePreviewTextView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.amountPayablePreviewTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        //constraint automated ui element
        self.linkAccountTitle.topAnchor.constraint(equalTo: amountPayablePreviewTextView.bottomAnchor, constant: 0).isActive = true
        self.linkAccountTitle.leftAnchor.constraint(equalTo: amountPayablePreviewTextView.leftAnchor, constant: 0).isActive = true
        self.linkAccountTitle.rightAnchor.constraint(equalTo: amountPayablePreviewTextView.rightAnchor, constant: 0).isActive = true
        self.linkAccountTitle.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        self.learnMoreLbl.topAnchor.constraint(equalTo: linkAccountTitle.bottomAnchor, constant: 8.0).isActive = true
        self.learnMoreLbl.leftAnchor.constraint(equalTo: linkAccountTitle.leftAnchor, constant: 0.0).isActive = true
        self.learnMoreLbl.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        self.learnMoreLbl.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        self.walletSectionView.topAnchor.constraint(equalTo: learnMoreLbl.bottomAnchor, constant: 0.0).isActive = true
        self.walletSectionView.leftAnchor.constraint(equalTo: learnMoreLbl.leftAnchor, constant: 0.0).isActive = true
        self.walletSectionView.rightAnchor.constraint(equalTo: linkAccountTitle.rightAnchor, constant: 0.0).isActive = true
        self.walletSectionView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        
        self.walletTypeIcon.leftAnchor.constraint(equalTo: walletSectionView.leftAnchor, constant: 12.0).isActive = true
        self.walletTypeIcon.centerYAnchor.constraint(equalTo: walletSectionView.centerYAnchor, constant: 0.0).isActive = true
        self.walletTypeIcon.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        self.walletTypeIcon.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        self.linkAccountInstruction.leftAnchor.constraint(equalTo: walletTypeIcon.rightAnchor, constant: 8.0).isActive = true
        self.linkAccountInstruction.rightAnchor.constraint(equalTo: walletSectionView.rightAnchor, constant: -8.0).isActive = true
        self.linkAccountInstruction.topAnchor.constraint(equalTo: walletTypeIcon.topAnchor, constant: 8.0).isActive = true
        self.linkAccountInstruction.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        
        self.linkOptionLbl.topAnchor.constraint(equalTo: walletSectionView.bottomAnchor, constant: 8.0).isActive = true
        self.linkOptionLbl.rightAnchor.constraint(equalTo: walletSectionView.rightAnchor, constant: 0.0).isActive = true
        self.linkOptionLbl.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        self.linkOptionLbl.widthAnchor.constraint(equalToConstant: 200.0).isActive = true
        
        if self.isEditScreen {
            self.nextButton.anchorWithConstantsToTop(top: nil, left: scrollView.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
            self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        } else {
            self.backButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
            self.backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
            self.backButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
            
            self.nextButton.anchorWithConstantsToTop(top: nil, left: backButton.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
            self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        
        self.updateUI()
        self.setPreview(rule: self.rulePeriods.first!)
        
        self.fromEditGoalAssistance()
    }
    
    func fromEditGoalAssistance(){
        if isEditingAssistant == true {
            self.getRecuringSetting(theGoal: goal)
        }
    }
    
    func getRecuringSetting(theGoal: Goal?){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().getRecurringDebitSettings(goalId: theGoal!.id) { (status, response, message) in
            
            print("get recurring data: ",status)
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                
                self.periodButton.setTitle(response?.frequency, for: .normal)
                self.canUpdateRecurring = true
                
                if response?.payment_method != "" {
                    print("selected wallet data: \(response?.payment_method ?? "")")
                    self.walletTypeIcon.isHidden = true
                    self.linkAccountInstruction.text = "\(response?.payment_method ?? "")"
                    self.linkAccountInstruction.underline()
                    self.linkOptionLbl.text = "Unlink this option"
                    self.linkOptionLbl.underline()
                    self.isLinked = true
                }else{
                    self.walletTypeIcon.image = UIImage(named: "Bank")
                    self.linkAccountInstruction.text = "Link a mobile money account"
                    self.linkAccountInstruction.underline()
                    self.linkOptionLbl.text = "Link an option"
                    self.linkOptionLbl.underline()
                    self.isLinked = false
                }
                
            } else{
                //ViewControllerHelper.showPrompt(vc: self, message: message)
                let alert = UIAlertController(title: "Recurring Debit", message: "\(message)\n Do you want to set it up?", preferredStyle: .alert)
                let yes = UIAlertAction(title: "Yes", style: .default, handler: { (isYes) in
                    self.showmenu()
                })
                let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
                alert.addAction(no)
                alert.addAction(yes)
                let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
                subview.backgroundColor = UIColor.white
                alert.view.tintColor = UIColor.black
                let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
                imageView.image = UIImage(named: "WalletIcon")
                alert.view.addSubview(imageView)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    func updateUI()  {
        
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        
        let messageHeader = "Step 3 out of 4.\n".formatAsAttributed(font: normalFont, color: UIColor.darkGray)
        let messageBody = "Set a reminder to help you reach this goal?".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        
        self.headingTextView.attributedText = result
        self.headingTextView.textAlignment = .center
    }
    
    //MARK - create goals here
    @objc func createGoal(){
        
        if self.isEditScreen {
            
            // Update goal reminder
            let goalAssist = GoalAssist(goalId: self.goal!.id, amount: self.goal!.goalAssistAmount, period: self.goal!.goalAssistPeriod, currency: self.goal!.goalAssistCurrency)
            
            self.utilViewHolder.showActivityIndicator()
            self.apiService.updateGoalAssist(goalAssist: goalAssist, completion: { (status, message) in
                self.utilViewHolder.hideActivityIndicator()
                print(message)
                if self.canUpdateRecurring == true {
                    self.updateRecurringDebit()
                }else{
                    self.setupRecurringForNewGoal()
                }
                
            })
        }else {
            self.goal?.assistModeEnabled = true
            self.goal?.goalAssistPeriod = self.periodButton.currentTitle!
            self.goNext()
        }
    }
    
    func updateRecurringDebit() {
        print("goal id::::: ",self.goalId)
        self.apiService.updateRecurringDebitSettings(goalId: self.goalId, frequency: self.goal!.goalAssistPeriod, amount: self.goal!.goalAssistAmount, paymentMethodId: self.paymentMethodId, paymentType: self.paymentMethodType) { (status, response, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print(status)
                print(withMessage)
                print(response as Any)
                
                self.delegate?.setAssistant(hasAssistant: true)
                self.navigationController?.popViewController(animated: true)
                //self.navigationController?.popToRootViewController(animated: true)
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.createGoal), object: nil)
            }else{
                print(status)
                print(withMessage)
                print(response as Any)
            }
        }
    }
    
    func setupRecurringForNewGoal(){
        print("goal id::::: ",self.goalId)
        self.apiService.setupRecurringDebit(goalId: self.goalId, frequency: self.goal!.goalAssistPeriod, amount: self.goal!.goalAssistAmount, paymentMethodId: self.paymentMethodId, paymentType: self.paymentMethodType) { (response, status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print(status)
                print(withMessage)
                print(response)
                
                self.delegate?.setAssistant(hasAssistant: true)
                self.navigationController?.popViewController(animated: true)
                //self.navigationController?.popToRootViewController(animated: true)
                //NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.createGoal), object: nil)
            }else{
                print(status)
                print(withMessage)
                print(response)
            }
        }
    }
    
    //MARK - create goals here
    @objc func skipGoal(){
        self.goal?.goalAssistPeriod = ""
        self.goal?.assistModeEnabled = false
        self.goNext()
    }
    
    func goNext()  {
        let destination = GoalPayoutController()
        destination.goal = self.goal
        destination.image = self.image
        destination.amountToDebit = self.goal!.goalAssistAmount
        destination.frequency = self.periodButton.currentTitle!
        destination.paymentMethodId = self.paymentMethodId
        destination.paymentMethodType = self.paymentMethodType
        destination.isAutoDeductionState = self.isAutoDebit
        self.navigationController?.pushViewController(destination, animated: true)
    }
    //MARK - pick the period type
    @objc func pickType(_ sender: UIButton)  {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        for item in rulePeriods {
            let defaultAction = UIAlertAction(title: "\(item.title)", style: .default) { (action) in
                sender.setTitle(item.title, for: .normal)
                self.goal?.goalAssistPeriod = item.title
                self.setPreview(rule: item)
            }
            alertController.addAction(defaultAction)
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func setPreview(rule: RulePeriodItem)  {
        self.goal?.goalAssistPeriod = rule.title
        
        if self.delegate != nil {
            self.delegate?.setGoalRule(rule: rule)
        }
        
        let amountToSpread = Double(self.goal!.targetAmount)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dueDate = formatter.date(from: self.goal!.dueDate)
        var days = Mics.daysBetween(start: Date(), end: dueDate!)
        
        if days < 1 {
            days = 1
        }
        
        if rule.type == PaymentRule.daily {
            self.goal?.goalAssistAmount = (amountToSpread / Double(days)).rounded(toPlaces: 0)
            self.amountPayablePreviewTextView.text = "Every day you will be reminded to deposit GHS \(self.goal?.goalAssistAmount ?? 0.0) for the next \(days) days."
        }
        
        if rule.type == PaymentRule.weekly {
            let weeks = (Double(days) / 7.0).rounded(.up)
            self.goal?.goalAssistAmount = (amountToSpread / weeks).rounded(toPlaces: 0)
            self.amountPayablePreviewTextView.text = "Every week you will be reminded to deposit GHS \(self.goal?.goalAssistAmount ?? 0.0) for the next \(weeks) weeks."
        }
        
        if rule.type == PaymentRule.monthly {
            let months = (Double(days) / 30.0).rounded(.up)
            self.goal?.goalAssistAmount = (amountToSpread / months).rounded(toPlaces: 0)
            self.amountPayablePreviewTextView.text = "Every month you will be reminded to deposit GHS \(self.goal?.goalAssistAmount ?? 0.0) for the next \(months) months."
        }
                        
    }
    
}

extension NewGoalRuleController: SelectWalletDelegate {
    
    //MARK:- link unlink
    @objc func linkUnlink(){
        if isLinked == true {
            //MARK:- unlink action
            
            let messages = "Are you sure you want to unlink this mobile money account"
            
            let alert = UIAlertController(title: "Confirm", message: messages, preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default, handler: { (finished) in
                
                self.isAutoDebit = false
                
                self.walletTypeIcon.image = UIImage(named: "Bank")
                self.linkAccountInstruction.text = "Link a mobile money account"
                self.linkAccountInstruction.underline()
                self.linkOptionLbl.text = "Link an option"
                self.linkOptionLbl.isHidden = true
                self.linkOptionLbl.underline()
                self.isLinked = false
                
            })
            
            let later = UIAlertAction(title: "Later", style: .destructive, handler: { (finished) in
                return
            })
            
            alert.addAction(later)
            alert.addAction(yes)
            
            let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
            subview.backgroundColor = UIColor.white
            alert.view.tintColor = UIColor.black
            let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
            imageView.image = UIImage(named: "WalletIcon")
            alert.view.addSubview(imageView)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            self.showmenu()
        }
    }
        
    //MARK:- Show Menu
    @objc func showmenu(){
        print("show menu")
        self.walletOptions.delegate = self
        self.walletOptions.isMomoOnly = true
        self.walletOptions.showOptions()
    }

    func didSelectAddNew() {
        
        let destination = AddMobileMoneyWalletController()
        destination.walletDelegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func didSelectPicker() {
        
    }
    
    func didSelectWallet(item: Wallet) {
        
        let messages = "Are you sure you want to link this mobile money account"
        
        let alert = UIAlertController(title: "Confirm", message: messages, preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { (finished) in

            self.paymentMethodId = item.id
            self.paymentMethodType = item.paymentType
            self.isAutoDebit = true
            
            print("selected wallet data: \(item)")
            self.walletTypeIcon.image = UIImage(named: "\(item.icon)")
            self.linkAccountInstruction.text = "\(item.type) - \(item.number)"
            self.linkAccountInstruction.underline()
            self.linkOptionLbl.text = "Unlink this option"
            self.linkOptionLbl.isHidden = false
            self.linkOptionLbl.underline()
            self.isLinked = true
            
        })
        
        let later = UIAlertAction(title: "Later", style: .destructive, handler: { (finished) in
            return
        })
        
        alert.addAction(later)
        alert.addAction(yes)
        
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor.white
        alert.view.tintColor = UIColor.black
        let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
        imageView.image = UIImage(named: "WalletIcon")
        alert.view.addSubview(imageView)
        self.present(alert, animated: true, completion: nil)
    }

    //MARK:- setup recurring debit
    func setupRecurringDebit(id: Int,frequency: String,amount: Double,paymentMethodId: Int,paymentType: String){
        self.utilViewHolder.showActivityIndicator()
        self.apiService.setupRecurringDebit(goalId: id, frequency: frequency, amount: amount, paymentMethodId: paymentMethodId, paymentType: paymentMethodType) { (response, status, message) in
            self.utilViewHolder.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                ViewControllerHelper.showPrompt(vc: self, message: "Account added successfully", completion: { (ifFinished) in
                    self.navigationController?.popToRootViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.createGoal), object: ApiCallStatus.SUCCESS)
                })
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
}
