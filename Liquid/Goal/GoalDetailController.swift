//
//  GoalDetailController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class GoalDetailController: UIViewController {
    
    var goal: Goal?
    let collectionId = "collectionId"
    let goalActivityCellId = "goalActivityCellId"
    let genericActionCellId = "genericActionCellId"
    let genericDelActionCellId = "genericDelActionCellId"
    let genericSectionCellId = "genericSectionCellId"
    let goalAssistCellId = "goalAssistCellId"
    
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let actionItem = ActionItem(name: "Withdraw Funds", action: "WITHDRAWN")
    let actionItemShareGoal = ActionItem(name: "Share Goal", action: "SHARE")
    let actionItemExitGoal = ActionItem(name: "Exit Goal", action: "EXIT")
    let actionItemManageGoal = ActionItem(name: "Manage Shared Goal", action: "MANAGE")
    let actionItemDelate = ActionItem(name: "Delete Goal", action: "DELETED")
    
    var isGoalOwner = false
    var goalImage = ""
    
    var menuDelagate:SelectWalletDelegate?
    let walletOptions = WalletSelectOptionView()
    var recurringData : RecurringDebit?
    
    let user = User.getUser()!
    var goals = [Any]()
    var participant = [Participants]()
    
    var isAutoDebit = false
    var isLinked = false
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()
    
    lazy var addFundButton: UIButton = {
        let button = ViewControllerHelper.baseRoudImageButton()
        button.setTitle("+ Add Funds", for: .normal)
        button.addTarget(self, action: #selector(fundGoal), for: .touchUpInside)
        return button
    }()
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.hex(hex: "FFFFFF")
        self.fetchAllParticipants()
        if let rcd = RecurringDebit.getRecurringDebit() {
            self.recurringData = rcd
        }
        
        if let goalIn = self.goal {
            self.navigationItem.title = "\(goalIn.name)"
        }   else {
            self.navigationItem.title = "Goal"
        }
        
        if self.isGoalOwner == true {
            self.setNavItem()
        }

        self.setUpUI()
    }
    
    //MARK:- VIEW DID LOAD
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(addParticipantListen(notification:)), name: NSNotification.Name(rawValue: "addParticipantListen"), object: nil)
        self.collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func addParticipantListen(notification: Notification){
        print("addParticipantListen method called")
        self.refreshCall()
    }
    
    func refreshCall(){
        self.refreshControl.endRefreshing()
        self.getGoal()
    }
    
    //MARK:- setup main layouts
    func setUpUI()  {
        
        self.view.addSubview(collectionView)
        self.view.addSubview(addFundButton)
        
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.collectionView.register(GoalDetailCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(GoalActivityCell.self, forCellWithReuseIdentifier: goalActivityCellId)
        self.collectionView.register(GenericActionCell.self, forCellWithReuseIdentifier: genericActionCellId)
        self.collectionView.register(GenericSectionCell.self, forCellWithReuseIdentifier: genericSectionCellId)
        self.collectionView.register(GoalDeleteCell.self, forCellWithReuseIdentifier: genericDelActionCellId)
        self.collectionView.register(GoalAssistCell.self, forCellWithReuseIdentifier: goalAssistCellId)
        self.collectionView.register(DataCell.self, forCellWithReuseIdentifier: "dataCell")
        self.collectionView.register(NoActivityCell.self, forCellWithReuseIdentifier: "noActivityCell")
        
        
        self.addFundButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.addFundButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.addFundButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        //MARK:- ended notification
        NotificationCenter.default.addObserver(self, selector: #selector(receivedEndedNotification(notification:)), name: NSNotification.Name(Key.doneGoalEdit), object: nil)
        
        // Add pull to refresh
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshGoals(_:)), for: .valueChanged)
        
        guard let thisGoal = self.goal else { return }
        
        let visitStatus = UserDefaults.standard.bool(forKey: "visitedDetailView")
        
        if visitStatus == false {
            UserDefaults.standard.set(true, forKey: "visitedDetailView")
            if thisGoal.balance == 0.0 {
                let messages = "Would you like to put money in your new liquid goal NOW ?"
                
                let alert = UIAlertController(title: "\n", message: messages, preferredStyle: .alert)
                let updateNow = UIAlertAction(title: "Yes", style: .default, handler: { (finished) in
                    self.fundGoal()
                })
                
                let exitApp = UIAlertAction(title: "No", style: .destructive, handler: { (finished) in
                    
                })
                
                alert.addAction(exitApp)
                alert.addAction(updateNow)
                
                let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
                subview.backgroundColor = UIColor.white
                alert.view.tintColor = UIColor.black
                let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
                imageView.image = UIImage(named: "WalletIcon")
                alert.view.addSubview(imageView)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func refreshGoals(_ sender: Any) {
        self.refreshControl.endRefreshing()
        self.getGoal()
    }
    
    @objc func receivedEndedNotification(notification: Notification){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setNavItem()  {
        
        let editAction = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(handleEditAction))
        //handleEditAction
        navigationItem.rightBarButtonItem = editAction
    }
    
    @objc func coachData(){
        //self.getRecuringSetting()
    }
    
    @objc func handleEditAction()  {
        let destination = EditGoalController()
        destination.goal = self.goal
        self.navigationController?.pushViewController(destination, animated: true)
        //self.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
    }
    
    func fetchAllParticipants(){
        self.apiService.getAllParticipants(goalId: self.goal!.id) { (status, participants, message) in
            print("get all participants: ",participants?.count as Any, " ", status)
            
            if status == ApiCallStatus.SUCCESS {
                
                for p in participants! {
                    print("ok: ",p)
                    self.participant.append(p)
                }
                self.getGoal()
            }
            
            if status ==  ApiCallStatus.FAILED {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
            
            if status == ApiCallStatus.DETAIL {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    func getGoal() {
        if let goalIn = self.goal {
           viewControllerHelper.showActivityIndicator()
            self.apiService.getGoal(goal: goalIn) { (status, goal, withMessage) in
                self.viewControllerHelper.hideActivityIndicator()
                
                self.goals = [Any]()
                self.goals.removeAll()
                
                if let goalRetrievd = goal {
                    self.goal = goalRetrievd
                    
                    self.goals.append(goalRetrievd)
        
                    if self.isGoalOwner {
                        
                        if goalRetrievd.balance > 0 {
                            self.goals.append(self.actionItem)
                        }
                        if self.goal!.is_shared {
                            self.goals.append(self.actionItemManageGoal)
                        }else{
                            self.goals.append(self.actionItemShareGoal)
                        }
                    }else{
                        self.goals.append(self.actionItemExitGoal)
                    }
                    
                    //MARK:- activities
                    self.goals.append("RECENT ACTIVITIES")
                    
                    self.collectionView.reloadData()
                    
                    //MARK:- show assist if is goal owner
                    if self.isGoalOwner == true {
                        
                        print("XXXXXX assistModeEnabled::: ",goalIn.assistModeEnabled)
                        print("XXXXXX isLinked::: ",self.isLinked)
                        
                        if goalIn.assistModeEnabled == true && self.isLinked == true  {
                            
                            self.goals.append("ASSISTANTS")
                            
                            print("assistance: ",self.recurringData?.id ?? "nothing")
                            if self.recurringData?.id != 0 {
                                let goalAssist = ["title": "Goal Assist", "image": "GoalAssist", "details": "GHS \(self.recurringData?.amount ?? 0.0) \(self.recurringData?.frequency ?? "")"]
                                
                                let goalAssistLinker = ["title": "Linked Account", "image": "Bank", "details": "\(self.recurringData?.payment_method ?? "") - \(self.user.phone)"]
                                
                                self.goals.append(goalAssist)
                                self.goals.append(goalAssistLinker)
                            }else{
                                let goalAssist = ["title": "Goal Assist", "image": "GoalAssist", "details": "GHS \(self.recurringData?.amount ?? 0.0) \(self.recurringData?.frequency ?? "")"]
                                
                                //let goalAssistLinker = ["title": "Linked Account", "image": "Bank", "details": "Direct debiting"]
                                
                                self.goals.append(goalAssist)
                                //self.goals.append(goalAssistLinker)
                                
                            }
                            
                        }else{
                            self.goals.append("ASSISTANTS")
                            
                            print("assistance: ",self.recurringData?.id ?? "nothing")
                            if self.recurringData?.id != 0 {
                                let goalAssist = ["title": "Goal Assist", "image": "GoalAssist", "details": "GHS \(self.recurringData?.amount ?? 0.0) \(self.recurringData?.frequency ?? "")"]
                                
                                let goalAssistLinker = ["title": "Linked Account", "image": "Bank", "details": "\(self.recurringData?.payment_method ?? "") - \(self.user.phone)"]
                                
                                self.goals.append(goalAssist)
                                self.goals.append(goalAssistLinker)
                            }
                        }
                        
                        //MARK:- investments
                        self.goals.append("INVESTMENT PAYOUT ACCOUNT")
                        
                        let issuer = goal!.withdrawalDestinationIssuer
                        let issuerComponents = issuer.split(separator: "-")
                        
                        if issuerComponents.count > 0 {
                            self.goals.append(["title":goal!.withdrawalDestinationIdentifier, "image": Mics.iconNameFromPhoneNumber(number: goal!.withdrawalDestinationIdentifier), "details": String(issuerComponents[0]) ])
                        }
                        
                        self.goals.append("NEXT OF KIN")
                        self.goals.append(["title": self.goal!.nextOfKinName, "image": "avatar_nextofkin", "details": self.goal!.nextKinContact])
                        
                        print("XXXXXX::: ",goalIn.assistModeEnabled)
                        //self.setUpAssitantSection(goal: goalIn)
                        
                        //if goalIn.assistModeEnabled == true {
                            //self.setUpAssitantSection(goal: goalRetrievd)
                        //}
                    }
                    
                    let url = "\(ApiUrl().getGoalTransactions())"
                    self.getGoalTransactions(url: url)
                }
            }
        }
    }
    
    
    @objc func fundGoal()  {
        let user = User.getUser()!
        
        if user.has_full_account {
            let wallets = Wallet.all()
            if wallets.isEmpty {
                self.navigationController?.pushViewController(NewPaymentOptionController(), animated: true)
                return
            }
            let destination = FundAccountController()
            destination.isGoalAddFunds = true
            destination.goal = self.goal
            
            self.navigationController?.pushViewController(destination, animated: true)
        } else {
            let destination = UpgradeAccountController()
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    @objc func withDrawCash()  {
        let destination = WithdrawFromGoalAmountViewController()
        destination.isGoalWithdrawFunds = true
        destination.goal = self.goal
        
        self.navigationController?.pushViewController(destination, animated: true)
        //self.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
    }
    
    @objc func deleteGoal()  {
        ViewControllerHelper.showPrompt(vc: self, message: "Delete this goal?") { (isDone) in
            if isDone {
                self.viewControllerHelper.showActivityIndicator()
                self.apiService.pauseGoal(goal: self.goal!) { (status, goal, withMessage) in
                    self.viewControllerHelper.hideActivityIndicator()
                    if status == ApiCallStatus.SUCCESS {
                      UserDefaults.standard.set(true, forKey: Key.createGoal)
                      self.navigationController?.popViewController(animated: true)
                    } else {
                      self.viewControllerHelper.hideActivityIndicator()
                    }
                }
            }
        }
    }
    
    func setUpAssitantSection(goal: Goal)  {
        self.apiService.getGoalAssistInfo(goalId: goal.id) { (status, assist, withMessage) in

            let realm = try! Realm()

            if status == ApiCallStatus.SUCCESS {

                try! realm.write({
                    self.goal!.hasAssistant = true
                })

                self.goals.append("ASSISTANTS")
                let goalAssist = ["title": "Goal Assist", "image": "GoalAssist", "details": "Contribute \(assist!.currency) \(assist!.amount) to your target amount"]
                self.goals.append(goalAssist)

                self.collectionView.reloadData()
            } else {
                print("error setting up assistant")

                try! realm.write({
                    self.goal!.hasAssistant = false
                })
            }
        }
    }
    
    func getGoalTransactions(url: String)   {
        let params = ["goal_id":self.goal!.id]
        self.apiService.getGoalTransaction(url: url, params: params) { (status, transactions, withMessag) in
            if status == ApiCallStatus.SUCCESS {
                if let trnsactionsIn = transactions {
                    
                    if let index = self.goals.firstIndex(where: { $0 as? String == "RECENT ACTIVITIES" }){
                        self.collectionView.reloadData()
                        //MARK: passing goal section
                        for history in trnsactionsIn.reversed() {
                            self.goals.insert(history, at: index + 1)
                        }
                        
                        if trnsactionsIn.count == 0 {
                            self.goals.insert(NoActivity(), at: index + 1)
                        }
                    }
                    
                    self.collectionView.reloadData()
                }
            }
        }
    }
}

extension GoalDetailController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.goals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.goals[indexPath.row]
        
        if item is Goal {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! GoalDetailCell
            cell.item = self.goals[indexPath.row] as? Goal
            cell.bgView.cornerRadius = 10
            cell.bgView.shadowColor = UIColor.white
            cell.isGoalOwner = self.isGoalOwner
            cell.cellDelegate = self
            cell.index = indexPath
            
            return cell
        }
        
        //MARK: goal activity section
        if item is GoalActivity {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: goalActivityCellId, for: indexPath) as! GoalActivityCell
            cell.item = self.goals[indexPath.row] as? GoalActivity
            if cell.item?.issuer == "LIQUID_GOAL_ACCOUNT" {
                                
                if  !((self.goalImage.isEmpty)) {
                    cell.imageView.af_setImage(
                        withURL: URL(string: (self.goalImage))!,
                        placeholderImage: Mics.placeHolder(),
                        imageTransition: .crossDissolve(0.2)
                    )
                } else {
                    cell.imageView.image = Mics.placeHolder()
                }
            }
            return cell
        }
        
        //MARK: the goal assist section
        if item is GoalAssist {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: goalAssistCellId, for: indexPath) as! GoalAssistCell
            cell.item = self.goals[indexPath.row] as? GoalAssist
            return cell
        }
        
        //MARK: action section
        if item is ActionItem {
            let actionItem = item as! ActionItem
            if actionItem.action == "DELETED" {
                let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: genericDelActionCellId, for: indexPath) as! GoalDeleteCell
                cell.item = self.goals[indexPath.row] as? ActionItem
                return cell
            }
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: genericActionCellId, for: indexPath) as! GenericActionCell
            cell.item = self.goals[indexPath.row] as? ActionItem
            return cell
        }
        
        if item is [String: String] {
            let itemArr = item as! [String: String]
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "dataCell", for: indexPath) as! DataCell
            cell.item = itemArr
            return cell
        }
        
        if item is NoActivity {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noActivityCell", for: indexPath) as! NoActivityCell
            return cell
        }
        
        
        let itemString = item as? String
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: genericSectionCellId, for: indexPath) as! GenericSectionCell
        cell.item = itemString
        cell.hideSeparator = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = self.goals[indexPath.row]
        
        let itemWidth = collectionView.frame.width
        var itemHeight = CGFloat(250)
        
        if item is Goal {
           itemHeight = CGFloat(350)
        }
        
        if item is GoalActivity || item is ActionItem || item is String || item is GoalAssist {
           itemHeight = CGFloat(50)
        }
        
        if item is [String: String] {
            itemHeight = CGFloat(100)
        }
        
        if item is NoActivity {
            itemHeight = CGFloat(150)
        }
        
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.goals[indexPath.row]
        
        if item is ActionItem {
            let actionItem = item as! ActionItem
            if actionItem.action == "DELETED" {
              self.deleteGoal()
            }
            else if actionItem.action == "SHARE"{
                let vc = AddParticipantVC()
                vc.goalId = self.goal!.id
                vc.goalName = self.goal!.name
                self.navigationController?.pushFade(vc)
            }else if actionItem.action == "MANAGE"{
                //let layout = UICollectionViewFlowLayout()
                let vc = GoalPaticipantsVC()
                vc.goalId = self.goal!.id
                self.navigationController?.pushFade(vc)
            }else if actionItem.action == "EXIT"{
                //delete or remove yourself from the goal
                self.exitGoal()
            }else {
              self.withDrawCash()
            }
        }
        
        //let index = self.goals.firstIndex(where: { $0 as? String == "ASSISTANTS" })! + 1
        //if indexPath.row == index {
        //    self.linkUnlink()
        //}
    }
    
    @objc func exitGoal(){
        
        let alert = UIAlertController(title: "Exit Goal", message: "Are you sure you want to leave this goal?", preferredStyle: .alert)
        let updateNow = UIAlertAction(title: "Yes", style: .default, handler: { (finished) in
            self.apiService.removeParticipant(goal_id: self.goal!.id, customer_id: self.user.id) { (status, item, message) in
                self.viewControllerHelper.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    //self.navigationController?.popViewController(animated: true)
                    self.collectionView.reloadData()
                }else{
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            }
        })
        
        let exitApp = UIAlertAction(title: "No", style: .destructive, handler: { (finished) in

        })
        
        alert.addAction(exitApp)
        alert.addAction(updateNow)
        
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor.white
        alert.view.tintColor = UIColor.black
        let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 40, height: 40))
        imageView.image = UIImage(named: "WalletIcon")
        alert.view.addSubview(imageView)
        self.present(alert, animated: true, completion: nil)
    }
}

extension GoalDetailController: SelectWalletDelegate {
    
    //MARK:- link unlink
    @objc func linkUnlink(){
        if isLinked == true {
            //MARK:- unlink action
            
            let messages = "Are you sure you want to unlink this mobile money account"
            
            let alert = UIAlertController(title: "Confirm", message: messages, preferredStyle: .alert)
            let yes = UIAlertAction(title: "Yes", style: .default, handler: { (finished) in
                
                self.viewControllerHelper.showActivityIndicator()
                self.apiService.disableRecurringDebit(goalId: self.goal!.id, completion: { (status, response, message) in
                    self.viewControllerHelper.hideActivityIndicator()
                    if status == ApiCallStatus.SUCCESS {
                        self.isAutoDebit = false
                        self.isLinked = false
                        self.collectionView.reloadData()
                        ViewControllerHelper.showPrompt(vc: self, message: "mobile money account has been unlinked successfully")
                    }
                    
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                })
                
            })
            
            let later = UIAlertAction(title: "Cancel", style: .cancel, handler: { (finished) in
                return
            })
            
            alert.addAction(later)
            alert.addAction(yes)
            
            let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
            subview.backgroundColor = UIColor.white
            alert.view.tintColor = UIColor.black
            let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
            imageView.image = UIImage(named: "WalletIcon")
            alert.view.addSubview(imageView)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            self.showmenu()
        }
    }
    
    //MARK:- Show Menu
    @objc func showmenu(){
        print("show menu")
        let vc = NewGoalRuleController()
        vc.goal = self.goal
        vc.isFromGoalDetail = true
        self.navigationController?.show(vc, sender: self)
        //self.walletOptions.delegate = self
        //self.walletOptions.showOptions()
    }
    
    func didSelectAddNew() {
        
    }
    
    func didSelectPicker() {
        
    }
    
    func didSelectWallet(item: Wallet) {
        
        let messages = "Are you sure you want to link this mobile money account"
        
        let alert = UIAlertController(title: "Confirm", message: messages, preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { (finished) in
            self.isLinked = true
            
        })
        
        let later = UIAlertAction(title: "Cancel", style: .cancel, handler: { (finished) in
            return
        })
        
        alert.addAction(later)
        alert.addAction(yes)
        
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor.white
        alert.view.tintColor = UIColor.black
        let imageView = UIImageView(frame: CGRect(x: 12, y: 10, width: 35, height: 35))
        imageView.image = UIImage(named: "WalletIcon")
        alert.view.addSubview(imageView)
        self.present(alert, animated: true, completion: nil)
    }    
}

extension GoalDetailController: detailHeaderDelegate {
    func onClick(index: Int) {
        if self.participant.count > 0 {
            //let layout = UICollectionViewFlowLayout()
            //let vc = AddParticipantVC()
            //vc.participants.append(contentsOf: self.participant)
            //vc.goalId = self.goal!.id
            //self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = AddParticipantVC()
            vc.goal = self.goal
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
