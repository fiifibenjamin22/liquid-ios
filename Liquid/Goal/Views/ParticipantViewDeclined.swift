//
//  ParticipantViewDeclined.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol ConfirmParticipantDelegate: AnyObject {
    func proceed()
    func dismiss()
}

class ParticipantViewDeclined: UIViewController {

    var delegate: ConfirmParticipantDelegate?
    var shareCode = ""
    var partNumber = ""
    var goalName = ""
    
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var codeTxt: UILabel!
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.codeTxt.text = shareCode
        self.infoLbl.text = "Oops! we couldn’t find a user  with this \(partNumber).\n\nShare your referral code with your friends and family so they can link their account to this goal \(goalName). "
        
        self.codeTxt.layer.cornerRadius = 8
        self.codeTxt.layer.masksToBounds = true
        
        self.view.layer.cornerRadius = 50
        self.view.layer.masksToBounds = true
    }
    
    @IBAction func confirmActionBtn(_ sender: Any) {
        self.delegate?.proceed()
    }
    
    @IBAction func cancelActionBtn(_ sender: Any) {
        self.delegate?.dismiss()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
