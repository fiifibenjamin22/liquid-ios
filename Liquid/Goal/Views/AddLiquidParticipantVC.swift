//
//  AddLiquidParticipantVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 17/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol ConfirmLiquidParticipantDelegate: AnyObject {
    func proceedToShare(custom: String, custId:Int)
    func dismissShare()
}

class AddLiquidParticipantVC: UIViewController,UITextFieldDelegate {
    
    var delegate: ConfirmLiquidParticipantDelegate?
    var ownerName = ""
    var participantName = ""
    var customMessage = ""
    var customerId = 0
    
    @IBOutlet weak var participantNamelbl: UILabel!
    @IBOutlet weak var staicTextPlusParticipantName: UILabel!
    @IBOutlet weak var customeMessageField: UITextField!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var CancelBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.participantNamelbl.text = "\(participantName)"
        self.participantNamelbl.textAlignment = .left
        self.staicTextPlusParticipantName.text = "You are about to add \(participantName) to this goal, please confirm."
    }
    
    @IBAction func shareAction(_ sender: Any) {
        print(self.customeMessageField.text ?? "")
        let mesg = self.customeMessageField.text ?? ""
        delegate?.proceedToShare(custom: mesg,custId: self.customerId)
    }
    
    @IBAction func CancelAction(_ sender: Any) {
        delegate?.dismissShare()
    }
    
    
}
