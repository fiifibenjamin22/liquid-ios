//
//  AddCardController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import RealmSwift

protocol ConfirmCardDelegate: AnyObject {
    func proceed(cardId: Int)
    func dismiss()
}

protocol ConfirmCardAmountDelegate: AnyObject {
    func proceed(cardId: Int, amount: String)
    func dismiss()
}

class AddCardController: ControllerWithBack, ConfirmCardDelegate, ConfirmCardAmountDelegate {
    
    var holdersName = ""
    var holderNumber = ""
    var holderMonthExpiration = "0"
    var holderYearExpiration = "1990"
    var holderCVV = ""
    let apiService = ApiService()
    let viewControllerUtil = ViewControllerHelper()
    var isAddingFund = false
    var isGoalDestination = false
    
    var popup: PopupDialog?
    
    lazy var contentView: CardView = {
        let blackView = CardView()
        blackView.backgroundColor = UIColor.hex(hex: "FAFAFA")
        return blackView
    }()

    lazy var holdersTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Card Holder Name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        return textField
    }()
    
    lazy var cardNumberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Card Number")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        textField.delegate = self
        return textField
    }()
    
    lazy var cardMMTextField: UITextField = {
        let textField = baseUITextField(placeholder: "MM")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        textField.delegate = self
        return textField
    }()
    
    lazy var cardYYYYTextField: UITextField = {
        let textField = baseUITextField(placeholder: "YYYY")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        textField.delegate = self
        return textField
    }()
    
    lazy var cardCVVTextField: UITextField = {
        let textField = baseUITextField(placeholder: "CVV")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        return textField
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Save", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(save), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar(title: "Add Bank Card")
        
        self.setUpLayout()
    }
    
    
    func setUpLayout()  {
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(contentView)
        self.view.addSubview(nextButton)
        self.contentView.addSubview(holdersTextField)
        self.contentView.addSubview(cardNumberTextField)
        self.contentView.addSubview(cardMMTextField)
        self.contentView.addSubview(cardYYYYTextField)
        self.contentView.addSubview(cardCVVTextField)
        
        
        self.contentView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.contentView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        self.holdersTextField.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.holdersTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        

        self.cardNumberTextField.anchorWithConstantsToTop(top: holdersTextField.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.cardNumberTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let width = (self.view.frame.width - 96.0)/3
        self.cardMMTextField.anchorWithConstantsToTop(top: cardNumberTextField.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.cardMMTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.cardMMTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.cardYYYYTextField.anchorWithConstantsToTop(top: cardNumberTextField.bottomAnchor, left: cardMMTextField.trailingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.cardYYYYTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.cardYYYYTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.cardCVVTextField.anchorWithConstantsToTop(top: cardNumberTextField.bottomAnchor, left: cardYYYYTextField.trailingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.cardCVVTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.cardCVVTextField.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.holdersName = self.holdersTextField.text!
        self.holderMonthExpiration = self.cardMMTextField.text!
        self.holderYearExpiration = self.cardYYYYTextField.text!
        self.holderNumber = self.cardNumberTextField.text!
        self.holderCVV = self.cardCVVTextField.text!
        
        self.changeButtonStatus()
    }
    
    //MARK:- save the card information
    @objc func save(){
        let name = self.holdersName
        let number = self.holderNumber.replacingOccurrences(of: " ", with: "")
        let type = Mics.getCardType(numberOnly: number)
        let user = User.getUser()!
        let month = Int(self.holderMonthExpiration)
        let year = Int(self.holderYearExpiration)
        
        // Validate card expiry date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-yyyy"
        if let expiryDate = dateFormatter.date(from: "\(self.holderMonthExpiration)-\(self.holderYearExpiration)") {
            if expiryDate < Date() {
                ViewControllerHelper.showPrompt(vc: self, message: "The expiry date has already passed")
                return
            }
        } else {
            ViewControllerHelper.showPrompt(vc: self, message: "The expiry date is invalid")
            return
        }
        
        
        
        self.viewControllerUtil.showActivityIndicator()
        
        if self.isGoalDestination {
            let withdrawalDestination = WithdrawalDestination(customerId: User.getUser()!.cutomerId, issuer: name, identifier: number, type: "BANK", name: "")
            self.apiService.newGoalDestinationCall(item: withdrawalDestination) { (status, id, withMessage) in
                if status == ApiCallStatus.SUCCESS {
                   self.navigationController?.popViewController(animated: true)
                } else {
                    self.viewControllerUtil.hideActivityIndicator()
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
            return
        }
        
        let params = ["payment_type":PaymentOptionType.card.rawValue,"customer_id":user.cutomerId,"account_id":user.accountId,"card_name": holdersName,"card_number":number,"cvv":holderCVV,"expiry_month":month ?? 0,"expiry_year":year ?? 1990,"card_type": type.uppercased()] as [String : Any]
        self.apiService.addPaymentOption(params: params) { (status, wallet, withMessage) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                if let walletId = wallet?.id {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cardAdded"), object: ApiCallStatus.SUCCESS)
                    self.dismiss(animated: true, completion: nil)
                    /*
                    let confirm_vc = VerifyCardPopupViewController(nibName: "VerifyCardPopupView", bundle: nil)
                    confirm_vc.delegate = self
                    confirm_vc.cardId = Int(walletId)
                    self.popup = PopupDialog(viewController: confirm_vc)
                    self.present(self.popup!, animated: true, completion: nil)*/
                } else {
                    
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func proceed(cardId: Int){
        // Do bank verification
        self.popup?.dismiss()
        let params = ["card_id": cardId, "currency": "GHS", "cvv": holderCVV] as [String : Any]
        self.viewControllerUtil.showActivityIndicator()
        self.apiService.microDebitCard(params: params, completion: {(status, refId, message) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                let confirm_vc = VerifyCardAmountPopupViewController(nibName: "VerifyCardAmountPopupView", bundle: nil)
                confirm_vc.delegate = self
                confirm_vc.cardId = cardId
                self.popup = PopupDialog(viewController: confirm_vc)
                self.present(self.popup!, animated: true, completion: nil)
            } else if status == ApiCallStatus.DETAIL {
                if refId == -3 {
                    let confirm_vc = VerifyCardAmountPopupViewController(nibName: "VerifyCardAmountPopupView", bundle: nil)
                    confirm_vc.delegate = self
                    confirm_vc.cardId = cardId
                    self.popup = PopupDialog(viewController: confirm_vc)
                    self.present(self.popup!, animated: true, completion: nil)
                    
                    UIApplication.shared.open(URL(string: message)!, options: [:], completionHandler: nil)
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        })
    }
    
    func dismiss() {
        self.popup?.dismiss()
        if self.isAddingFund {
            weak var pvc = self.presentingViewController
            self.dismiss(animated: true, completion: {
                let destination = FundAccountController()
                pvc?.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            })
        } else {
            //MARK: adding new wallet
            UserDefaults.standard.set(true, forKey: Key.walletAdded)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func proceed(cardId: Int, amount: String) {
        self.popup?.dismiss()
        let params = ["card_id": cardId, "currency": "GHS", "value": Double(amount)!] as [String : Any]
        self.viewControllerUtil.showActivityIndicator()
        self.apiService.verifyDebitCard(params: params, completion: {(status, cardId, message) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                NotificationCenter.default.post(name: NSNotification.Name("loadCoach"), object: self)
                let realm = try! Realm()
                let wallet = realm.object(ofType: Wallet.self, forPrimaryKey: cardId)
                
                if wallet != nil {
                    try! realm.write() {
                        wallet!.payment_verified = true
                        Wallet.save(data: wallet!)
                    }
                }
                
                self.dismiss(animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        })
    }
    
    func changeButtonStatus()  {
        let valid = Mics.matchesRegex(regex: "[^0-9]", text: self.holderNumber)
        if !holdersName.isEmpty && !holderCVV.isEmpty && !holderNumber.isEmpty && !holderYearExpiration.isEmpty && !holderMonthExpiration.isEmpty && valid {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }  else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
}

extension AddCardController: UITextFieldDelegate {
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cardMMTextField {
            guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
                return false
            }
            
            if range.location > 2 {
                return false
            }
        } else if textField == cardYYYYTextField {
            guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
                return false
            }
            if range.length <= 4 {
                return true
            }
        } else {
           return self.delegateCardNumber(textField: textField, range: range, string: string)
        }
        return true
    }
    
    func delegateCardNumber(textField:UITextField,range: NSRange,string: String) -> Bool {
        if Int(range.location) == 19 {
            return false
        }
        if string.count == 0 {
            return true
        }
        if (Int(range.location) == 4) || (Int(range.location) == 9) || (Int(range.location) == 14) {
            let str = "\(textField.text ?? "") "
            textField.text = str
        }
        return true
    }

}


