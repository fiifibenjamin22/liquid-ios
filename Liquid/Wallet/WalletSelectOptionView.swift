//
//  WalletSelectOptionView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 18/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class WalletSelectOptionView: NSObject {
    
    let cellHeight = 50
    let collectionId = "collectionId"
    var walletes = [Wallet]()
    var delegate: SelectWalletDelegate?
    var isFundingWallet = false
    var isBankTransfer = false
    var isMomoOnly = false
    
    lazy var blackView: UIView = {
        let blackView = UIView()
        blackView.isUserInteractionEnabled = true
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return blackView
    }()
    
    lazy var contentView: UIView = {
        let blackView = UIView()
        blackView.layer.cornerRadius = 10
        blackView.dropShadow()
        blackView.backgroundColor = UIColor.white
        return blackView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
        textView.text = "Select Payment Option"
        return textView
    }()
    
    lazy var newPaymentButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Add a new payment option", for: .normal)
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.setTitleColor(color, for: .normal)
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(handleNewWallet), for: .touchUpInside)
        button.layer.borderColor = color.cgColor
        return button
    }()
    
    lazy var closeButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "Close"), for: .normal)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        return collectionIn
    }()
    
    
    override init() {
        super.init()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(WalletCell.self, forCellWithReuseIdentifier: collectionId)
        
    }
    
    func loadData()  {
        
        let user = User.getUser()!
        
        let currentWallets = self.walletes
        let newWallets = Wallet.all()
        let addedWalletList = newWallets.filter({ !currentWallets.contains($0) })
        self.walletes.removeAll()
        
        if isMomoOnly == true {
            self.walletes.append(contentsOf: Wallet.allMomoWallets())
        }else{
            self.walletes.append(contentsOf: Wallet.all())
            if user.goalBalance > 0.0 {
                self.walletes.append(Wallet.getLiquidWalet())
            }
            //self.walletes.append(Wallet.getLiquidWalet())
            if isBankTransfer == true {
                self.walletes.append(Wallet.bankTransfer())
            }
        }

        self.collectionView.reloadData()
        
        if addedWalletList.count > 0 {
            let wallet = addedWalletList[0]
            if let index = self.walletes.firstIndex(of: wallet) {
                print(index)
            }
        }
    }
    
    func goalFundingPaymentOption(){
    
    }
    
    
    func showOptions()  {
        self.loadData()
        
        if let window = UIApplication.shared.keyWindow {
            let height = CGFloat(walletes.count * cellHeight)
            let y = window.frame.height - height
            window.addSubview(blackView)
            window.addSubview(contentView)
            contentView.addSubview(messageTextView)
            contentView.addSubview(collectionView)
            contentView.addSubview(newPaymentButton)
            contentView.addSubview(closeButton)
            
            self.blackView.frame = window.frame
            self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height + 250)
            self.messageTextView.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            self.collectionView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: newPaymentButton.topAnchor, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
            
            self.newPaymentButton.anchorWithConstantsToTop(top: nil, left: contentView.leadingAnchor, bottom: closeButton.topAnchor, right: contentView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
            self.newPaymentButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.closeButton.anchorWithConstantsToTop(top: nil, left: contentView.leadingAnchor, bottom: contentView.bottomAnchor, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -44, rightConstant: -16)
            self.closeButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.contentView.frame = CGRect(x: 0, y: y - 250, width: window.frame.width, height: height + 250)
            })
        }
    
    }
    
    
    @objc func handleClose() {
        self.closePicker()
    }
    
    
    @objc func handleNewWallet() {
        self.closePicker()
        self.delegate?.didSelectAddNew()
    }
    
    @objc func handleDismiss(sender: UITapGestureRecognizer) {
        self.closePicker()
    }
    
    func closePicker() {
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.removeFromSuperview()
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
                self.contentView.removeFromSuperview()
            }
        })
    }
}

extension WalletSelectOptionView:  UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return walletes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = walletes[indexPath.row]
        print("print momo wallets: ",item)
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! WalletCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: CGFloat(cellHeight))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectWallet(item: self.walletes[indexPath.row])
        self.closePicker()
    }
}
