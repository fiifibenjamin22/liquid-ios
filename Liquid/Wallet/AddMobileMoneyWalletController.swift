//
//  AddMobileMoneyWalletController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ContactsUI
import PhoneNumberKit

class AddMobileMoneyWalletController: ControllerWithBack {
    
    var walletName = ""
    var walletNumber = ""
    var walletNetwork = ""
    let networkTypeSelectorView = NetworkTypeSelectorView()
    var delegate: SelectNetworkDelegate?
    var walletDelegate: SelectWalletDelegate?
    
    let apiService = ApiService()
    let viewControllerUtil = ViewControllerHelper()
    var isAddingFund = false
    var isGoalDestination = false
    
    lazy var contentView: CardView = {
        let blackView = CardView()
        blackView.backgroundColor = UIColor.hex(hex: "FAFAFA")
        return blackView
    }()
    
    lazy var holdersTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Account Name")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        return textField
    }()
    
    lazy var walletNumberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Phone Number")
        textField.keyboardType = UIKeyboardType.alphabet
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPhoneFromContact)))
        imageView.image = UIImage(named: "AddContact")
        imageView.isUserInteractionEnabled = true
        
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.rightView = imageView
        
        textField.setBottomBorder(color: UIColor.darkGray.cgColor)
        textField.backgroundColor = UIColor.hex(hex: "FAFAFA")
        
        return textField
    }()
    
    let phoneNumberErrorLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.red
        textView.backgroundColor = UIColor.clear
        textView.text = "Please enter a valid phone number."
        textView.font = UIFont(name: Font.Roboto.Light, size: 16)!
        textView.isHidden = true
        return textView
    }()
    
    lazy var networkButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
        button.setTitle("Choose Network Type", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 8,bottom: 0,right: 0)
        button.addBottomBorder(UIColor.darkGray, height: 0.5)
        button.addTarget(self, action: #selector(pickNetwork(_:)), for: .touchUpInside)
        
        let frameWidth = self.view.frame.width - 64
        let image = UIImage(named: "DownArrowSmall")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0)
        return button
    }()

    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Save", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(save), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar(title: "Add Mobile Money Wallet")
        
        self.setUpLayout()
    }
    
    
    func setUpLayout()  {
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(contentView)
        self.view.addSubview(nextButton)
        self.contentView.addSubview(holdersTextField)
        self.contentView.addSubview(walletNumberTextField)
        self.contentView.addSubview(phoneNumberErrorLabel)
        self.contentView.addSubview(networkButton)

        self.contentView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.contentView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        self.walletNumberTextField.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.walletNumberTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.phoneNumberErrorLabel.anchorWithConstantsToTop(top: walletNumberTextField.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.phoneNumberErrorLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        self.holdersTextField.anchorWithConstantsToTop(top: phoneNumberErrorLabel.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.holdersTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.networkButton.anchorWithConstantsToTop(top: holdersTextField.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.networkButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.walletName = self.holdersTextField.text!
        self.walletNumber =  self.walletNumberTextField.text!
        
        do {
            let phoneNumberKit = PhoneNumberKit()
            print("Validating: " + self.walletNumber)
            _ = try phoneNumberKit.parse(self.walletNumber, withRegion: "GH", ignoreType: true)
        }
        catch {
            self.togglePhoneNumberError(show: true)
            return
        }
        
        self.togglePhoneNumberError(show: false)
        self.changeButtonStatus()
    }
    
    func togglePhoneNumberError(show: Bool){
        self.phoneNumberErrorLabel.isHidden = !show
    }
    
    
    func checkWalletNumberExists(number: String) -> Bool {
        for wallet in Wallet.all(){
            
            do {
                let phoneNumberKit = PhoneNumberKit()
                let walletNo = try phoneNumberKit.parse(wallet.number, withRegion: "GH", ignoreType: true)
                let currentNo = try phoneNumberKit.parse(number, withRegion: "GH", ignoreType: true)

                if phoneNumberKit.format(walletNo, toType: .e164) == phoneNumberKit.format(currentNo, toType: .e164) {
                    return true
                }
            }
            catch {
                continue
            }
        }
        return false
    }
    //MARK:- save the card information
    @objc func save(){
        do {
            let phoneNumberKit = PhoneNumberKit()
            print("Validating: " + self.walletNumber)
            _ = try phoneNumberKit.parse(self.walletNumber, withRegion: "GH", ignoreType: true)
        }
        catch {
            ViewControllerHelper.showPrompt(vc: self, message: "Please enter a valid phone number")
            return
        }
        
        
        
        // Check if wallet number already exists
        
        if self.checkWalletNumberExists(number: self.walletNumber){
            ViewControllerHelper.showPrompt(vc: self, message: "You already have a wallet with the same phone number")
            return
        }
        self.viewControllerUtil.showActivityIndicator()

        //MARK: saving the destination
        let network = String(self.walletNetwork.split(separator: " ").first!)
        if self.isGoalDestination {
            let withdrawalDestination = WithdrawalDestination(customerId: User.getUser()!.cutomerId, issuer: "\(network) - \(walletNumber)", identifier: walletNumber, type: "MOMO", name: walletName)
            self.apiService.newGoalDestinationCall(item: withdrawalDestination) { (status, id, withMessage) in
                if status == ApiCallStatus.SUCCESS {
                    self.viewControllerUtil.hideActivityIndicator()
                    NotificationCenter.default.post(name: NSNotification.Name("loadCoach"), object: self)
                    self.navigationController?.popViewController(animated: true, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.payoutWalletAdded), object: ApiCallStatus.SUCCESS)
                    })
                } else {
                    self.viewControllerUtil.hideActivityIndicator()
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
            return
        }
        
        let user = User.getUser()!
        let params = ["payment_type":PaymentOptionType.momo.rawValue,"customer_id": user.cutomerId,"payment_name":walletName,"payment_number": walletNumber,"payment_network": network] as [String : Any]

        self.apiService.addPaymentOption(params: params) { (status, wallet, withMessage) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                print("Added Payment method")
                NotificationCenter.default.post(name: NSNotification.Name("loadCoach"), object: self)
                if self.isAddingFund {
                    weak var pvc = self.presentingViewController

                    self.dismiss(animated: true, completion: {
                        let destination = FundAccountController()
                        pvc?.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
                    })
                } else {
                    UserDefaults.standard.set(true, forKey: Key.walletAdded)
                    self.walletDelegate?.didSelectWallet(item: wallet!)
                    self.dismiss(animated: true, completion: nil)
                }
            }  else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func changeButtonStatus()  {
        if !walletNumber.isEmpty && !walletName.isEmpty && !walletNetwork.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func pickNetwork(_ sender: UIButton)  {
        self.view.endEditing(true)
        networkTypeSelectorView.delegate = self
        networkTypeSelectorView.showOptions()
    }
    
}

extension AddMobileMoneyWalletController: SelectNetworkDelegate {
    
    func didSelectNetwork(item: AirtimeProvider) {
        
        self.walletNetwork = item.name
        self.networkButton.setTitle(item.name, for: .normal)

        let image = UIImage(named: item.imageName)
        if let iv = self.networkButton.viewWithTag(1) as? UIImageView {
            iv.image = image
        } else {
            let iv = UIImageView(frame: CGRect(x: 5, y: 10, width: 30, height: 30))
            iv.image = image
            iv.tag = 1
            self.networkButton.addSubview(iv)
            self.networkButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
        }


        self.changeButtonStatus()
    }

}

extension AddMobileMoneyWalletController : CNContactPickerDelegate {
    @objc func selectPhoneFromContact(sender: UITapGestureRecognizer){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.predicateForEnablingContact = NSPredicate(format:"phoneNumbers.@count > 0",argumentArray: nil)
        contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'", argumentArray: nil)
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let contactName = CNContactFormatter.string(from: contact, style: .fullName) ?? ""
        for number in contact.phoneNumbers {
            let number = (number.value).value(forKey: "digits") as? String
            if let numberReal = number {
                self.walletNumberTextField.text = numberReal
                self.holdersTextField.text = contactName
            }
        }
        picker.dismiss(animated: true, completion: nil)
        self.textFieldDidChange(textField: self.walletNumberTextField)
    }
}
