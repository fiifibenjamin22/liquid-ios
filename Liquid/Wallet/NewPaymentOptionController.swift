//
//  NewPaymentOptionController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class NewPaymentOptionController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let cellSection = "cellHeading"
    var hasToSendMoneyAfter = false
    var isGoalDestination = false
    var delegate: SelectWalletDelegate?
    
    var isFromPaymentOption = false
    var index: IndexPath?
    var wallets = [Wallet]()
    
    var momoPaymentOptions = [String]()
    var bankPaymentOptions = [String]()
    
    var providers = [
        ReceivingWalletType(name: "Mobile Wallet",imageName: "MobileMoney",type: .ALL),
    ]
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isGoalDestination {
            setUpNavigationBar(title: "Add An Investment Payout Account")
            providers.append(ReceivingWalletType(name: "Bank Account",imageName: "Bank",type: .NONE))
        } else {
            setUpNavigationBar(title: "Add Payment Option")
            providers.append(ReceivingWalletType(name: "Bank Card",imageName: "Bank",type: .NONE))
        }
        
        //MARK:- parse all provides
        self.collectionView.reloadData()
        self.setUpLayout()
        
        if self.isFromPaymentOption == true {
            
            for wallet in self.wallets {
                print("all wallets",wallet)
                if wallet.paymentType == "momo" {
                    momoPaymentOptions.append(wallet.paymentType)
                }else if wallet.paymentType == "card"{
                    bankPaymentOptions.append(wallet.paymentType)
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(receivedEndedNotification(notification:)), name: NSNotification.Name(Key.payoutWalletAdded), object: nil)
    }
    
    @objc func receivedEndedNotification(notification: Notification){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpLayout()  {
        self.view.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.collectionView.register(WalletCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(PaymentOptionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellSection)
        
    }
    
}

extension NewPaymentOptionController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return providers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! WalletCell
        cell.walletType = self.providers[indexPath.row]
        cell.addBottomBorder(.lightGray, height: 1.0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(65)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellSection, for: indexPath) as! PaymentOptionHeader
            
            if self.isGoalDestination {
                reusableview.text = "WHAT KIND OF PAYOUT ACCOUNT DO YOU WANT TO ADD?"
            } else {
                reusableview.text = "WHAT KIND OF OPTION DO YOU WANT TO ADD?"
            }

            return reusableview
        default:  fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(provider: self.providers[indexPath.row])
    }
    
    //MARK:- clicked wallet type
    func transactionClicked(provider:ReceivingWalletType) {
        if provider.type == .NONE {
            //let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
            //if passcodeEnabled {
            if self.isGoalDestination {
                let destination = AddBankDestinationViewController()
                self.navigationController?.pushViewController(destination, animated: true)
            } else {
                
                if self.momoPaymentOptions.count <= 4 {
                    let destination = AddCardController()
                    destination.isGoalDestination = self.isGoalDestination
                    self.navigationController?.pushViewController(destination, animated: true)
                }else{
                    ViewControllerHelper.showPrompt(vc: self, message: "Sorry, you cannot add any additional debit card to your account.") { (isFinished) in
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
                
                
           /* } else {
                let destination = AuthenticateController()
                self.navigationController?.pushViewController(destination, animated: true)
            }*/
        } else {
            

            if self.isFromPaymentOption == true {
                
                if self.momoPaymentOptions.count <= 4 {
                    let destination = AddMobileMoneyWalletController()
                    destination.walletDelegate = self.delegate
                    destination.isGoalDestination = self.isGoalDestination
                    self.navigationController?.pushViewController(destination, animated: true)
                }else{
                    ViewControllerHelper.showPrompt(vc: self, message: "Sorry, you cannot add any additional mobile money wallet to your account.") { (isFinished) in
                        self.navigationController?.popViewController(animated: true)
                    }
                }

            }else{
                let destination = AddMobileMoneyWalletController()
                destination.walletDelegate = self.delegate
                destination.isGoalDestination = self.isGoalDestination
                self.navigationController?.pushViewController(destination, animated: true)

            }


        }
    }
}


