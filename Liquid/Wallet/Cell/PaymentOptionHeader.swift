//
//  PaymentOptionHeader.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class PaymentOptionHeader: BaseCellHeader {
    
    var text: String? {
        didSet {
            self.messageTextView.text = text
        }
    }
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 24)!)
        textView.text = "WHAT KIND OF OPTION DO YOU WANT TO ADD?"
        return textView
    }()
    
    override func setUpViews() {
        super.setUpViews()
        backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        
        addSubview(messageTextView)
        messageTextView.text = self.text
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        
    }
}

