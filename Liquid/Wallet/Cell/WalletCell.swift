//
//  WalletCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 18/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class WalletCell: BaseCell {
    
    var item: Wallet? {
        didSet {
            guard let unwrapedItem = item else {return}
            if unwrapedItem.paymentType == "card" {
                self.messageTextView.text = "\(unwrapedItem.number)"
            } else if unwrapedItem.paymentType == "momo" {
                self.messageTextView.text = "\(unwrapedItem.name) - \(unwrapedItem.number)"
            } else {
                self.messageTextView.text = "\(unwrapedItem.name)"
            }
            
            print(unwrapedItem.icon)
            self.iconImageView.image = Mics.walletNetworkIcon(name: unwrapedItem.icon)
        }
    }
    
    var goalDestination: GoalDestination? {
        didSet {
            guard let unwrapedItem = goalDestination else {return}
            self.messageTextView.text = "\(unwrapedItem.issuer)"
            let issuerParts = unwrapedItem.issuer.split(separator: " ")
            
            self.iconImageView.image = Mics.walletIconFromIssuer(name: String(issuerParts[0]))
        }
    }
    
    
    var walletType: ReceivingWalletType? {
        didSet {
            guard let unwrapedItem = walletType else {return}
            self.messageTextView.text = "\(unwrapedItem.name)"
            self.iconImageView.image = UIImage(named: unwrapedItem.imageName)
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Thin, size: 18)!)
        return textView
    }()
    
    let arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "Next")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "WalletIcon")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
   
        addSubview(iconImageView)
        addSubview(messageTextView)
        addSubview(arrowImageView)
        
        let frameHeight = frame.height - 23
        self.iconImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 11.5, leftConstant: 16, bottomConstant: -11.5, rightConstant: -16)
        self.iconImageView.widthAnchor.constraint(equalToConstant: frameHeight).isActive = true
        self.iconImageView.layer.cornerRadius = frameHeight/2
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: iconImageView.trailingAnchor, bottom: bottomAnchor, right: arrowImageView.leadingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        self.arrowImageView.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.arrowImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
    }
}
