//
//  NetworkTypeSelectorView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 21/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class NetworkTypeSelectorView: NSObject {
    
    let cellHeight = 50
    let collectionId = "collectionId"
    var providers = [AirtimeProvider]()
    var delegate: SelectNetworkDelegate?
    
    lazy var blackView: UIView = {
        let blackView = UIView()
        blackView.isUserInteractionEnabled = true
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return blackView
    }()
    
    lazy var contentView: UIView = {
        let blackView = UIView()
        blackView.layer.cornerRadius = 10
        blackView.dropShadow()
        blackView.backgroundColor = UIColor.white
        return blackView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        textView.text = "Select Network"
        return textView
    }()
    
    lazy var closeButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "Close"), for: .normal)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        return collectionIn
    }()
    
    
    override init() {
        super.init()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(ProviderCell.self, forCellWithReuseIdentifier: collectionId)
        
        let providerMtn = AirtimeProvider(name: "MTN Mobile Money", imageName: "MTNmomo", type: .MTN_AIRTIME)
        let providerTigo = AirtimeProvider(name: "TIGO Cash", imageName: "TigoCash", type: .TIGO_AIRTIME)
        let providerAirtel = AirtimeProvider(name: "Airtel Money", imageName: "AirtelMoney", type: .AIRTEL_AIRTIME)
        let providerVodafone = AirtimeProvider(name: "Vodafone Cash", imageName: "VodafoneCash", type: .VODAFONE_AIRTIME)
        
        self.providers.append(providerMtn)
        self.providers.append(providerTigo)
        self.providers.append(providerAirtel)
        self.providers.append(providerVodafone)
        self.collectionView.reloadData()
    }
    
    func showOptions()  {
        if let window = UIApplication.shared.keyWindow {
            let height = CGFloat(providers.count * cellHeight)
            let y = window.frame.height - height
            window.addSubview(blackView)
            window.addSubview(contentView)
            contentView.addSubview(messageTextView)
            contentView.addSubview(collectionView)
            contentView.addSubview(closeButton)
            
            self.blackView.frame = window.frame
            self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height + 250)
            self.messageTextView.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            self.collectionView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: closeButton.topAnchor, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
            
            self.closeButton.anchorWithConstantsToTop(top: nil, left: contentView.leadingAnchor, bottom: contentView.bottomAnchor, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -44, rightConstant: -16)
            self.closeButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.contentView.frame = CGRect(x: 0, y: y - 250, width: window.frame.width, height: height + 250)
            })
        }
    }
    
    
    @objc func handleClose() {
        self.closePicker()
    }
    
    
    @objc func handleDismiss(sender: UITapGestureRecognizer) {
        self.closePicker()
    }
    
    func closePicker() {
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.removeFromSuperview()
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
                self.contentView.removeFromSuperview()
            }
        })
    }
}

extension NetworkTypeSelectorView:  UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return providers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = providers[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! ProviderCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: CGFloat(cellHeight))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectNetwork(item: self.providers[indexPath.row])
        self.closePicker()
    }
}

