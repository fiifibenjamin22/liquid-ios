//
//  VerifyCardAmountPopupViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 15/12/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class VerifyCardAmountPopupViewController: UIViewController {
    var delegate: ConfirmCardAmountDelegate?
    var cardId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func verifyAmount(_ sender: Any) {
        let amountTxt = self.view.viewWithTag(2) as! UITextField
        self.delegate?.proceed(cardId: cardId!, amount: amountTxt.text!)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.delegate?.dismiss()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
