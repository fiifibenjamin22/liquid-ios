//
//  ConfirmFundController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 21/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import SwiftyJSON

class ConfirmFundController: BaseScrollViewController {
    
    var amount = "0"
    let walletOptions = WalletSelectOptionView()
    var senderWallet: Wallet?
    let apiService = ApiService()
    let utilViewController = ViewControllerHelper()
    var code = ""
    var cvv = ""
    var transactionRef = ""
    var voucher = ""
    
    var goal: Goal?
    var isGoalAddFunds = false
    var isGoalWithdrawFunds = false
    
    let user = User.getUser()!
    var paymentMethod = PaymentMethodTypes.FUND_ACCOUNT.rawValue
    var targetAccountType = TargetIssuer.LIQUID_ACCOUNT.rawValue
    var targetAccount = User.getUser()!.phone

    var popup: PopupDialog?
    
    lazy var amountCardView: AmountCardView = {
        let amountView = AmountCardView()
        amountView.amount = self.amount
        amountView.delagate = self
        amountView.translatesAutoresizingMaskIntoConstraints = false
        return amountView
    }()
    
    let tipTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "TRANSACTION DETAILS"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(completeLoading), for: .touchUpInside)
        return button
    }()
    
    lazy var backButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "BackArrow"), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        //MARK:- withd
        if self.isGoalWithdrawFunds {
            setUpNavigationBarWhite(title: "Confirm Withdrawal")
            self.setUpWithDrawalStuffs()
        } else if self.isGoalAddFunds {
            setUpNavigationBarWhite(title: "Fund Goal")
        } else {
            setUpNavigationBarWhite(title: "Fund Account")
        }
        
        scrollView.addSubview(tipTextView)
        scrollView.addSubview(amountCardView)
        view.addSubview(nextButton)
        view.addSubview(backButton)
        
        let width = self.view.frame.width
    
        self.tipTextView.anchorToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor)
        self.tipTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.tipTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.amountCardView.anchorWithConstantsToTop(top: tipTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right:nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.amountCardView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.amountCardView.heightAnchor.constraint(equalToConstant: CGFloat(ConstantHeight.WALLET_CARD_HEIGHT.rawValue)).isActive = true
        
        self.backButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: backButton.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.amountCardView.updateAmount(amount: self.amount)
        
        let wallets = Wallet.all()
        if !wallets.isEmpty {
            self.senderWallet = wallets[0]
            self.setWallet(wallet: wallets[0])
        }
    }
    
    func setUpWithDrawalStuffs()  {
        self.amountCardView.walletPickerButton.isHidden = true
        self.amountCardView.updateGoalInfo(goal: self.goal!)
        
        self.nextButton.isEnabled = true
        self.nextButton.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
    }
    
    @objc func completeLoading(){
        guard let sourceWallet = self.senderWallet else {
            ViewControllerHelper.showPrompt(vc: self, message: "Transaction require a wallet to complete")
            return
        }
        
        let code =  Mics.sourceWalletType(wallet: sourceWallet)
        if code ==  SourceIssuer.VODAFONE_CASH.rawValue && self.code.isEmpty {
            ViewControllerHelper.showVoucherPrompt(vc: self, message: "Provide voucher code to proceed") { (isDone, code) in
                self.code = code
            }
            return
        }
        
        if sourceWallet.paymentType == "card" {
            let confirm_vc = CVVEntryViewController(nibName: "CVVEntryView", bundle: nil)
            confirm_vc.delegate = self
            self.popup = PopupDialog(viewController: confirm_vc)
            self.present(self.popup!, animated: true, completion: nil)
            return
        }
        
        self.confirmTransaction()
    }
    
    func initTransaction(){
        //MARK: go to the amount section
        
        if self.isGoalWithdrawFunds {
            paymentMethod = PaymentMethodTypes.WITHDRAW_CASH.rawValue
            targetAccountType = TargetIssuer.LIQUID_ACCOUNT.rawValue
            targetAccount = "\(user.accountId)"
            let goalWallet = Wallet()
            goalWallet.paymentType = "GoalWithraw"
            goalWallet.number = user.phone
            self.senderWallet = goalWallet
        }
        
        if self.isGoalAddFunds {
            paymentMethod = PaymentMethodTypes.MANAGE_GOAL.rawValue
            targetAccountType = TargetIssuer.LIQUID_GOAL_ACCOUNT.rawValue
            targetAccount = "\(self.goal!.id)"
        }
        
        guard let sourceWallet = self.senderWallet else {
            ViewControllerHelper.showPrompt(vc: self, message: "Choose the wallet fund from.")
            return
        }
        
        if sourceWallet.icon == MomoKeys.VODAFONE.rawValue && self.code.isEmpty {
            ViewControllerHelper.showPrompt(vc: self, message: "Provide your voucher code and proceed.")
            return
        }
        
        self.utilViewController.showActivityIndicator()
        print("Wallet type: st: \(sourceWallet.type) pt: \(sourceWallet.paymentType)")
        self.apiService.initTransaction(amount: Double(self.amount)!, methodType: paymentMethod, reference: "", sourceWallet: sourceWallet, issuerToken: self.code, targetType: targetAccountType, targetAccount: targetAccount, theGoal: goal!) { (status, confirmation, message) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if let confirmationIn = confirmation {
                    let amountString = String(format: "%.2f", Double(self.amount)!.rounded(toPlaces: 2))
                    let feeString = String(format: "%.2f", confirmationIn.fees)
                    let totalString = String(format: "%.2f", confirmationIn.total)
                    let displayMessage =  "\(confirmationIn.currency) \(amountString)\n\(confirmationIn.currency) \(feeString)"
                    self.amountCardView.amountFee.text = displayMessage
                    self.amountCardView.totalamountFee.text = "\(confirmationIn.currency) \(totalString)"
                    self.voucher = confirmationIn.transferVoucher
                    self.transactionRef = confirmationIn.reference
                    
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }

    
    func confirmTransaction(){
        guard let sourceWallet = self.senderWallet else {
            ViewControllerHelper.showPrompt(vc: self, message: "Choose the wallet fund from.")
            return
        }
        
        self.utilViewController.showActivityIndicator()
        
        if sourceWallet.icon != "bankTransfers" {
            //MARK: go to the amount section
            self.apiService.confirmTransaction(amount: Double(self.amount)!, methodType: paymentMethod, reference: self.transactionRef, sourceWallet: sourceWallet, cvv: self.cvv, issuerToken: self.code, targetType: targetAccountType, targetAccount: targetAccount) { (response, status, message) in
                self.utilViewController.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    if response != JSON.null {
                        let weburl = response["web_prompt"].stringValue
                        if weburl != "" {
                            let destination = LQWebViewController()
                            destination.url = weburl
                            destination.title = "Verify Wallet Ownership"
                            self.navigationController?.setViewControllers([(self.navigationController?.viewControllers.first)!, destination], animated: true)
                        } else {
                            ViewControllerHelper.showPrompt(vc: self, message: message, completion: { (isDone) in
                                if isDone {
                                    //MARK: if new goal created, pop all tje way back, else dismis
                                    if  UserDefaults.standard.bool(forKey: Key.createGoal) {
                                        self.navigationController?.popToRootViewController(animated: true)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.createGoal), object: ApiCallStatus.SUCCESS)
                                        return
                                    }
                                    self.navigationController?.setViewControllers(Array((self.navigationController?.viewControllers.dropLast(2))!), animated: true)
                                    //self.dismiss(animated: true, completion: nil)
                                }
                            })
                        }
                    }
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            }
        }else{
            self.saveBankTransactionVoucher()
        }
    }
    
    func saveBankTransactionVoucher(){
        self.utilViewController.showActivityIndicator()
        self.apiService.getTransferVouch(amount: Double(self.amount)!, methodType: paymentMethod, reference: self.transactionRef, issuerToken: self.code, targetType: targetAccountType, targetAccount: "\(goal!.id)",theGoal: goal!) { (response, status, message) in
            self.utilViewController.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let vc = BankTransferDetailVC()
                vc.voucher = self.voucher
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    @objc func backAction(){
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}

extension ConfirmFundController : SelectWalletDelegate {
    
    func didSelectAddNew() {
        let destination = NewPaymentOptionController()
        destination.delegate = self
        let nav = UINavigationController(rootViewController: destination)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    func didSelectPicker() {
        self.walletOptions.isFundingWallet = true
        if self.isGoalAddFunds {
            self.walletOptions.isFundingWallet = false
        }
        self.walletOptions.delegate = self
        self.walletOptions.isBankTransfer = true
        walletOptions.showOptions()
    }
    
    func didSelectWallet(item: Wallet) {
        self.senderWallet = item
        self.amountCardView.walletUILable.setTitle(item.name, for: .normal)  
        self.amountCardView.walletIconImageView.image = Mics.walletNetworkIcon(name: item.icon)
        
        self.nextButton.isEnabled = true
        self.nextButton.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        
        if item.icon == MomoKeys.VODAFONE.rawValue {
           self.showVoucherPrompt()
        } else {
            self.initTransaction()
        }
    }
    
    func setWallet(wallet: Wallet)  {
        self.amountCardView.walletUILable.setTitle(wallet.name, for: .normal)
        self.amountCardView.walletIconImageView.image = Mics.walletNetworkIcon(name: wallet.icon)
        
        self.nextButton.isEnabled = true
        self.nextButton.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        
        self.initTransaction()
    }
    
    func showVoucherPrompt() {
        ViewControllerHelper.showVoucherPrompt(vc: self, message: "To complete transaction, enter your Vodafone Voucher code") { (isDone, voucherCode) in
            if isDone {
               self.code = voucherCode
            }
        }
    }
}

extension ConfirmFundController: CVVEntryDelegate {
    func proceed(cvv: String) {
        self.popup?.dismiss()
        self.cvv = cvv
        self.confirmTransaction()
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
}
