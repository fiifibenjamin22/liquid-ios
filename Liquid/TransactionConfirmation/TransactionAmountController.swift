//
//  AirtimeAmountController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 17/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class TransactionAmountController: BaseScrollViewController {
   
    var contact:FavouriteContact?
    var isFavourite = false
    
    var provider: TargetIssuer?
    var walletType: TargetIssuer?
    var name = ""
    var isArtime = false
    var isData = false
    var isReceivingMoney = false
    var isSendingMoney = false
    var isUtility = false
    
    lazy var maxTransfer: Double = {
        if self.isArtime || self.isData {
            return 300.00
        } else if self.isUtility {
            return 10000.00
        } else if self.isSendingMoney {
            return 2000.00
        }
        
        return 0.00
    }()
    
    let recipientView: RecipientView = {
      let recipientUIView = RecipientView()
      recipientUIView.translatesAutoresizingMaskIntoConstraints = false
      return recipientUIView
    }()
    
    let tipTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "HOW MUCH WOULD YOU LIKE TO TRANSFER?"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let sectionView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        uiView.translatesAutoresizingMaskIntoConstraints = false
        return uiView
    }()
    
    let currencyLabel: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 20)!)
        label.text = "GHS"
        return label
    }()
    
    lazy var amountTextField: UITextField = {
        let color = UIColor.white
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.delegate = self
        textField.textAlignment = .center
        textField.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 50)!)
        textField.textColor = UIColor.white
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: "0",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }()
    
    lazy var maxLabel: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 20)!)
        
        label.text = "Enter an amount between GHS 1.00 and GHS \(self.maxTransfer)"
        return label
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Confirm", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(buyAirtime), for: .touchUpInside)
        return button
    }()
    
    lazy var backButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "BackArrow"), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        if isSendingMoney {
            setUpNavigationBarWhite(title: "Send \(name)")
        } else if isArtime {
            setUpNavigationBarWhite(title: "Buy \(name) Airtime")
        } else if isReceivingMoney {
            setUpNavigationBarWhite(title: "Receving \(name)")
        } else if isData {
            setUpNavigationBarWhite(title: "Buy \(name) Data")
        } else if isUtility {
            setUpNavigationBarWhite(title: "Pay \(name) Bill")
        } else {
            setUpNavigationBarWhite(title: "\(name)")
        }
        
        scrollView.addSubview(recipientView)
        scrollView.addSubview(tipTextView)
        scrollView.addSubview(sectionView)
        sectionView.addSubview(currencyLabel)
        sectionView.addSubview(amountTextField)
        sectionView.addSubview(maxLabel)
        view.addSubview(nextButton)
        view.addSubview(backButton)
        
        let width = scrollView.frame.width
        self.recipientView.anchorToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil)
        self.recipientView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.recipientView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- set the recipient information
        if  let contact = self.contact {
            self.recipientView.item = contact
        }
        
        self.tipTextView.anchorToTop(top: recipientView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor)
        self.tipTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.tipTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.sectionView.anchorToTop(top: tipTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor)
        self.sectionView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.sectionView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.currencyLabel.anchorToTop(top: sectionView.topAnchor, left: sectionView.leadingAnchor, bottom: nil, right: sectionView.trailingAnchor)
        self.currencyLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.amountTextField.anchorToTop(top: currencyLabel.bottomAnchor, left: sectionView.leadingAnchor, bottom: nil, right: sectionView.trailingAnchor)
        self.amountTextField.heightAnchor.constraint(equalToConstant: 130).isActive = true
        
        self.maxLabel.anchorToTop(top: nil, left: sectionView.leadingAnchor, bottom: sectionView.bottomAnchor, right: sectionView.trailingAnchor)
        self.maxLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.backButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: backButton.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    }
    
    @objc func buyAirtime(){
        //MARK: go to the amount section
        if  let contact = self.contact, let amount = self.amountTextField.text {
            let destination = TransactionConfirmController()
            destination.contact = contact
            destination.amount = amount
            destination.isFavourite = self.isFavourite
            
    
            destination.name = self.name
            destination.isData = self.isData
            destination.isArtime = self.isArtime
            destination.isUtility = self.isUtility
            destination.isSendingMoney = self.isSendingMoney
            destination.isReceivingMoney = self.isReceivingMoney
            
            self.navigationController?.pushViewController(destination, animated: true)
        }
      
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
       self.toggleButton()
    }
    
    
    func toggleButton()  {
        let text = self.amountTextField.text!
        if !text.isEmpty && text != "." {
            let amount = Double(text)!
            if amount > 4.99 && amount <= self.maxTransfer {
                self.nextButton.isEnabled = true
                self.nextButton.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
            }   else {
                self.nextButton.isEnabled = false
                self.nextButton.backgroundColor = UIColor.gray
            }
        }  else {
            self.nextButton.isEnabled = false
            self.nextButton.backgroundColor = UIColor.gray
        }
    }
    
}

extension TransactionAmountController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //Allowing only degetes with dots
        guard CharacterSet(charactersIn: ".0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        
        //Empty spaces, for cases of delete
        if string.isEmpty {
            return true
        }
        
        //Preventing the number after zero
        let textIn = textField.text!
        if !textIn.isEmpty && (textIn == "0" && string != "." ) {
            return false
        }
        
        return true
    }
}
