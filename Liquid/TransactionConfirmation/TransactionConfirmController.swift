//
//  AirtimeConfirmController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 17/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import SwiftyJSON
import BiometricAuthentication

protocol CVVEntryDelegate: AnyObject {
    func proceed(cvv: String)
    func dismiss()
}

class TransactionConfirmController: BaseScrollViewController {
    
    var contact: FavouriteContact?
    var amount = "0"
    let walletOptions = WalletSelectOptionView()
    var senderWallet: Wallet?
    var isFavourite = false
    let apiService = ApiService()
    let utilViewController = ViewControllerHelper()
    var provider: TargetIssuer?
    var name = ""
    var issuerToken = ""
    var isArtime = false
    var isData = false
    var isReceivingMoney = false
    var isSendingMoney = false
    var isUtility = false
    var methodTye = PaymentMethodTypes.BUY_AIRTIME.rawValue
    var transactionRef = ""
    var popup: PopupDialog?
    var cvv = ""
    var goal: Goal?
    
    let recipientView: RecipientView = {
        let recipientUIView = RecipientView()
        recipientUIView.translatesAutoresizingMaskIntoConstraints = false
        return recipientUIView
    }()
    
    lazy var amountCardView: AmountCardView = {
        let amountView = AmountCardView()
        amountView.delagate = self
        amountView.translatesAutoresizingMaskIntoConstraints = false
        return amountView
    }()
    
    let tipTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "TRANSACTION DETAILS"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Pay", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(confirmTransaction), for: .touchUpInside)
        return button
    }()
    
    lazy var backButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "BackArrow"), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        if isSendingMoney {
            setUpNavigationBarWhite(title: "Send \(name)")
            self.methodTye = PaymentMethodTypes.SEND_MONEY.rawValue
        } else if isArtime {
            setUpNavigationBarWhite(title: "Buy \(name) Airtime")
            self.methodTye = PaymentMethodTypes.BUY_AIRTIME.rawValue
        } else if isReceivingMoney {
            setUpNavigationBarWhite(title: "Request \(name)")
            self.methodTye = PaymentMethodTypes.SEND_MONEY.rawValue
        } else if isData {
            setUpNavigationBarWhite(title: "Buy \(name) Data")
            self.methodTye = PaymentMethodTypes.BUY_DATA.rawValue
        } else if isUtility {
            setUpNavigationBarWhite(title: "Pay Utility Bill")
            self.methodTye = PaymentMethodTypes.PAY_BILL.rawValue
        }
        
        scrollView.addSubview(recipientView)
        scrollView.addSubview(tipTextView)
        scrollView.addSubview(amountCardView)
        view.addSubview(nextButton)
        view.addSubview(backButton)
        
        let width = self.view.frame.width
        self.recipientView.anchorToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil)
        self.recipientView.heightAnchor.constraint(equalToConstant: CGFloat(ConstantHeight.RECIPIENT_CARD_HEIGHT.rawValue)).isActive = true
        self.recipientView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK:- set the recipient information
        if  let contact = self.contact {
            self.recipientView.item = contact
        }
        
        self.tipTextView.anchorToTop(top: recipientView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor)
        self.tipTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.tipTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.amountCardView.anchorWithConstantsToTop(top: tipTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right:nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.amountCardView.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.amountCardView.heightAnchor.constraint(equalToConstant: CGFloat(ConstantHeight.WALLET_CARD_HEIGHT.rawValue)).isActive = true
        
        self.backButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: backButton.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.buyAirtime()
        self.amountCardView.updateAmount(amount: self.amount)
        
        let wallets = Wallet.all()
        if !wallets.isEmpty {
            self.senderWallet = wallets[0]
            self.setWallet(wallet: wallets[0])
        }
    }
    
    //MARK: start the authentication procedure
    func startAuthenticationSection()  {
        let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        print(passcodeEnabled)
        print("Going to authenticate")
        
        if  passcodeEnabled {
            let destination = AuthenticateController()
            destination.pinDelegate = self
            destination.isAuthOnly = true
            let nav = UINavigationController(rootViewController: destination)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        } else if fingerPrintEnabled {
            tapBioMetricSection()
        }
    }
    
    @objc func tapBioMetricSection(){
        // start authentication
        BioMetricAuthenticator.authenticateWithPasscode(reason: "Use your fingerprint to unlock", completion: { result in
            switch result {
            case .success( _):
                // authentication successful
                self.comfirmCompletion()
            case .failure(let error):
                // do nothing on canceled
                if error == .canceledByUser || error == .canceledBySystem {
                    return
                }
                    // device does not support biometric (face id or touch id) authentication
                else if error == .biometryNotAvailable {
                    self.showErrorAlert(message: error.message())
                }
                    // show alternatives on fallback button clicked
                else if error == .fallback {
                    // here we're entering username and password
                }
                    // No biometry enrolled in this device, ask user to register fingerprint or face
                else if error == .biometryNotEnrolled {
                    self.showGotoSettingsAlert(message: error.message())
                }
                    // Biometry is locked out now, because there were too many failed attempts.
                    // Need to enter device passcode to unlock.
                else if error == .biometryLockedout {
                    self.showErrorAlert(message: error.message())
                }
                    // show error on authentication failed
                else {
                    self.showErrorAlert(message: error.message())
                }
            }
            
        })
    }
    
    func showAlert(title: String, message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showErrorAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showGotoSettingsAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message) { (done) in
            /*let url = URL(string: "App-Prefs:root=TOUCHID_PASSCODE")
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }*/
        }
    }
    
    func buyAirtime(){
        guard let sourceWallet = self.senderWallet else {
            //ViewControllerHelper.showPrompt(vc: self, message: "Transaction require a wallet to complete") { (isFinished) in
            //    self.utilViewController.hideActivityIndicator()
            //    self.didSelectPicker()
            //}
            //self.didSelectPicker()
           return
        }
        //MARK: go to the amount section
        if  let contactIn = self.contact, !amount.isEmpty {
            
            print("print contact in: ", contactIn)
            //guard let goals = goal else { return }
            
            self.utilViewController.showActivityIndicator()
            self.apiService.initTransaction(amount: Double(self.amount)!, methodType: self.methodTye, reference: "", sourceWallet: sourceWallet, issuerToken: "", targetType: contactIn.network, targetAccount: contactIn.number, theGoal: nil) { (status, confirmation, message) in
                self.utilViewController.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    if let confirmationIn = confirmation {
                        
                        let amountString = String(format: "%.2f", Double(self.amount)!.rounded(toPlaces: 2))
                        let feeString = String(format: "%.2f", confirmationIn.fees)
                        //let totalAmount = confirmationIn.total + confirmationIn.fees
                        let totalString = String(format: "%.2f", confirmationIn.total)
                        //self.amount = "\(totalAmount)"
                        let displayMessage =  "\(confirmationIn.currency) \(amountString)\n\(confirmationIn.currency) \(feeString)"
                        self.amountCardView.amountFee.text = displayMessage
                        self.amountCardView.totalamountFee.text = "\(confirmationIn.currency) \(totalString)"
                        self.transactionRef = confirmationIn.reference
                        
                        //self.confirmTransaction()
                    } else {
                      ViewControllerHelper.showPrompt(vc: self, message: message)
                    }
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            }
      }
    }
    
    @objc func confirmTransaction(){
        guard let sourceWallet = self.senderWallet else {
            ViewControllerHelper.showPrompt(vc: self, message: "Transaction require a wallet to complete")
            return
        }
        
        let code =  Mics.sourceWalletType(wallet: sourceWallet)
        if code ==  SourceIssuer.VODAFONE_CASH.rawValue && self.issuerToken.isEmpty {
            ViewControllerHelper.showVoucherPrompt(vc: self, message: "Provide voucher code to proceed") { (isDone, code) in
                self.issuerToken = code
            }
            return
        }

        if sourceWallet.paymentType == "card" {
            
            let confirm_vc = CVVEntryViewController(nibName: "CVVEntryView", bundle: nil)
            confirm_vc.delegate = self
            self.popup = PopupDialog(viewController: confirm_vc)
            self.present(self.popup!, animated: true, completion: nil)
            return
        }

        // Check if pin is set
        let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        if (sourceWallet.paymentType == "card" || sourceWallet.paymentType == "momo") && (passcodeEnabled || fingerPrintEnabled ){
            self.startAuthenticationSection()
            return
        }
        //MARK: go to the amount section
        self.comfirmCompletion()
    }
    
    func comfirmCompletion()  {
        guard self.senderWallet != nil else {
            ViewControllerHelper.showPrompt(vc: self, message: "Transaction require a wallet to complete")
            return
        }
        
        //MARK: go to the amount section
        self.utilViewController.showActivityIndicator()
        if let sourceWallet = self.senderWallet {
            
            self.apiService.confirmTransaction(amount: Double(self.amount)!, methodType: self.methodTye, reference: self.transactionRef, sourceWallet: sourceWallet, cvv: self.cvv, issuerToken: self.issuerToken, targetType: self.contact!.network, targetAccount: self.contact!.number) { (response, status, message) in
                self.utilViewController.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                    // Check for link
                    if response != JSON.null {
                        let weburl = response["web_prompt"].stringValue
                        
                        if weburl != "" {
                            let destination = LQWebViewController()
                            destination.url = weburl
                            destination.title = "VERIFY WALLET OWNERSHIP"
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadCoach"), object: self); self.navigationController?.setViewControllers([(self.navigationController?.viewControllers.first)!, destination], animated: true)
                            //UIApplication.shared.open(URL(string: weburl)!, options: [:], completionHandler: nil)
                        } else {
                            ViewControllerHelper.showPrompt(vc: self, message: message, completion: { (isDone) in
                                if isDone {
                                    if self.isFavourite {
                                        FavouriteContact.saveRemote(data: self.contact!)
                                    }
                                    self.navigationController?.popToRootViewController(animated: true)
                                }
                            })
                        }
                    }
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            }
        } else {
            ViewControllerHelper.showPrompt(vc: self, message: "Choose the wallet fund from.")
        }
   
    }
    
    @objc func backAction(){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: wallet delegate
extension TransactionConfirmController : SelectWalletDelegate {
    
    func didSelectAddNew() {
        let destination = NewPaymentOptionController()
        destination.delegate = self
        let nav = UINavigationController(rootViewController: destination)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
        //self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func didSelectPicker() {
       self.walletOptions.delegate = self
       self.walletOptions.showOptions()
    }
    
    func didSelectWallet(item: Wallet) {
        
        let walletAmount = Double(item.amount)
        let payableAmount = Double(amount)
        
        if item.name == "Liquid Rewards/Refunds" && (Unicode.CanonicalCombiningClass(rawValue: UInt8(walletAmount!)) < Unicode.CanonicalCombiningClass(rawValue: UInt8(payableAmount!))) {
            let alert = UIAlertController(title: "\n", message: "You don’t have enough funds in your liquid rewards", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (isFinished) in
                self.didSelectPicker()
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }else{
            self.senderWallet = item
            self.setWallet(wallet: item)
        }
    }
    
    func setWallet(wallet: Wallet)  {
        self.amountCardView.walletUILable.setTitle(wallet.name, for: .normal)
        self.amountCardView.walletIconImageView.image = Mics.walletNetworkIcon(name: wallet.icon)
        
        self.nextButton.isEnabled = true
        self.nextButton.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        
        self.buyAirtime()
    }
}

//MARK: pin delegate
extension TransactionConfirmController: PinDelegate {
    
    func didSetPin() {
       self.comfirmCompletion()
    }
    
    func didFailed() {
        
    }
    
    func didCanceled() {
        
    }
}

extension TransactionConfirmController: CVVEntryDelegate {
    func proceed(cvv: String) {
        self.popup?.dismiss()
        self.cvv = cvv
        self.comfirmCompletion()
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
}
