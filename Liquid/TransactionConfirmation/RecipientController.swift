//
//  BuyArtimeController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ContactsUI
import PhoneNumberKit

class RecipientController: ControllerWithBack {
    
    let viewController = ViewControllerHelper()
    var provider: TargetIssuer?
    var name = ""
    var isArtime = false
    var isData = false
    var isReceivingMoney = false
    var isSendingMoney = false
    var isUtility = false
    var number = ""
    

    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "RECIPIENT DETAILS"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    lazy var phoneNumberTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Phone Number")
        textField.keyboardType = UIKeyboardType.numberPad
        textField.rightViewMode = UITextField.ViewMode.always
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: "AddContact")
        imageView.isUserInteractionEnabled = true
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        
        
        if self.isUtility {
            textField.placeholder = "Account number"
        } else {
            textField.rightView = imageView
        }
        
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.delegate = self
        return textField
    }()
    
    lazy var recipientNameTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Enter Description")
        textField.keyboardType = UIKeyboardType.alphabet
        return textField
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(buyAirtime), for: .touchUpInside)
        return button
    }()
    
    lazy var backButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "BackArrow"), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    lazy var favouriteSwitch: UISwitch = {
        let button = ViewControllerHelper.baseUISwitch()
        button.isOn = true
        return button
    }()
    
    let favouriteLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "Save as favourite"
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        if isSendingMoney {
            setUpNavigationBar(title: "Send \(name)")
        } else if isArtime {
          setUpNavigationBar(title: "Buy \(name) Airtime")
        } else if isReceivingMoney {
            setUpNavigationBar(title: "Request \(name)")
        } else if isData {
            setUpNavigationBar(title: "Buy \(name) Data")
        } else if isUtility {
           setUpNavigationBar(title: "Pay \(name) Bill")
        }
        
        self.view.addSubview(messageTextView)
        self.view.addSubview(phoneNumberTextField)
        self.view.addSubview(recipientNameTextField)
        self.view.addSubview(favouriteSwitch)
        self.view.addSubview(favouriteLabel)
        self.view.addSubview(backButton)
        self.view.addSubview(nextButton)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.phoneNumberTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.phoneNumberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.recipientNameTextField.anchorWithConstantsToTop(top: phoneNumberTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.recipientNameTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.favouriteSwitch.anchorWithConstantsToTop(top: recipientNameTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.favouriteSwitch.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        self.favouriteLabel.anchorWithConstantsToTop(top: recipientNameTextField.bottomAnchor, left: favouriteSwitch.trailingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 23, leftConstant: 8, bottomConstant: 0, rightConstant: -16)
        self.favouriteLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.backButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.backButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.nextButton.anchorWithConstantsToTop(top: nil, left: backButton.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
       self.toggleButton()
    }
    
    @objc func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    func toggleButton()  {
        let name = self.recipientNameTextField.text!
        let number = self.phoneNumberTextField.text!
        
        if (number.count == 3){
            
            print("prefix: ",number.prefix(3))
            print("prefix name: ",self.name)
            if (self.name == "Glo" && number.prefix(3) != "023"){
                ViewControllerHelper.showPromptWithCancel(vc: self, message: "The \(number.prefix(3)) you have entered is not a Glo number, Enter a correct number OR select a suitable network to continue.", nextActionTitle: "Edit") { (finished) in
                    self.navigationController?.popViewController(animated: true)
                }
                return
            }
            
            if (self.name == "MTN" && (number.prefix(3) != "024" && number.prefix(3) != "054" && number.prefix(3) != "055")) {
                ViewControllerHelper.showPromptWithCancel(vc: self, message: "The \(number.prefix(3)) you have entered is not an MTN number, Enter a correct number OR select a suitable network to continue.", nextActionTitle: "Edit") { (finished) in
                    self.navigationController?.popViewController(animated: true)
                }
                return
            }

            if (self.name == "Vodafone" && (number.prefix(3) != "020" && number.prefix(3) != "050")){
                ViewControllerHelper.showPromptWithCancel(vc: self, message: "The \(number.prefix(3)) you have entered is not a Vodafone number, Enter a correct number OR select a suitable network to continue.", nextActionTitle: "Edit") { (finished) in
                    self.navigationController?.popViewController(animated: true)
                }
            }

            if (self.name == "Airtel" && (number.prefix(3) != "026" || number.prefix(3) != "056")){
                ViewControllerHelper.showPromptWithCancel(vc: self, message: "The \(number.prefix(3)) you have entered is not an Airtel number, Enter a correct number OR select a suitable network to continue.", nextActionTitle: "Edit") { (finished) in
                    self.navigationController?.popViewController(animated: true)
                }
            }

            if (self.name == "Tigo" && (number.prefix(3) != "027" && number.prefix(3) != "057")){
                ViewControllerHelper.showPromptWithCancel(vc: self, message: "The \(number.prefix(3)) you have entered is not a Tigo number, Enter a correct number OR select a suitable network to continue.", nextActionTitle: "Edit") { (finished) in
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }
        
        if isSendingMoney || isArtime || isReceivingMoney {
            
            do {
                let phoneNumberKit = PhoneNumberKit()
                print("Validating: " + number)
                _ = try phoneNumberKit.parse(number, withRegion: "GH", ignoreType: true)
                
                if !name.isEmpty {
                    self.enableButton()
                } else {
                    self.disAbleButton()
                }
            } catch {
                self.disAbleButton()
            }
            
            /*
            if Mics.validNumber(number: "+233\(number)") && !name.isEmpty {
               self.enableButton()
            } else {
              self.disAbleButton()
            }*/
        }  else {
            if !number.isEmpty && !name.isEmpty {
               self.enableButton()
            }  else {
               self.disAbleButton()
            }
        }
    }
    
    func enableButton() {
        self.nextButton.isEnabled = true
        let color = UIColor.hex(hex: Key.primaryHexCode)
        self.nextButton.backgroundColor = color
    }
    
    func disAbleButton()  {
        self.nextButton.isEnabled = false
        let color = UIColor.gray
        self.nextButton.backgroundColor = color
    }
    //MARK:- action section
    
    @objc func tap(sender: UITapGestureRecognizer){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.predicateForEnablingContact = NSPredicate(format:"phoneNumbers.@count > 0",argumentArray: nil)
        contactPicker.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'", argumentArray: nil)
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    @objc func buyAirtime(){
        //MARK: go to the amount section
        if let provider = self.provider {
            
            //MARK: if its receving show input field
            if isReceivingMoney {
                let destination = FundAccountController()
                let contact = FavouriteContact()
                contact.id = 1
                contact.network = provider.rawValue
                contact.name = self.recipientNameTextField.text!
                contact.number = self.phoneNumberTextField.text!
                destination.contact = contact
                destination.isFavourite = self.favouriteSwitch.isOn
                destination.name = self.name
                destination.isRequestFunds = true
                self.navigationController?.pushViewController(destination, animated: true)
                return
            }
            
            //MARK: continue for the rest
            if self.isData {
                let destination = SelectBundleViewController()
                let contact = FavouriteContact()
                contact.id = 0
                contact.network = provider.rawValue
                contact.name = self.recipientNameTextField.text!
                contact.number = self.phoneNumberTextField.text!
                destination.contact = contact
                destination.isFavourite = self.favouriteSwitch.isOn
                destination.name = self.name
                
                destination.isData = self.isData
                destination.isArtime = self.isArtime
                destination.isUtility = self.isUtility
                destination.isSendingMoney = self.isSendingMoney
                destination.isReceivingMoney = self.isReceivingMoney
                
                self.navigationController?.pushViewController(destination, animated: true)
            } else {
                let destination = TransactionAmountController()
                let contact = FavouriteContact()
                contact.id = 0
                contact.network = provider.rawValue
                contact.name = self.recipientNameTextField.text!
                contact.number = self.phoneNumberTextField.text!
                destination.contact = contact
                destination.isFavourite = self.favouriteSwitch.isOn
                destination.name = self.name
                
                destination.isData = self.isData
                destination.isArtime = self.isArtime
                destination.isUtility = self.isUtility
                destination.isSendingMoney = self.isSendingMoney
                destination.isReceivingMoney = self.isReceivingMoney
                self.navigationController?.pushViewController(destination, animated: true)
            }
        }
    }
    
    @objc func backAction(){
       self.navigationController?.popViewController(animated: true)
    }
    
}

extension RecipientController : UITextFieldDelegate {
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "+0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if isSendingMoney || isArtime || isReceivingMoney {
            return newLength <= 13
        }
        return newLength <= 20
    }

}

extension RecipientController : CNContactPickerDelegate {
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        let contactName = CNContactFormatter.string(from: contact, style: .fullName) ?? ""
        for number in contact.phoneNumbers {
            let number = (number.value).value(forKey: "digits") as? String
            if let numberReal = number {
               self.phoneNumberTextField.text = numberReal
               self.recipientNameTextField.text = contactName
            }
        }
        picker.dismiss(animated: true, completion: nil)
        self.toggleButton()
    }
    
}



