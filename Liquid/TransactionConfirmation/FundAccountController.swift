//
//  FundAccountController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 07/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class FundAccountController: BaseScrollViewController {
    
    let buttonSize = CGFloat(60)
    var goal: Goal?
    var isGoalAddFunds = false
    var isGoalWithdrawFunds = false
    var isRequestFunds = false
    
    var contact: FavouriteContact?
    var isFavourite = false
    var name = ""
    
    let currencyUILabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .center
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 24)!)
        textView.text = "GHS"
        return textView
    }()
    
    let amountTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.text = ""
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 30)!)
        return textView
    }()
    
    let limitsTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.text = "Enter an amount between GHS 5.00 and\nGHS 2000.00"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Regular", size: 15)!)
        
        return textView
    }()
    
    lazy var continueButton: UIButton = {
        let button = ViewControllerHelper.skipBaseButton()
        button.setTitle("Continue", for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        button.setImage(UIImage(named: "ArrowRight"), for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.white.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 10,left: 20,bottom: 10,right: 5)
        button.layer.borderWidth = 1
        button.addTarget(self, action: #selector(continueToAdd), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    lazy var dotButton: UIButton = {
        let button = baseButton(number: ".")
        button.titleEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 20, right: 0)
        return button
    }()
    
    lazy var oneButton: UIButton = {
        let button = baseButton(number: "1")
        return button
    }()
    
    lazy var twoButton: UIButton = {
        let button = baseButton(number: "2")
        return button
    }()
    
    lazy var threeButton: UIButton = {
        let button = baseButton(number: "3")
        return button
    }()
    
    lazy var fourButton: UIButton = {
        let button = baseButton(number: "4")
        return button
    }()
    
    lazy var fiveButton: UIButton = {
        let button = baseButton(number: "5")
        return button
    }()
    
    lazy var sixButton: UIButton = {
        let button = baseButton(number: "6")
        return button
    }()
    
    lazy var sevenButton: UIButton = {
        let button = baseButton(number: "7")
        return button
    }()
    
    lazy var eightButton: UIButton = {
        let button = baseButton(number: "8")
        return button
    }()
    
    lazy var nineButton: UIButton = {
        let button = baseButton(number: "9")
        return button
    }()
    
    lazy var zeroButton: UIButton = {
        let button = baseButton(number: "0")
        return button
    }()
    
    lazy var clearButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Clear", for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderWidth = 0
        button.titleLabel?.textAlignment = .center
        button.layer.borderColor = UIColor.white.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
        button.addTarget(self, action: #selector(clearClicked), for: .touchUpInside)
        return button
    }()


    func baseButton(number: String) -> UIButton {
        let button = ViewControllerHelper.plainButton()
        button.setTitle(number, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = buttonSize/2
        button.titleLabel?.textAlignment = .center
        button.contentHorizontalAlignment = .center
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 30)!)
        button.addTarget(self, action: #selector(actionClicked(_:)), for: .touchUpInside)
        return button
    }
    
    
    override func setUpView() {
        super.setUpView()
        if self.isGoalAddFunds {
            setUpNavigationBarWhiteExtra(title: "Fund Goal")
        } else {
            setUpNavigationBarWhiteExtra(title: "Fund Account")
        }
        
        
        if self.isGoalWithdrawFunds {
           setUpNavigationBarWhiteExtra(title: "Withdraw Funds")
        }
        if self.isRequestFunds {
            setUpNavigationBarWhiteExtra(title: "Request Funds")
        }
      
        self.setUpViews()
    }
    
    func setUpViews()  {
        
       self.view.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
       self.scrollView.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        
        
       self.scrollView.addSubview(currencyUILabel)
       self.scrollView.addSubview(amountTextView)
        self.scrollView.addSubview(limitsTextView)
       self.view.addSubview(continueButton)
       self.scrollView.addSubview(twoButton)
       self.scrollView.addSubview(oneButton)
       self.scrollView.addSubview(threeButton)
       self.scrollView.addSubview(fourButton)
       self.scrollView.addSubview(fiveButton)
       self.scrollView.addSubview(sixButton)
       self.scrollView.addSubview(sevenButton)
       self.scrollView.addSubview(eightButton)
       self.scrollView.addSubview(nineButton)
       self.scrollView.addSubview(zeroButton)
       self.scrollView.addSubview(clearButton)
       self.scrollView.addSubview(dotButton)

       let width = self.view.frame.width - 32
       self.currencyUILabel.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
       self.currencyUILabel.widthAnchor.constraint(equalToConstant: width).isActive = true
       self.currencyUILabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
       
        self.amountTextView.anchorWithConstantsToTop(top: currencyUILabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.amountTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.amountTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
       
        self.limitsTextView.anchorWithConstantsToTop(top: amountTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.limitsTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.limitsTextView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        //MARK: first row
        self.twoButton.anchorWithConstantsToTop(top: limitsTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.twoButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.twoButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.twoButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        self.oneButton.anchorWithConstantsToTop(top: limitsTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.oneButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.oneButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.oneButton.trailingAnchor.constraint(equalTo: twoButton.leadingAnchor, constant: -30).isActive = true
        
        self.threeButton.anchorWithConstantsToTop(top: limitsTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.threeButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.threeButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.threeButton.leadingAnchor.constraint(equalTo: twoButton.trailingAnchor, constant: 30).isActive = true
        
        //MARK: second row
        self.fiveButton.anchorWithConstantsToTop(top: twoButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.fiveButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.fiveButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.fiveButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        self.fourButton.anchorWithConstantsToTop(top: twoButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.fourButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.fourButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.fourButton.trailingAnchor.constraint(equalTo: fiveButton.leadingAnchor, constant: -30).isActive = true
        
        self.sixButton.anchorWithConstantsToTop(top: twoButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.sixButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.sixButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.sixButton.leadingAnchor.constraint(equalTo: fiveButton.trailingAnchor, constant: 30).isActive = true
        
        
        //MARK: third row
        self.eightButton.anchorWithConstantsToTop(top: sixButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.eightButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.eightButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.eightButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        self.sevenButton.anchorWithConstantsToTop(top: sixButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.sevenButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.sevenButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.sevenButton.trailingAnchor.constraint(equalTo: eightButton.leadingAnchor, constant: -30).isActive = true
        
        self.nineButton.anchorWithConstantsToTop(top: sixButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.nineButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.nineButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.nineButton.leadingAnchor.constraint(equalTo: eightButton.trailingAnchor, constant: 30).isActive = true
        

        //MARK: fourth row
        self.zeroButton.anchorWithConstantsToTop(top: nineButton.bottomAnchor, left: nil, bottom: scrollView.bottomAnchor, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: -80, rightConstant: 0)
        self.zeroButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.zeroButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.zeroButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        self.dotButton.anchorWithConstantsToTop(top: nineButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.dotButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.dotButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.dotButton.trailingAnchor.constraint(equalTo: zeroButton.leadingAnchor, constant: -30).isActive = true
        
        self.clearButton.anchorWithConstantsToTop(top: nineButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.clearButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        self.clearButton.trailingAnchor.constraint(equalTo: nineButton.trailingAnchor, constant: 0).isActive = true
        self.clearButton.leadingAnchor.constraint(equalTo: zeroButton.trailingAnchor, constant: 16).isActive = true
        
        
       self.continueButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
       self.continueButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    }
    
    @objc func continueToAdd(){
        let amount = amountTextView.text!
        if amount.isEmpty {
            return
        }
        
        //MARK: requesting for funds section
        if isRequestFunds {
            print("REQUESTING DONE")
            return
        }
        
        let wallets = Wallet.all()
        if wallets.isEmpty {
            self.navigationController?.pushViewController(NewPaymentOptionController(), animated: true)
        }  else {
            let destination = ConfirmFundController()
            destination.amount = amount
            destination.isGoalWithdrawFunds = self.isGoalWithdrawFunds
            destination.isGoalAddFunds = self.isGoalAddFunds
            destination.goal = self.goal
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    @objc func actionClicked(_ sender: UIButton){
        let textAlreadyIn = self.amountTextView.text!
        let text = sender.currentTitle!
        if textAlreadyIn.contains(".") && text == "." {
            return
        }
        let allText = "\(textAlreadyIn)\(text)"
        self.amountTextView.text = "\(allText)"
        self.didPressNumber()
    }
    
    @objc func clearClicked(_ sender: UIButton){
        self.amountTextView.text = ""
        self.didPressNumber()
    }
    
    func didPressNumber(){
        if let amount = Int(self.amountTextView.text) {
            if amount < 5 || amount > 2000 {
                self.continueButton.isHidden = true
            } else {
                self.continueButton.isHidden = false
            }
        } else {
            self.continueButton.isHidden = true
        }
    }
}


