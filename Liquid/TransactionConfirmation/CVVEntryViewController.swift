//
//  CVVEntryViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 11/02/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class CVVEntryViewController: UIViewController {
    var delegate: CVVEntryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func proceed(_ sender: Any) {
        let cvvTxt = self.view.viewWithTag(1) as! UITextField
        self.delegate!.proceed(cvv: cvvTxt.text!)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.delegate?.dismiss()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
