//
//  BankTransferDetailVC.swift
//  Liquid
//
//  Created by Benjamin Acquah on 13/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class BankTransferDetailVC: BaseScrollViewController {
    
    var voucher = ""
    let apiService = ApiService()
    let utilViewController = ViewControllerHelper()
    
    var smsValue = ""
    var emailValue = ""
    var bothLists = ""
    
    var iconImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "bankTransfers")
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    var pageDescription: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "To fund your goal with a bank transfer, Please send the money to the bank account below:"
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        return lbl
    }()
    
    var referenceSection: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "TRANSACTION REFERENCE"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textColor = .lightGray
        lbl.backgroundColor = UIColor.hex(hex: "#fafafa")
        return lbl
    }()
    
    var referenceLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Reference"
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        return lbl
    }()
    
    lazy var referenceCode: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = voucher
        lbl.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.bold)
        lbl.textColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.textAlignment = .right
        return lbl
    }()
    
    var bankAccountSection: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "BANK ACCOUNT DETAILS"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textColor = .lightGray
        lbl.backgroundColor = UIColor.hex(hex: "#fafafa")
        return lbl
    }()
    
    //acount name
    var accountNameLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Account Name"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()

    var accountName: UILabel = {
        let user = User.getUser()!
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Liquid Financial Services"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    //seperator
    var accountNameSeperatorView: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    //account number
    var accountNumberLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Account Number"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    var accountNumber: UILabel = {
        let user = User.getUser()!
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "2011120128111"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    //seperator
    var accountNumberSeperatorView: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    //bank name
    var bankNameLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Bank Name"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    var bankName: UILabel = {
        let user = User.getUser()!
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Guarantee Trust Bank"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    //seperator
    var bankNameSeperatorView: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    //bank branch
    var bankBranchLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Branch Name"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    var bankBranchName: UILabel = {
        let user = User.getUser()!
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Ridge (Head Office)"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .right
        return lbl
    }()
    
    //seperator
    var bottomSeperatorView: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    //Bottom View
    var bottomView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    //seperator line
    var seperator: UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    //instruction label
    var sendInstructionLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Send me this information via"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    //email label
    var emailLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Email"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    //email switch
    lazy var emailSwitch: UISwitch = {
        let email = ViewControllerHelper.baseUISwitch()
        email.setOn(false, animated: true)
        return email
    }()
    
    //sms label
    var smsLabel: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "SMS"
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    //sms switch
    var smsSwitch: UISwitch = {
        let sms = ViewControllerHelper.baseUISwitch()
        sms.setOn(false, animated: true)
        return sms
    }()
    
    //done btn
    lazy var doneBtn: UIButton = {
        let done = ViewControllerHelper.baseButton()
        done.setTitle("Done", for: .normal)
        done.translatesAutoresizingMaskIntoConstraints = false
        done.addTarget(self, action: #selector(doneBtnAction), for: .touchUpInside)
        return done
    }()
    
    override func setUpView() {
        super.setUpView()
        
        self.scrollView.addSubview(iconImage)
        self.scrollView.addSubview(pageDescription)
        self.scrollView.addSubview(referenceSection)
        self.scrollView.addSubview(referenceLabel)
        self.scrollView.addSubview(referenceCode)
        self.scrollView.addSubview(bankAccountSection)
        
        self.scrollView.addSubview(accountNameLabel)
        self.scrollView.addSubview(accountName)
        self.scrollView.addSubview(accountNameSeperatorView)
        
        self.scrollView.addSubview(accountNumberLabel)
        self.scrollView.addSubview(accountNumber)
        self.scrollView.addSubview(accountNumberSeperatorView)
        
        self.scrollView.addSubview(bankNameLabel)
        self.scrollView.addSubview(bankName)
        self.scrollView.addSubview(bankNameSeperatorView)
        
        self.scrollView.addSubview(bankBranchLabel)
        self.scrollView.addSubview(bankBranchName)
        
        self.scrollView.addSubview(bottomView)
        self.scrollView.addSubview(seperator)
        self.scrollView.addSubview(sendInstructionLabel)
        self.bottomView.addSubview(doneBtn)
        self.bottomView.addSubview(emailLabel)
        self.bottomView.addSubview(emailSwitch)
        self.bottomView.addSubview(smsLabel)
        self.bottomView.addSubview(smsSwitch)
        
        iconImage.topAnchor.constraint(equalTo: self.scrollView.topAnchor, constant: 8).isActive = true
        iconImage.leftAnchor.constraint(equalTo: self.scrollView.leftAnchor, constant: 12).isActive = true
        iconImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        iconImage.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        pageDescription.topAnchor.constraint(equalTo: self.iconImage.topAnchor, constant: 0).isActive = true
        pageDescription.leftAnchor.constraint(equalTo: self.iconImage.rightAnchor, constant: 8).isActive = true
        pageDescription.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -8).isActive = true
        pageDescription.bottomAnchor.constraint(equalTo: self.iconImage.bottomAnchor, constant: 0).isActive = true
        
        referenceSection.topAnchor.constraint(equalTo: self.iconImage.bottomAnchor, constant: 16).isActive = true
        referenceSection.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        referenceSection.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        referenceSection.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        referenceLabel.topAnchor.constraint(equalTo: self.referenceSection.bottomAnchor, constant: 4).isActive = true
        referenceLabel.leftAnchor.constraint(equalTo: self.referenceSection.leftAnchor, constant: 8).isActive = true
        referenceLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        referenceLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        referenceCode.topAnchor.constraint(equalTo: self.referenceLabel.topAnchor, constant: 0).isActive = true
        referenceCode.leftAnchor.constraint(equalTo: self.referenceLabel.rightAnchor, constant: 8).isActive = true
        referenceCode.rightAnchor.constraint(equalTo: self.referenceSection.rightAnchor, constant: -8).isActive = true
        referenceCode.bottomAnchor.constraint(equalTo: self.referenceLabel.bottomAnchor, constant: 0).isActive = true
        
        bankAccountSection.topAnchor.constraint(equalTo: self.referenceLabel.bottomAnchor, constant: 8).isActive = true
        bankAccountSection.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        bankAccountSection.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        bankAccountSection.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //bank name
        accountNameLabel.topAnchor.constraint(equalTo: bankAccountSection.bottomAnchor, constant: 8).isActive = true
        accountNameLabel.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 8).isActive = true
        accountNameLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2).isActive = true
        accountNameLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        accountName.topAnchor.constraint(equalTo: accountNameLabel.topAnchor, constant: 0).isActive = true
        accountName.leftAnchor.constraint(equalTo: accountNameLabel.rightAnchor, constant: 0).isActive = true
        accountName.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        accountName.bottomAnchor.constraint(equalTo: accountNameLabel.bottomAnchor, constant: 0).isActive = true
        
        accountNameSeperatorView.topAnchor.constraint(equalTo: accountName.bottomAnchor, constant: 0).isActive = true
        accountNameSeperatorView.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 0).isActive = true
        accountNameSeperatorView.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        accountNameSeperatorView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        //account number
        accountNumberLabel.topAnchor.constraint(equalTo: accountNameSeperatorView.bottomAnchor, constant: 8).isActive = true
        accountNumberLabel.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 8).isActive = true
        accountNumberLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2).isActive = true
        accountNumberLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        accountNumber.topAnchor.constraint(equalTo: accountNumberLabel.topAnchor, constant: 0).isActive = true
        accountNumber.leftAnchor.constraint(equalTo: accountNumberLabel.rightAnchor, constant: 0).isActive = true
        accountNumber.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        accountNumber.bottomAnchor.constraint(equalTo: accountNumberLabel.bottomAnchor, constant: 0).isActive = true
        
        accountNumberSeperatorView.topAnchor.constraint(equalTo: accountNumber.bottomAnchor, constant: 0).isActive = true
        accountNumberSeperatorView.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 0).isActive = true
        accountNumberSeperatorView.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        accountNumberSeperatorView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        //bank name
        bankNameLabel.topAnchor.constraint(equalTo: accountNumberSeperatorView.bottomAnchor, constant: 8).isActive = true
        bankNameLabel.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 8).isActive = true
        bankNameLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2).isActive = true
        bankNameLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true

        bankName.topAnchor.constraint(equalTo: bankNameLabel.topAnchor, constant: 0).isActive = true
        bankName.leftAnchor.constraint(equalTo: bankNameLabel.rightAnchor, constant: 0).isActive = true
        bankName.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        bankName.bottomAnchor.constraint(equalTo: bankNameLabel.bottomAnchor, constant: 0).isActive = true

        bankNameSeperatorView.topAnchor.constraint(equalTo: bankName.bottomAnchor, constant: 0).isActive = true
        bankNameSeperatorView.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 0).isActive = true
        bankNameSeperatorView.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        bankNameSeperatorView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        //branch name
        bankBranchLabel.topAnchor.constraint(equalTo: bankNameSeperatorView.bottomAnchor, constant: 8).isActive = true
        bankBranchLabel.leftAnchor.constraint(equalTo: bankAccountSection.leftAnchor, constant: 8).isActive = true
        bankBranchLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width / 2).isActive = true
        bankBranchLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true

        bankBranchName.topAnchor.constraint(equalTo: bankBranchLabel.topAnchor, constant: 0).isActive = true
        bankBranchName.leftAnchor.constraint(equalTo: bankBranchLabel.rightAnchor, constant: 0).isActive = true
        bankBranchName.rightAnchor.constraint(equalTo: bankAccountSection.rightAnchor, constant: 0).isActive = true
        bankBranchName.bottomAnchor.constraint(equalTo: bankBranchLabel.bottomAnchor, constant: 0).isActive = true
        
        //bottom view
        bottomView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -70).isActive = true
        bottomView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        bottomView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        //done button
        doneBtn.bottomAnchor.constraint(equalTo: self.bottomView.bottomAnchor, constant: -8).isActive = true
        doneBtn.leftAnchor.constraint(equalTo: self.bottomView.leftAnchor, constant: 8).isActive = true
        doneBtn.rightAnchor.constraint(equalTo: self.bottomView.rightAnchor, constant: -8).isActive = true
        doneBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        //email label
        emailLabel.leftAnchor.constraint(equalTo: self.doneBtn.leftAnchor, constant: 0).isActive = true
        emailLabel.bottomAnchor.constraint(equalTo: self.doneBtn.topAnchor, constant: -8).isActive = true
        emailLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        emailLabel.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        emailSwitch.leftAnchor.constraint(equalTo: emailLabel.rightAnchor, constant: 8).isActive = true
        emailSwitch.bottomAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: -8).isActive = true
        emailSwitch.heightAnchor.constraint(equalToConstant: 20).isActive = true
        emailSwitch.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        //sms label
        smsLabel.leftAnchor.constraint(equalTo: self.emailSwitch.rightAnchor, constant: 8).isActive = true
        smsLabel.bottomAnchor.constraint(equalTo: self.doneBtn.topAnchor, constant: -8).isActive = true
        smsLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        smsLabel.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        smsSwitch.leftAnchor.constraint(equalTo: smsLabel.rightAnchor, constant: 8).isActive = true
        smsSwitch.bottomAnchor.constraint(equalTo: smsLabel.bottomAnchor, constant: -8).isActive = true
        smsSwitch.heightAnchor.constraint(equalToConstant: 20).isActive = true
        smsSwitch.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        seperator.topAnchor.constraint(equalTo: self.bottomView.topAnchor, constant: 0).isActive = true
        seperator.leftAnchor.constraint(equalTo: self.bottomView.leftAnchor, constant: 0).isActive = true
        seperator.rightAnchor.constraint(equalTo: self.bottomView.rightAnchor, constant: 0).isActive = true
        seperator.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
        
        sendInstructionLabel.topAnchor.constraint(equalTo: self.seperator.bottomAnchor, constant: 0).isActive = true
        sendInstructionLabel.leftAnchor.constraint(equalTo: self.seperator.leftAnchor, constant: 8).isActive = true
        sendInstructionLabel.rightAnchor.constraint(equalTo: self.seperator.rightAnchor, constant: 0).isActive = true
        sendInstructionLabel.bottomAnchor.constraint(equalTo: self.smsLabel.topAnchor, constant: 0).isActive = true

    }
    
    @objc func doneBtnAction(){
        
        if (emailSwitch.isOn && smsSwitch.isOn){
            self.bothLists = "email,sms"
        }
        
        if (emailSwitch.isOn && !smsSwitch.isOn){
            self.bothLists = "email"
        }
        
        if (smsSwitch.isOn && !emailSwitch.isOn){
            self.bothLists = "sms"
        }
        
        self.share_via(destinations: bothLists)
    }
    
    func share_via(destinations: String) {
        self.utilViewController.showActivityIndicator()
        apiService.share_voucher_via(destination_values: destinations, voucher: self.voucher) { (response, status, message) in
            if status == ApiCallStatus.SUCCESS {
                self.utilViewController.hideActivityIndicator()
                self.navigationController?.popToRootViewController(animated: true)
            }else{
                self.utilViewController.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
}
