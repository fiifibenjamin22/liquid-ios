//
//  BilldetailCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class BilldetailCell: BaseCell {
    
    var item: BillDetail? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = "\(unwrapedItem.name)"
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Thin, size: 18)!)
        return textView
    }()
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(messageTextView)
        
        let frameHeight = frame.height - 23
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
    }
}
