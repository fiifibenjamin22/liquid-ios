//
//  SelectBundleViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class SelectBundleViewController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let cellSection = "cellHeading"
    
    var billDetails = [BillDetail]()
    
    var contact: FavouriteContact?
    var isFavourite = false
    
    var provider: TargetIssuer?
    var walletType: TargetIssuer?
    var name = ""
    
    let utilViewController = ViewControllerHelper()
    
    var isArtime = false
    var isData = false
    var isReceivingMoney = false
    var isSendingMoney = false
    var isUtility = false
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //MARK:- parse all provides
        self.collectionView.reloadData()
        self.setUpLayout()
        
        self.utilViewController.showActivityIndicator()
        ApiService().getBillDetails(issuer: contact!.network, identifier: contact!.number, completion: {(status, item, message) in
            print(item)
            self.utilViewController.hideActivityIndicator()
            for bundle in item["options"].arrayValue {
                var bd = BillDetail()
                bd.name = bundle["name"].stringValue
                bd.bundle = bundle["bundle"].stringValue
                bd.value = bundle["value"].intValue
                self.billDetails.append(bd)
            }
            print(message)
            self.collectionView.reloadData()
        })
    }
    
    @objc func receivedEndedNotification(notification: Notification){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpLayout()  {
        self.view.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.collectionView.register(BilldetailCell.self, forCellWithReuseIdentifier: collectionId)
    }
    
}

extension SelectBundleViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return billDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! BilldetailCell
        cell.item = self.billDetails[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(65)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let amount = self.billDetails[indexPath.row].value
        if  let contact = self.contact {
            let destination = TransactionConfirmController()
            destination.contact = contact
            destination.amount = "\(amount)"
            destination.isFavourite = self.isFavourite
            
            
            destination.name = self.name
            destination.isData = self.isData
            destination.isArtime = self.isArtime
            destination.isUtility = self.isUtility
            destination.isSendingMoney = self.isSendingMoney
            destination.isReceivingMoney = self.isReceivingMoney
            
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
}


