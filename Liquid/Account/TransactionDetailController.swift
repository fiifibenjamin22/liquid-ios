//
//  TransactionDetailController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class TransactionDetailController: BaseScrollViewController {
    
    var transactionId = 0
    let utilViewHolder = ViewControllerHelper()
    let apiService = ApiService()
    
    
    let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let recipientView: RecipientView = {
        let recipientUIView = RecipientView()
        recipientUIView.translatesAutoresizingMaskIntoConstraints = false
        return recipientUIView
    }()
    
    let detailTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "DETAILS"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let detailSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let amountLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "Amount"
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let amountField: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "GHS 100.00"
        return textView
    }()
    
    let amountSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let feesLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "Fees"
        return textView
    }()
    
    let feesField: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "GHS 1.0"
        return textView
    }()
    
    let feesSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let totalSumLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.font = UIFont(name: Font.Roboto.Bold, size: 14)
        textView.text = "Total Sum"
        return textView
    }()
    
    let totalSumField: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "GHS 11.0"
        return textView
    }()
    
    let totalSumSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let statusTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "STATUS"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let statusSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let dateLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "Date"
        return textView
    }()
    
    let dateField: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let dateSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let transactionIdLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "Transaction ID"
        return textView
    }()
    
    let transactionIdField: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let transactionIdSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let transactionStatusLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "Status"
        return textView
    }()
    
    let transactionStatusField: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .right
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let transactionStatusSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let paidTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "PAID WITH"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let paidSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let paidWithLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.text = "********"
        return textView
    }()
    
    let paidWithField: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let paidWithSplitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        setUpNavigationBarWhite(title: "Transaction Detail")
        
        self.getData(id: transactionId)
    }
    
    
    func getData(id:Int)  {
        self.utilViewHolder.showActivityIndicator()
        self.apiService.transactionDetail(id: self.transactionId) { (status, transaction, withMessage) in
            self.utilViewHolder.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
               print("ITEM \(transaction!)")
               self.addLayout(item: transaction!)
            }
        }
    }
    
    func addLayout(item: TransactionHistory)  {
        self.scrollView.addSubview(container)
        self.container.addSubview(recipientView)
        self.container.addSubview(detailTextView)
        self.container.addSubview(detailSplitLine)
        self.container.addSubview(amountLabel)
        self.container.addSubview(amountField)
        self.container.addSubview(amountSplitLine)
        
        self.container.addSubview(feesLabel)
        self.container.addSubview(feesField)
        self.container.addSubview(feesSplitLine)
        
        self.container.addSubview(totalSumLabel)
        self.container.addSubview(totalSumField)
        self.container.addSubview(totalSumSplitLine)
        
        self.container.addSubview(statusTextView)
        self.container.addSubview(statusSplitLine)
        
        self.container.addSubview(dateLabel)
        self.container.addSubview(dateField)
        self.container.addSubview(dateSplitLine)
        
        self.container.addSubview(transactionIdLabel)
        self.container.addSubview(transactionIdField)
        self.container.addSubview(transactionIdSplitLine)
        
        self.container.addSubview(transactionStatusLabel)
        self.container.addSubview(transactionStatusField)
        self.container.addSubview(transactionStatusSplitLine)
        
        self.container.addSubview(paidTextView)
        self.container.addSubview(paidSplitLine)
        
        self.container.addSubview(paidWithLabel)
        self.container.addSubview(paidWithField)
        self.container.addSubview(paidWithSplitLine)
        
        let width = self.view.frame.width
        
        self.container.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.container.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.recipientView.anchorToTop(top: container.topAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.recipientView.heightAnchor.constraint(equalToConstant: CGFloat(ConstantHeight.RECIPIENT_CARD_HEIGHT.rawValue)).isActive = true
        self.recipientView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.detailTextView.anchorToTop(top: recipientView.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.detailTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.detailTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.detailSplitLine.anchorToTop(top: detailTextView.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.detailSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.detailSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        //MARK: amount section here
        self.amountLabel.anchorToTop(top: detailSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.amountLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.amountLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.amountField.anchorToTop(top: detailSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.amountField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.amountField.widthAnchor.constraint(equalToConstant: width - 150).isActive = true
        
        self.amountSplitLine.anchorToTop(top: amountLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.amountSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.amountSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        //MARK: fees section here
        self.feesLabel.anchorToTop(top: amountSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.feesLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.feesLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.feesField.anchorToTop(top: amountSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.feesField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.feesField.widthAnchor.constraint(equalToConstant: width - 150).isActive = true
        
        self.feesSplitLine.anchorToTop(top: feesLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.feesSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.feesSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK: total fees section here
        self.totalSumLabel.anchorToTop(top: feesSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.totalSumLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.totalSumLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.totalSumField.anchorToTop(top: feesSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.totalSumField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.totalSumField.widthAnchor.constraint(equalToConstant: width - 150).isActive = true
        
        self.totalSumSplitLine.anchorToTop(top: totalSumLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.totalSumSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.totalSumSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        //MARK: status section
        self.statusTextView.anchorToTop(top: totalSumSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.statusTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.statusTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.statusSplitLine.anchorToTop(top: statusTextView.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.statusSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.statusSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        //MARK: date section here
        self.dateLabel.anchorToTop(top: statusSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.dateLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.dateLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.dateField.anchorToTop(top: statusSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.dateField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.dateField.widthAnchor.constraint(equalToConstant: width - 150).isActive = true
        
        self.dateSplitLine.anchorToTop(top: dateLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.dateSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.dateSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK: ID section here
        self.transactionIdLabel.anchorToTop(top: dateSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.transactionIdLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.transactionIdLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.transactionIdField.anchorToTop(top: dateSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.transactionIdField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.transactionIdField.widthAnchor.constraint(equalToConstant: width - 150).isActive = true
        
        self.transactionIdSplitLine.anchorToTop(top: transactionIdLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.transactionIdSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.transactionIdSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        
        //MARK: transaction status section here
        self.transactionStatusLabel.anchorToTop(top: transactionIdSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.transactionStatusLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.transactionStatusLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.transactionStatusField.anchorToTop(top: transactionIdSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.transactionStatusField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.transactionStatusField.widthAnchor.constraint(equalToConstant: width - 150).isActive = true
        
        self.transactionStatusSplitLine.anchorToTop(top: transactionStatusLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.transactionStatusSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.transactionStatusSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK: payment section
        self.paidTextView.anchorToTop(top: transactionStatusSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.paidTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.paidTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.paidSplitLine.anchorToTop(top: paidTextView.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.paidSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.paidSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        //MARK: paid with section here
        self.paidWithLabel.anchorToTop(top: paidSplitLine.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.paidWithLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.paidWithLabel.widthAnchor.constraint(equalToConstant: width - 50).isActive = true
        
        //self.paidWithField.anchorToTop(top: paidSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor)
        self.paidWithField.anchorWithConstantsToTop(top: paidSplitLine.bottomAnchor, left: nil, bottom: nil, right: container.trailingAnchor, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.paidWithField.heightAnchor.constraint(equalToConstant: 42).isActive = true
        self.paidWithField.widthAnchor.constraint(equalToConstant: 42).isActive = true
        
        self.paidWithSplitLine.anchorToTop(top: paidWithLabel.bottomAnchor, left: container.leadingAnchor, bottom: nil, right: nil)
        self.paidWithSplitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.paidWithSplitLine.widthAnchor.constraint(equalToConstant: width).isActive = true
        
       
        //setting section
        self.amountField.text = "\(item.currency) \(item.amount)"
        self.totalSumField.text = "\(item.currency) \(item.totalAmount)"
        self.feesField.text = "\(item.currency) \(item.fees)"
        self.dateField.text = "\(item.datePerformed)"
        self.transactionIdField.text = "\(item.message)"
        self.transactionStatusField.text = "\(item.status)"
        self.paidWithLabel.text = "\(item.senderName)"
        self.paidWithField.image = Mics.networkIcon(name: item.sourceAccount)
        
        let messageHeader = "\(item.recipientName)\n".formatAsAttributed(fontSize: 20, color: UIColor.darkText)
        let messageBody = "\(item.type)".formatAsAttributed(fontSize: 16, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        self.recipientView.nameTextView.attributedText = result
        
        //self.recipientView.nameTextView.text = "\(item.recipientName)\n\(item.type)"
        if  !(item.recipientProfile.isEmpty) {
            self.self.recipientView.iconImageView.af_setImage(
                withURL: URL(string: (item.recipientProfile))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.self.recipientView.iconImageView.image = Mics.userPlaceHolder()
        }
        
        //MARK: status label color
        if item.status == Key.statusFailed {
            self.transactionStatusField.textColor = UIColor.hex(hex: Key.colorTransactionfailed)
        } else if item.status == Key.statusPending {
            self.transactionStatusField.textColor = UIColor.hex(hex: Key.colorTransactionPending)
        } else {
           self.transactionStatusField.textColor = UIColor.hex(hex: Key.colorTransactionSuccess)
        }
        
    }
    
    
    
}
