//
//  EditPhoneNumberController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import ADCountryPicker

class EditPhoneNumberController: ControllerWithBack {
    
    var number = ""
    let apiService = ApiService()
    let utilViewHolder = ViewControllerHelper()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "Please enter your new phone number"
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    lazy var phoneNumberView: PhoneNumberView = {
        let view = PhoneNumberView()
        view.selectPicker = self
        view.delegate = self
        return view
    }()
    
    lazy var passwordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
      
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    let passwordHintLable: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textAlignment = .left
        textView.textColor = UIColor.red
        textView.text = AppConstants.passwordHintMessage
        textView.isHidden = true
        return textView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Change Phone Number")
        
        self.setUpUI()
        
    }
    
    func setUpUI()  {
      
        self.view.addSubview(messageTextView)
        self.view.addSubview(phoneNumberView)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(passwordHintLable)
        self.view.addSubview(nextButton)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.phoneNumberView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.phoneNumberView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.passwordTextField.anchorWithConstantsToTop(top: phoneNumberView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.passwordHintLable.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordHintLable.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
    }
    
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        self.toggleButtonStatus()
        self.passwordHintLable.isHidden = true
        if textField == self.passwordTextField && !Validator.isValidPassword(password: textField.text!) {
            self.passwordHintLable.isHidden = true
        }
    }
    
    //MARK - auto advance input fields
    @objc func toggleButtonStatus(){
        let number = self.phoneNumberView.phoneNumber
        let password = self.passwordTextField.text!
        if Validator.validNumber(number: number) && Validator.isValidPassword(password: password) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }   else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func nextAction(){
        let user = User.getUser()!
        self.number = self.phoneNumberView.phoneNumber
        let password = self.passwordTextField.text!
        
        if  self.number == user.phone {
            ViewControllerHelper.showPrompt(vc: self, message: "New number appears to be the same as old number.")
            return
        }
        
        self.utilViewHolder.showActivityIndicator()
        self.apiService.checkNumber(number: self.number, justCheck: true) { (status, withMessage, response) in
            if status == ApiCallStatus.WARNING {
                
                print("phone number: \(self.number)")
                
                self.apiService.changeNumberStepOne(newNumber: self.number, password: password) { (status, withMessage) in
                    self.utilViewHolder.hideActivityIndicator()
                    
                    let response = String(withMessage)
                    
                    let responseJson = response.convertToDictionary()
                    let errorCheck = responseJson?["error"]
                    
                    let errorToString = errorCheck as? String ?? ""
//                    let splitString = errorToString.split(separator: ".")
//                    guard let err = splitString[1] else {return}
//                    print(err)
                    
                    if (errorToString == "err.PasswordInvalid"){
                        ViewControllerHelper.showPrompt(vc: self, message: "Your password is invalid, kindly check and enter again")
                        
                    }else{
                        if status == ApiCallStatus.SUCCESS {
                            let destination = VerificationCodeController()
                            destination.number = self.number
                            destination.isChangingNumber = true
                            self.navigationController?.pushViewController(destination, animated: true)
                        } else {
                            ViewControllerHelper.showPrompt(vc: self, message: "Something went wrong changing your phone number. Try again  later.")
                        }
                    }
                }
            } else if status == ApiCallStatus.SUCCESS {
                 self.utilViewHolder.hideActivityIndicator()
                  ViewControllerHelper.showPrompt(vc: self, message: "Number already in use, please try again with different number.")
            } else {
                 self.utilViewHolder.hideActivityIndicator()
                 ViewControllerHelper.showPrompt(vc: self, message: "Something went wrong changing your phone number. Try again  later.")
            }
        }
       
    }
}

extension EditPhoneNumberController: TextChangeDelegate {
    func didChange(text: String) {
        toggleButtonStatus()
    }
}


extension EditPhoneNumberController: CountryPickerDelegate,ADCountryPickerDelegate {
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String) {
        
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        let text = Mics.flag(country: code) + " " + dialCode
        self.phoneNumberView.countryButton.setTitle(text, for: .normal)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func clickCountryPicker() {
        ViewControllerHelper.presentCountryPicker(vc: self, delegate: self)
    }
}
