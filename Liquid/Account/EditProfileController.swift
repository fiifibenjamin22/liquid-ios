//
//  EditProfileController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 21/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import CropViewController
import RealmSwift

class EditProfileController: BaseScrollViewController {
    
    var imagePicker : UIImagePickerController!
    var profileImage: UIImage?
    let apiService = ApiService()
    let user = User.getUser()!
    let realm = try! Realm()
    let utilViewHelper = ViewControllerHelper()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let profileCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ProfileWave")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let editIConImageView: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.clipsToBounds = true
        button.layer.masksToBounds = true
        button.setImage(UIImage(named: "CameraEditIcon"), for: .normal)
        button.addTarget(self, action: #selector(pickImage(_:)), for: .touchUpInside)
        return button
    }()
    
    let headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var numberTextViewLabel: UITextView = {
        return baseLabel(title: "PHONE NUMBER")
    }()
    
    lazy var numberVerifiedLabel: UITextView = {
        let textView = baseLabel(title: "VERIFIED")
        textView.textColor = UIColor.hex(hex: "A9BF90")
        textView.textAlignment = .right
        textView.font = UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.heavy)
        return textView
    }()
    
    lazy var numberCardView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var numberTextView: UITextView = {
        let user = User.getUser()!
        let textView = baseLabel(title: user.phone)
        textView.textColor = UIColor.darkText
        return textView
    }()
    
    lazy var numberEditButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.addTarget(self, action: #selector(changePhoneNumber(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var emailTextViewLabel: UITextView = {
        return baseLabel(title: "EMAIL")
    }()
    
    lazy var emailVerifiedLabel: UITextView = {
        let textView = baseLabel(title: EmailStatus.notVerified.rawValue)
        textView.textColor = UIColor.hex(hex: "A9BF90")
        textView.textAlignment = .right
        textView.font = UIFont.systemFont(ofSize: 19, weight: UIFont.Weight.heavy)
        return textView
    }()
    
    
    lazy var emailCardView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var emailTextView: UITextView = {
        let user = User.getUser()!
        let textView = baseLabel(title: user.email)
        textView.textColor = UIColor.darkText
        return textView
    }()
    
    lazy var emailEditButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.addTarget(self, action: #selector(changeEmailAddress(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var addressTextViewLabel: UITextView = {
        return baseLabel(title: "ADDRESS")
    }()
    
    lazy var addressCardView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var addressTextView: UITextView = {
        let user = User.getUser()!
        let textView = baseLabel(title: user.streetAddress)
        textView.textColor = UIColor.darkText
        return textView
    }()
    
    lazy var addressEditButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.addTarget(self, action: #selector(pickLocation(_:)), for: .touchUpInside)
        return button
    }()
    

    func baseLabel(title:String) -> UITextView  {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.text = title
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        return textView
    }
    
    override func setUpView() {
        super.setUpView()
    
        setUpNavigationBarWhite(title: "Edit User Details")
        scrollView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        
        self.addViews()
    }
    
    func addViews() {
        
        self.scrollView.addSubview(headerView)
        self.scrollView.addSubview(profileImageView)
        self.scrollView.addSubview(profileCoverImageView)
        self.scrollView.addSubview(editIConImageView)
        self.scrollView.addSubview(numberTextViewLabel)
        self.scrollView.addSubview(numberVerifiedLabel)
        self.scrollView.addSubview(emailTextViewLabel)
        self.scrollView.addSubview(emailVerifiedLabel)
        self.scrollView.addSubview(addressTextViewLabel)
        self.scrollView.addSubview(numberCardView)
        self.scrollView.addSubview(emailCardView)
        self.scrollView.addSubview(addressCardView)
        
        self.numberCardView.addSubview(numberTextView)
        self.numberCardView.addSubview(numberEditButton)
        
        self.emailCardView.addSubview(emailTextView)
        self.emailCardView.addSubview(emailEditButton)
        
        self.addressCardView.addSubview(addressTextView)
        self.addressCardView.addSubview(addressEditButton)
        
        let width = self.view.frame.width
        self.headerView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.headerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.headerView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.profileImageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 16).isActive = true
        self.profileImageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileImageView.layer.cornerRadius = 50
        
        self.profileCoverImageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 16).isActive = true
        self.profileCoverImageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        self.profileCoverImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileCoverImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.profileCoverImageView.layer.cornerRadius = 50
        
        self.editIConImageView.bottomAnchor.constraint(equalTo: profileCoverImageView.bottomAnchor, constant: 15).isActive = true
        self.editIConImageView.trailingAnchor.constraint(equalTo: profileCoverImageView.trailingAnchor, constant: 15).isActive = true
        self.editIConImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.editIConImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //MARK: number section
        self.numberVerifiedLabel.font = UIFont.boldSystemFont(ofSize: 14)
        self.numberVerifiedLabel.anchorWithConstantsToTop(top: headerView.bottomAnchor, left: nil, bottom: nil, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.numberVerifiedLabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
        self.numberVerifiedLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.numberTextViewLabel.anchorWithConstantsToTop(top: headerView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: numberVerifiedLabel.leadingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.numberTextViewLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.numberCardView.anchorWithConstantsToTop(top: numberTextViewLabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.numberCardView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.numberCardView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.numberEditButton.anchorWithConstantsToTop(top: numberCardView.topAnchor, left: nil, bottom: numberCardView.bottomAnchor, right: numberCardView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.numberEditButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.numberEditButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.numberEditButton.setImage(UIImage(named: "EditIcon"), for: .normal)
        
        self.numberTextView.anchorWithConstantsToTop(top: numberCardView.topAnchor, left: numberCardView.leadingAnchor, bottom: numberCardView.bottomAnchor, right: numberEditButton.leadingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        self.numberTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //MARK: email section
        self.emailVerifiedLabel.font = UIFont.boldSystemFont(ofSize: 14)
        self.emailVerifiedLabel.anchorWithConstantsToTop(top: numberCardView.bottomAnchor, left: nil, bottom: nil, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.emailVerifiedLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        self.emailVerifiedLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.emailTextViewLabel.anchorWithConstantsToTop(top: numberCardView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: numberVerifiedLabel.leadingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.emailTextViewLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //CARD
        self.emailCardView.anchorWithConstantsToTop(top: emailTextViewLabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.emailCardView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.emailCardView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.emailEditButton.anchorWithConstantsToTop(top: emailCardView.topAnchor, left: nil, bottom: emailCardView.bottomAnchor, right: emailCardView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.emailEditButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.emailEditButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.emailEditButton.setImage(UIImage(named: "EditIcon"), for: .normal)
        
        self.emailTextView.anchorWithConstantsToTop(top: emailCardView.topAnchor, left: emailCardView.leadingAnchor, bottom: emailCardView.bottomAnchor, right: numberEditButton.leadingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        self.emailTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        //MARK: address section
        self.addressTextViewLabel.anchorWithConstantsToTop(top: emailCardView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: scrollView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.addressTextViewLabel.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.addressCardView.anchorWithConstantsToTop(top: addressTextViewLabel.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: scrollView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -50, rightConstant: 0)
        self.addressCardView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.addressCardView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.addressEditButton.anchorWithConstantsToTop(top: addressCardView.topAnchor, left: nil, bottom: addressCardView.bottomAnchor, right: addressCardView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.addressEditButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.addressEditButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.addressEditButton.setImage(UIImage(named: "EditIcon"), for: .normal)
        
        self.addressTextView.anchorWithConstantsToTop(top: addressCardView.topAnchor, left: addressCardView.leadingAnchor, bottom: addressCardView.bottomAnchor, right: numberEditButton.leadingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        self.addressTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        print("user profile email: ",self.user.email)
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUpdateNotification(notification:)), name: NSNotification.Name(Key.updatedMobile), object: nil)
        
        self.updateProfile()
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewDidLoad()
//        self.updateProfile()
//
//        //NotificationCenter.default.addObserver(self, selector: #selector(receiveUpdateNotification(notification:)), name: NSNotification.Name(Key.updatedMobile), object: nil)
//    }
    
    @objc func receiveUpdateNotification(notification: Notification){
        
        var removedZero = ""
        let afterCountryCode = user.phone[4]
        
        if afterCountryCode == "0" {
            //let count = user.phone.count
            var theNumber = user.phone
            let index = user.phone.index(theNumber.startIndex, offsetBy: 4)
            theNumber.remove(at: index)
            removedZero = theNumber
        }
        
        
        self.numberTextView.text = removedZero
        self.emailTextView.text = user.email
        self.addressTextView.text = user.streetAddress

        if  (user.picture != "") {

            print("user image after: ",user.picture)

            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )

        }else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }

        let status = user.emailStatus == "" ? EmailStatus.notVerified.rawValue : user.emailStatus
        if status == EmailStatus.notVerified.rawValue  {
            self.emailVerifiedLabel.textColor = UIColor.red
            self.emailVerifiedLabel.text = status
        }  else {
            self.emailVerifiedLabel.textColor = UIColor.hex(hex: Key.colorTransactionSuccess)
            self.emailVerifiedLabel.text = status
        }
    }
    
    func updateProfile()  {
        let user = User.getUser()!
        if  !(user.picture.isEmpty) {
            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
        let status = user.emailStatus == "" ? EmailStatus.notVerified.rawValue : user.emailStatus
        if status == EmailStatus.notVerified.rawValue  {
            self.emailVerifiedLabel.textColor = UIColor.hex(hex: Key.colorTransactionfailed)
        }  else {
            self.emailVerifiedLabel.textColor = UIColor.hex(hex: Key.colorTransactionSuccess)
        }
        
        //var removedZero = "+2330240865997"
        let afterCountryCode = user.phone[4]//user.phone[user.phone.index(user.phone.startIndex, offsetBy: 4)]
        
        print("phone number without country code: ",afterCountryCode)
        
        if afterCountryCode == "0" {
            //let count = user.phone.count
            var theNumber = user.phone
            let index = user.phone.index(theNumber.startIndex, offsetBy: 4)
            print("phone number without country code: ",index)
            theNumber.remove(at: index)
            //removedZero = theNumber
            self.numberTextView.text = theNumber
        }else{
            self.numberTextView.text = user.phone
        }
        
        self.emailVerifiedLabel.text = status
        self.emailVerifiedLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        self.addressTextView.text = user.streetAddress
        self.emailTextView.text = user.email
        
//        let user = User.getUser()!
//        print("user image before: ",user.picture)
//
//        if (user.picture == "") {
//            apiService.getCustomer(customerNumber: user.cutomerId) { (status, theuser, withMessage) in
//                if status == ApiCallStatus.SUCCESS {
//
//                    if theuser?.picture == "" {
//                        self.profileImageView.image = Mics.userPlaceHolder()
//                        return
//                    }
//
//                    //update the user data in realm db
//                    try! self.realm.write({
//                        self.user.picture = theuser!.picture
//                        self.user.email = theuser!.email
//                    })
//                    User.save(data: self.user)
//
//                    self.profileImageView.af_setImage(
//                        withURL: URL(string: (self.user.picture))!,
//                        placeholderImage: Mics.userPlaceHolder(),
//                        imageTransition: .crossDissolve(0.2)
//                    )
//
//                    //send notification accross app for profile picture update
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.profilePictureChanged), object: ApiCallStatus.SUCCESS)
//                }else{
//                    self.profileImageView.image = Mics.userPlaceHolder()
//                }
//            }
//        }
//
//        if  (user.picture != "") {
//
//            print("user image after: ",user.picture)
//
//            self.profileImageView.af_setImage(
//                withURL: URL(string: (user.picture))!,
//                placeholderImage: Mics.userPlaceHolder(),
//                imageTransition: .crossDissolve(0.2)
//            )
//
//        }else {
//            self.profileImageView.image = Mics.userPlaceHolder()
//        }
//
//
//        let status = user.emailStatus == "" ? EmailStatus.notVerified.rawValue : user.emailStatus
//        print("email verified status: \(status)")
//        if status == EmailStatus.notVerified.rawValue  {
//           self.emailVerifiedLabel.textColor = UIColor.red
//            self.emailVerifiedLabel.text = status
//        }  else {
//           self.emailVerifiedLabel.textColor = UIColor.hex(hex: Key.colorTransactionSuccess)
//            self.emailVerifiedLabel.text = status
//        }
//        self.emailVerifiedLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
//        self.addressTextView.text = user.streetAddress
//        self.emailTextView.text = user.email
    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        if self.user.emailStatus.isEmpty || self.user.emailStatus == EmailStatus.notVerified.rawValue {
            let destination = VerificationCodeController()
            destination.isVerifyEmail = true
            destination.email = user.email
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
    @objc func changePhoneNumber(_ sender: UIButton)  {
        let destination  = EditNumberPromptController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func changeEmailAddress(_ sender: UIButton)  {
        let destination  = EditEmailController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func pickLocation(_ sender: UIButton)  {
        let destination  = CurrentLocationController()
        destination.isUpdate = true
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func pickImage(_ sender: UIButton) {
        let alertController = UIAlertController(title: "LIQUID", message: "SELECT OPTION", preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: "Take photo", style: .default) { (action) in
            self.takePhoto()
        }
        
        let profileAction = UIAlertAction(title: "Pick from libary", style: .default) { (action) in
           self.pickFromLibrary()
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(profileAction)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func pickFromLibrary() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func takePhoto()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }  else {
            ViewControllerHelper.showPrompt(vc: self, message: "Your device does not support camera options.")
        }
    }
    
}

extension EditProfileController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[.originalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: .default, image: image)
        cropController.delegate = self
        picker.dismiss(animated: true, completion: {
            self.present(cropController, animated: true, completion: nil)
        })
        
    }
    
    //MARK:- the cropper delegate
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        cropViewController.dismiss(animated: true, completion: nil)
        
        self.profileImage = image
        self.profileImageView.image = image
        
        //MARK: make api call to update profile
        self.utilViewHelper.showActivityIndicator()
        self.apiService.saveSelfieProfile(image: image) { (status,url,withMessage) in
            
            self.utilViewHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.updatedMobile), object: self)
                self.updateProfile()
            }  else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
        
    }
    
}

