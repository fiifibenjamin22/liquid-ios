//
//  TransactionHistoryController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class TransactionHistoryController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let sectionId = "sectionId"
    var transactions = [Any]()
    let viewHelper = ViewControllerHelper()
    let apiService = ApiService()
    var url = ""
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Transaction History")
        
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.collectionView.register(GenericSectionCell.self, forCellWithReuseIdentifier: sectionId)
        self.collectionView.register(TransactionHistoryCell.self, forCellWithReuseIdentifier: collectionId)
        
        self.transactions.append("TRANSACTIONS")
        collectionView.reloadData()
        
        self.getData(url: "\(ApiUrl().getTransacitons())")
    }
    
    func getData(url:String)  {
        self.viewHelper.showActivityIndicator()
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"page_number":1,"limit":50]
        self.apiService.getTransaction(url: url, params: params) { (status, transactions, withMessage) in
            self.viewHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if let allTransactions = transactions {
                    for item in allTransactions {
                        if item.recipientName != "" {
                            self.transactions.append(item)
                        }
                    }
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
}

extension TransactionHistoryController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.transactions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.transactions[indexPath.row]
        if item is String {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: sectionId, for: indexPath) as! GenericSectionCell
            cell.item = item as? String
            return cell
        }
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! TransactionHistoryCell
        cell.item = self.transactions[indexPath.row] as? TransactionHistory
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = self.transactions[indexPath.row]
        let itemWidth = collectionView.frame.width
        if item is String {
            let itemHeight = CGFloat(50)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        let itemHeight = CGFloat(90)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item  = self.transactions[indexPath.row]
        if item is TransactionHistory {
            let transaction = item as! TransactionHistory
            let destination = TransactionDetailController()
            destination.transactionId = transaction.id
            self.navigationController?.pushViewController(destination, animated: true)
        }
    }
    
}
