//
//  AddReferralViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 26/02/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog

class AddReferralViewController: BaseScrollViewController, LQConfirmReferral {
    
    var imagePicker : UIImagePickerController!
    var profileImage: UIImage?
    let apiService = ApiService()
    let user = User.getUser()!
    let viewControllerUtil = ViewControllerHelper()
    var popup: PopupDialog?
    
    lazy var headingText: UITextView = {
        let label = baseLabel(title: "Received an invitation code from a friend?")
        label.textAlignment = .center
        return label
    }()
    
    lazy var labelText: UITextView = {
        let label = baseLabel(title: "Enter it below")
        label.textAlignment = .center
        return label
    }()
    
    lazy var invitationCodeTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Invitation Code")
        textField.keyboardType = UIKeyboardType.alphabet
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.spellCheckingType = .no
        
        return textField
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor: color])
        
        return textField
    }
    
    
    func baseLabel(title:String) -> UITextView  {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.text = title
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        return textView
    }
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Continue", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 0
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(addReferral), for: .touchUpInside)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        
        setUpNavigationBarWhite(title: "")
        scrollView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        
        self.addViews()
    }
    
    func addViews() {
        self.scrollView.addSubview(headingText)
        self.scrollView.addSubview(labelText)
        self.scrollView.addSubview(invitationCodeTextField)
        self.scrollView.addSubview(nextButton)
        
        let width = view.frame.width
        self.headingText.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.headingText.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.headingText.heightAnchor.constraint(equalToConstant: 50).isActive = true
    
        
        self.labelText.anchorWithConstantsToTop(top: headingText.bottomAnchor, left: headingText.leadingAnchor, bottom: nil, right: headingText.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.labelText.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.invitationCodeTextField.anchorWithConstantsToTop(top: labelText.bottomAnchor, left: labelText.leadingAnchor, bottom: nil, right: labelText.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.invitationCodeTextField.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.invitationCodeTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true

        self.nextButton.anchorWithConstantsToTop(top: nil, left: scrollView.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -8, rightConstant: 0)
        self.nextButton.widthAnchor.constraint(equalToConstant: width - 32).isActive = true
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        self.changeButtonStatus()
    }
    
    func changeButtonStatus()  {
        if !self.invitationCodeTextField.text!.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        } else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func addReferral(){
        self.viewControllerUtil.showActivityIndicator()

        self.apiService.addReferralCode(code: self.invitationCodeTextField.text!, completion: { (response, status, withMessage) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                
                print("invitation response: \(response["referrer_name"])")
                print("invitation response: \(status)")
                print("invitation response: \(withMessage)")
                
                let theInviter = response["referrer_name"]
                
                User.setHasUsedInviteCode(status: true)
                let confirm_vc = ReferralConfirmViewController(nibName: "ReferralConfirmView", bundle: nil)
                confirm_vc.delegate = self
                confirm_vc.inviter = "\(theInviter)"
                self.popup = PopupDialog(viewController: confirm_vc)
                self.present(self.popup!, animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        })
    }
    
    func proceed() {
        self.popup?.dismiss()
        self.navigationController?.popViewController(animated: true)
    }
}


