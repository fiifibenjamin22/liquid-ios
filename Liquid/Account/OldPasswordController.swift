//
//  OldPasswordController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class OldPasswordController: ControllerWithBack {
   
    let apiService = ApiService()
    let viewhelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "Please enter your old password"
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var passwordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter old password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.rightViewMode = UITextField.ViewMode.always
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: "Eye")
        imageView.isUserInteractionEnabled = true
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.rightView = imageView
        
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(goNext), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.white
        setUpNavigationBar(title: "Change Password")
        
        //MARK:- settting up the UI elements
        self.setUpUI()
    }
    
    func setUpUI()  {
        self.view.addSubview(messageTextView)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(nextButton)
        
        //MARK:- anchoring elements
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.passwordTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
    
        self.setNavItem()
    }
    
    //MARK - toggle button
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text!
        if !text.isEmpty {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }   else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
    
    @objc func goNext(){
        self.viewhelper.showActivityIndicator()
        let user = User.getUser()!
        let password = self.passwordTextField.text!
         self.apiService.checkPassword(number: user.phone, password: password) { (status, withMessage) in
            self.viewhelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
               let destination = NewPasswordController()
               destination.password = password
               self.navigationController?.pushViewController(destination,animated: true)
            }  else {
               ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
         }
    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        let isPassworMode = self.passwordTextField.isSecureTextEntry
        if isPassworMode {
            imageView.image = UIImage(named: "EyeClosed")
            self.passwordTextField.isSecureTextEntry = false
        }  else {
            imageView.image = UIImage(named: "Eye")
            self.passwordTextField.isSecureTextEntry = true
        }
    }
    
    func setNavItem()  {
        let menuBack = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancelIcon))
        navigationItem.rightBarButtonItem = menuBack
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    
    @objc func handleCancelIcon()  {
        self.dismiss(animated: true, completion: nil)
    }
    
}
