//
//  EditNumberPromptController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class EditNumberPromptController: ControllerWithBack {
   

    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "If you would like to use a different phone number, please set it here so you can receive alerts and notifications from your LIQUID App."
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    let iconImageview: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.image = UIImage(named: "PhoneTransfer")
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Change Phone Number")
        
        self.setUpUI()
        
    }
    
    func setUpUI()  {
        
        self.view.addSubview(messageTextView)
        self.view.addSubview(iconImageview)
        self.view.addSubview(nextButton)
        
        self.iconImageview.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.iconImageview.heightAnchor.constraint(equalToConstant: 200).isActive = true
        self.iconImageview.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.iconImageview.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: iconImageview.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 130).isActive = true
        
       
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
    }
    
    @objc func nextAction(){
        let destination  = EditPhoneNumberController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}
