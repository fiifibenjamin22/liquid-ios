//
//  EditEmailController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class EditEmailController: ControllerWithBack {
    
    var email = ""
    let apiService = ApiService()
    let utilViewHolder = ViewControllerHelper()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.text = "Please enter your new email address"
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var emailTextField: UITextField = {
        let textField = baseUITextField(placeholder: "Email Address")
        textField.keyboardType = UIKeyboardType.emailAddress
        return textField
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        return button
    }()
    
    func baseUITextField(placeholder:String) -> UITextField {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.setBottomBorder(color: UIColor.lightGray.cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: placeholder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Change Email Address")
        
        self.setUpUI()
    }
    
    func setUpUI()  {
        
        self.view.addSubview(messageTextView)
        self.view.addSubview(emailTextField)
        self.view.addSubview(nextButton)
        
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.emailTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.emailTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        if textField.text!.containsEmoji{
            let theImoji = textField.text?.emojis[0]
            let alertController = UIAlertController(title: "Email Error", message: "your email must not contain emojis \(theImoji ?? ""), kindly correct and proceed", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Correct it", style: .default) { (finished) in
                textField.text?.removeLast()
            }
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        self.email = textField.text!
        self.toggleButtonStatus()
    }
    
    @objc func nextAction(){
        self.utilViewHolder.showActivityIndicator()
        
        // Check email exists
        self.apiService.checkEmail(email: self.email) { (status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                self.updateEmail()
            } else {
                self.utilViewHolder.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
        
    }
    
    func updateEmail(){
        self.apiService.triggerEmailChange(email: self.email) { (status, withMessage) in
            self.utilViewHolder.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                //User.updateUserEmail(email: self.email)
                //User.updateUserEmailStatus(status: EmailStatus.notVerified.rawValue)
                ViewControllerHelper.showPrompt(vc: self, message: "Verification sent to your email. Enter the code to proceed.", completion: { (isDone) in
                    self.verifyWithCode()
                })
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: "Verification failed, try again.")
            }
        }
    }
    //MARK: - verify with code
    func verifyWithCode()  {
        let alertController = UIAlertController(title: "Verify", message: "Enter verification code you received.", preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Proceed", style: .default) { (confirm) in
            if let textField = alertController.textFields?.first, let code = textField.text {
                //peform sending logic here
                print(code)
                if code == "" {
                    ViewControllerHelper.showPrompt(vc: self, message: "Please enter a verification code")
                }else{
                    
                    if code.containsEmoji{
                        let theImoji = textField.text?.emojis[0]
                        let alertController = UIAlertController(title: "Verification Error", message: "your verification code does not contain emojis \(theImoji ?? ""), kindly correct and proceed", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Try Again", style: .default) { (finished) in
                            textField.text?.removeLast()
                            self.verifyWithCode()
                        }
                        alertController.addAction(alertAction)
                        self.present(alertController, animated: true, completion: nil)
                        return
                    }
                    
                    self.completeVerification(code: code)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (cancelled) in
            
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Verification Code"
            textField.keyboardType = .numberPad
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
//        ViewControllerHelper.showVoucherPrompt(vc: self, message: "Enter verification code you received.") { (isDone, code) in
//            if isDone {
//                if code == "" {
//                    ViewControllerHelper.showPrompt(vc: self, message: "Please enter a verification code")
//                }
//                self.completeVerification(code: code)
//            }
//        }
    }
    
    func completeVerification(code:String){
        self.utilViewHolder.showActivityIndicator()
        self.apiService.verifyEmailChange(pin: code, completion: { (status, withMessage) in
            self.utilViewHolder.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                User.updateUserEmail(email: self.email)
                User.updateUserEmailStatus(status: EmailStatus.verified.rawValue)
                //self.dismiss(animated: true, completion: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Key.updatedMobile), object: ApiCallStatus.SUCCESS)
                self.navigationController?.popViewController(animated: true)
            }  else {
               //ViewControllerHelper.showPrompt(vc: self, message: "Verification failed, try again.")
                ViewControllerHelper.showPrompt(vc: self, message: "Verification failed, try again.", completion: { (isDone) in
                    self.verifyWithCode()
                })
            }
        })
    }
    
    //MARK - auto advance input fields
    @objc func toggleButtonStatus(){
        let email = self.emailTextField.text!
        if Validator.isValidEmail(email: email) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }   else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
    }
}
