//
//  NewPasswordController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class NewPasswordController: ControllerWithBack {
    
    let apiService = ApiService()
    let viewhelper = ViewControllerHelper()
    var password = ""
    var new_password_1 = ""
//    var new_password_2 = ""
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    var confirmPassword = false
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    lazy var passwordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter new password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.rightViewMode = UITextField.ViewMode.always
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: "Eye")
        imageView.isUserInteractionEnabled = true
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.rightView = imageView
        
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    let passwordHintLable: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.text = AppConstants.passwordHintMessage
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        textView.backgroundColor = UIColor.hex(hex: "e6f8ff")
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 15, bottom: 0, right: 15)
        
        return textView
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Next", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(goNext), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.white
        setUpNavigationBar(title: "Change Password")
        
        //MARK:- settting up the UI elements
        self.setUpUI()
    }
    
    func setUpUI()  {
        self.view.addSubview(messageTextView)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(passwordHintLable)
        self.view.addSubview(nextButton)
        
        let width = self.view.frame.width - 32
        //MARK:- anchoring elements
        self.messageTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        let font = UIFont(name: Font.Roboto.Bold, size: 16)!
        let fontLight = UIFont(name: Font.Roboto.Light, size: 14)!
        var titletext = "Please enter your new password.\n".formatAsAttributed(font: font, color: UIColor.darkText)
        if self.confirmPassword {
            titletext = "Please re-enter your new password.\n".formatAsAttributed(font: font, color: UIColor.darkText)
        }
        
        _ = "\(AppConstants.passwordHintMessage)".formatAsAttributed(font: fontLight, color: UIColor.lightGray)


        let resultingText = NSMutableAttributedString()
        resultingText.append(titletext)
        //resultingText.append(message)

        self.messageTextView.attributedText = resultingText
        self.messageTextView.textAlignment = .center

        self.passwordTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true

        self.passwordHintLable.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordHintLable.heightAnchor.constraint(equalToConstant: 120).isActive = true
        self.passwordHintLable.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.nextButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.nextButton.heightAnchor.constraint(equalToConstant: 44).isActive = true

        self.setNavItem()
    }
    
    //MARK - toggle button
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text!
        if  Validator.isValidPassword(password: text) {
            self.nextButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.nextButton.backgroundColor = color
        }   else {
            self.nextButton.isEnabled = false
            let color = UIColor.gray
            self.nextButton.backgroundColor = color
        }
        
        self.passwordHintLable.text = Validator.getPasswordMessage(password: text)
    }
    
    @objc func goNext(){
        //MARK: change password
        if self.confirmPassword {
            
            let textPassword = self.passwordTextField.text!
            
            self.viewhelper.showActivityIndicator()
            
            if new_password_1 != textPassword {
                self.viewhelper.hideActivityIndicator()
                let alert = UIAlertController(title: "", message: "Your passwords do not match", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default) { (finished) in
                    self.navigationController?.popViewController(animated: true)
                }
                
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                return
            }else{
                
                self.apiService.changePassword(oldPassword: self.password, newPassword: textPassword) { (status, withMessage) in
                    self.viewhelper.hideActivityIndicator()
                    if status == ApiCallStatus.SUCCESS {
                        let banner = NotificationBanner(title: "Success", subtitle: "You have successfully changed your password.", style: .success)
                        banner.show()
                        
                        self.navigationController?.popToRootViewController(animated: true)
                    }  else {
                        ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                    }
                }
            }
        } else {
            let confirm_vc = NewPasswordController()
            confirm_vc.confirmPassword = true
            confirm_vc.password = self.password
            confirm_vc.new_password_1 = self.passwordTextField.text!
            self.navigationController?.pushViewController(confirm_vc, animated: true)
        }

    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        let isPassworMode = self.passwordTextField.isSecureTextEntry
        if isPassworMode {
            imageView.image = UIImage(named: "EyeClosed")
            self.passwordTextField.isSecureTextEntry = false
        }  else {
            imageView.image = UIImage(named: "Eye")
            self.passwordTextField.isSecureTextEntry = true
        }
    }
    
    func setNavItem()  {
        let menuBack = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancelIcon))
        navigationItem.rightBarButtonItem = menuBack
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    
    @objc func handleCancelIcon()  {
        self.dismiss(animated: true, completion: nil)
    }
    
}
