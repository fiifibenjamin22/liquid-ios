//
//  ReferralConfirmViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 23/03/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
protocol LQConfirmReferral: AnyObject {
    func proceed()
}

class ReferralConfirmViewController: UIViewController {
    var delegate: LQConfirmReferral?
    var inviter = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let lblText = self.view.viewWithTag(1) as! UILabel
        lblText.text = "Your invitation code is valid. \(inviter) will get a reward for inviting you. You could get a reward too by inviting your friends to join LIQUID."
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.delegate!.proceed()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
