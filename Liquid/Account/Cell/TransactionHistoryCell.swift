//
//  TransactionCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class TransactionHistoryCell: BaseCell {
  
    var item: TransactionHistory? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.amountTextView.text = "\(Mics.convertDoubleToCurrency(amount: unwrapedItem.amount))"
            self.statusTextView.text = "\(unwrapedItem.status)"
            self.statusTextView.attributedText = self.prepareText(text: unwrapedItem.status)
            
            let messageHeader = "\(unwrapedItem.type)\n".formatAsAttributed(fontSize: 16, color: UIColor.darkText)
            let messageBody = "\(unwrapedItem.recipientName)".formatAsAttributed(fontSize: 14, color: UIColor.darkGray)
            let result = NSMutableAttributedString()
            result.append(messageHeader)
            result.append(messageBody)
            self.messageTextView.attributedText = result
            
            if unwrapedItem.status == Key.statusFailed {
              self.statusTextView.textColor = UIColor.hex(hex: Key.colorTransactionfailed)
            } else if unwrapedItem.status == Key.statusPending {
              self.statusTextView.textColor = UIColor.hex(hex: Key.colorTransactionPending)
            } else {
              self.statusTextView.textColor = UIColor.hex(hex: Key.colorTransactionSuccess)
            }
            
            if  !(unwrapedItem.recipientProfile.isEmpty) {
                self.iconImageView.af_setImage(
                    withURL: URL(string: (unwrapedItem.recipientProfile))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.iconImageView.image = Mics.placeHolder()
            }
            
        }
    }
    
    func prepareText(text:String) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        if text == Key.statusFailed {
           attachment.image = UIImage(named: "TransactionFailed")
        } else if text == Key.statusPending {
           attachment.image = UIImage(named: "TransactionFailed")
        }  else {
           attachment.image = UIImage(named: "TransactionSuccess")
        }
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: "\(text)  ")
        myString.append(attachmentString)
        return myString
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Thin, size: 16)!)
        return textView
    }()
    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        return textView
    }()
    
    let statusTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Thin, size: 14)!)
        return textView
    }()
    
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(iconImageView)
        addSubview(messageTextView)
        addSubview(amountTextView)
        addSubview(statusTextView)
        addSubview(splitLine)
        
        let height = frame.height - 50
        self.iconImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.iconImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.iconImageView.layer.cornerRadius = height/2
        
        self.statusTextView.anchorWithConstantsToTop(top: amountTextView.bottomAnchor, left: nil, bottom: splitLine.topAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -8, rightConstant: -8)
        self.statusTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.amountTextView.anchorWithConstantsToTop(top: topAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -8)
        self.amountTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.amountTextView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: iconImageView.trailingAnchor, bottom: splitLine.topAnchor, right: amountTextView.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -8)
        
        self.splitLine.anchorWithConstantsToTop(top: nil, left: iconImageView.trailingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.splitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
    
    }
    
}
