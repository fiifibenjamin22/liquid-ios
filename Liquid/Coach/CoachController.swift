//
//  CoachController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class CoachController: UITableViewController {
    
    let headerId = "headerId"
    let cellId = "cellId"
    var coach = Coach()
    var categories = [CoachCategories]()
    var selectedIndex = -1
    var isCollapsed = false
    let viewControllerHelper = ViewControllerHelper()
    let vw = CoachHeaderView()
    var nextUrl = ApiUrl().notifications()
    var page = 1
    
    var arrayColors = [[UIColor.hex(hex: "02A9E8"), UIColor.hex(hex: "00F6FF")],[UIColor.hex(hex: "FF2E00"), UIColor.hex(hex: "FF8A00")],[UIColor.hex(hex: "0500FF"), UIColor.hex(hex: "CC00ED")]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getallNotifications()
        self.getCoachData()
        self.tableView.backgroundColor = .white
        self.navigationItem.title = "Coach"
        self.tableView.register(CoachCell.self, forCellReuseIdentifier: cellId)
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 240
        self.tableView.rowHeight = UITableView.automaticDimension
        
        //self.tableView.scrol
        
        //tableView.contentInset.top = -1 * 90
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.y)
        if scrollView.contentOffset.y < -1 {
            UIView.animate(withDuration: 0.25, animations: {
                self.tableView.contentInset.top = 0
            })
        }
        
        if scrollView.contentOffset.y > 1 {
           UIView.animate(withDuration: 0.25, animations: {
              self.tableView.contentInset.top = -1 * 90
           })
        }
    }
    
    override func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset.y)
      if scrollView.contentOffset.y < 0 {
        UIView.animate(withDuration: 0.25, animations: {
          self.tableView.contentInset.top = 0
        })
      } else if scrollView.contentOffset.y > 1 {
        UIView.animate(withDuration: 0.25, animations: {
          self.tableView.contentInset.top = -1 * self.vw.frame.size.height
        })
      }
    }
    
    //Get Coach Data
    func getCoachData(){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().getcoachData { (status, data, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.coach = data!
                self.categories.removeAll()
                for c in self.coach.CoachCategories {
                    self.categories.append(c)
                }
                self.tableView.reloadData()
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    func setNavItem(badgeNumber: Int)  {
        
        let image = UIImage(named: "NotificationBell")?.withRenderingMode(.alwaysOriginal)
        let menuNotification: UIBarButtonItem?
        if badgeNumber == 0 {
            menuNotification = UIBarButtonItem(badge: "", title: image!, target: self, action: #selector(handleNotification))
        }else{
            menuNotification = UIBarButtonItem(badge: "\(badgeNumber)", title: image!, target: self, action: #selector(handleNotification))
        }
        navigationItem.rightBarButtonItems = [menuNotification!]
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
    }
    
    func getallNotifications(){
        ApiService().getNotifications(url: nextUrl, pageNumber: self.page, isMore: true) { (status, notifications,currentPage, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                var allNotes = [Notification]()
                allNotes.removeAll()
                for i in notifications! {
                    if i.isRead == false {
                        allNotes.append(i)
                    }
                }
                self.setNavItem(badgeNumber: allNotes.count)
            }else{
                print("FAILED")
            }
        }
    }
    
    @objc func handleNotification()  {
        let destination = NotificationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = CoachHeaderView()
        return vw
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coach.CoachCategories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CoachCell
        if indexPath.item == 1 {
            cell.isHidden = true
        }
        cell.selectionStyle = .none
        cell.index = indexPath
        cell.mainCardView.colors = self.arrayColors[indexPath.row]
        cell.item = self.categories[indexPath.row]
        print("print list items: ",self.categories[indexPath.row].name)
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectedIndex == indexPath.row && isCollapsed == true {
            return 400
        }else{
            if indexPath.item == 1 {
                return 0
            }
            return 200.0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if selectedIndex == indexPath.row {
            if self.isCollapsed == false {
                self.isCollapsed = true
            }else{
                self.isCollapsed = false
            }
        }else{
            self.isCollapsed = true
        }
        self.selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension CoachController: CoachDelegate {
    func didOpen(Indexes: IndexPath) {
        tableView.deselectRow(at: Indexes, animated: true)
        if selectedIndex == Indexes.row {
            if self.isCollapsed == false {
                self.isCollapsed = true
            }else{
                self.isCollapsed = false
            }
        }else{
            self.isCollapsed = true
        }
        self.selectedIndex = Indexes.row
        tableView.reloadRows(at: [Indexes], with: .automatic)
    }
}
