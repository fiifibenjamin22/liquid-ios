//
//  CoachHeaderCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import GradientView

class CoachCell: BaseTableCell {
    
    let mainCellId = "mainCellId"
    var tasks = [CoachTask]()
    var delegate: CoachDelegate?
    var isOpened = false
    var index: IndexPath?
    
    var item: CoachCategories? {
        didSet {
            guard let unwrapedItem = item else {return}
            print(unwrapedItem)
            if isOpened {
                self.coachTitle.text = unwrapedItem.name
            }
            self.coachTitle.text = "\(unwrapedItem.name)"
            self.tasks.removeAll()
            self.tasks.append(contentsOf: unwrapedItem.CoachTask)
            self.collectionView.reloadData()
        }
    }
    
    lazy var mainCardView: GradientView = {
        let v = GradientView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 12
        v.layer.masksToBounds = true
        v.layer.shadowColor = UIColor.gray.cgColor
        v.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        v.layer.shadowRadius = 5.0
        v.layer.shadowOpacity = 0.24
        v.locations = [0.3, 1.0]
        v.direction = .vertical
        v.isUserInteractionEnabled = true
        v.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleOpener)))
        return v
    }()
    
    let coachTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor = .white
        lbl.font = UIFont.boldSystemFont(ofSize: 17)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.backgroundColor = .clear
        collectionIn.isScrollEnabled = false
        return collectionIn
    }()
    
    lazy var arrowImage: UIImageView = {
       let img = UIImageView()
        img.image = UIImage(named: "ChevronDown")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = .white
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    override func setUpView() {
        super.setUpView()
        self.collectionView.register(TasksCell.self, forCellWithReuseIdentifier: mainCellId)
        
        addSubview(mainCardView)
        self.mainCardView.addSubview(coachTitle)
        mainCardView.addSubview(collectionView)
        mainCardView.addSubview(arrowImage)
        
        mainCardView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        mainCardView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 24).isActive = true
        mainCardView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24).isActive = true
        mainCardView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
        
        coachTitle.topAnchor.constraint(equalTo: mainCardView.topAnchor, constant: 12).isActive = true
        coachTitle.leftAnchor.constraint(equalTo: mainCardView.leftAnchor, constant: 12).isActive = true
        coachTitle.rightAnchor.constraint(equalTo: mainCardView.rightAnchor, constant: -8).isActive = true
        coachTitle.heightAnchor.constraint(equalToConstant: 50).isActive = true
                
        self.collectionView.topAnchor.constraint(equalTo: coachTitle.bottomAnchor, constant: 20).isActive = true
        self.collectionView.leftAnchor.constraint(equalTo: self.mainCardView.leftAnchor, constant: 8).isActive = true
        self.collectionView.rightAnchor.constraint(equalTo: self.mainCardView.rightAnchor, constant: -8).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.mainCardView.bottomAnchor, constant: -8).isActive = true
        
        arrowImage.layer.borderWidth = 2.0
        arrowImage.layer.cornerRadius = 10.0
        arrowImage.layer.borderColor = UIColor.clear.cgColor
        arrowImage.layer.masksToBounds = true
        arrowImage.contentMode = .scaleAspectFit
        arrowImage.rightAnchor.constraint(equalTo: coachTitle.rightAnchor, constant: -16).isActive = true
        arrowImage.centerYAnchor.constraint(equalTo: coachTitle.centerYAnchor).isActive = true
        arrowImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        arrowImage.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
}

extension CoachCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mainCellId, for: indexPath) as! TasksCell
        cell.item = self.tasks[indexPath.row]
        cell.taskCell = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.mainCardView.frame.size.width, height: 70.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    @objc func handleOpener(){
        self.delegate?.didOpen(Indexes: index!)
    }
}
