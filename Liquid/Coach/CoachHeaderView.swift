//
//  CoachHeaderView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class CoachHeaderView: UIView {
    
    let user = User.getUser()!
    let coach = Coach.getCoach()!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    let card: CardView = {
        let card = CardView()
        card.cornerRadius = 35
        card.shadowOffsetHeight = 2
        card.backgroundColor = .white
        card.isUserInteractionEnabled = true
        return card
    }()
    
    let profileImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "AvatarLady")
        return imageView
    }()
    
    let profileCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ProfileWave")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let balancelbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.black
        textView.text = ""
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 17)!)
        return textView
    }()
    
    let earningslbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = ""
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 17)!)
        return textView
    }()
    
    let balanceCurrencylbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.black
        textView.text = "GHS "
        textView.textAlignment = .right
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 17)!)
        return textView
    }()
    
    let earningsCurrencylbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "GHS "
        textView.textAlignment = .right
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 17)!)
        return textView
    }()
    
    let balanceAmountlbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.black
        textView.text = "1,500.00"
        textView.textAlignment = .right
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 17)!)
        return textView
    }()
    
    let earningsAmountlbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "200.00"
        textView.textAlignment = .right
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 17)!)
        return textView
    }()
    
    func setupView() {
        
        let placeholder = UIImage(named: placeholderImage)
        guard let imageUrl = URL(string: user.picture) else { return }
        
        if !(user.picture.isEmpty) {
            self.profileImageView.setImage(url: imageUrl, placeholder: placeholder)
            } else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
        
        addSubview(card)
        card.addSubview(profileImageView)
        profileImageView.addSubview(profileCoverImageView)
        
        card.addSubview(balancelbl)
        card.addSubview(balanceCurrencylbl)
        card.addSubview(balanceAmountlbl)
        
        card.addSubview(earningslbl)
        card.addSubview(earningsCurrencylbl)
        card.addSubview(earningsAmountlbl)
        
        card.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 16, widthConstant: 0, heightConstant: 70)
        
        profileImageView.centerYAnchor.constraint(equalTo: card.centerYAnchor).isActive = true
        profileImageView.leftAnchor.constraint(equalTo: card.leftAnchor, constant: 5).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        profileImageView.layer.cornerRadius = 30.0
        
        profileCoverImageView.anchor(profileImageView.topAnchor, left: profileImageView.leftAnchor, bottom: profileImageView.bottomAnchor, right: profileImageView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        //balanceAmountlbl.text = "\(user.rank)"
        balanceAmountlbl.anchor(profileImageView.topAnchor, left: nil, bottom: nil, right: card.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 24, widthConstant: 50, heightConstant: 30)
        balanceAmountlbl.isHidden = true
        
        balanceCurrencylbl.anchor(balanceAmountlbl.topAnchor, left: nil, bottom: balanceAmountlbl.bottomAnchor, right: balanceAmountlbl.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        balanceCurrencylbl.isHidden = true
        
        print("Rank: ",coach.rank)
        
        balancelbl.text = "Rank: \(coach.rank)"
        balancelbl.anchor(balanceCurrencylbl.topAnchor, left: profileImageView.rightAnchor, bottom: balanceCurrencylbl.bottomAnchor, right: balanceCurrencylbl.leftAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        earningslbl.text = "\(coach.points) points to next rank"
        earningslbl.textColor = .lightGray
        earningslbl.anchor(balancelbl.bottomAnchor, left: balancelbl.leftAnchor, bottom: profileImageView.bottomAnchor, right: balancelbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        earningsCurrencylbl.isHidden = true
        earningsCurrencylbl.anchor(earningslbl.topAnchor, left: earningslbl.rightAnchor, bottom: earningslbl.bottomAnchor, right: balanceCurrencylbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        earningsAmountlbl.isHidden = true
        earningsAmountlbl.text = "\(Mics.convertDoubleToNoCurrencySymbolCurrency(amount: self.user.totalearnings))"
        earningsAmountlbl.anchor(earningsCurrencylbl.topAnchor, left: earningsCurrencylbl.rightAnchor, bottom: earningsCurrencylbl.bottomAnchor, right: balanceAmountlbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
