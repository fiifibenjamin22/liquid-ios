//
//  QuickSelectCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 17/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class QuickSelectCell: BaseCell {
    
    var item: String?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = "GHS \(unwrapedItem)"
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 18)!)
        return textView
    }()
    
    let arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "Next")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
    
        addSubview(messageTextView)
        addSubview(arrowImageView)
        
    
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: arrowImageView.leadingAnchor, topConstant: 4, leftConstant: 0, bottomConstant: -4, rightConstant: -8)
        self.arrowImageView.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.arrowImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
    }
}
