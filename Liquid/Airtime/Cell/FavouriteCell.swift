//
//  FavouriteCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class FavouriteCell: BaseCell {
    
    var item: FavouriteContact?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.nameLabel.text = unwrapedItem.name
            self.letterImageView.image = Mics.imageWith(name: unwrapedItem.name)
            self.iconImageView.image = Mics.networkIcon(name: unwrapedItem.network)
        }
    }
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let letterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 12)!)
        return textView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.clear
        addSubview(letterImageView)
        addSubview(iconImageView)
        addSubview(nameLabel)
        
        let height = frame.height - 16 - 20
        self.letterImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.letterImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.letterImageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        self.letterImageView.bottomAnchor.constraint(equalTo: nameLabel.topAnchor, constant: -8).isActive = true
        self.letterImageView.layer.cornerRadius = height/2
        
        self.iconImageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        self.iconImageView.topAnchor.constraint(equalTo: letterImageView.topAnchor, constant: 8).isActive = true
        self.iconImageView.trailingAnchor.constraint(equalTo: letterImageView.trailingAnchor, constant: 8).isActive = true
        
        self.nameLabel.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -8, rightConstant: -8)
        self.nameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.letterImageView.image = nil
        self.iconImageView.image = nil
        self.nameLabel.text = nil
    }
}
