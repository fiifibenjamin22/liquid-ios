//
//  RecipientView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 17/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class RecipientView: BaseUIView {
    
    var item: FavouriteContact? {
        didSet {
            guard let unwrapedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
            let messageHeader = "\(unwrapedItem.name)\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
            let messageBody = "\(unwrapedItem.number)".formatAsAttributed(font: normalFont, color: UIColor.darkText)
            let result = NSMutableAttributedString()
            result.append(messageHeader)
            result.append(messageBody)
            
            self.nameTextView.attributedText = result
            self.iconImageView.image = Mics.networkIcon(name: unwrapedItem.network)
        }
    }
    
    var itemTwo: FavouriteContact? {
        didSet {
            guard let unwrapedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
            let messageHeader = "\(unwrapedItem.name)\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
            let messageBody = "\(unwrapedItem.number)".formatAsAttributed(font: normalFont, color: UIColor.darkText)
            let result = NSMutableAttributedString()
            result.append(messageHeader)
            result.append(messageBody)
            
            self.nameTextView.attributedText = result
            self.iconImageView.image = Mics.networkIcon(name: unwrapedItem.network)
        }
    }
    
    var goal: Goal? {
        didSet {
            guard let unwrapedItem = goal else { return }
            
            let issuer = unwrapedItem.withdrawalDestinationIssuer
            let issuerComponents = issuer.split(separator: "-")
            let user = User.getUser()!
            
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
            let messageHeader = "\(user.firstName) \(user.lastName)\n".formatAsAttributed(font: boldFont, color: UIColor.darkText)
            let messageBody = "\(issuerComponents[0])".formatAsAttributed(font: normalFont, color: UIColor.darkText)
            let result = NSMutableAttributedString()
            result.append(messageHeader)
            result.append(messageBody)
            
            self.nameTextView.attributedText = result
            self.iconImageView.image = UIImage(named: Mics.iconNameFromPhoneNumber(number: goal!.withdrawalDestinationIdentifier))
        }
    }
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.text = "RECIPIENT"
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        return textView
    }()
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let nameTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        textView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        return textView
    }()
    
    
    override func setUpLayout() {
        super.setUpLayout()
        backgroundColor = UIColor.white
        
        addSubview(messageTextView)
        addSubview(iconImageView)
        addSubview(nameTextView)
        
        self.messageTextView.anchorToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        self.iconImageView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: leadingAnchor, bottom: nil, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.iconImageView.widthAnchor.constraint(equalToConstant: 68).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: 68).isActive = true
        self.iconImageView.layer.cornerRadius = 34
        
        self.nameTextView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: iconImageView.trailingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 8, bottomConstant: -16, rightConstant: -16)
        
    }
}
