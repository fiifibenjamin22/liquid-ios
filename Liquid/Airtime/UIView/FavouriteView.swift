//
//  FavouriteView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class FavouriteView: BaseCell {
    
    let collectionId = "collectionId"
    var favourites = [FavouriteContact]()
    var delegate: FavouriteDelegate?
    let font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 12)!)
    
    var favs: [FavouriteContact]? {
        didSet  {
           guard let favourites = self.favs else { return }
           self.favourites.removeAll()
           self.favourites.append(contentsOf: favourites)
           self.collectionView.reloadData()
        }
    }
    
    lazy var favouriteLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "MY FAVOURITES"
        textView.font = self.font
        return textView
    }()
    
    lazy var seeAllLabel: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.backgroundColor = UIColor.clear
        if self.favourites.count > 5 {
            button.setTitle("See All", for: .normal)
        }
        button.addTarget(self, action: #selector(seeAllFavourite(_:)), for: .touchUpInside)
        button.titleLabel?.font = self.font
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.hex(hex: AppConstants.favouriteBackgroundColor)
        addSubview(collectionView)
        addSubview(favouriteLabel)
        addSubview(seeAllLabel)
        
        self.favouriteLabel.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: seeAllLabel.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        self.favouriteLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.seeAllLabel.anchorWithConstantsToTop(top: topAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: -16)
        self.seeAllLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.seeAllLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.collectionView.anchorWithConstantsToTop(top: favouriteLabel.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 8, bottomConstant: 0, rightConstant: -8)
        self.collectionView.register(FavouriteCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.reloadData()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPress.minimumPressDuration = 0.5
        longPress.delaysTouchesBegan = true
        self.collectionView.addGestureRecognizer(longPress)
    }
    
    @objc func seeAllFavourite(_ sender: UIButton)  {
        self.delegate?.didSelectAll()
    }
    
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        if (gestureRecognizer.state != UIGestureRecognizer.State.ended){
            return
        }
        let p = gestureRecognizer.location(in: self.collectionView)
        if let indexPath = self.collectionView.indexPathForItem(at: p) {
            self.delegate?.removeContact(contact: self.favourites[indexPath.row])
        }
    }
}

extension FavouriteView: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favourites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = favourites[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! FavouriteCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = CGFloat(100)
        let itemHeight = collectionView.frame.height
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(item: self.favourites[indexPath.row])
    }
    
}

extension FavouriteView {
    //MARK:- clicked here
    func transactionClicked(item:FavouriteContact) {
        self.delegate?.didSelectContact(contact: item)
    }
}

