//
//  AmountCardView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 17/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class AmountCardView: BaseUIView {
    
    var delagate:SelectWalletDelegate?
    var amount = "0.00"
    
    let amountFeeLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.textAlignment = .left
        textView.text = "Amount\n*Fee"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 14)!)
        textView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        return textView
    }()
    
    let amountFee: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.textAlignment = .right
        textView.text = "GHS 0.0\nGHS 0"
        textView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        return textView
    }()
    
    let totalAmountLabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.textAlignment = .left
        textView.text = "Total Amount"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 18)!)
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    let totalamountFee: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.textAlignment = .right
        textView.text = ""
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    let middleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.hex(hex: "0E537C")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let payWithLable: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.textColor = UIColor.white
        label.textAlignment = .left
        label.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        label.text = "Pay With"
        return label
    }()
    
    let bgImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.backgroundColor = UIColor.hex(hex: "146CA1")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    let walletIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.clear
        imageView.image = UIImage(named: "AppIcon")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var walletUILable: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.textColor = UIColor.white
        button.titleLabel?.textAlignment = .left
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
        button.setTitle(AppConstants.defaultWalletName, for: .normal)
        button.addTarget(self, action: #selector(pickWallet), for: .touchUpInside)
        return button
    }()
    
    lazy var walletPickerButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "ArrowDown"), for: .normal)
        button.addTarget(self, action: #selector(pickWallet), for: .touchUpInside)
        return button
    }()
    
  
    override func setUpLayout() {
        super.setUpLayout()
        
        addSubview(bgImageView)
        addSubview(amountFeeLabel)
        addSubview(amountFee)
        addSubview(middleView)
        addSubview(payWithLable)
        addSubview(walletIconImageView)
        addSubview(walletUILable)
        addSubview(walletPickerButton)
        middleView.addSubview(totalAmountLabel)
        middleView.addSubview(totalamountFee)
        
        self.bgImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        let amountHeight = CGFloat(ConstantHeight.WALLET_CARD_HEIGHT.rawValue) * 0.3
        let middleSection = CGFloat(ConstantHeight.WALLET_CARD_HEIGHT.rawValue) * 0.3
      
        self.amountFeeLabel.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        self.amountFeeLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.amountFeeLabel.heightAnchor.constraint(equalToConstant: amountHeight).isActive = true
        
        self.amountFee.anchorWithConstantsToTop(top: topAnchor, left: amountFeeLabel.trailingAnchor, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -8)
        self.amountFee.heightAnchor.constraint(equalToConstant: amountHeight).isActive = true
        
        self.middleView.anchorWithConstantsToTop(top: amountFeeLabel.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.middleView.heightAnchor.constraint(equalToConstant: middleSection).isActive = true
        
        self.totalAmountLabel.anchorWithConstantsToTop(top: middleView.topAnchor, left: middleView.leadingAnchor, bottom: middleView.bottomAnchor, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: -8, rightConstant: 0)
        self.totalAmountLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.totalamountFee.anchorWithConstantsToTop(top: middleView.topAnchor, left: totalAmountLabel.trailingAnchor, bottom: middleView.bottomAnchor, right: middleView.trailingAnchor, topConstant: 16, leftConstant: 8, bottomConstant: -16, rightConstant: -8)
        
        self.payWithLable.anchorWithConstantsToTop(top: middleView.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: -8)
        self.payWithLable.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.walletPickerButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.walletPickerButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.walletPickerButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        let heightMics = (CGFloat(ConstantHeight.WALLET_CARD_HEIGHT.rawValue) * 0.4) - 20 - 8 - 8 - 16
        self.walletIconImageView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 8, bottomConstant: -8, rightConstant: 0)
        self.walletIconImageView.widthAnchor.constraint(equalToConstant: heightMics).isActive = true
        self.walletIconImageView.heightAnchor.constraint(equalToConstant: heightMics).isActive = true
        
        self.walletUILable.anchorWithConstantsToTop(top: payWithLable.bottomAnchor, left: walletIconImageView.trailingAnchor, bottom: bottomAnchor, right: walletPickerButton.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -4)
        
        self.walletIconImageView.layer.cornerRadius = heightMics/2
        self.totalamountFee.text = "GHS \(self.amount)"
        self.amountFee.text = "GHS \(self.amount)\nGHS 0"
        
        /*
        guard let amountEntered = Double(self.amount) else { return }
        let feeCharged : Double = amountEntered * 0.10
        let totalAmount: Double = amountEntered + feeCharged
        
        print("charged fee \(feeCharged) on \(self.amount)")
        
        self.totalamountFee.text = "GHS \(totalAmount)"
        self.amountFee.text = "GHS \(self.amount)\nGHS \(feeCharged)"
        */
        
    }
    
    @objc func pickWallet(){
       self.delagate?.didSelectPicker()
    }
    
    func updateAmount(amount:String) {
        self.totalamountFee.text = "GHS \(amount)"
        self.amountFee.text = "GHS \(amount)\nGHS 0"
        
        /*
        guard let amountEntered = Double(amount) else { return }
        let feeCharged : Double = amountEntered * 0.10
        let totalAmount: Double = amountEntered + feeCharged
        print("charged fee \(feeCharged) on \(amount)")
        
        self.totalamountFee.text = "GHS \(totalAmount)"
        self.amountFee.text = "GHS \(amount)\nGHS \(feeCharged)"
        */
    }
    
    func updateWallet(wallet:Wallet) {
         self.walletUILable.setTitle(wallet.name, for: .normal)
        
    }
    
    func updateGoalInfo(goal:Goal) {
        self.walletPickerButton.isHidden = true
        self.walletPickerButton.removeTarget(self, action: #selector(pickWallet), for: .touchUpInside)
        self.walletUILable.removeTarget(self, action: #selector(pickWallet), for: .touchUpInside)
        self.walletUILable.setTitle(goal.name, for: .normal)
      
        if  !(goal.image.isEmpty) {
            self.walletIconImageView.af_setImage(
                withURL: URL(string: (goal.image))!,
                placeholderImage: Mics.placeHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.walletIconImageView.image = Mics.placeHolder()
        }
    }
}
