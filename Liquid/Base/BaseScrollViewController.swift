//
//  BaseScrollViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class BaseScrollViewController: UIViewController {
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: self.view.bounds)
        scrollView.backgroundColor = UIColor.clear
        scrollView.alwaysBounceVertical = true
        scrollView.isPagingEnabled = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(scrollView)

        self.scrollView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.setUpView()
    }
    
    func setUpNavigationBar(title:String = "")  {
        let image = UIImage(named: "BackArrow")?.withRenderingMode(.alwaysOriginal)
        self.baseNavigation(title: title, image: image!)
    }
    
    func setUpNavigationBarWhite(title:String = "")  {
        let image = UIImage(named: "BackWhite")?.withRenderingMode(.alwaysOriginal)
        self.baseNavigation(title: title, image: image!)
        
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
    }
    
    func setUpNavigationBarWhiteExtra(title:String = "")  {
        let image = UIImage(named: "BackWhite")?.withRenderingMode(.alwaysOriginal)
        self.baseNavigation(title: title, image: image!)
    }
    
    func baseNavigation(title:String = "", image:UIImage)  {
        
        self.view.backgroundColor = UIColor.white
        navigationItem.title = title
        
        let menuBack = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleCancel))
        
        if (self.navigationController?.viewControllers.count)! > 1 {
            navigationItem.leftBarButtonItem = menuBack
        }
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
    }
    
    
    @objc func handleCancel()  {
        if let navigation = self.navigationController {
            let controllers = navigation.viewControllers
            if controllers.count == 1 {
                self.dismiss(animated: true, completion: nil)
            }  else {
                navigation.popViewController(animated: true)
            }
        }  else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func setUpView()  {
        
    }
    
}
