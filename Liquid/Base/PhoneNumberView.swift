//
//  PhoneNumberView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class PhoneNumberView: BaseUIView {

    var selectPicker: CountryPickerDelegate?
    var delegate: TextChangeDelegate?
    var phoneNumber = ""
    
    lazy var countryButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        let text = Mics.flag(country: "GH")
        button.setTitle("\(text) +233", for: .normal)
        button.layer.cornerRadius = 0
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.addTarget(self, action: #selector(pickCountry), for: .touchUpInside)
        
        let frameWidth = CGFloat(110)
        let image = UIImage(named: "DownArrow")!
        button.imageView?.contentMode = .scaleAspectFit
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: frameWidth  - image.size.width*1.5, bottom: 0, right: 0);
        
        return button
    }()
    
    lazy var numberTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.numberPad
        textField.delegate = self
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        textField.textColor = UIColor.darkText
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter your mobile number",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        return textField
    }()

    

   override func setUpLayout()  {
    
        addSubview(countryButton)
        addSubview(numberTextField)
    
        self.countryButton.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil,topConstant:0 , leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.countryButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
    
        self.numberTextField.anchorWithConstantsToTop(top: topAnchor, left: countryButton.trailingAnchor, bottom: bottomAnchor, right: trailingAnchor,topConstant:0 , leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.numberTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    @objc private func pickCountry() {
        self.selectPicker?.clickCountryPicker()
    }
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        let code = self.countryButton.currentTitle!.split(separator: " ")[1]
        self.phoneNumber = "\(code)\(textField.text!)"
        self.delegate?.didChange(text: textField.text!)
    }
    
}

extension PhoneNumberView : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
    
}

