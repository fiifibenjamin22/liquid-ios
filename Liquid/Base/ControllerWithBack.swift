//
//  ControllerWithBack.swift
//  Liquid
//
//  Created by Benjamin Acquah on 02/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class ControllerWithBack: UIViewController {
    
    func setUpNavigationBar(title:String = "")  {
        let image = UIImage(named: "BackWhite")?.withRenderingMode(.alwaysOriginal)
        self.baseNavigation(title: title, image: image!)
        
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
    }
    
    func setUpShowNavigationBar(title:String = "")  {
        let image = UIImageView(image: UIImage(named: "chevron-right")?.withRenderingMode(.alwaysTemplate).rotate(radians: .pi))
        image.tintColor = .white
        image.contentMode = .scaleAspectFill
        self.baseShowNavigation(title: title, image: image)
        
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
    }
    
    func setUpNavigationBarDark(title:String = "")  {
        let image = UIImage(named: "BackArrow")?.withRenderingMode(.alwaysOriginal)
        self.baseNavigation(title: title, image: image!)
    }
    
    func baseShowNavigation(title:String = "", image:UIImageView)  {
        self.view.backgroundColor = UIColor.white
        navigationItem.title = title
        
        let menuBack = UIBarButtonItem(image: image.image, style: .plain, target: self, action: #selector(handleCancel))
        navigationItem.leftBarButtonItem = menuBack
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    func baseNavigation(title:String = "", image:UIImage)  {
        self.view.backgroundColor = UIColor.white
        navigationItem.title = title
        
        let menuBack = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleCancel))
        navigationItem.leftBarButtonItem = menuBack
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }

    
    func setTitle(title:String = "")  {
        self.view.backgroundColor = UIColor.white
     
        navigationItem.title = ""
        navigationController?.navigationBar.isTranslucent = false
        
        //this kind off covers the first one
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text = title
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        navigationItem.titleView = titleLabel
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func handleCancel()  {
        if let navigation = self.navigationController {
            let controllers = navigation.viewControllers
            if controllers.count == 1 {
                self.dismiss(animated: true, completion: nil)
            }  else {
                navigation.popViewController(animated: true)
            }
        }  else {
           dismiss(animated: true, completion: nil)
        }
    }
    
}
