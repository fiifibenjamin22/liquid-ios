//
//  BaseUIView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class BaseUIView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUpLayout()
    }
    
    func setUpLayout()  {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

