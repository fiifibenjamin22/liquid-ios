//
//  BaseTableCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class BaseTableHeaderCell: UITableViewHeaderFooterView {
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpLayout()  {
        
    }
}
