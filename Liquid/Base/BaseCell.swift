//
//  BaseCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class BaseCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        self.setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpView(){
        
    }
}

