//
//  BaseTableCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class BaseTableCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style , reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpView(){
        
    }
    
}
