//
//  Images.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

struct Images {
    
    static let barBg = "UIBarBg"
    static let roundButton = "RoundButton"
    static let splashLogo = "SplashLogo"
    
    struct Account {
        static let imviteBg = "ShareBg"
        static let profileWave = "ProfileWave"
        static let accountBg = "MyAccountBg"
        static let edit = "Edit"
        static let fingerprint = "Fingerprint"
        static let socialInstagram = "SocialInstagram"
        static let socialTwitter = "SocialTwitter"
        static let socialFacebook = "SocialFacebook"
        static let socialLinkedin = "Linkedin"
    }
    
    struct Onboard {
        static let logoQ = "LogoQ"
        static let eyeClosed = "EyeClosed"
        static let eye = "Eye"
    }
    
}
