//
//  LQWebViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 12/02/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class LQWebViewController: UIViewController, UIWebViewDelegate {
    var url: String?
    let utilViewController = ViewControllerHelper()

    lazy var webView: UIWebView = {
        let webview = UIWebView()
        webview.backgroundColor = UIColor.clear
        webview.translatesAutoresizingMaskIntoConstraints = false
        return webview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(webView)
        webView.delegate = self
        
        self.navigationItem.title = self.title!
        
        let frame = self.view.frame.width
        self.webView.anchorWithConstantsToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: -16, rightConstant: 0)
        self.webView.widthAnchor.constraint(equalToConstant: frame).isActive = true
        
        self.webView.loadRequest(URLRequest(url: URL(string: self.url!)!))
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.utilViewController.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.utilViewController.hideActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.utilViewController.hideActivityIndicator()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
