//
//  Validator.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation
import PhoneNumberKit

class Validator {
    static func isValidEmail(email:String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    static func isValidPassword(password: String?) -> Bool {
        guard password != nil else { return false }
        
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if password!.rangeOfCharacter(from: characterset.inverted) != nil {
            //contains special characters
            return false
        }

        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    static func validNumber(number: String) -> Bool {
        let phoneNumberKit = PhoneNumberKit()
        do {
            let phoneNumber = try phoneNumberKit.parse(number, withRegion: "GH", ignoreType: true)
            return !phoneNumber.notParsed()
        }
        catch {
            return false
        }
    }
    
    static func hasNumber(password: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "0123456789")
        if password.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        
        return false
    }
    
    static func hasEightCharacters(password: String) -> Bool {
        return password.count > 7
    }
    
    static func hasUppercase(password: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if password.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        
        return false
    }
    
    static func hasSpecialCharacter(password: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if password.rangeOfCharacter(from: characterset.inverted) != nil {
            //contains special characters
            return true
        }
        
        return false
    }
    
    static func getPasswordMessage(password: String) -> String {
        var passwordHintMessage = "Password should:\n"
        passwordHintMessage = passwordHintMessage + (Validator.hasEightCharacters(password: password) ? "✓" : "○")
        passwordHintMessage = passwordHintMessage + " Have at least of 8 characters\n"
        passwordHintMessage = passwordHintMessage + (Validator.hasNumber(password: password) ? "✓" : "○")
        passwordHintMessage = passwordHintMessage + " Contain a number\n"
        passwordHintMessage = passwordHintMessage + (Validator.hasUppercase(password: password) ? "✓" : "○")
        passwordHintMessage = passwordHintMessage + " Contain an uppercase letter\n"
        passwordHintMessage = passwordHintMessage + (Validator.hasSpecialCharacter(password: password) ? "○" : "✓")
        passwordHintMessage = passwordHintMessage + " Not contain any special characters"
        return passwordHintMessage
    }
    
}
