//
//  Font.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation
import UIKit

struct Font {
    struct Roboto {
        static let Bold = "Roboto-Bold"
        static let Light = "Roboto-Light"
        static let Thin = "Roboto-Thin"
        static let Regular = "Roboto-Regular"
        static let LightItalic = "Roboto-LightItalic"
        static let ThinItalic = "Roboto-ThinItalic"
        static let Italic = "Roboto-Italic"
    }
    
    struct RobotoMetric {
        static let bold16 = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
        static let light16 = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 16)!)
        static let thin16 = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 16)!)
    }
}
