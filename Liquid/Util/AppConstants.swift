//
//  AppConstants.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

struct AppConstants {
    
    //MARK: password hint section
    static let passwordHintMessage = "Password should:\n○ Have at least of 8 characters\n○ Contain a number\n○ Contain an uppercase letter\n○ Not contain any special characters"
    
    //MARK: support sections
    static let tollNumber = "+233 55 252 4532"
    static let supportEmail = "support@liquid.com.gh"
    static let busineesWhatsAppLine = "+233 55 252 4532"
    
    //MARK: social media section
    static let facebookAccount = "https://www.facebook.com/GetLiquidGh/"
    static let linkedInAccount = "https://www.linkedin.com/company/getliquidgh/"
    static let twitterAccount = "https://www.twitter.com/GetLiquidgh"
    static let instagramAccount = "https://www.instagram.com/getliquidgh/"
    
    static let faqUrl = "https://liquid.com.gh/app/faqs"
    static let termsUrl = "https://liquid.com.gh/app/terms-and-conditions"
    static let goalTermsUrl = "https://liquid.com.gh/app/terms-and-conditions-investments"
    
    //MARK: share message
    static let inviteMessage = "Hi there, I just signed up with a great new investment app called LIQUID. Click here https://liquid.app to download and use my referral code -referralcode- to join me and many others with exclusive access to saving towards our financial goals and freedom. Have any questions? "
    
    static let inviteToGoalMessage = "I want you to be a part of my goal on LIQUID. To do so you will need to download the liquid app to securely contribute and monitor progress on my goal. Get started now by downloading the app here https://liquid.app/  and use my referral code -referralcode- when signing up."
    
    //"Hi there, I just signed up with a great new investment app called LIQUID. Click here https://liquid.app to download and use my referral code -referralcode- to join me and many others with exclusive access to saving towards our financial goals and freedom. Have any questions? Visit https://www.liquid.com.gh/"
    static let waitListLink = "https://www.waitlist.liquid.com.gh"
    
    
    //MARK: wallet
    static let defaultWalletName = "Liquid Rewards/Refunds"
    static let bankTransferWalletName = "Bank Transfer"
    
    //MARK: color
    static let favouriteBackgroundColor = "E5F3FD"
    
    //MARK: terms
    static let termsLink = "https://www.waitlist.liquid.com.gh"
    static let privacyLink = "https://www.waitlist.liquid.com.gh"
    
    
}
