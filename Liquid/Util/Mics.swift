//
//  Mic.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import SwiftyJSON
import Firebase
import PhoneNumberKit

struct Mics {
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func hexStringToUIColor (hex:String,alpha:Double) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
    public static func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }

    static func failureMessage () -> String {
        return "Failed to get data, something unexpeced went wrong -- probably internet connection"
    }
    
    static func countryShortCode() -> String {
        var code = "Gh";
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String{
            code = countryCode
        }
        return code.uppercased()
    }
    
    static func attributeText(text:String,fontSize:Int,color: UIColor) -> NSAttributedString{
        let combination = NSMutableAttributedString()
        let yourAttributes = [NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: UIFont.systemFont(ofSize: CGFloat(fontSize))]
        let partOne = NSMutableAttributedString(string: text, attributes: yourAttributes)
        combination.append(partOne)
        return combination
    }
    
    
    static func dateNowInInt() -> Int{
        let someDate = Date()
        let timeInterval = someDate.timeIntervalSince1970
        return Int(timeInterval)
    }
    
    
    static func setDate(dateString: String) -> String  {
        // MARK: Date from string with custom format
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)!
        
        //local readable time
        dateFormatter.dateFormat = "EEE, MMM d, yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date)
        return timeStamp
    }
    
    static func setDateForGoalActivity(dateString: String) -> String  {
        // MARK: Date from string with custom format
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
        let date = dateFormatter.date(from: dateString)!
        
        //local readable time
        dateFormatter.dateFormat = "EEE, MMM d, yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date)
        return timeStamp
    }
    
    static func setGoalDate(dateString: String) -> String  {
        // MARK: Date from string with custom format
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "dd-MM-yyyy"
        if let date = dateFormatter.date(from: dateString) {
            //local readable time
            dateFormatter.dateFormat = "EEE, MMM d, yyyy"
            dateFormatter.timeZone = NSTimeZone.local
            let timeStamp = dateFormatter.string(from: date)
            return timeStamp
        }
        
        return ""
    }
    
    
    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    static func getHeightOfLabel(text: NSAttributedString, fontSize:CGFloat, width: CGFloat,numberOfLines:Int) -> CGFloat{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.regular)
        label.attributedText = text
        label.numberOfLines = numberOfLines
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size = label.sizeThatFits(CGSize(width: CGFloat(width), height: CGFloat(Float.greatestFiniteMagnitude)))
        return size.height
    }
    
    static func getHeightOfTextView(text: NSAttributedString, fontSize:CGFloat, width: CGFloat,numberOfLines:Int) -> CGFloat{
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.regular)
        textView.attributedText = text
        textView.textContainer.maximumNumberOfLines = numberOfLines
        textView.textContainer.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size = textView.sizeThatFits(CGSize(width: CGFloat(width), height: CGFloat(Float.greatestFiniteMagnitude)))
        return size.height
    }
    
    static func convertDoubleToCurrency(amount: Double) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencyCode = "GHS"
        numberFormatter.currencySymbol = "GHS "
        return numberFormatter.string(from: NSNumber(value: amount))!
    }
    
    static func convertDoubleToNoCurrencySymbolCurrency(amount: Double) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencyCode = "GHS"
        numberFormatter.currencySymbol = ""
        return numberFormatter.string(from: NSNumber(value: amount))!
    }
    
    static func getCountryCallingCode(countryRegionCode:String)->String{
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
        
    }

    static func placeHolder() -> UIImage {
        return  UIImage(named: "iTunesArtwork")!
    }
    
    static func userPlaceHolder() -> UIImage {
        return  UIImage(named: "ProfileWave")!
    }
    
    static func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    static func currentTime(date: Date = Date()) -> String {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return "\(hour):\(minutes)"
    }
    
    static func walletNetworkColor(name: String) -> UIColor{
        let nameIn = name.uppercased()
        var color = UIColor.yellow
        switch nameIn {
        case "MTN":
            color = UIColor.hex(hex: "FDBD2C")
        case "VODAFONE":
            color = UIColor.red
        case "GLO":
            color = UIColor.green
        case "TIGO":
            color = UIColor.blue
        case "AIRTEL":
            color = UIColor.red
        case "VISA":
            color = UIColor.hex(hex: "2A3986")
        case "MASTERCARD":
            color = UIColor.hex(hex: "C9242D")
        default:
            color = UIColor.hex(hex: "2A3986")
        }
        return color
    }
    
    static func payoutIcon(name: String) -> UIImage{
        var image = UIImage(named: "MobileMoney")
        switch name {
        case "MOMO":
            image = UIImage(named: "MobileMoney")
        case "MTN":
            image = UIImage(named: "MTNmomo")
        case "TIGO":
            image = UIImage(named: "TigoCash")
        case "AirtelMoney":
            image = UIImage(named: "AirtelMoney")
        case "Vodafone":
            image = UIImage(named: "VodafoneCash")
        default:
            image = UIImage(named: "BankTransfer")
        }
        return image!
    }
    
    static func iconNameFromPhoneNumber(number: String) -> String {
        var image = "Bank"
        
        if Mics.matchCodes(number: number, codes: ["024", "054", "055"]){
            image = "MTNmomo"
        } else if Mics.matchCodes(number: number, codes: ["027", "057"]){
            image = "TigoCash"
        } else if Mics.matchCodes(number: number, codes: ["020", "050"]){
            image = "VodafoneCash"
        } else if Mics.matchCodes(number: number, codes: ["026", "056"]){
            image = "AirtelMoney"
        }
        
        return image
    }
    
    
    static func iconFromPhoneNumber(number: String) -> UIImage {
        var image = UIImage(named: "BankTransfer")

        if Mics.matchCodes(number: number, codes: ["024", "054", "055"]){
            image = UIImage(named: "MTNmomo")
        } else if Mics.matchCodes(number: number, codes: ["027", "057"]){
            image = UIImage(named: "TigoCash")
        } else if Mics.matchCodes(number: number, codes: ["020", "050"]){
            image = UIImage(named: "VodafoneCash")
        } else if Mics.matchCodes(number: number, codes: ["026", "056"]){
            image = UIImage(named: "AirtelMoney")
        }
        
        return image!
    }
    
    static func walletIconFromIssuer(name: String) -> UIImage {
        var image = UIImage(named: "BankTransfer")
        switch name {
        case "MTN":
            image = UIImage(named: "MTNmomo")
        case "Vodafone":
            image = UIImage(named: "VodafoneCash")
        case "TIGO":
            image = UIImage(named: "TigoCash")
        case "Airtel":
            image = UIImage(named: "AirtelMoney")
        default:
            image = UIImage(named: "BankTransfer")
        }
        return image!
    }
    
    static func matchCodes(number: String, codes: [String]) -> Bool {
        for code in codes {
            if number.starts(with: code){
                return true
            }
            var newCode = code
            newCode.removeFirst()
        
            if number.starts(with: "233" + newCode){
                return true
            }
        }
        return false
    }
    
    static func walletBackNetworkIcon(name: String) -> UIImage {
        let nameIn = name.uppercased()
        var image = UIImage(named: "mtn_card")
        switch nameIn {
        case "MTN":
            image = UIImage(named: "mtn_card")
        case "VODAFONE":
            image = UIImage(named: "vodafone_card")
        case "GLO":
            image = UIImage(named: "NetworkGlo")
        case "TIGO":
            image = UIImage(named: "tigo_card")
        case "AIRTEL":
            image = UIImage(named: "airtel_card")
        case "VISA":
            image = UIImage(named: "visa_card")
        case "MASTERCARD":
            image = UIImage(named: "master_card")
        default:
            image = UIImage(named: "AppIcon")
        }
        return image!
    }
    
    static func walletNetworkIcon(name: String) -> UIImage {
        let nameIn = name.uppercased()
        print("print name of icon::::: ",nameIn)
        var image = UIImage(named: "NetworkMtn")
        switch nameIn {
        case "MTN":
            image = UIImage(named: "NetworkMtn")
        case "VODAFONE":
            image = UIImage(named: "NetworkVodafone")
        case "GLO":
            image = UIImage(named: "NetworkGlo")
        case "TIGO":
            image = UIImage(named: "NetworkTigo")
        case "AIRTEL":
            image = UIImage(named: "NetworkAirtel")
        case "VISA":
            image = UIImage(named: "VisaIcon")
        case "MASTERCARD":
            image = UIImage(named: "MasterCardIcon")
        case "BANKTRANSFERS":
            image = UIImage(named: "bankTransfers")
        default:
            image = UIImage(named: "SplashLogo")
        }
        return image!
    }
    
    
    static func walletIcon(name:String) -> UIImage {
        var image = UIImage(named: "NetworkMtn")
        switch name {
        case "MTN":
            image = UIImage(named: "MTNMobileMoney")
        case "Vodafone":
            image = UIImage(named: "NetworkVodafone")
        case "Glo":
            image = UIImage(named: "NetworkGlo")
        case "Tigo":
            image = UIImage(named: "TigoCash")
        case "Airtel":
            image = UIImage(named: "AirtelMoney")
        case "VISA":
            image = UIImage(named: "VisaBg")
        default:
            image = UIImage(named: "AppIcon")
        }
        return image!
    }
    
    
    //MARK: network icon
    static func networkIcon(name: String) -> UIImage {
        var image = UIImage(named: "NetworkMtn")
        switch name {
            case TargetIssuer.MTN_AIRTIME.rawValue:
                image = UIImage(named: "NetworkMtn")
            case TargetIssuer.VODAFONE_AIRTIME.rawValue:
                image = UIImage(named: "NetworkVodafone")
            case TargetIssuer.GLO_AIRTIME.rawValue:
                image = UIImage(named: "NetworkGlo")
            case TargetIssuer.TIGO_AIRTIME.rawValue:
                image = UIImage(named: "NetworkTigo")
            case TargetIssuer.AIRTEL_AIRTIME.rawValue:
                image = UIImage(named: "NetworkAirtel")
            case TargetIssuer.LIQUID_ACCOUNT.rawValue:
                image = UIImage(named: "AppIcon")
            case TargetIssuer.MTN_MOMO.rawValue:
                image = UIImage(named: "MTNMobileMoney")
            case TargetIssuer.VODAFONE_CASH.rawValue:
                image = UIImage(named: "NetworkGlo")
            case TargetIssuer.TIGO_CASH.rawValue:
                image = UIImage(named: "NetworkTigo")
            case TargetIssuer.AIRTEL_MONEY.rawValue:
                image = UIImage(named: "NetworkAirtel")
            case TargetIssuer.BUSY4G_DATA.rawValue:
                image = UIImage(named: "Busy")
            case TargetIssuer.SURFLINE_DATA.rawValue:
                image = UIImage(named: "Surfline")
            case TargetIssuer.BANK_ACCOUNT_VIA_ACH.rawValue:
                image = UIImage(named: "BankZenith")
            case TargetIssuer.BANK_ACCOUNT_VIA_INSTAPAY.rawValue:
                image = UIImage(named: "BankZenith")
            case TargetIssuer.BOXOFFICE_TV.rawValue:
                image = UIImage(named: "BoxOffice")
            case TargetIssuer.DSTV_TV.rawValue:
                image = UIImage(named: "DStv")
            case TargetIssuer.DEBIT_CARD.rawValue:
                image = UIImage(named: "VisaIcon")
            case TargetIssuer.VODAFONE_BROADBAND_DATA.rawValue:
                image = UIImage(named: "NetworkVodafone")
            case TargetIssuer.GOTV_TV.rawValue:
                image = UIImage(named: "Gotv")
            case TargetIssuer.ECG_POSTPAID_BILLPAYMENT.rawValue:
                image = UIImage(named: "Ecg")
            
            default:
                image = UIImage(named: "AppIcon")
        }
        return image!
    }
    
    static func isAirtime(network:String) -> Bool {
       return network == "AIRTEL_AIRTIME" || network == "MTN_AIRTIME" || network == "GLO_AIRTIME" || network == "TIGO_AIRTIME" || network == "VODAFONE_AIRTIME"
    }
    
    static func isData(network:String) -> Bool {
        return network == "BUSY4G_DATA" || network == "MTN_POSTPAID_DATA" || network == "VODAFONE_BROADBAND_DATA" || network == "SURFLINE_DATA" || network == "SURFLINE_PLUS_DATA"
    }
    
    static func isSendingMoney(network:String) -> Bool {
        return network == "MTN_MOMO" || network == "TIGO_CASH" || network == "VODAFONE_CASH" || network == "AIRTEL_MONEY"
    }
    

    static func targetIssuerToName(target:String) -> String {
        var name = "Service"
        switch target {
        case "AIRTEL_AIRTIME":
            name = "Airtel"
        case "MTN_AIRTIME":
             name = "MTN"
        case "GLO_AIRTIME":
             name = "Glo"
        case "TIGO_AIRTIME":
             name = "Tigo"
        case "VODAFONE_AIRTIME":
            name = "Vodafone"
        case SourceIssuer.MTN_MOMO.rawValue:
            name = "Mobile Money"
        case SourceIssuer.AIRTEL_MONEY.rawValue:
            name = "Airtel Money"
        case SourceIssuer.TIGO_CASH.rawValue:
            name = "Tigo Cash"
        case SourceIssuer.VODAFONE_CASH.rawValue:
            name = "Vodafone Cash"
        case "BUSY4G_DATA":
            name = "Busy 4G"
        case "MTN_POSTPAID_DATA":
            name = "MTN Postpaid Data"
        case "VODAFONE_BROADBAND_DATA":
            name = "Vodafone Broadband"
        case "SURFLINE_DATA":
            name = "Surfline Data"
        case "SURFLINE_PLUS_DATA":
            name = "Surfline Plus Data"
        default:
             name = "Service"
        }
        return name
    }
    
    static func sourceWalletType(wallet: Wallet) -> String{
        var sourceType = SourceIssuer.MTN_MOMO.rawValue
        let icon = wallet.icon.uppercased()
        switch icon {
        case "MTN":
            sourceType = SourceIssuer.MTN_MOMO.rawValue
        case "VODAFONE":
            sourceType = SourceIssuer.VODAFONE_CASH.rawValue
        case "TIGO":
            sourceType = SourceIssuer.TIGO_CASH.rawValue
        case "AIRTEL":
            sourceType = SourceIssuer.TIGO_CASH.rawValue
        case "GOALWITHRAW":
            sourceType = SourceIssuer.LIQUID_GOAL_ACCOUNT.rawValue
        case "VISA":
            sourceType = SourceIssuer.DEBIT_CARD.rawValue
        case "MASTERCARD":
            sourceType = SourceIssuer.DEBIT_CARD.rawValue
        default:
            sourceType = SourceIssuer.LIQUID_ACCOUNT.rawValue
        }
        return sourceType
    }
    
    
  
    static func imageWith(name: String?) -> UIImage? {
            let frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            let nameLabel = UILabel(frame: frame)
            nameLabel.textAlignment = .center
            nameLabel.backgroundColor = UIColor.random
            nameLabel.textColor = .white
            nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
            var initials = ""
            if let initialsArray = name?.components(separatedBy: " ") {
                if let firstWord = initialsArray.first {
                    if let firstLetter = firstWord.first {
                        initials += String(firstLetter).capitalized
                    }
                }
                if initialsArray.count > 1, let lastWord = initialsArray.last {
                    if let lastLetter = lastWord.first {
                        initials += String(lastLetter).capitalized
                    }
                }
            } else {
                return nil
            }
            nameLabel.text = initials
            UIGraphicsBeginImageContext(frame.size)
            if let currentContext = UIGraphicsGetCurrentContext() {
                nameLabel.layer.render(in: currentContext)
                let nameImage = UIGraphicsGetImageFromCurrentImageContext()
                return nameImage
            }
            return nil
    }
    
    static func showTime() -> String {
        let hour = Calendar.current.component(.hour, from: Date())
        var message = "Hello"
        if hour >= 0 && hour < 12 {
            message = "Good Morning"
        } else if hour >= 12 && hour < 17 {
            message = "Good Afternoon"
        } else if hour >= 17 {
            message = "Good Evening"
        }
        return message
    }
    
    
    static func setMainTheme()  {
        let color = UIColor.hex(hex: Key.primaryHexCode)
        UINavigationBar.appearance().barTintColor = color
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().tintColor = UIColor.hex(hex: Key.primaryHexCode)
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    static func setOnboardTheme(){
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor.darkGray
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.darkGray]
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().tintColor = UIColor.hex(hex: Key.primaryHexCode)
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    static func getGoogleApiKey() -> String {
        var keys: NSDictionary?
        var googleKey = ""
        if let path = Bundle.main.path(forResource: "Keys", ofType: "plist") {
            keys = NSDictionary(contentsOfFile: path)
            if let dict = keys {
                googleKey = (dict["GoogleApiKey"] as? String)!
                print("KEY \(googleKey)")
            }
        }
        return googleKey
    }
    
    static func getGooglePlaceApiKey() -> String {
        var keys: NSDictionary?
        var googleKey = ""
        if let path = Bundle.main.path(forResource: "Keys", ofType: "plist") {
            keys = NSDictionary(contentsOfFile: path)
            if let dict = keys {
                googleKey = (dict["GooglePlaceKey"] as? String)!
                print("KEY \(googleKey)")
            }
        }
        return googleKey
    }
    
    
    
    static func trackUsage(eventName:String,title:String) {
        let user = User.getUser()!
        Analytics.setUserProperty(eventName, forName: "\(user.lastName.uppercased())_\(user.firstName.uppercased())")
        Analytics.logEvent(eventName.uppercased(), parameters: [
            AnalyticsParameterItemID: "id-\(title)",
            AnalyticsParameterItemName: title,
            AnalyticsParameterContentType: "cont"
            ])
    }
    
    static func validNumber(number: String) -> Bool {
        let phoneNumberKit = PhoneNumberKit()
        do {
            let phoneNumber = try phoneNumberKit.parse(number, withRegion: "GH", ignoreType: true)
            return !phoneNumber.notParsed()
        } catch {
            return false
        }
    }
    
    static func loginRequired() -> Bool {
        let lastDate = UserDefaults.standard.integer(forKey: Key.lastLogin)
        let currentTime = Int(Date().timeIntervalSince1970)
        let dif = (currentTime - lastDate) * 1000
        return dif > 540000
    }
    
    static func startController(controller: UIViewController){
        let user = User.getUser()
        if Mics.loginRequired() {
            
            let destination = LoginController()
            destination.number = user!.phone
            controller.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            
        } else if user!.isLocked {
            
            let destination = LockScreenController()
            if let controllerIn = controller.navigationController {
                controllerIn.pushViewController(destination, animated: true)
            } else {
                controller.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            }
            
        } else if !user!.hasAddress && !user!.hasSkippedAddress {

            let destination = CurrentLocationController()
            if let controllerIn = controller.navigationController {
                //controllerIn.pushViewController(destination, animated: true)
                controllerIn.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            } else {
                controller.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            }
            
        } else if !user!.hasAcceptedTermsOfAgreement && !user!.hasSkippedAcceptedTermsOfAgreement {
            user!.setSecuringAccount(state: false)
            let destination = TermsController()
            destination.isJustTerms = true
            controller.present(destination, animated: true, completion: nil)
        }  else {
            user!.setSecuringAccount(state: false)
            
            Mics.setMainTheme()
            UIApplication.shared.keyWindow?.rootViewController = HomeTabController()
        }
    }
    
    static func startNextControllerForUpgrade(controller: UIViewController) {
        let user = User.getUser()
        
        if !user!.hasSelfie && !user!.hasSkippedSelfie {
            let destination = ProfilePhotoMesageController()
            if let controllerIn = controller.navigationController {
                controllerIn.pushViewController(destination, animated: true)
            } else {
                controller.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            }
        }  else if !user!.hasExtraKyc {
            let destination = AboutSelfController()
            if let controllerIn = controller.navigationController {
                controllerIn.pushViewController(destination, animated: true)
            } else {
                controller.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            }
        } else if !user!.hasIdDocument {
            let destination = DocumentController()
            if let controllerIn = controller.navigationController {
                controllerIn.pushViewController(destination, animated: true)
            } else {
                controller.present(UINavigationController(rootViewController: destination), animated: true, completion: nil)
            }
        } else if !user!.hasAcceptedTermsOfAgreement {
            user!.setSecuringAccount(state: false)
            let destination = TermsController()
            destination.isJustTerms = true
            controller.present(destination, animated: true, completion: nil)
        }  else {
            user!.setSecuringAccount(state: false)
            Mics.setMainTheme()
            controller.present(HomeTabController(), animated: true, completion: nil)
        }
    }
    
    static func updateLoginTime(){
        let timestamp = Int(Date().timeIntervalSince1970)
        UserDefaults.standard.set(timestamp, forKey: Key.lastLogin)
    }
    
   static func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
   static func getCardType(numberOnly: String) -> String {
        var type = "Unkown"
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card.rawValue
                break
            }
        }
        return type
    }
    
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
