import Foundation

class SHA256: NSObject {
    let tinkConfig = try! TINKAllConfig()
    let path = Bundle.main.path(forResource: "testKeyBin", ofType: "pliq") // file path for file "data.txt"
    var factory: TINKAead?
    var aesKey = Obsufucator().reveal(key: Encrypter().getAESKey())
    
    override init() {
        super.init()
        try! TINKConfig.register(tinkConfig)
        let reader = try! TINKBinaryKeysetReader(serializedKeyset: Data(contentsOf: URL(fileURLWithPath: path!)))
        let handle = try! TINKKeysetHandle(cleartextKeysetHandleWith: reader)
        self.factory = try! TINKAeadFactory.primitive(with: handle)
    }
    
    func encryptString(string: String) -> String {
        // Encryption
        let encData = string.data(using: .utf8)!
        let encText = try! factory!.encrypt(encData, withAdditionalData: aesKey.data(using: .utf8)!)
        return encText.base64EncodedString()
    }
    
    func decryptString(string: String) -> String {
        
        if let data = Data(base64Encoded: string) {
            
            let stringedData = "\(data)"
            let splitData = stringedData.components(separatedBy: " ")
            let encondedCharacter = splitData[0]
            print("print the character: \(encondedCharacter)")
            
            if encondedCharacter == "0"{
                return "please you do not own this account"
            }
            
            let decText = try! factory!.decrypt(data, withAdditionalData: aesKey.data(using: .utf8)!)
            return String(bytes: decText, encoding: .utf8)!
        } else {
            return ""
        }
    }
    
}

