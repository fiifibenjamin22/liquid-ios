//
//  AlertConfirmViewController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 13/05/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol AlertConfirmDelegate: AnyObject {
    func proceed()
    func dismiss()
}

class AlertConfirmViewController: UIViewController {
    
    var delegate: AlertConfirmDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func later(_ sender: Any) {
        self.delegate?.dismiss()
    }
    
    @IBAction func letsDoIt(_ sender: Any) {
        self.delegate?.proceed()
    }

}
