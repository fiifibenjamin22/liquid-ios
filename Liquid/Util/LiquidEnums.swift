//
//  LiquidEnums.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

enum MessageType {
    case failed
    case success
    case warning
    case info
}

enum ApiCallStatus {
    case FAILED
    case SUCCESS
    case WARNING
    case DETAIL
}

enum TransactionType {
    case AIRTIME
    case DATA
    case UTILITY
    case SENDING
    case RECEIVING
    case NONE
    case WITHDRAWAL
}

enum NetworkProvider: String {
    case ALL = "ALL"
    case NONE = "NONE"
    case MTN = "MTN"
    case VODAFONE = "Vodafone"
    case GLO = "Glo"
    case AIRTEL = "Airtel"
    case TIGO = "Tigo"
    case BUSY = "Busy"
    case SURFLINE = "Surfline"
}

enum WalletProvider: String {
    case NONE = "NONE"
    case FAVOURITE = "FAVOURITES"
    case LIQUID = "LIQUID"
    case MTN = "MTN Mobile Money"
    case VODAFONE = "Vodafone Cash"
    case AIRTEL = "Airtel Money"
    case TIGO = "Tigo Cash"
    case BANK = "Bank Account"
}

enum TokenProvider: String {
    case NONE = "NONE"
    case NAME = "NAME"
    case CODE = "CODE"
    case SECTION = "SECTION"
}

enum SettingType: String {
    case NONE = "NONE"
    case NAME = "NAME"
    case UPGRADE = "UPGRADE"
    case SHARE = "SHARE"
    case STATEMENT = "STATEMENT"
    case HISTORY = "HISTORY"
    case INVITE = "INVITE"
    case SECURITY = "SECURITY"
    case PAYMENT = "PAYMENT"
    case LOGOUT = "LOG"
    case LEGAL = "LEGAL"
    case TERMS = "TERMS"
    case SUPPORT = "SUPPORT"
    case FAQ = "FAQ"
    case ADD_REFERRAL = "ADD_REFERRAL"
}
enum BalanceType: String {
    case NONE = "NONE"
    case GOALS = "GOALS"
    case AVAILABLE = "AVAILABLE"
}

enum HomeFeedTyps: String {
    case PROFILE = "PROFILE"
    case BALANCE = "BALANCE"
    case REWARD = "REWARD"
    case FAVOURITE = "FAVOURITE"
    case COACH = "COACH"
    case GOAL = "GOAL"
    case NONE = "NONE"
}

enum UserStatus: String {
    case ACTIVE = "active"
    case NOT_VERIFIED = "phone_not_verified"
    case NO_PASSWORD = "no_password"
    case NO_ACCOUNT = "no_account_found"
    case NO_DOCUMENT = "no_document"
    case NO_PROFILE = "no_profile"
    case NO_TERMS = "no_terms"
    case NO_ADDRESS = "no_address"
    case NO_ABOUT = "no_about"
}

enum ConstantHeight: Double {
    case WALLET_CARD_HEIGHT = 240.0
    case RECIPIENT_CARD_HEIGHT = 150
}

enum SourceIssuer: String {
    case MTN_MOMO = "MTN_MOMO"
    case TIGO_CASH = "TIGO_CASH"
    case AIRTEL_MONEY = "AIRTEL_MONEY"
    case VODAFONE_CASH = "VODAFONE_CASH"
    case DEBIT_CARD = "DEBIT_CARD"
    case LIQUID_ACCOUNT = "LIQUID_ACCOUNT"
    case LIQUID_GOAL_ACCOUNT = "LIQUID_GOAL_ACCOUNT"
    case PARTNER_BANK = "PARTNER_BANK"
    case PARTNER_AGENT = "PARTNER_AGENT"
    case BANK_ACCOUNT = "BANK_ACCOUNT"
}

enum TargetIssuer: String {
    case NONE = "NONE"
    case MTN_AIRTIME = "MTN_AIRTIME"
    case VODAFONE_AIRTIME = "VODAFONE_AIRTIME"
    case TIGO_AIRTIME = "TIGO_AIRTIME"
    case AIRTEL_AIRTIME = "AIRTEL_AIRTIME"
    case GLO_AIRTIME = "GLO_AIRTIME"
    case BUSY4G_DATA = "BUSY4G_DATA"
    case MTN_POSTPAID_DATA = "MTN_POSTPAID_DATA"
    case VODAFONE_BROADBAND_DATA = "VODAFONE_BROADBAND_DATA"
    case SURFLINE_DATA = "SURFLINE_DATA"
    case SURFLINE_PLUS_DATA = "SURFLINE_PLUS_DATA"
    case ECG_POSTPAID_BILLPAYMENT = "ECG_POSTPAID_BILLPAYMENT"
    case GOTV_TV = "GOTV_TV"
    case BOXOFFICE_TV = "BOXOFFICE_TV"
    case DSTV_TV = "DSTV_TV"
    case IC_GOAL_ACCOUNT = "IC_GOAL_ACCOUNT"
    case LIQUID_GOAL_ACCOUNT = "LIQUID_GOAL_ACCOUNT"
    case CASH_WITHDRAWAL = "CASH_WITHDRAWAL"
    case LIQUID_ACCOUNT = "LIQUID_ACCOUNT"
    case MTN_MOMO = "MTN_MOMO"
    case TIGO_CASH = "TIGO_CASH"
    case AIRTEL_MONEY = "AIRTEL_MONEY"
    case VODAFONE_CASH = "VODAFONE_CASH"
    case BANK_ACCOUNT_VIA_INSTAPAY = "BANK_ACCOUNT_VIA_INSTAPAY"
    case BANK_ACCOUNT_VIA_ACH = "BANK_ACCOUNT_VIA_ACH"
    case TV = "TV"
    case ALL = "ALL"
    case DEBIT_CARD = "DEBIT_CARD"
}

enum PaymentMethodTypes: String {
    case BUY_AIRTIME = "BUY_AIRTIME"
    case PAY_BILL = "PAY_BILL"
    case FUND_ACCOUNT = "FUND_ACCOUNT"
    case SEND_MONEY = "SEND_MONEY"
    case BUY_DATA = "BUY_DATA"
    case MANAGE_GOAL = "MANAGE_GOAL"
    case WITHDRAW_CASH = "WITHDRAW_CASH"
}

enum MomoKeys: String {
    case VODAFONE = "Vodafone"
}

enum Liquid:String {
    case CODE = "LIQUID"
    case NAME = "MY LIQUID WALLET"
}

enum BankTransfer: String {
    case CODE = "BANK_TRANSFER"
    case NAME = "BANK TRANSFER"
}

enum PaymentOptionType:String {
    case card = "card"
    case momo = "momo"
}


enum EmailStatus:String {
    case verified = "VERIFIED"
    case notVerified = "NOT VERIFIED"
}

enum PaymentRule:String {
    case daily = "Daily"
    case weekly = "Weekly"
    case monthly = "Monthly"
}

enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}


