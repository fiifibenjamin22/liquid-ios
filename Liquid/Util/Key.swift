//
//  Key.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

struct Key {
    static let primaryHexCode = "#20BAFC"
    static let secondaryHexCode = "#63656a"
    static let tertiaryHexCode = "#898B8E"
    static let backgroundColor = "#E8E8E8"
    static let viewBackgroundColor = "#FAFAFA"
    static let googlePlacesApiKey = "AIzaSyCQ8Rj-ycSomdhqXI9d66_6weUvexs7S2Q"
    static let defaultBearerToken = "cn389ncoiwuencr"
    static let profileHeaderHeight = 100
    static let balanceSectionHeight = 300
    static let notificationCount = "NOTIFICATION_COUNT"
    static let createGoal = "CREATE_GOAL"
    static let manageGoal = "MANAGE_GOAL"
    static let transaction = "TRANSACT"
    
    static let updatedMobile = "UPDATE_MOBILE"
    static let updatedEmail = "UPDATE_EMAIL"
    
    static let doneGoalEdit = "GOAL_EDITED"
    static let walletAdded = "WALLET_ADDED"
    static let passcodeEnabled = "PASSCODE_ENABLED"
    static let fingerPrintEnabled = "BIO_ENABLED"
    static let profilePictureChanged = "PROFILE_CHANGED"
    static let lastLogin = "LAST_LOGIN"
    static let fingerPrintPin = "FINGERPRINT_PIN"
    static let profileUpdated = "PROFILE_UPDATED"
    static let colorTransactionSuccess = "417505"
    static let colorTransactionfailed = "D0021B"
    static let colorTransactionPending = "FFBF00"
    
    static let statusPending = "PENDING"
    static let statusFailed = "FAILED"
    static let statusSuccess = "SUCCESS"
    
    static let payoutWalletAdded = "PAYOUT_WALLET_ADDED"
     static let APP_NAME = "Liquid"
    
   
}
