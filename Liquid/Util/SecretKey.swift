//
//  SecretKey.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/10/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation

class SecretKey {
     static let segmentA = "yJd5^9O%2qdc"
     static let  segmentB = "EwvCo&i6bha5"
     static let segmentC = "BWJDwF%52m^p"
     static let segmentD = "!D#%X!G50zor"
     static let segmentE = "7rb7c&pQdtW*"
}
