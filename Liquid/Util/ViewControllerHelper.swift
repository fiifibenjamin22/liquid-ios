//
//  ViewControllerHelper.swift
//  Liquid
//
//  Created by Benjamin Acquah on 01/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire
import Dodo
import ADCountryPicker
import DLRadioButton
import LinearProgressBarMaterial
import GradientView

class ViewControllerHelper {
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    func showActivityIndicator() {
        if let window = UIApplication.shared.keyWindow {
            container.frame = window.frame
            container.center = window.center
            container.backgroundColor = UIColor.hexStringToUIColor(hex: "0xffffff", alpha: 0.3)
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center = window.center
            loadingView.backgroundColor = UIColor.hexStringToUIColor(hex: "0x444444", alpha: 0.7)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            
            loadingView.addSubview(activityIndicator)
            container.addSubview(loadingView)
            window.addSubview(container)
            activityIndicator.startAnimating()
            
            self.container.alpha = 0
            
            UIView.animate(withDuration: 0.5, animations: {
                self.container.alpha = 1
            })
        }
        
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    
    static func baseField() -> UITextField {
        let textField = UITextField()
        let font = UIFont(name: Font.Roboto.Light, size: 16)
        textField.font = UIFontMetrics.default.scaledFont(for: font!)
        textField.textAlignment = .left
        textField.textColor = UIColor.white
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.layer.borderWidth = 0
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0)
        textField.backgroundColor = UIColor.clear
        textField.text = ""
        textField.borderStyle = UITextField.BorderStyle.none
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    
    static func progressBar ()-> LinearProgressBar {
        let progressView = LinearProgressBar()
        progressView.heightForLinearBar = 2
        progressView.backgroundColor = UIColor.white
        progressView.progressBarColor = UIColor.hex(hex: Key.primaryHexCode)
        progressView.backgroundProgressBarColor = UIColor.white
        progressView.translatesAutoresizingMaskIntoConstraints = false
        return progressView
    }

    
    static func mainBaseField(placeHolder:String) -> UITextField {
        let textField = UITextField()
        textField.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        textField.textAlignment = .center
        textField.textColor = UIColor.white
        textField.layer.borderColor = UIColor.white.cgColor
        textField.layer.borderWidth = 1
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0)
        textField.backgroundColor = UIColor.clear
        textField.borderStyle = UITextField.BorderStyle.none
        textField.attributedPlaceholder =  NSAttributedString(string: placeHolder,
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
    
    
    static func baseLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor.gray
        label.backgroundColor = UIColor.clear
        label.text = ""
        label.lineBreakMode = .byClipping
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    static func baseProgressBar() -> UIProgressView {
        let progressBar = UIProgressView(progressViewStyle: .bar)
        progressBar.progress = 0
        progressBar.trackTintColor = UIColor.clear
        progressBar.tintColor = UIColor.hex(hex: "6DDCF1")
        progressBar.backgroundColor = UIColor.clear
        progressBar.layer.borderWidth = 1.0
        progressBar.layer.borderColor = UIColor.hex(hex: "69B1D2").cgColor
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.layer.cornerRadius = 5
        progressBar.clipsToBounds = true
        return progressBar
    }
    
    static func basePageControl() -> UIPageControl {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.currentPageIndicatorTintColor = UIColor.hex(hex: Key.primaryHexCode)
        pc.pageIndicatorTintColor = UIColor.darkGray
        pc.hidesForSinglePage = true
        pc.transform = CGAffineTransform(scaleX: 2, y: 2)
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }
    
    static func baseTextView() -> UITextView {
        let label = UITextView()
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.backgroundColor = UIColor.clear
        label.text = ""
        label.isEditable = false
        label.isScrollEnabled = false
        label.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textContainer.lineBreakMode = .byWordWrapping
        
        return label
    }
    
    static func baseCollectionView(direction: UICollectionView.ScrollDirection = UICollectionView.ScrollDirection.horizontal) -> UICollectionView {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = direction
        flow.minimumInteritemSpacing = 0
        flow.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero,collectionViewLayout: flow)
        collectionView.backgroundColor = UIColor.clear
        collectionView.bounces = true
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }
        
    static func baseVerticalCollectionView() -> UICollectionView {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .vertical
        flow.minimumInteritemSpacing = 0
        flow.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero,collectionViewLayout: flow)
        collectionView.backgroundColor = UIColor.white
        collectionView.bounces = true
        collectionView.alwaysBounceVertical = true
        collectionView.alwaysBounceHorizontal = false
        collectionView.showsVerticalScrollIndicator = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }

    static func baseUISwitch() -> UISwitch {
        let uiSwitchIn = UISwitch(frame: CGRect(x: 0, y: 0, width: 80, height: 22))
        uiSwitchIn.isOn = false
        uiSwitchIn.onTintColor = UIColor.hex(hex: Key.primaryHexCode)
        uiSwitchIn.tintColor = UIColor.hex(hex: Key.backgroundColor)
        uiSwitchIn.translatesAutoresizingMaskIntoConstraints = false
        return uiSwitchIn
    }
    
    static func baseButton() -> UIButton {
        let button = UIButton()
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        button.backgroundColor = color
        button.layer.cornerRadius = 0
        button.layer.borderWidth = 0
        button.layer.borderColor = color.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        button.titleLabel?.textAlignment = .center
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: -20)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        button.clipsToBounds = true
        
        return button
    }
    
    static func baseButtonWithRightImage() -> ButtonWithRightImage {
        let button = ButtonWithRightImage()
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        button.backgroundColor = color
        button.layer.cornerRadius = 0
        button.layer.borderWidth = 0
        button.layer.borderColor = color.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        
        button.clipsToBounds = true
        
        return button
    }
    
    static func newBaseButton() -> UIButton {
        let button = UIButton()
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
        button.backgroundColor = color
        button.layer.cornerRadius = 0
        button.layer.borderWidth = 0
        button.layer.borderColor = color.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        button.titleLabel?.textAlignment = .center
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: -20)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.layer.cornerRadius = 5
        
        return button
    }
    
    static func baseRoudImageButton() -> UIButton {
        let button = baseButton()
        button.backgroundColor = UIColor.clear
        button.setBackgroundImage(UIImage(named: Images.roundButton), for: .normal)
        return button
    }
    
    static func skipBaseButton() -> UIButton {
        let button = UIButton()
        let color = UIColor.lightGray
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1.5
        button.layer.borderColor = color.cgColor
        button.setTitleColor(color, for: .normal)
        button.titleLabel?.textAlignment = .center
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    static func plainButton() -> UIButton {
        let button = UIButton()
        let color = UIColor.clear
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 0
        button.layer.borderWidth = 0
        button.semanticContentAttribute = .forceRightToLeft
        button.titleLabel?.textAlignment = .center
        button.layer.borderColor = color.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    static func plainImageButton() -> UIButton {
        let button = UIButton()
        let color = UIColor.clear
        let font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        button.titleLabel?.font = font
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 0
        button.layer.borderWidth = 1
        button.layer.borderColor = color.cgColor
        button.setTitleColor(UIColor.white, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    
    static func tempButton() -> UIButton {
        let button = UIButton()
        let color = UIColor.hex(hex: Key.backgroundColor)
        let font = UIFont(name: "Roboto-Bold", size: 16)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: font!)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = color
        button.layer.cornerRadius = 2
        button.layer.borderWidth = 0.5
        button.layer.borderColor = color.cgColor
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.contentHorizontalAlignment = .left
        let spacing = CGFloat(10)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0)
        return button
    }
    
    static func baseImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .center
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    static func baseStackView() -> UIStackView {
        let container = UIStackView()
        container.distribution = .fillEqually
        container.axis = .horizontal
        container.spacing = 5
        container.backgroundColor = UIColor.clear
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }
    
    static func baseRadioButton(title : String, color : UIColor) -> DLRadioButton {
        let radioButton = DLRadioButton()
        let font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        radioButton.titleLabel!.font = font
        radioButton.setTitle(title, for: []);
        radioButton.setTitleColor(color, for: [])
        radioButton.iconColor = UIColor.hex(hex: Key.primaryHexCode)
        radioButton.indicatorColor = UIColor.hex(hex: Key.primaryHexCode)
        radioButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        return radioButton
    }
    
    static func baseCheckButton() -> DLRadioButton {
        let color = UIColor.hex(hex: Key.primaryHexCode)
        let radioButton = DLRadioButton()
        radioButton.titleLabel!.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        radioButton.setTitle("", for: []);
        radioButton.setTitleColor(color, for: [])
        radioButton.iconColor = color
        radioButton.indicatorColor = color
        radioButton.isIconSquare = true
        radioButton.selected()
        radioButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        return radioButton
    }
    
    
    
    static func placeCall(phone:String){
        let formatedNumber = phone.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        guard let number = URL(string: "telprompt://" + formatedNumber) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(number)
        }
    }
    
    static func openLink(url:String,vc: UIViewController){
        if let requestUrl = NSURL(string: url) {
            let svc = SFSafariViewController(url: requestUrl as URL)
            vc.present(svc, animated: true, completion: nil)
            //UIApplication.sharedApplication().openURL(requestUrl)
        }
    }
    
    static func showPrompt(vc: UIViewController,message: String) {
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        let alertView = UIAlertController(title: "Liquid", message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        alertView.addAction(alertAction)
        vc.present(alertView, animated: true, completion: nil)
    }
    
    static func showPrompt(vc: UIViewController,message: String, completion: @escaping (Bool)-> ()) {
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: {(Alert:UIAlertAction!) -> Void in
            completion(true)
        })
        let alertView = UIAlertController(title: "Liquid", message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        alertView.addAction(alertAction)
        vc.present(alertView, animated: true, completion: nil)
    }
    
    static func showPromptWithCancel(vc: UIViewController, message: String, nextActionTitle:String, completion: @escaping (Bool)-> ()) {
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: {(Alert:UIAlertAction!) -> Void in
            completion(true)
        })
        
        let alertCancelAction = UIAlertAction(title:nextActionTitle, style: .default, handler: {(Alert: UIAlertAction!) -> Void in
            
        })
        
        let alertView = UIAlertController(title: Key.APP_NAME, message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        
        alertView.addAction(alertCancelAction)
        alertView.addAction(alertAction)
        
        alertView.preferredAction = alertAction
        
        vc.present(alertView, animated: true, completion: nil)
    }
    
    static func showConfirmationPrompt(vc: UIViewController,message: String, completion: @escaping (Bool)-> ()) {
        let alertAction = UIAlertAction(title: "Confirm", style: .default, handler: {(Alert:UIAlertAction!) -> Void in
            completion(true)
        })
        let alertView = UIAlertController(title: "Liquid", message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        alertView.addAction(alertAction)
        vc.present(alertView, animated: true, completion: nil)
    }
    
    static func showVoucherPrompt(vc: UIViewController,message: String, completion: @escaping (Bool,String)-> ()) {
        let alertController = UIAlertController(title: "Liquid", message: message, preferredStyle: .alert)
        alertController.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Code"
        }
        let alertAction = UIAlertAction(title: "Confirm", style: .default, handler: {(Alert:UIAlertAction!) -> Void in
            let codeTextField = alertController.textFields![0] as UITextField
            completion(true, codeTextField.text!)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(Alert:UIAlertAction!) -> Void in
            
        })
        alertController.addAction(alertAction)
        alertController.addAction(cancelAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    static func showRemoveFavouriteCotact(vc: UIViewController, item: FavouriteContact,completion: @escaping (Bool)-> ()){
        let sender = vc.view!
        let alertController = UIAlertController(title: "LIQUID", message: "Remove Favourite", preferredStyle: .actionSheet)
        alertController.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        let defaultAction = UIAlertAction(title: "Delete Saved Contact", style: .default) { (action) in
            FavouriteContact.deleteConatct(contact: item)
            completion(true)
        }
        alertController.addAction(defaultAction)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        alertController.addAction(alertActionCancel)
        alertController.modalPresentationStyle = .popover
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        vc.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlert(vc: UIViewController,message: String,type:MessageType){
        let view = vc.view!
        view.dodo.topAnchor = view.safeAreaLayoutGuide.topAnchor
        view.dodo.bottomAnchor = view.safeAreaLayoutGuide.bottomAnchor
        view.dodo.style.leftButton.icon = .close
        view.dodo.style.leftButton.onTap = { /* Button tapped */ }
        view.dodo.style.leftButton.hideOnTap = true
        switch type {
        case .failed:
            view.dodo.error(message)
        case .info:
            view.dodo.info(message)
        case .success:
            view.dodo.success(message)
        case .warning:
            view.dodo.warning(message)
        }
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when) {
            view.dodo.hide()
        }
    }
    
    //MARK: sharing -- present sharing screen
    static func presentSharer(targetVC: UIViewController,message:String){
        let link = "https://www.liquid.com.gh/"
        var sharingContent = [AnyObject]()
        sharingContent.append(message as AnyObject)
        sharingContent.append(link as AnyObject)
        let activityViewController = UIActivityViewController(activityItems: sharingContent, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = targetVC.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        targetVC.present(activityViewController, animated: true, completion: nil)
    }
    
    static func startNext(vc: UIViewController){
        let destination = VerificationCodeController()
        let navVc = UINavigationController(rootViewController: destination)
        vc.present(navVc, animated: true, completion: nil)
    }
    
    static func presentCountryPicker(vc: UIViewController, delegate: ADCountryPickerDelegate){
        let picker = ADCountryPicker()
        picker.delegate = delegate
        picker.showCallingCodes = true
        picker.showFlags = true
        picker.pickerTitle = "SELECT A COUNTRY"
        picker.defaultCountryCode = "GH"
        picker.alphabetScrollBarTintColor = UIColor.white
        picker.alphabetScrollBarBackgroundColor = UIColor.clear
        picker.closeButtonTintColor = UIColor.darkText
        picker.showCallingCodes = false
        
        let font = UIFont(name: "Roboto-Light", size: 14)
        picker.font = UIFontMetrics.default.scaledFont(for: font!)
        picker.flagHeight = 40
        picker.hidesNavigationBarWhenPresentingSearch = true
        picker.searchBarBackgroundColor = UIColor.white
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        vc.present(pickerNavigationController, animated: true, completion: nil)
    }
    
   static func startWhatsapp() {
        let urlWhats = "whatsapp://send?phone=\(AppConstants.busineesWhatsAppLine)&text=Hello"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.openURL(whatsappURL)
                }
            }
        }
    }
    
    func addGradientToView(view: UIView, gradColors: Array<CGColor>){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradColors
        gradientLayer.locations = [0.0, 0.6, 0.8]
        gradientLayer.frame = view.bounds
        //gradientLayer.cornerRadius = 10.0
        //gradientLayer.masksToBounds = true
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

