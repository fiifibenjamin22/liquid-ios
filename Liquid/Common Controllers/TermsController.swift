//
//  TermsController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import DLRadioButton

class TermsController: UIViewController {
    var isJustTerms = false
    let fontLight = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 15)!)
    let fontBold = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 15)!)
    let defaultColor = UIColor.hex(hex: Key.primaryHexCode)
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    
    
    let topImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.image = UIImage(named: "tc")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()


    lazy var contentlabel: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.darkText
        let font = UIFont.init(name: Font.Roboto.Bold, size: 20)!
        let oneMore = "One more thing\n\n".formatAsAttributed(font: font, color: .darkText)
        let message = "Before you complete this process, it's important that you look through and agree with our terms".formatAsAttributed(font: fontLight, color: .darkText)
        let result = NSMutableAttributedString()
        result.append(oneMore)
        result.append(message)
        label.attributedText = result
        label.textAlignment = .center
        return label
    }()
    
    let mainStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.distribution = .fillProportionally
        container.axis = .vertical
        return container
    }()
    
    let termsStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.distribution = .fillProportionally
        container.axis = .horizontal
        container.addBottomBorder(UIColor.lightGray, height: 1.0)
        container.alignment = .center
        return container
    }()
    
    let privacyStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.distribution = .fillProportionally
        container.axis = .horizontal
        container.alignment = .center
        container.addBottomBorder(UIColor.lightGray, height: 1.0)
        return container
    }()
    
    lazy var termsButton: DLRadioButton = {
        let button = ViewControllerHelper.baseRadioButton(title: "", color: UIColor.darkGray)
        
        button.isIconSquare = true
        button.isMultipleSelectionEnabled = true
        let termsText = NSMutableAttributedString()
        button.iconSize = 20
        
        button.addTarget(self, action: #selector(radioToggled), for: .touchUpInside)
        return button
    }()
    
    lazy var privacyButton: DLRadioButton = {
        let button = ViewControllerHelper.baseRadioButton(title: "", color: UIColor.darkGray)
        button.isIconSquare = true
        button.isMultipleSelectionEnabled = true
        button.iconSize = 20
        button.addTarget(self, action: #selector(radioToggled), for: .touchUpInside)
        return button
    }()
    
    lazy var termslabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        let termsText = NSMutableAttributedString()
        
        let font = UIFont.init(name: Font.Roboto.Regular , size: 14)!
        termsText.append("I understand and agree to ".formatAsAttributed(font: font, color: .darkText))
        
        let termsLink = NSMutableAttributedString(attributedString: "Terms & Conditions".formatAsAttributed(font: fontBold, color: UIColor.hex(hex: Key.primaryHexCode)))
        termsLink.addAttribute(.link, value: "https://liquid.com.gh/app/terms-and-conditions", range: NSRange(location: 0, length: termsLink.length))
        termsText.append(termsLink)
        textView.attributedText = termsText
        textView.textAlignment = .left
        textView.isUserInteractionEnabled = true

        return textView
    }()
    
    lazy var privacylabel: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        let policyText = NSMutableAttributedString()
        let font = UIFont.init(name: Font.Roboto.Regular , size: 15)!
        policyText.append("I understand and agree to the ".formatAsAttributed(font: font, color: .darkText))
        let privacyLink = NSMutableAttributedString(attributedString: "Privacy policy".formatAsAttributed(font: fontBold, color: UIColor.hex(hex: Key.primaryHexCode)))
        privacyLink.addAttribute(.link, value: "https://liquid.com.gh/app/privacy-policy", range: NSRange(location: 0, length: privacyLink.length))
        policyText.append(privacyLink)
            
        textView.attributedText = policyText
//        let linkAttributes: [NSAttributedString.Key : Any]? = [
//            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): defaultColor,
//            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineColor.rawValue): defaultColor,
//            NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single
//        ]
//
//        textView.linkTextAttributes = linkAttributes
        textView.textAlignment = .left
        textView.isUserInteractionEnabled = true
        
        return textView
    }()
    
    lazy var signUpButton: UIButton = {
        let button = ViewControllerHelper.newBaseButton()
        button.setTitle("I agree", for: .normal)
        let color = UIColor.hex(hex: "e9e8e9")
        button.backgroundColor = color
        button.isEnabled = false
        button.addTarget(self, action: #selector(signUpDone), for: .touchUpInside)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.title = "Terms and Conditions"
        
        
        self.view.addSubview(signUpButton)
        self.view.addSubview(topImageView)
        self.view.addSubview(contentlabel)
        self.view.addSubview(mainStackView)
        //self.view.addSubview(privacylabel)
     
       
        self.topImageView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.topImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.topImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.topImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.contentlabel.anchorWithConstantsToTop(top: topImageView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.contentlabel.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.contentlabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        self.mainStackView.anchorWithConstantsToTop(top: contentlabel.bottomAnchor, left: view.leadingAnchor, bottom: nil, right: view.trailingAnchor, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.mainStackView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.mainStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        //self.signUpButton.anchorToTop(top: nil, left: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.trailingAnchor)
        self.signUpButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, leftConstant: 15, rightConstant: -15, bottomConstant: -15)

        
        self.signUpButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.termsStackView.addArrangedSubview(termslabel)
        self.termsStackView.addArrangedSubview(termsButton)
        self.mainStackView.addArrangedSubview(self.termsStackView)
        
        self.privacyStackView.addArrangedSubview(privacylabel)
        self.privacyStackView.addArrangedSubview(privacyButton)
        self.mainStackView.addArrangedSubview(self.privacyStackView)
    }
    
    @objc func tap(sender: UITapGestureRecognizer){
        let view = sender.view!
        if view == termslabel {
            let nav = UINavigationController(rootViewController: TermsAndConditionController())
            nav.modalPresentationStyle = .fullScreen
          self.present(nav, animated: true, completion: nil)
        }
        if view == privacylabel {
            let nav = UINavigationController(rootViewController: TermsAndConditionController())
            nav.modalPresentationStyle = .fullScreen
           self.present(nav, animated: true, completion: nil)
        }
    }
    
    @objc func radioToggled(){
        if self.termsButton.isSelected && self.privacyButton.isSelected {
            self.signUpButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.signUpButton.backgroundColor = color
        } else {
            self.signUpButton.isEnabled = false
            let color = UIColor.hex(hex: "e9e8e9")
            self.signUpButton.backgroundColor = color
        }
    }
    
    @objc func signUpDone(){
        if !self.termsButton.isSelected || !self.privacyButton.isSelected {
            ViewControllerHelper.showPrompt(vc: self, message: "Please accept the terms and conditions")
            return
        }
        
        User.updateUserTerms(status: true)
        Mics.setMainTheme()
        self.acceptTheTerms()
    }
    
    func acceptTheTerms() {
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.acceptTerms { (status, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                if self.isJustTerms {
                    self.present(HomeTabController(), animated: true, completion: nil)
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }   else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }

}
