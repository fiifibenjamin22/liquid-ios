//
//  FavouriteListController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 03/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class FavouriteListController: ControllerWithBack {
    
    let collectionId = "collectionId"
    var favourites = [FavouriteContact]()
    var delegate: FavouriteDelegate?
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(collectionView)
        
        setUpNavigationBar(title: "Favourites")
        
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 16, rightConstant: -16)
        self.collectionView.register(FavouriteCell.self, forCellWithReuseIdentifier: collectionId)
        
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPress.minimumPressDuration = 0.5
        longPress.delaysTouchesBegan = true
        self.collectionView.addGestureRecognizer(longPress)
        
        self.getData()
    }
    
    func getData()  {
        self.favourites.removeAll()
        self.favourites.append(contentsOf: FavouriteContact.all())
        self.collectionView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshFavourite"), object: self)
    }
    
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        if (gestureRecognizer.state != UIGestureRecognizer.State.ended){
            return
        }
        let p = gestureRecognizer.location(in: self.collectionView)
        if let indexPath = self.collectionView.indexPathForItem(at: p) {
            let contact = self.favourites[indexPath.row]
            ViewControllerHelper.showRemoveFavouriteCotact(vc: self, item: contact) { (isDone) in
                if isDone {
                   self.getData()
                }
            }
        }
    }
}

extension FavouriteListController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favourites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = favourites[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! FavouriteCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(3 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(3))
        return CGSize(width: size, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(item: self.favourites[indexPath.row])
    }
    
    //MARK:- clicked here
    func transactionClicked(item:FavouriteContact) {
        self.navigationController?.popViewController(animated: true)
        self.delegate?.didSelectContact(contact: item)
    }
    
}

