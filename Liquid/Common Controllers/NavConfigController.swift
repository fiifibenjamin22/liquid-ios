//
//  NavConfigController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 16/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class NavConfigController: UIViewController {
    
    //MARK:- set up navigation
    func setUpNavigationBar(title:String = "Liquid")  {
        self.setUpBaseNavigation(title: title)
        navigationController?.navigationBar.barTintColor = UIColor.hex(hex: Key.primaryHexCode)
    }
    
    //MARK:- set up plain navigation
    func setUpNavigationBarPlain()  {
        self.setUpBaseNavigation(title: "")
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    func setUpBaseNavigation(title:String)  {
        navigationItem.title = ""
        navigationController?.navigationBar.isTranslucent = false
        //this kind off covers the first one
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.text = title
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        navigationItem.titleView = titleLabel
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

}
