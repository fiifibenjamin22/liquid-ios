//
//  GenericHeader.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GenericHeader: BaseCellHeader {
    
    var item: String?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = unwrapedItem
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    override func setUpViews() {
        super.setUpViews()
        backgroundColor = UIColor.white
        
        addSubview(messageTextView)
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        
    }
}
