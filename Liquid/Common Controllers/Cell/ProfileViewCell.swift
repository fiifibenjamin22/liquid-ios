//
//  ProfileView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class ProfileViewCell: BaseCell {
    
    let profileSectionHeight = CGFloat(Key.profileHeaderHeight)

    let profileImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let profileCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ProfileWave")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let profileNameTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .justified
        return textView
    }()
    
    let user = User.getUser()!
    
    override func setUpView() {
        super.setUpView()
        
        self.addSubview(profileImageView)
        self.addSubview(profileCoverImageView)
        self.addSubview(profileNameTextView)
        
        self.profileImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.profileImageView.widthAnchor.constraint(equalToConstant: profileSectionHeight).isActive = true
        self.profileImageView.layer.cornerRadius = profileSectionHeight/2
        
        self.profileCoverImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: nil, topConstant: 50, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.profileCoverImageView.widthAnchor.constraint(equalToConstant: profileSectionHeight).isActive = true
        self.profileCoverImageView.layer.cornerRadius = profileSectionHeight/2

        self.profileNameTextView.anchorWithConstantsToTop(top: topAnchor, left: profileImageView.trailingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 60, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        
        let headerfont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        let middlefont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 30)!)
        let messageHeader = "\(Mics.showTime()),\n".formatAsAttributed(font: headerfont, color: UIColor.darkText)
        let messageBody = "\(user.firstName)".formatAsAttributed(font: middlefont, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        
        self.profileNameTextView.attributedText = result
        self.profileNameTextView.textAlignment = .left
        
        if  !(user.picture.isEmpty) {
            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleImagUpdate(notification:)), name: NSNotification.Name(Key.profilePictureChanged), object: nil)
    }
    
    @objc func handleImagUpdate(notification: Notification){
        if  !(user.picture.isEmpty) {
            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
    }

}

