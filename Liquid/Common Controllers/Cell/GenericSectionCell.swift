//
//  GenericSectionCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GenericSectionCell: BaseCell {
    var hideSeparator: Bool? {
        didSet {
            self.separatorLine.isHidden = hideSeparator!
        }
    }
    
    var item: String?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = unwrapedItem
        }
    }
    
    var attributedItem: NSAttributedString?{
        didSet {
            guard let unwrapedItem = attributedItem else { return }
            self.messageTextView.attributedText = unwrapedItem
        }
    }
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.lightGray
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 14)!)
        return textView
    }()
    
    let separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        
        return view
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        
        addSubview(messageTextView)
        addSubview(separatorLine)
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.separatorLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.separatorLine.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
    }
}



















class NotificationSectionCell: BaseTableCell {
    var hideSeparator: Bool? {
        didSet {
            self.separatorLine.isHidden = hideSeparator!
        }
    }
    
    var item: String?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = unwrapedItem
        }
    }
    
    var attributedItem: NSAttributedString?{
        didSet {
            guard let unwrapedItem = attributedItem else { return }
            self.messageTextView.attributedText = unwrapedItem
        }
    }
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.lightGray
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 16, bottom: 15, right: 16)
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        return textView
    }()
    
    let separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.isHidden = true
        return view
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        
        addSubview(messageTextView)
        addSubview(separatorLine)
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.separatorLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.separatorLine.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15)
    }
}
