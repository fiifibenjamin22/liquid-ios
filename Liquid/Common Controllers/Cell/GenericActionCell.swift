//
//  GenericActionCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GenericActionCell: BaseCell {
    
    var item: ActionItem? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = "\(unwrapedItem.name)"
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        return textView
    }()
    
    let arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "Next")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let topSplitView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bottomSplitView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(topSplitView)
        addSubview(messageTextView)
        addSubview(arrowImageView)
        addSubview(bottomSplitView)
        
        self.topSplitView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.topSplitView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: topSplitView.bottomAnchor, left: leadingAnchor, bottom: bottomSplitView.topAnchor, right: arrowImageView.leadingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        self.arrowImageView.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.arrowImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        self.bottomSplitView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.bottomSplitView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
    }
    
}
