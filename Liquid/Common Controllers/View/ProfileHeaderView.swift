//
//  ProfileHeaderView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//



import UIKit

class ProfileHeaderView: BaseUIView {
    
    var height = CGFloat(80)
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let profileCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ProfileWave")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    override func setUpLayout() {
        super.setUpLayout()
        
        
        self.addSubview(profileImageView)
        self.addSubview(profileCoverImageView)
        
        self.profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.profileImageView.layer.cornerRadius = height/2
        
        self.profileCoverImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.profileCoverImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.profileCoverImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.profileCoverImageView.layer.cornerRadius = height/2
        
        let user = User.getUser()!
        if  !(user.picture.isEmpty) {
            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
        
    }
}
