//
//  InviteFriendCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class InviteFriendCell: BaseCell {
    
    var item: Friend? {
        didSet {
            guard let unwrapedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 18)!)
            let normalFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 14)!)
            
            let attributes = [NSAttributedString.Key.foregroundColor: UIColor.darkText, NSAttributedString.Key.font: boldFont]
            let andAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: normalFont]
            
            let header = NSMutableAttributedString(string: "\(unwrapedItem.inviteeName)", attributes: attributes)
            let and = NSMutableAttributedString(string: "\n\(unwrapedItem.inviteeContact)", attributes: andAttributes)
            
            let combinedText = NSMutableAttributedString()
            combinedText.append(header)
            combinedText.append(and)
            
            self.messageTextView.attributedText = combinedText
            
            if  !(unwrapedItem.inviteeSelfieUrl.isEmpty) {
                self.imageView.af_setImage(
                    withURL: URL(string: (unwrapedItem.inviteeSelfieUrl))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.imageView.image = Mics.placeHolder()
            }
            
            
        }
    }
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 18)!)
        return textView
    }()
    
    let cardView: CardView = {
        let card = CardView()
        card.backgroundColor = UIColor.white
        return card
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(cardView)
        self.cardView.addSubview(imageView)
        self.cardView.addSubview(messageTextView)
        
        self.cardView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        let profileSize = CGFloat(60)
        self.imageView.widthAnchor.constraint(equalToConstant: profileSize).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: profileSize).isActive = true
        self.imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        self.imageView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 8).isActive = true
        self.imageView.layer.cornerRadius = profileSize/2
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: imageView.trailingAnchor, bottom: cardView.bottomAnchor, right: cardView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
    
    }
    
}
