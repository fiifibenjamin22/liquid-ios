//
//  InviteFriendsController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 27/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class InviteFriendsController: ControllerWithBack {
    
    let collectionId = "collectionId"
    var friends = [Friend]()
    let apiService = ApiService()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    let cardView: CardView = {
        let card = CardView()
        card.backgroundColor = UIColor.white
        return card
    }()
    
    let codePreviewTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkGray
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        textView.textAlignment = .left
        textView.text = "Friends who used your code - 0 Friends"
        return textView
    }()
    
    let inviteHintTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkGray
        textView.text = "For each person who signs up for LIQUID using your referral code, you get closer to unlocking awesome prizes."
        textView.textAlignment = .center
        return textView
    }()
    
    lazy var shareButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.setTitle("Share Code", for: .normal)
        button.setTitleColor(color, for: .normal)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.layer.borderColor = color.cgColor
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(shareCode(_:)), for: .touchUpInside)
        return button
    }()
    
    let codeBgImageView: UIImageView = {
        let imageview = ViewControllerHelper.baseImageView()
        imageview.image = UIImage(named: Images.Account.imviteBg)
        imageview.contentMode = .scaleAspectFill
        imageview.layer.cornerRadius = 10
        return imageview
    }()
    
    let codeTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        textView.textAlignment = .center
        return textView
    }()
    
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let profileCoverImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: Images.Account.profileWave)
        return imageView
    }()
    
    override func viewDidLoad() {
        setUpNavigationBar(title: "Invite Friends")
        
        self.setUpLayout()
        self.getInvitees()
    }
    
    func setUpLayout()  {
        self.view.addSubview(cardView)
        self.view.addSubview(collectionView)
        self.view.addSubview(codePreviewTextView)
        self.cardView.addSubview(inviteHintTextView)
        self.cardView.addSubview(shareButton)
        self.cardView.addSubview(codeBgImageView)
        self.cardView.addSubview(codeTextView)
        self.cardView.addSubview(profileImageView)
        self.cardView.addSubview(profileCoverImageView)
        
        self.cardView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: codePreviewTextView.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        
        self.inviteHintTextView.anchorWithConstantsToTop(top: nil, left: cardView.leadingAnchor, bottom: cardView.bottomAnchor, right: cardView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        self.inviteHintTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        self.shareButton.anchorWithConstantsToTop(top: nil, left: cardView.leadingAnchor, bottom: inviteHintTextView.topAnchor, right: cardView.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        self.shareButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //MARK:- code section
        self.codeBgImageView.anchorWithConstantsToTop(top: cardView.topAnchor, left: cardView.leadingAnchor, bottom: shareButton.topAnchor, right: cardView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.profileImageView.anchorWithConstantsToTop(top: codeBgImageView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.profileCoverImageView.anchorWithConstantsToTop(top: codeBgImageView.topAnchor, left: nil, bottom: nil, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        let height = CGFloat(100)
        self.profileImageView.centerXAnchor.constraint(equalTo: codeBgImageView.centerXAnchor, constant: 0).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.profileImageView.layer.cornerRadius = height/2
        
        self.profileCoverImageView.centerXAnchor.constraint(equalTo: codeBgImageView.centerXAnchor, constant: 0).isActive = true
        self.profileCoverImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.profileCoverImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.profileCoverImageView.layer.cornerRadius = height/2
        
        self.codeTextView.anchorWithConstantsToTop(top: nil, left: codeBgImageView.leadingAnchor, bottom: codeBgImageView.bottomAnchor, right: codeBgImageView.trailingAnchor, topConstant: 0, leftConstant: 4, bottomConstant: -4, rightConstant: -4)
        self.codeTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        //MARK:- end of section
        
        
        self.codePreviewTextView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: collectionView.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        self.codePreviewTextView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.collectionView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        self.collectionView.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -4, rightConstant: 0)
        self.collectionView.register(InviteFriendCell.self, forCellWithReuseIdentifier: collectionId)
    
        let user = User.getUser()!
        self.codeTextView.text = "Your Code:\n\(user.referralCode.uppercased())"
        if  !(user.picture.isEmpty) {
            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
    }
    
    //MARK:- fetch data from storage
    func getInvitees()  {
        self.apiService.getReferrals { (status, friends, withMessage) in
            if (status == ApiCallStatus.SUCCESS){
                self.codePreviewTextView.text = "Friends who used your code - \(friends!.count)"
                self.friends.append(contentsOf: friends!)
                self.collectionView.reloadData()
            }
        }
       
    }
    
    
    @objc func shareCode(_ sender: UIButton){
        let code = codeTextView.text!
        let inviteMessage = AppConstants.inviteMessage.replacingOccurrences(of: "-referralcode-", with: code)
        ViewControllerHelper.presentSharer(targetVC: self, message: "\(inviteMessage)")
    }
}


extension InviteFriendsController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = friends[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! InviteFriendCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = CGFloat(300)
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.clicked(item: self.friends[indexPath.row])
    }
    
    //MARK:- clicked here
    func clicked(item:Friend) {
        print("ITEM \(item)")
    }
    
}

