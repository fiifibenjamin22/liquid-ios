//
//  DestinationWalletController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class DestinationWalletController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let cellSection = "cellHeading"
    let favouriteSection = "favouriteSection"
    var favourites = [FavouriteContact]()
    
    var providers = [
        //ReceivingWalletType(name: "LIQUID Wallet", imageName: "WalletIcon",type: .LIQUID_ACCOUNT),
        ReceivingWalletType(name: "MTN Mobile Money",imageName: "MTNmomo",type: .MTN_MOMO),
        ReceivingWalletType(name: "Vodafone Cash",imageName: "VodafoneCash",type: .VODAFONE_CASH),
        ReceivingWalletType(name: "Airtel Money",imageName: "AirtelMoney",type: .AIRTEL_MONEY),
        ReceivingWalletType(name: "Tigo Cash",imageName: "TigoCash",type: .TIGO_CASH),
       // ReceivingWalletType(name: "Bank Account",imageName: "BankTransfer",type: .BANK_ACCOUNT_VIA_ACH),
       // ReceivingWalletType(name: "Direct Bank Transfer",imageName: "BankTransfer",type: .BANK_ACCOUNT_VIA_INSTAPAY)
    ]
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar(title: "Request Money")
        
        self.showFavourite()
        self.setUpLayout()
    }
    
    func showFavourite()  {
        //MARK:- parse all provides
        let favouritesLocal = FavouriteContact.filterByTypeMoneyTransfer()
        if !favouritesLocal.isEmpty {
            self.favourites.append(contentsOf: favouritesLocal)
            let favouriteSection = ReceivingWalletType(name: "My Favourites", imageName: "", type: .ALL)
            self.providers.insert(favouriteSection, at: 0)
        }
        self.collectionView.reloadData()
    }
    
    func setUpLayout()  {
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.collectionView.register(ReceivingWalletCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(ProviderCellSection.self, forCellWithReuseIdentifier: cellSection)
        self.collectionView.register(FavouriteView.self, forCellWithReuseIdentifier: favouriteSection)
    }
    
}

extension DestinationWalletController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return providers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = providers[indexPath.row]
        if item.type == .NONE {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellSection, for: indexPath) as! ProviderCellSection
            cell.item = "WHERE DO YOU WANT TO REQUEST FUNDS FROM?"
            return cell
        }
        
        if item.type == .ALL {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: favouriteSection, for: indexPath) as! FavouriteView
            cell.delegate = self
            return cell
        }
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! ReceivingWalletCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = providers[indexPath.row]
        let itemWidth = collectionView.frame.width
        if item.type == .ALL {
            let itemHeight = CGFloat(120)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(walletType: self.providers[indexPath.row])
    }
    
    //MARK:- clicked wallet type
    func transactionClicked(walletType:ReceivingWalletType) {
        let destination = RecipientController()
        destination.isReceivingMoney = true
         destination.name = walletType.name
        destination.provider = walletType.type
        self.navigationController?.pushFade(destination)
    }
}

extension DestinationWalletController: FavouriteDelegate {
    
    //did select the favourite contact
    func didSelectContact(contact: FavouriteContact) {
        let destination = TransactionAmountController()
        destination.contact = contact
        destination.name = "\(Mics.targetIssuerToName(target: contact.network))"
        destination.isReceivingMoney = true
        destination.provider = TargetIssuer(rawValue: contact.network)
        self.navigationController?.pushFade(destination)
    }
    
    func didSelectAll() {
        let destination = FavouriteListController()
        destination.delegate = self
        self.navigationController?.pushFade(destination)
    }
    
    func removeContact(contact: FavouriteContact) {
        ViewControllerHelper.showRemoveFavouriteCotact(vc: self, item: contact) { (isDone) in
            if isDone {
                self.favourites.removeAll()
                self.providers.remove(at: 0)
                self.showFavourite()
            }
        }
    }
   
}

