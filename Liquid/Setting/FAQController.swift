//
//  FAQController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 21/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class FAQController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let cellHeader = "cellHeader"
    let cellMainHeader = "cellHeaderMain"
    
    var faqs = [FAQItem]()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.sectionHeaderHeight = 70
        tableView.separatorStyle = .singleLine
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    let welcome = "You might not know it yet, but you’re home at last. Here your money will work harder for you than it ever has before, bringing a lot more of your dreams within reach. Money should move quickly and smoothly and that’s what we’re about – simplifying your financial world so that you can get on with the business of enjoying life. We want to see you setting goals, building savings, enjoying rewards,transacting seamlessly and investing wisely. We don’t want you to spend hours on things that take minutes – and we never want to see you in a queue again."
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        
        setUpNavigationBar(title: "FAQs")
        self.setUpViews()
        
        //header in
        faqs.append(FAQItem(title: "LIQUID",content: welcome,isOpened: false))
        
        //rest of section
        faqs.append(FAQItem(title: "Welcome to LIQUID",content: welcome,isOpened: true))
        faqs.append(FAQItem(title: "Signing in to LIQUID",content: welcome,isOpened: false))
        faqs.append(FAQItem(title: "Signing out of LIQUID",content: welcome,isOpened: false))
        faqs.append(FAQItem(title: "Getting Money Into Liquid",content: welcome,isOpened: false))
        faqs.append(FAQItem(title: "Goals and how to use them",content: welcome,isOpened: false))
        faqs.append(FAQItem(title: "Making goals",content: welcome,isOpened: false))
        faqs.append(FAQItem(title: "Editing your goals",content: welcome,isOpened: false))
        
        tableView.reloadData()
        
        
    }
    
    func setUpViews()  {
        
        self.view.addSubview(tableView)
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        
        self.tableView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.tableView.register(FAQCell.self, forCellReuseIdentifier: collectionId)
        self.tableView.register(FAQCollapsibleTableViewHeader.self, forHeaderFooterViewReuseIdentifier: cellHeader)
        self.tableView.register(FAQCellHeader.self, forHeaderFooterViewReuseIdentifier: cellMainHeader)
    }
}

//MARK:- swiping collection view delagates implementation
extension FAQController: UITableViewDelegate, UITableViewDataSource{
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return faqs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let faq = faqs[section]
        return !faq.isOpened ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: collectionId) as! FAQCell
        cell.item = faqs[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       let item = faqs[indexPath.section]
       let width = tableView.frame.width - 32
       let height = Mics.getHeightOfLabel(text: item.content.formatAsAttributed(fontSize: 16, color: UIColor.lightGray), fontSize: 16, width: width, numberOfLines: 0)
       return CGFloat(32 + height)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerText = faqs[section].title
        
        if headerText == "LIQUID" {
            let headerOne = tableView.dequeueReusableHeaderFooterView(withIdentifier: cellMainHeader) as! FAQCellHeader
            return headerOne
        } else {
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: cellHeader) as! FAQCollapsibleTableViewHeader
            header.titleTextView.text = headerText
            header.arrowLabel.image = UIImage(named: "ChevronDown")
            header.setCollapsed(collapsed: faqs[section].isOpened)
            
            header.section = section
            header.delegate = self
            
            return header
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerText = faqs[section].title
        if headerText == "LIQUID" {
            return 80.0
        }   else {
            return 44.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var faqItem = faqs[indexPath.row]
        faqItem.isOpened = !faqItem.isOpened
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                
            }, completion: { success in
                print("success")
        })
        
    }
}

extension FAQController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(header: FAQCollapsibleTableViewHeader, section: Int) {
        
        let isOpened = !faqs[section].isOpened
        faqs[section].isOpened = isOpened
        header.setCollapsed(collapsed: isOpened)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
