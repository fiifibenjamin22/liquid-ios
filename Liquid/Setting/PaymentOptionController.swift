//
//  PaymentOptionController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 28/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog
import RealmSwift

class PaymentOptionController: BaseScrollViewController {
    
    let liquidCollectionId = "liquidCollectionId"
    let debitCollectionId = "debitCollectionId"
    var walletes = [Wallet]()
    var currentWallet: Wallet?
    let apiservice = ApiService()
    let utilViewHolder = ViewControllerHelper()
    var contentTopConstraint: NSLayoutConstraint?
    let viewControllerUtil = ViewControllerHelper()
    var popup: PopupDialog?
    let apiService = ApiService()
    var indexes: IndexPath?
    
    //MARK:- UI elements setup
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.currentPageIndicatorTintColor = UIColor.gray
        pc.pageIndicatorTintColor = UIColor.hex(hex: Key.primaryHexCode)
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }()
    
    let headerImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: Images.barBg)
        return imageView
    }()
    
    let removeButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitle("Remove Card", for: .normal)
        button.layer.borderColor = UIColor.darkGray.cgColor
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 2
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.addTarget(self, action: #selector(removeWallet(_:)), for: .touchUpInside)
        return button
    }()
    
    let noWalletsLbl: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "NO WALLETS ADDED YET"
        lbl.font = UIFont.boldSystemFont(ofSize: 18)
        lbl.isHidden = true
        lbl.textAlignment = .center
        return lbl
    }()
    
    let upperContentTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.contentInset = UIEdgeInsets(top: 10, left: 8, bottom: 16, right: 8)
        textView.linkTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.red] as [NSAttributedString.Key: Any]
        return textView
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    lazy var addCardButton: UIButton = {
        let button = ViewControllerHelper.baseRoudImageButton()
        button.setTitle("+ Add", for: .normal)
        button.addTarget(self, action: #selector(addCard), for: .touchUpInside)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        setUpNavigationBar(title: "Payment Options")
        
        let width = self.view.frame.height - 32
        self.scrollView.addSubview(headerImageView)
        self.scrollView.addSubview(collectionView)
        self.scrollView.addSubview(pageControl)
        self.scrollView.addSubview(upperContentTextView)
        self.scrollView.addSubview(addCardButton)
        self.scrollView.addSubview(removeButton)
        self.scrollView.addSubview(noWalletsLbl)
        
        //self.view.addSubview(verifyCardButton)
        upperContentTextView.delegate = self
        
        self.headerImageView.anchorWithConstantsToTop(top: self.scrollView.safeAreaLayoutGuide.topAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.headerImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.collectionView.anchorWithConstantsToTop(top: self.scrollView.safeAreaLayoutGuide.topAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.collectionView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
        self.pageControl.anchorWithConstantsToTop(top: collectionView.bottomAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.pageControl.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        self.removeButton.anchorWithConstantsToTop(top: pageControl.bottomAnchor, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.removeButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //self.removeButton.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.noWalletsLbl.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0.0).isActive = true
        self.noWalletsLbl.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0.0).isActive = true
        self.noWalletsLbl.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.noWalletsLbl.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        //self.verifyCardButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: upperContentTextView.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        //self.verifyCardButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        self.upperContentTextView.anchorWithConstantsToTop(top: nil, left: self.scrollView.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        //self.upperContentTextView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        self.upperContentTextView.bottomAnchor.constraint(equalTo: self.scrollView.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        self.upperContentTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        contentTopConstraint = self.upperContentTextView.topAnchor.constraint(equalTo: pageControl.bottomAnchor, constant: 16)
        contentTopConstraint?.isActive = true
        
        
        self.addCardButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: self.scrollView.safeAreaLayoutGuide.bottomAnchor, right: self.scrollView.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.addCardButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.addCardButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.collectionView.register(LiquidCardCell.self, forCellWithReuseIdentifier: liquidCollectionId)
        self.collectionView.register(DebitCardCell.self, forCellWithReuseIdentifier: debitCollectionId)
        
       
        self.loadAllWallets()
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUpdateNotification(notification:)), name: NSNotification.Name("cardAdded"), object: nil)
    }
    
    @objc func receiveUpdateNotification(notification: Notification){
        self.loadAllWallets()
    }
    
    //MARK: reload if new wallet of payment option is added
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if  UserDefaults.standard.bool(forKey: Key.walletAdded) {
            UserDefaults.standard.set(false, forKey: Key.walletAdded)
            self.loadAllWallets()
        }
    }
    
    //MARK: load all walllets from DB and add the default wallet
    func loadAllWallets()  {
        self.walletes.removeAll()
        self.walletes.append(contentsOf: Wallet.all())
        self.walletes.append(Wallet.getLiquidWalet())
        self.collectionView.reloadData()
        self.pageControl.numberOfPages = self.walletes.count
        
//        if self.walletes.count <= 4 {
//            self.addCardButton.isHidden = false
//        }else{
//            self.addCardButton.isHidden = true
//        }
        
        if walletes.count > 0 {
            self.setContent(position: self.pageControl.currentPage)
            self.noWalletsLbl.isHidden = true
        }else{
            self.noWalletsLbl.isHidden = false
            self.removeButton.isHidden = true
        }
    }
    
    
    func setContent(position: Int)  {
    
        self.upperContentTextView.text = ""
        
        currentWallet = walletes[position]
        
        let fontLight = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 20)!)
        let fontLightSmall = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        let nameLabel = "Wallet Name".formatAsAttributed(font: fontLightSmall, color: UIColor.hex(hex: Key.primaryHexCode))
        let name = "\n\(currentWallet!.name)".formatAsAttributed(font: fontLight, color: UIColor.darkGray)
        
        let cardLabel = "\n\nWallet Number".formatAsAttributed(font: fontLightSmall, color: UIColor.hex(hex: Key.primaryHexCode))
        let cardName = "\n\(currentWallet!.number)".formatAsAttributed(font: fontLight, color: UIColor.darkGray)
        
        let serviceLabel = "\n\nService Provider Details".formatAsAttributed(font:fontLightSmall, color: UIColor.hex(hex: Key.primaryHexCode))
        let serviceDetail = "\n\(currentWallet!.type)".formatAsAttributed(font: fontLight, color: UIColor.darkGray)
        
        let resultText = NSMutableAttributedString()

        /*
        if !currentWallet!.payment_verified && currentWallet!.type == "card" {
            let verifyText = NSMutableAttributedString(attributedString: "Verify Card\n\n".formatAsAttributed(font: UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!), color: .red))
            verifyText.setAsLink(textToFind: "Verify Card", linkURL: "verifyCard")

            resultText.append(verifyText)
        }*/
        
        // Change remove text
        if currentWallet?.paymentType == "card" {
            self.removeButton.setTitle("Remove Card", for: .normal)
        } else if currentWallet?.paymentType == "momo" {
            self.removeButton.setTitle("Remove Mobile Wallet", for: .normal)
        }
        
        resultText.append(nameLabel)
        resultText.append(name)
        resultText.append(cardLabel)
        resultText.append(cardName)
        resultText.append(serviceLabel)
        resultText.append(serviceDetail)
        
        self.upperContentTextView.attributedText = resultText
        
        if currentWallet!.paymentType == Liquid.CODE.rawValue {
            self.removeButton.isHidden = true
            contentTopConstraint?.isActive = false
            contentTopConstraint = self.upperContentTextView.topAnchor.constraint(equalTo: pageControl.bottomAnchor, constant: 16)
            contentTopConstraint?.isActive = true
         }  else {
            self.removeButton.isHidden = false
            contentTopConstraint?.isActive = false
            contentTopConstraint = self.upperContentTextView.topAnchor.constraint(equalTo: removeButton.bottomAnchor, constant: 16)
            contentTopConstraint?.isActive = true
        }
        
        self.view.layoutIfNeeded()
    }
    
    //MARK: removing the wallet here
    @objc func removeWallet(_ sender: UIButton){
        if let wallet = self.currentWallet {
            self.utilViewHolder.showActivityIndicator()
            self.apiservice.removePaymentMethod(walletId: wallet.id,paymentType: wallet.paymentType) { (status, withMessage) in
                self.utilViewHolder.hideActivityIndicator()
                if status == ApiCallStatus.SUCCESS {
                   self.loadAllWallets()
                } else {
                   ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            }
        } else {
           ViewControllerHelper.showPrompt(vc: self, message: "Cannot remove the selected wallets")
        }
    }
    
    //MARK: adding the wallet action
    @objc func addCard(){
       let destination = NewPaymentOptionController()
        destination.index = self.indexes
        destination.isFromPaymentOption = true
        destination.wallets.append(contentsOf: self.walletes)
        let nav = UINavigationController(rootViewController: destination)
        nav.modalPresentationStyle = .fullScreen
       self.present(nav, animated: true, completion: nil)
    }
    
    func verifyCard(){
        let card = currentWallet
        
        let confirm_vc = VerifyCardPopupViewController(nibName: "VerifyCardPopupView", bundle: nil)
        confirm_vc.delegate = self
        confirm_vc.cardId = card!.id
        self.popup = PopupDialog(viewController: confirm_vc)
        self.present(self.popup!, animated: true, completion: nil)
        
        /*
        let confirm_vc = VerifyCardAmountPopupViewController(nibName: "VerifyCardAmountPopupView", bundle: nil)
        confirm_vc.delegate = self
        confirm_vc.cardId = card!.id
        self.popup = PopupDialog(viewController: confirm_vc)
        self.present(self.popup!, animated: true, completion: nil)*/
    }
}
//MARK:- swiping collection view delagates implementation
extension PaymentOptionController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.walletes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let wallet = walletes[indexPath.row]
        self.indexes = indexPath
        if wallet.paymentType == Liquid.CODE.rawValue {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: liquidCollectionId, for: indexPath) as! LiquidCardCell
            cell.item = wallet
            return cell
        }
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: debitCollectionId, for: indexPath) as! DebitCardCell
        cell.item = wallet
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let cellWidth : CGFloat = collectionView.frame.width - 50
        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        return UIEdgeInsets(top: 15, left: edgeInsets, bottom: 0, right: edgeInsets)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width - 50
        let itemHeight = collectionView.frame.height
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (_) in
            self.collectionView.invalidateIntrinsicContentSize()
            if (self.pageControl.currentPage == 0){
                self.collectionView.contentOffset = .zero
            } else{
                DispatchQueue.main.async {
                    let indexPath = IndexPath(item: self.pageControl.currentPage, section: 0)
                    self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                }
            }
        }, completion: {(bear_) in
            
        })
    }
    
    
    func snapToCenter() {
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        let pageNumber = indexPath.item
        self.pageControl.currentPage = pageNumber
        self.setContent(position: pageNumber)
        
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
    }
    
}

extension PaymentOptionController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        snapToCenter()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            snapToCenter()
        }
    }
    
}

extension PaymentOptionController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        self.verifyCard()
        return false
    }
}

extension PaymentOptionController: ConfirmCardAmountDelegate, CVVEntryDelegate, ConfirmCardDelegate {
    // Back from proceeding
    func proceed(cardId: Int){
        self.popup?.dismiss()
        let confirm_vc = CVVEntryViewController(nibName: "CVVEntryView", bundle: nil)
        confirm_vc.delegate = self
        self.popup = PopupDialog(viewController: confirm_vc)
        self.present(self.popup!, animated: true, completion: nil)
        return
    }
    
    //Back from CVV
    func proceed(cvv: String) {
        // Do bank verification
        self.popup?.dismiss()
        let cardId = currentWallet!.id
        let params = ["card_id": cardId, "currency": "GHS", "cvv": cvv] as [String : Any]
        self.viewControllerUtil.showActivityIndicator()
        self.apiService.microDebitCard(params: params, completion: {(status, refId, message) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                let confirm_vc = VerifyCardAmountPopupViewController(nibName: "VerifyCardAmountPopupView", bundle: nil)
                confirm_vc.delegate = self
                confirm_vc.cardId = cardId
                self.popup = PopupDialog(viewController: confirm_vc)
                self.present(self.popup!, animated: true, completion: nil)
            } else if status == ApiCallStatus.DETAIL {
                if refId == -3 {
                    let confirm_vc = VerifyCardAmountPopupViewController(nibName: "VerifyCardAmountPopupView", bundle: nil)
                    confirm_vc.delegate = self
                    confirm_vc.cardId = cardId
                    self.popup = PopupDialog(viewController: confirm_vc)
                    self.present(self.popup!, animated: true, completion: nil)
                    
                    UIApplication.shared.open(URL(string: message)!, options: [:], completionHandler: nil)
                } else {
                    ViewControllerHelper.showPrompt(vc: self, message: message)
                }
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        })
    }
    
    // Back from entering amount
    func proceed(cardId: Int, amount: String) {
        self.popup?.dismiss()
        
        let params = ["card_id": cardId, "currency": "GHS", "value": Double(amount)!] as [String : Any]
        self.viewControllerUtil.showActivityIndicator()
        self.apiService.verifyDebitCard(params: params, completion: {(status, cardId, message) in
            self.viewControllerUtil.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                // Show verification popup
                let wallet = self.currentWallet!
                
                let realm = try! Realm()
                try! realm.write() {
                    wallet.payment_verified = true
                    Wallet.save(data: wallet)
                }
                
                self.setContent(position: self.walletes.firstIndex(of: wallet)!)
                self.dismiss(animated: true, completion: nil)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        })
    }
    
    func dismiss() {
        self.popup?.dismiss()
    }
}
