//
//  LiquidCardCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 28/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class LiquidCardCell: BaseCell {
    
    let user = User.getUser()!
    
    var item: Wallet? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = "Available Balance\n\(user.currency) \(unwrapedItem.amount)"
            self.titleTextView.text = unwrapedItem.name
        }
    }
    
    
    let baseView: CardView = {
        let cardView = CardView()
        cardView.backgroundColor = UIColor.white
        cardView.cornerRadius = 10
        cardView.shadowOffsetHeight = 2
        cardView.translatesAutoresizingMaskIntoConstraints = false
        return cardView
    }()
    
    let titleTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 20)!)
        return textView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.clear
        
        addSubview(baseView)
        addSubview(messageTextView)
        addSubview(titleTextView)
        
        self.baseView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.titleTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.titleTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.titleTextView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: titleTextView.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 32, bottomConstant: -16, rightConstant: -32)
        
    }
}
