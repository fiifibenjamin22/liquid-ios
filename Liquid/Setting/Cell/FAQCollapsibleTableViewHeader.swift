//
//  FAQCollapsibleTableViewHeader.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/05/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: FAQCollapsibleTableViewHeader, section: Int)
}

class FAQCollapsibleTableViewHeader: BaseTableHeaderCell {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    
    var item: FAQItem? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.titleTextView.text = unwrapedItem.title
        }
    }
    
    let arrowLabel: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.image = UIImage(named: "ChevronDown")
        return imageView
    }()
    
    
    let titleTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    

    override func setUpLayout() {
        super.setUpLayout()
        backgroundColor = UIColor.white
        
        addSubview(arrowLabel)
        addSubview(titleTextView)
       
        self.arrowLabel.anchorWithConstantsToTop(top: topAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.arrowLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.arrowLabel.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.titleTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: arrowLabel.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -8, rightConstant: -8)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(FAQCollapsibleTableViewHeader.tapHeader(_:)))
        addGestureRecognizer(gesture)
    }
    
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? FAQCollapsibleTableViewHeader else {
            return
        }
        delegate?.toggleSection(header: self, section: cell.section)
    }
    
    func setCollapsed(collapsed: Bool) {
       self.arrowLabel.rotate(collapsed ? 0.0 : .pi)
    }
    
}



