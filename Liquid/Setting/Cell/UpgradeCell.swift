//
//  UpgradeCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
class UpgradeCell: BaseCell {
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 18)!)
        return textView
    }()
    
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    var item: UpgradeSteps? {
        didSet {
            guard let unwrapedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
            let title = "\(unwrapedItem.title)\n".formatAsAttributed(font: boldFont, color: UIColor.darkGray)
            let font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
            let message = "\(unwrapedItem.message) ".formatAsAttributed(font: font, color: UIColor.darkText)
            let result = NSMutableAttributedString()
            result.append(title)
            result.append(message)
            self.messageTextView.attributedText = result
            self.iconImageView.image = UIImage(named:unwrapedItem.imageName)
           
        }
    }
    
    override func setUpView() {
        super.setUpView()
        
        
        backgroundColor = UIColor.white
        
        addSubview(messageTextView)
        addSubview(iconImageView)
        addSubview(splitLine)
        
       
        let height = frame.height
        self.iconImageView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 0)
        self.iconImageView.widthAnchor.constraint(equalToConstant: height/2).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: height/2).isActive = true
        self.iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: iconImageView.trailingAnchor, bottom: splitLine.topAnchor, right: trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: -4, rightConstant: -16)
        
        
        self.splitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        self.splitLine.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
    }

}
