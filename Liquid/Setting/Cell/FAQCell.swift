//
//  FAQCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 24/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell {
    
    var item: FAQItem? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.detailLabel.text = unwrapedItem.content
        }
    }
    
    let detailLabel: UILabel = {
        let label = ViewControllerHelper.baseLabel()
        label.numberOfLines = 0
        label.textColor = UIColor.lightGray
        return label
    }()
    
    // MARK: Initalizers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.white
        contentView.addSubview(detailLabel)
      
        self.detailLabel.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
