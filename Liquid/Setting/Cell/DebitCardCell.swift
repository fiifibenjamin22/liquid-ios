//
//  DebitCardCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 28/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class DebitCardCell: BaseCell {
    
    var item: Wallet? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = "\(unwrapedItem.name)\n\(unwrapedItem.number)"
            self.iconImageView.image = Mics.walletNetworkIcon(name: unwrapedItem.icon)
            self.baseView.backgroundColor = Mics.walletNetworkColor(name: unwrapedItem.icon)
            self.backImage.image = Mics.walletBackNetworkIcon(name: unwrapedItem.icon)
        }
    }
    

    let iconImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 18)!)
        return textView
    }()
    
    let baseView: CardView = {
        let cardView = CardView()
        cardView.backgroundColor = UIColor.white
        cardView.cornerRadius = 10
        cardView.shadowOffsetHeight = 2
        cardView.translatesAutoresizingMaskIntoConstraints = false
        return cardView
    }()
    
    let backImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "mtn")
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    let backImageOverlay: UIView = {
        let img = UIView()
        img.layer.masksToBounds = true
        img.layer.cornerRadius = 8.0
        img.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    func blurImage(usingImage image: UIImage, blurAmount: CGFloat) -> UIImage? {
        guard let ciImage = CIImage(image: image) else { return nil }
        let blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        blurFilter?.setValue(blurAmount, forKey: kCIInputRadiusKey)
        
        guard let outputImage = blurFilter?.outputImage else { return nil }
        return UIImage(ciImage: outputImage)
    }
    
    override func setUpView() {
        super.setUpView()
        self.backgroundColor = UIColor.clear
        
        self.addSubview(baseView)
        self.addSubview(backImage)
        self.backImage.addSubview(backImageOverlay)
        self.backImageOverlay.addSubview(messageTextView)
        self.backImageOverlay.addSubview(iconImageView)
        
        self.baseView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.backImage.topAnchor.constraint(equalTo: baseView.topAnchor).isActive = true
        self.backImage.leftAnchor.constraint(equalTo: baseView.leftAnchor).isActive = true
        self.backImage.rightAnchor.constraint(equalTo: baseView.rightAnchor).isActive = true
        self.backImage.bottomAnchor.constraint(equalTo: baseView.bottomAnchor).isActive = true
        
        self.backImageOverlay.topAnchor.constraint(equalTo: backImage.topAnchor).isActive = true
        self.backImageOverlay.leftAnchor.constraint(equalTo: backImage.leftAnchor).isActive = true
        self.backImageOverlay.rightAnchor.constraint(equalTo: backImage.rightAnchor).isActive = true
        self.backImageOverlay.bottomAnchor.constraint(equalTo: backImage.bottomAnchor).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: baseView.topAnchor, left: baseView.leadingAnchor, bottom: iconImageView.topAnchor, right: baseView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.iconImageView.anchorWithConstantsToTop(top: nil, left: baseView.leadingAnchor, bottom: baseView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.iconImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.iconImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        self.iconImageView.layer.borderWidth = 2
        self.iconImageView.layer.borderColor = UIColor.white.cgColor
        self.iconImageView.layer.cornerRadius = 15
    }
}
