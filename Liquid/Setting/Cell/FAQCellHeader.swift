//
//  FAQCellHeader.swift
//  Liquid
//
//  Created by Benjamin Acquah on 24/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class FAQCellHeader: BaseTableHeaderCell {
    
    let headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: "EEF7FE")
        return view
    }()
    
    let iConImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "IconFaqs")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "Your guide to using Liquid."
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        textView.textContainerInset = UIEdgeInsets(top: 15, left: 0, bottom: 10, right: 0)
        return textView
    }()
    
    
    override func setUpLayout() {
        super.setUpLayout()
        
        self.addSubview(headerView)
        self.headerView.addSubview(iConImageView)
        self.headerView.addSubview(messageTextView)
        
        self.headerView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        let width = 50.0
        self.iConImageView.anchorWithConstantsToTop(top: headerView.topAnchor, left: headerView.leadingAnchor, bottom: headerView.bottomAnchor, right: nil, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: 0)
        self.iConImageView.widthAnchor.constraint(equalToConstant: CGFloat(width)).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: headerView.topAnchor, left: iConImageView.trailingAnchor, bottom: headerView.bottomAnchor, right: headerView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
    }
    
}
