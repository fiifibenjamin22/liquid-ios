//
//  AccountInfoView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 27/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import DLRadioButton

class AccountInfoView: NSObject {
    
    let cellHeight = 50
    var delegate: ShareDelegate?
    let user = User.getUser()!
    
    lazy var blackView: UIView = {
        let blackView = UIView()
        blackView.isUserInteractionEnabled = true
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return blackView
    }()
    
    lazy var contentView: UIView = {
        let blackView = UIView()
        blackView.layer.cornerRadius = 10
        blackView.dropShadow()
        blackView.backgroundColor = UIColor.white
        return blackView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
        textView.text = "Account Information"
        return textView
    }()
    
    lazy var nameTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        let messageHeader = "Account Name\n".formatAsAttributed(fontSize: 12, color: UIColor.hex(hex: Key.primaryHexCode))
        let messageBody = "\(user.firstName) \(user.lastName)".formatAsAttributed(fontSize: 16, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        textView.attributedText = result
        return textView
    }()
    
    lazy var nameCheckBox: DLRadioButton = {
        let checkBox = ViewControllerHelper.baseCheckButton()
        checkBox.isSelected = true
        checkBox.iconColor = UIColor.hex(hex: Key.backgroundColor)
        checkBox.addTarget(self, action: #selector(AccountInfoView.logSelectedButton), for: UIControl.Event.touchUpInside)
        return checkBox
    }()
    
    lazy var accountNumberTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        let messageHeader = "Account Number\n".formatAsAttributed(fontSize: 12, color: UIColor.hex(hex: Key.primaryHexCode))
        let messageBody = "\(user.accountNumber)".formatAsAttributed(fontSize: 16, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        textView.attributedText = result
        return textView
    }()
    
    lazy var accountCheckBox: DLRadioButton = {
        let checkBox = ViewControllerHelper.baseCheckButton()
        checkBox.isSelected = true
        checkBox.iconColor = UIColor.hex(hex: Key.backgroundColor)
        checkBox.isMultipleSelectionEnabled = true
        checkBox.addTarget(self, action: #selector(AccountInfoView.logSelectedButton), for: UIControl.Event.touchUpInside)
        return checkBox
    }()
    
    lazy var phoneNumberTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        let messageHeader = "Phone Number\n".formatAsAttributed(fontSize: 12, color: UIColor.hex(hex: Key.primaryHexCode))
        let messageBody = "\(user.phone)".formatAsAttributed(fontSize: 16, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        textView.attributedText = result
        return textView
    }()
    
    lazy var numberCheckBox: DLRadioButton = {
        let checkBox = ViewControllerHelper.baseCheckButton()
        checkBox.isMultipleSelectionEnabled = true
        checkBox.addTarget(self, action: #selector(AccountInfoView.logSelectedButton), for: UIControl.Event.touchUpInside)
        return checkBox
    }()
    
    lazy var emailAddressTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .left
        let messageHeader = "Email Address\n".formatAsAttributed(fontSize: 12, color: UIColor.hex(hex: Key.primaryHexCode))
        let messageBody = "\(user.email)".formatAsAttributed(fontSize: 16, color: UIColor.darkGray)
        let result = NSMutableAttributedString()
        result.append(messageHeader)
        result.append(messageBody)
        textView.attributedText = result
        return textView
    }()
    
    lazy var emailCheckBox: DLRadioButton = {
        let checkBox = ViewControllerHelper.baseCheckButton()
        checkBox.isMultipleSelectionEnabled = true
        checkBox.addTarget(self, action: #selector(AccountInfoView.logSelectedButton), for: UIControl.Event.touchUpInside)
        return checkBox
    }()
    
    
    lazy var shareButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("SHARE DETAILS", for: .normal)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        let color = UIColor.hex(hex: Key.primaryHexCode)
        let image = UIImage(named: "Export")
        button.setImage(image, for: .normal)
        button.layer.borderColor =  color.cgColor
        button.setTitleColor(color, for: .normal)
        button.semanticContentAttribute = .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0,left: 32,bottom: 0,right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        button.addTarget(self, action: #selector(shareInfo), for: .touchUpInside)
        return button
    }()
    
    lazy var closeButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "Close"), for: .normal)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(handleClose), for: .touchUpInside)
        return button
    }()
    
  
    @objc private func logSelectedButton(radioButton : DLRadioButton) {
        if radioButton == nameCheckBox || radioButton == accountCheckBox {
           radioButton.isSelected = true
        }
    }
    
    override init() {
        super.init()
        
    }
    
    func showSection()  {
        if let window = UIApplication.shared.keyWindow {
            let height = CGFloat(250)
            let y = window.frame.height - height
            window.addSubview(blackView)
            window.addSubview(contentView)
            contentView.addSubview(messageTextView)
            contentView.addSubview(nameTextView)
            contentView.addSubview(nameCheckBox)
            contentView.addSubview(accountNumberTextView)
            contentView.addSubview(accountCheckBox)
            contentView.addSubview(emailAddressTextView)
            contentView.addSubview(emailCheckBox)
            contentView.addSubview(phoneNumberTextView)
            contentView.addSubview(numberCheckBox)
            contentView.addSubview(shareButton)
            contentView.addSubview(closeButton)
            
            self.blackView.frame = window.frame
            self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height + 200)
            
            self.messageTextView.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.messageTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            self.nameTextView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: nameCheckBox.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.nameTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            self.nameCheckBox.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: nil, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
            self.nameCheckBox.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.nameCheckBox.widthAnchor.constraint(equalToConstant: 20).isActive = true
            
            
            self.accountNumberTextView.anchorWithConstantsToTop(top: nameTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: accountCheckBox.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.accountNumberTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            self.accountCheckBox.anchorWithConstantsToTop(top: nameTextView.bottomAnchor, left: nil, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
            self.accountCheckBox.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.accountCheckBox.widthAnchor.constraint(equalToConstant: 20).isActive = true
            
            
            self.phoneNumberTextView.anchorWithConstantsToTop(top: accountNumberTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: numberCheckBox.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.phoneNumberTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            self.numberCheckBox.anchorWithConstantsToTop(top: accountNumberTextView.bottomAnchor, left: nil, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
            self.numberCheckBox.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.numberCheckBox.widthAnchor.constraint(equalToConstant: 20).isActive = true
            
            self.emailAddressTextView.anchorWithConstantsToTop(top: phoneNumberTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: emailCheckBox.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.emailAddressTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            self.emailCheckBox.anchorWithConstantsToTop(top: phoneNumberTextView.bottomAnchor, left: nil, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
            self.emailCheckBox.heightAnchor.constraint(equalToConstant: 50).isActive = true
            self.emailCheckBox.widthAnchor.constraint(equalToConstant: 20).isActive = true
            
            
            self.shareButton.anchorWithConstantsToTop(top: emailAddressTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.shareButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            self.closeButton.anchorWithConstantsToTop(top: nil, left: contentView.leadingAnchor, bottom: contentView.bottomAnchor, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -44, rightConstant: -16)
            self.closeButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.contentView.frame = CGRect(x: 0, y: y - 200, width: window.frame.width, height: height + 200)
            })
        }
    }
    
    
    //MARK: the sharing section delegate
    @objc func shareInfo() {
        var message = "Name: \(user.lastName) \(user.firstName)\nAccount Number: \(user.accountNumber)"
        if numberCheckBox.isSelected {
            message = "\(message)\nNumber: \(user.phone)"
        }
        if emailCheckBox.isSelected {
            message = "\(message)\nEmail: \(user.email)"
        }
        delegate?.didSelect(message: message)
    }
    
    @objc func handleClose() {
        self.closePicker()
    }
    
    @objc func handleDismiss(sender: UITapGestureRecognizer) {
        self.closePicker()
    }
    
    func closePicker() {
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.removeFromSuperview()
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
                self.contentView.removeFromSuperview()
            }
        })
    }
}

