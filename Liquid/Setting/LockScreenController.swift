//
//  LockScreenController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

//
//  LoginController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 02/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import BiometricAuthentication

class LockScreenController: ControllerWithBack {
    
    var number = ""
    let apiService = ApiService()
    let viewControllerHelper = ViewControllerHelper()
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.clear
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    let profileImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    
    let profileNameTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        return textView
    }()
    
    lazy var passwordTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.isSecureTextEntry = true
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Enter password",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        textField.rightViewMode = UITextField.ViewMode.always
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap)))
        imageView.image = UIImage(named: "Eye")
        imageView.isUserInteractionEnabled = true
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 10)
        textField.addPadding(UITextField.PaddingSide.left(10))
        textField.rightView = imageView
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    lazy var loginButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Unlock", for: .normal)
        button.setImage(UIImage(named: "NextWhite"), for: .normal)
        button.isEnabled = false
        button.backgroundColor = UIColor.gray
        button.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        return button
    }()
    
    lazy var fingerPrintLogin: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Fingerprint", for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(fingerPrintLoginAction), for: .touchUpInside)
        button.setImage(UIImage(named: "Fingerprint"), for: .normal)
        button.imageView?.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        button.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        return button
    }()
    
    lazy var forgotPasswordButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("I forgot my password", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(forgotPassword), for: .touchUpInside)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return button
    }()
    
    lazy var signUpButton: UIButton = {
        let user = User.getUser()!
        let button = ViewControllerHelper.plainButton()
        button.setTitle("I am not \(user.firstName)", for: .normal)
        button.contentHorizontalAlignment = .left
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(signUp), for: .touchUpInside)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return button
    }()
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.white

        //MARK:- settting up the UI elements
        self.setUpUI()
    }
    
    func setUpUI()  {
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        
        self.view.addSubview(profileImageView)
        self.view.addSubview(profileNameTextView)
        self.view.addSubview(messageTextView)
        self.view.addSubview(passwordTextField)
        self.view.addSubview(forgotPasswordButton)
        self.view.addSubview(signUpButton)
        self.view.addSubview(loginButton)
        
        
        //MARK:- anchoring elements
        self.profileImageView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: nil, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.profileImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        self.profileImageView.layer.cornerRadius = 40
        
        self.profileNameTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: profileImageView.trailingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 8, bottomConstant: 0, rightConstant: -16)
        self.profileNameTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.messageTextView.anchorWithConstantsToTop(top: profileImageView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.messageTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.passwordTextField.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 32, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.passwordTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        self.loginButton.anchorToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.loginButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        if fingerPrintEnabled {
            self.view.addSubview(fingerPrintLogin)
            self.fingerPrintLogin.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.fingerPrintLogin.heightAnchor.constraint(equalToConstant: 20).isActive = true
            self.forgotPasswordButton.anchorWithConstantsToTop(top: fingerPrintLogin.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        } else {
            self.forgotPasswordButton.anchorWithConstantsToTop(top: passwordTextField.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        }
        
        self.forgotPasswordButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        

        //MARK: submit button action
        self.signUpButton.anchorWithConstantsToTop(top: forgotPasswordButton.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.signUpButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
    
        let messageBody = "Please sign in to continue".formatAsAttributed(fontSize: 16, color: UIColor.darkText)
        let result = NSMutableAttributedString()
        result.append(messageBody)
        self.messageTextView.attributedText = result
        
        let user = User.getUser()!
        self.number = user.phone
        let welcomeFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 20)!)
        let message = "Welcome Back,\n".formatAsAttributed(fontSize: 20, color: UIColor.darkGray)
        let name = "\(user.firstName)".formatAsAttributed(font: welcomeFont, color: UIColor.darkText)
        let resultAll = NSMutableAttributedString()
        resultAll.append(message)
        resultAll.append(name)
        self.profileNameTextView.attributedText = resultAll
        self.profileNameTextView.contentInset = UIEdgeInsets(top: 20, left: 5, bottom: 5, right: 0)
        
        if  !(user.picture.isEmpty) {
            self.profileImageView.af_setImage(
                withURL: URL(string: (user.picture))!,
                placeholderImage: Mics.userPlaceHolder(),
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.profileImageView.image = Mics.userPlaceHolder()
        }
    }
    
    
    //MARK - auto advance input fields
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text!
        if Validator.isValidPassword(password: text) {
            self.loginButton.isEnabled = true
            let color = UIColor.hex(hex: Key.primaryHexCode)
            self.loginButton.backgroundColor = color
        }   else {
            self.loginButton.isEnabled = false
            let color = UIColor.gray
            self.loginButton.backgroundColor = color
        }
    }
    
    //MARK:- start login section
    @objc func loginButtonPressed(){
        login(withFingerPrint: false)
    }
    
    func login(withFingerPrint: Bool){
        self.view.endEditing(true)
        var password = self.passwordTextField.text!
        if withFingerPrint {
            password = User.getPassword()
        }
        
        self.viewControllerHelper.showActivityIndicator()
        self.apiService.loginUser(number: self.number, password: password, isFingerprint: withFingerPrint) { (status, user, withMessage) in
            if status == ApiCallStatus.SUCCESS, let userIn = user {
                User.lock(locked: false)
                self.getProfile(customerId: userIn.cutomerId)
            }   else {
                self.viewControllerHelper.hideActivityIndicator()
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    @objc func fingerPrintLoginAction(){
        let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        
        if passcodeEnabled {
            let authController = AuthenticateController()
            authController.modalPresentationStyle = .fullScreen
            authController.isAuthOnly = true
            authController.pinDelegate = self
            self.present(authController, animated: true, completion: nil)
        } else if fingerPrintEnabled {
            BioMetricAuthenticator.authenticateWithPasscode(reason: "Scan fingerprint to disable touch ID", completion: { result in
                switch result {
                case .success( _):
                    // authentication successful
                    self.login(withFingerPrint: true)
                case .failure(let error):
                    // do nothing on canceled
                    if error == .canceledByUser || error == .canceledBySystem {
                        return
                    }
                        // device does not support biometric (face id or touch id) authentication
                    else if error == .biometryNotAvailable {
                        self.showErrorAlert(message: error.message())
                    }
                        // show alternatives on fallback button clicked
                    else if error == .fallback {
                        // here we're entering username and password
                    }
                        // Biometry is locked out now, because there were too many failed attempts.
                        // Need to enter device passcode to unlock.
                    else if error == .biometryLockedout {
                        self.showErrorAlert(message: error.message())
                    }
                        // show error on authentication failed
                    else {
                        self.showErrorAlert(message: error.message())
                    }
                }
                
            })
        }
    }
    
    func showAlert(title: String, message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showErrorAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    //MARK:- get user profile
    func getProfile(customerId:Int)  {
        self.apiService.getAccountOverview(customerNumber: customerId) { (status, user, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS, let userIn = user {
                self.directToController(user: userIn)
            } else {
                ViewControllerHelper.showPrompt(vc: self, message: withMessage)
            }
        }
    }
    
    func directToController(user: User)  {
        if user.firstName.isEmpty || user.lastName.isEmpty {
            let destination = SignUpStepOneController()
            destination.number = self.number
            let navVc = UINavigationController(rootViewController: destination)
            navVc.modalPresentationStyle = .fullScreen
            self.present(navVc, animated: true, completion: nil)
        }  else {
           Mics.setMainTheme()
            let vc = HomeTabController()
            vc.modalPresentationStyle = .fullScreen
           self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func signUp(){
        let destination = OnboardViewController()
        destination.modalPresentationStyle = .fullScreen
        destination.isSwitchingAccount = true
        self.present(destination, animated: true, completion: nil)
    }
    
    @objc func forgotPassword(){
        let destination = ResetPasswordController()
        destination.number = self.number
        let navVc = UINavigationController(rootViewController: destination)
        navVc.modalPresentationStyle = .fullScreen
        self.present(navVc, animated: true, completion: nil)
    }
    

    @objc func tap(sender: UITapGestureRecognizer){
        let isPassworMode = self.passwordTextField.isSecureTextEntry
        if isPassworMode {
            imageView.image = UIImage(named: "EyeClosed")
            self.passwordTextField.isSecureTextEntry = false
        }  else {
            imageView.image = UIImage(named: "Eye")
            self.passwordTextField.isSecureTextEntry = true
        }
    }
    
}

extension LockScreenController: PinDelegate {
    func didSetPin() {
        self.login(withFingerPrint: true)
    }
    
    func didFailed() {
    }
    
    func didCanceled() {
    }
}
