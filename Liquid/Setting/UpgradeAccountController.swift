//
//  AccountInfoController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 27/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit


class UpgradeAccountController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let user = User.getUser()!
    var upgradeSteps = [UpgradeSteps]()
    
    lazy var updateProfileButton: UIButton = {
        let button = ViewControllerHelper.baseButton()
        button.setTitle("Let's Do This", for: .normal)
        button.setImage(UIImage(named: "NextAction"), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(startProfileUpdate(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    let headerTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.backgroundColor = UIColor.hex(hex: "F4FBFF")
        textView.textAlignment = .left
        textView.text = "Upgrade your account for higher transaction limits, access to high yield investments and exclusive discounts"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        textView.contentInset = UIEdgeInsets(top: 5, left: 16, bottom: 0, right: 16)
        textView.textContainerInset = UIEdgeInsets(top: 5, left: 16, bottom: 0, right: 16)
        return textView
    }()
    
//    let
    
    override func viewDidLoad() {
        setUpNavigationBar(title: "Upgrade Account")
        
        self.view.addSubview(headerTextView)
        self.view.addSubview(collectionView)
        self.view.addSubview(updateProfileButton)
        
//        upgradeSteps.append(UpgradeSteps(title: "Step \(self.upgradeSteps.count + 1)",message: "Take a selfie", imageName: "SelfieProfile"))
//        upgradeSteps.append(UpgradeSteps(title: "Step \(self.upgradeSteps.count + 1)",message: "Tell us about yourself", imageName: "AvatarLady"))
//        upgradeSteps.append(UpgradeSteps(title: "Step \(self.upgradeSteps.count + 1)",message: "Provide identification documents", imageName: "Passport"))
        
        if  !self.user.hasSelfie {
            upgradeSteps.append(UpgradeSteps(title: "Step \(self.upgradeSteps.count + 1)",message: "Take a selfie", imageName: "SelfieProfile"))
        }

        if  !self.user.hasExtraKyc {
            upgradeSteps.append(UpgradeSteps(title: "Step \(self.upgradeSteps.count + 1)",message: "Tell us about yourself", imageName: "AvatarLady"))
        }

        if  !self.user.hasIdDocument {
            upgradeSteps.append(UpgradeSteps(title: "Step \(self.upgradeSteps.count + 1)",message: "Provide identification documents", imageName: "Passport"))
        }
        
        self.headerTextView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: nil, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.headerTextView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.updateProfileButton.anchorWithConstantsToTop(top: nil, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.updateProfileButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
       
        self.collectionView.anchorWithConstantsToTop(top: headerTextView.bottomAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: updateProfileButton.topAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -10, rightConstant: 0)
        self.collectionView.register(UpgradeCell.self, forCellWithReuseIdentifier: collectionId)
        
        self.collectionView.reloadData()
    }
    
    @objc func startProfileUpdate(_ sender: UIButton){
        guard let user = User.getUser() else { return }
        
        print(user.hasSelfie)
        print(user.hasExtraKyc)
        print(user.hasIdDocument)
        
        user.setSecuringAccount(state: true)
        
        if !self.user.hasSelfie {
            let destination = ProfilePhotoMesageController()
            destination.isInApp = true
            self.navigationController?.pushViewController(destination, animated: true)
        } else if !self.user.hasExtraKyc {
            let destination = AboutSelfController()
            self.navigationController?.pushViewController(destination, animated: true)
        } else if !self.user.hasIdDocument {
            let destination = DocumentController()
            self.navigationController?.pushViewController(destination, animated: true)
        }
        
//        if (self.user.hasSelfie == false && self.user.hasExtraKyc == true && self.user.hasIdDocument == true) {
//            let destination = ProfilePhotoMesageController()
//            destination.isInApp = true
//            self.navigationController?.pushViewController(destination, animated: true)
//        } else if (self.user.hasSelfie == true && self.user.hasExtraKyc == false && self.user.hasIdDocument == true) {
//            let destination = AboutSelfController()
//            self.navigationController?.pushViewController(destination, animated: true)
//        } else if (self.user.hasSelfie == true && self.user.hasExtraKyc == true && self.user.hasIdDocument == false) {
//            let destination = DocumentController()
//            self.navigationController?.pushViewController(destination, animated: true)
//        }else{
//
//            //user does not have any
//            let destination = ProfilePhotoMesageController()
//            destination.isInApp = true
//            self.navigationController?.pushViewController(destination, animated: true)
//        }
    }
}

extension UpgradeAccountController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return upgradeSteps.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = upgradeSteps[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! UpgradeCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = CGFloat(125)
        return CGSize(width: collectionView.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.clicked(item: self.upgradeSteps[indexPath.row])
    }
    
    //MARK:- clicked here
    func clicked(item:UpgradeSteps) {
        
    }
    
}

