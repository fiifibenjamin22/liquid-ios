//
//  SupportController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 27/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import MessageUI



class SupportController: BaseScrollViewController {
    
    lazy var callUsTextView: UITextView = {
        let textView = bastTextView(title: "CALL OUR TOLL-FREE LINE")
        return textView
    }()
    
    lazy var callUsUIView: SuportSectionSlimView = {
        let baseView = SuportSectionSlimView()
        baseView.supportDelegate = self
        baseView.configureView(heading: "Toll-Free", message: AppConstants.tollNumber, actionText: "Call Us")
        baseView.translatesAutoresizingMaskIntoConstraints = false
        return baseView
    }()
    
    lazy var dropNoteTextView: UITextView = {
        let textView = bastTextView(title: "DROP US A NOTE")
        return textView
    }()
    
    lazy var dropUsUIView: SuportSectionSlimView = {
        let baseView = SuportSectionSlimView()
        baseView.supportDelegate = self
        baseView.configureView(heading: "Email Address", message: AppConstants.supportEmail, actionText: "Email Us")
        baseView.translatesAutoresizingMaskIntoConstraints = false
        return baseView
    }()
    
    lazy var letChatTextView: UITextView = {
        let textView = bastTextView(title: "LET'S CHAT")
        return textView
    }()
    
    lazy var chatUsUIView: SuportSectionSlimView = {
        let baseView = SuportSectionSlimView()
        baseView.supportDelegate = self
        baseView.configureView(heading: "WhatsApp", message: AppConstants.busineesWhatsAppLine, actionText: "WhatsApp")
        baseView.translatesAutoresizingMaskIntoConstraints = false
        return baseView
    }()
    
    lazy var socialTextView: UITextView = {
        let textView = bastTextView(title: "GET SOCIAL WITH US")
        return textView
    }()
    
    lazy var followTextView: UITextView = {
        let textView = bastTextView(title: "FOLLOW US")
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
        textView.backgroundColor = UIColor.white
        textView.font = boldFont
        return textView
    }()
    
    let socialStackView: UIStackView = {
        let container = ViewControllerHelper.baseStackView()
        container.distribution = .fillProportionally
        return container
    }()
    
    lazy var googleImageView: UIImageView = {
        let imageView = self.baseImageView(imageName: Images.Account.socialLinkedin)
        return imageView
    }()
    
    lazy var facebookImageView: UIImageView = {
        let imageView = self.baseImageView(imageName: Images.Account.socialFacebook)
        return imageView
    }()
    
    lazy var twitterImageView: UIImageView = {
        let imageView = self.baseImageView(imageName: Images.Account.socialTwitter)
        return imageView
    }()
    
    lazy var instagramImageView: UIImageView = {
        let imageView = self.baseImageView(imageName: Images.Account.socialInstagram)
        return imageView
    }()
    
    lazy var warningTextView: UITextView = {
        let textView = bastTextView(title: "Warning! We will never ask you for any of the following: Your password, mobile wallet PIN, your full debit/credit card number, card CVV, card expiry date. Only give out the last four numbers of your credit/debit card.")
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.red
        textView.font = boldFont
        textView.textAlignment = .center
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    func baseImageView(imageName:String) -> UIImageView {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: imageName)
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openSocialHundle(_:))))
        return imageView
    }
    
    func bastTextView(title:String) -> UITextView {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkGray
        textView.textAlignment = .left
        textView.backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Thin, size: 14)!)
        textView.text = title
        textView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        return textView
    }
    
    override func setUpView() {
        super.setUpView()
        
        setUpNavigationBarWhite(title: "Support")
        
        self.scrollView.addSubview(callUsTextView)
        self.scrollView.addSubview(callUsUIView)
        self.scrollView.addSubview(dropNoteTextView)
        self.scrollView.addSubview(dropUsUIView)
        self.scrollView.addSubview(chatUsUIView)
        self.scrollView.addSubview(letChatTextView)
        self.scrollView.addSubview(socialTextView)
        self.scrollView.addSubview(followTextView)
        self.scrollView.addSubview(socialStackView)
        self.scrollView.addSubview(warningTextView)
        
        let width = scrollView.frame.width
        self.letChatTextView.anchorWithConstantsToTop(top: scrollView.topAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.letChatTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.chatUsUIView.anchorWithConstantsToTop(top: letChatTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.chatUsUIView.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.chatUsUIView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        self.dropNoteTextView.anchorWithConstantsToTop(top: chatUsUIView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.dropNoteTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.dropUsUIView.anchorWithConstantsToTop(top: dropNoteTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.dropUsUIView.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.dropUsUIView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        self.callUsTextView.anchorWithConstantsToTop(top: dropUsUIView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.callUsTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.callUsUIView.anchorWithConstantsToTop(top: callUsTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.callUsUIView.widthAnchor.constraint(equalToConstant: width).isActive = true
        self.callUsUIView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        self.socialTextView.anchorWithConstantsToTop(top: callUsUIView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.socialTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.followTextView.anchorWithConstantsToTop(top: socialTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.followTextView.widthAnchor.constraint(equalToConstant: width).isActive = true
        
        self.socialStackView.anchorWithConstantsToTop(top: followTextView.bottomAnchor, left: scrollView.leadingAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        self.socialStackView.widthAnchor.constraint(equalToConstant: width - 16).isActive = true
        self.socialStackView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    
        self.socialStackView.addArrangedSubview(facebookImageView)
        self.socialStackView.addArrangedSubview(twitterImageView)
        self.socialStackView.addArrangedSubview(instagramImageView)
        self.socialStackView.addArrangedSubview(googleImageView)
        
        self.warningTextView.anchorWithConstantsToTop(top: socialStackView.bottomAnchor, left: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: -16, rightConstant: 0)
        self.warningTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        self.warningTextView.widthAnchor.constraint(equalToConstant: width - 16).isActive = true
        
  
    }
    
    //MARK: open and handle all social link opens
    @objc func openSocialHundle(_ sender: UITapGestureRecognizer){
        let view = sender.view!
        var link = AppConstants.facebookAccount
        if (view == googleImageView) {
            link = AppConstants.linkedInAccount
        } else if (view == facebookImageView){
            link = AppConstants.facebookAccount
        } else if (view == twitterImageView){
            link = AppConstants.twitterAccount
        } else {
           link = AppConstants.instagramAccount
        }
        ViewControllerHelper.openLink(url: link, vc: self)
    }

}

//MARK: delegate of the view clicks
extension SupportController: SupportDelegate {
    
    func clickedCall() {
       ViewControllerHelper.placeCall(phone: AppConstants.tollNumber)
    }
    
    func clickedChat() {
        ViewControllerHelper.startWhatsapp()
    }
    
    func clickedEmail() {
        sendEmail(vc:self , email: AppConstants.supportEmail, message: "")
    }
    
    func sendEmail(vc: UIViewController,email:String,message:String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("<p>\(message)</p>", isHTML: true)
            vc.present(mail, animated: true)
        } else {
            ViewControllerHelper.showPrompt(vc: self, message: "No email host found")
        }
    }
}

extension SupportController: MFMailComposeViewControllerDelegate {
   
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

class SuportSectionView: BaseUIView {
    
    var supportDelegate: SupportDelegate?
    
    lazy var baseTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.backgroundColor = UIColor.white
        return textView
    }()
    
    let actionButton: UIButton = {
        let button = ViewControllerHelper.plainImageButton()
        button.setTitle("Call Us", for: .normal)
        button.layer.cornerRadius = 10
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.layer.borderColor =  color.cgColor
        button.setTitleColor(color, for: .normal)
        button.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
        return button
    }()
    
    func configureView(heading:String,message:String,actionText:String) {
        let font = UIFont(name: Font.Roboto.Bold, size: 16)
        let boldFont = UIFontMetrics.default.scaledFont(for: font!)
        
        let fontNormal = UIFont(name: Font.Roboto.Bold, size: 20)
        let normalFont = UIFontMetrics.default.scaledFont(for: fontNormal!)
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.darkText, NSAttributedString.Key.font: boldFont]
        let andAttributes = [NSAttributedString.Key.foregroundColor: UIColor.hex(hex: Key.primaryHexCode), NSAttributedString.Key.font: normalFont]
        
        let header = NSMutableAttributedString(string: "\(heading)", attributes: attributes)
        let and = NSMutableAttributedString(string: "\n\(message)", attributes: andAttributes)
        
        let combinedText = NSMutableAttributedString()
        combinedText.append(header)
        combinedText.append(and)
        
        self.baseTextView.attributedText = combinedText
        self.baseTextView.textAlignment = .left
        self.actionButton.setTitle(actionText, for: .normal)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        addSubview(baseTextView)
        addSubview(actionButton)
        
        self.baseTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: actionButton.leadingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        
        self.actionButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -8)
        self.actionButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.actionButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.actionButton.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
    }
    
    @objc func handleAction(_ sender: UIButton)  {
       let actionText = sender.currentTitle!
        if actionText == "Email Us" {
           self.supportDelegate?.clickedEmail()
           return
        }
        
        if actionText == "Call Us" {
           self.supportDelegate?.clickedCall()
           return
        }
        
        if actionText == "Chat" {
           self.supportDelegate?.clickedChat()
           return
        }
    }
}

class SuportSectionSlimView: BaseUIView {
    
    var supportDelegate: SupportDelegate?
    
    lazy var baseTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.backgroundColor = UIColor.white
        return textView
    }()
    
    let actionButton: UIButton = {
        let button = ViewControllerHelper.plainImageButton()
        button.setTitle("Call Us", for: .normal)
        button.layer.cornerRadius = 10
        let color = UIColor.hex(hex: Key.primaryHexCode)
        button.layer.borderColor =  color.cgColor
        button.setTitleColor(color, for: .normal)
        button.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
        return button
    }()
    
    func configureView(heading:String,message:String,actionText:String) {
        let font = UIFont(name: Font.Roboto.Bold, size: 16)
        let boldFont = UIFontMetrics.default.scaledFont(for: font!)
        
        let fontNormal = UIFont(name: Font.Roboto.Bold, size: 20)
        let normalFont = UIFontMetrics.default.scaledFont(for: fontNormal!)
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.darkText, NSAttributedString.Key.font: boldFont]
        let andAttributes = [NSAttributedString.Key.foregroundColor: UIColor.hex(hex: Key.primaryHexCode), NSAttributedString.Key.font: normalFont]
        
        let text = NSMutableAttributedString(string: "\(message)", attributes: andAttributes)
        
        let combinedText = NSMutableAttributedString()
        combinedText.append(text)
        
        self.baseTextView.attributedText = combinedText
        self.baseTextView.textAlignment = .left
        self.actionButton.setTitle(actionText, for: .normal)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        addSubview(baseTextView)
        addSubview(actionButton)
        
        self.baseTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: nil, right: actionButton.leadingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0)
        self.baseTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.baseTextView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        self.actionButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -8)
        self.actionButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.actionButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.actionButton.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
    }
    
    @objc func handleAction(_ sender: UIButton)  {
        let actionText = sender.currentTitle!
        if actionText == "Email Us" {
            self.supportDelegate?.clickedEmail()
            return
        }
        
        if actionText == "Call Us" {
            self.supportDelegate?.clickedCall()
            return
        }
        
        if actionText == "WhatsApp" {
            self.supportDelegate?.clickedChat()
            return
        }
    }
}
