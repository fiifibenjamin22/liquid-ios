//
//  CalculatorController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 13/11/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class CalculatorController: BaseScrollViewController {
    
    let startToday: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.isHidden = false
        imageView.image = UIImage(named: "start_today")
        return imageView
    }()
    
    let card: CardView = {
        let card = CardView()
        card.backgroundColor = UIColor.white
        card.cornerRadius = 5
        card.shadowOffsetHeight = 2
        card.layer.borderWidth = 1
        card.layer.borderColor = UIColor.gray.cgColor
        card.translatesAutoresizingMaskIntoConstraints = false
        return card
    }()
    
    let howMuch: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.gray
        textView.text = "See how much you could’ve made"
        textView.textAlignment = .center
        textView.backgroundColor = .white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let accBalance: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.text = "ACCOUNT BALANCE"
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        return textView
    }()
    
    let amountLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "If you save \(Mics.convertDoubleToCurrency(amount: 1000.0)) a week"
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        return textView
    }()
    
    lazy var amountSlider: UISlider = {
        let amtSlider = UISlider()
        amtSlider.minimumTrackTintColor = UIColor.hex(hex: Key.primaryHexCode)
        amtSlider.maximumTrackTintColor = UIColor.hex(hex: Key.primaryHexCode).withAlphaComponent(0.5)
        amtSlider.thumbTintColor = UIColor.hex(hex: Key.primaryHexCode)
        amtSlider.maximumValue = 1000
        amtSlider.minimumValue = 50
        amtSlider.setValue(500, animated: true)
        amtSlider.addTarget(self, action: #selector(changeValue(_:)), for: .valueChanged)
        return amtSlider
    }()
    
    let LiquidAvgTextView: UIView = {
        let textView = UIView()
        textView.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let liquidAvgLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.text = "LIQUID AVG(16%)"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let liquidAmountLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.text = "GHS 1,600"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        return textView
    }()
    
    let liquidPerYearLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.text = "Interest per year"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return textView
    }()
    
    let TbilTextView: UIView = {
        let textView = UIView()
        textView.backgroundColor = UIColor.hex(hex: "#D8D8D8")
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let TbilAvgLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "Industrial AVG."
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let TbilAmountLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "GHS 1,200"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        return textView
    }()
    
    let TbilPerYearLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "Interest per year"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return textView
    }()
    
    let partnershipImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "partnership")
        return imageView
    }()
    
    lazy var icImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "ic")
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoIC)))
        return imageView
    }()
    
    let partnershiplbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.text = "ACCOUNT BALANCE"
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        return textView
    }()
    
    lazy var startImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "start")
        imageView.isUserInteractionEnabled = true
        imageView.isHidden = false
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createNewGoal)))
        return imageView
    }()
    
    override func setUpView() {
        super.setUpView()
        self.view.backgroundColor = .white
        self.navigationItem.title = "Calculator"
        
        let mainAmount: Double = Double(amountSlider.value)
        let PMT: Double = Double(mainAmount).rounded(toPlaces: 2)
        let rate: Double = 0.16
        let n: Double = 52.0
        let time: Double = 1.0
        
        let get_the_power = pow((1 + rate/n), n*time)
        let power_less_one = get_the_power - 1
        let left_hand_side_numberator = PMT * power_less_one
        let left_hand_side_denominator = rate / n
        let left_hand_side = left_hand_side_numberator / left_hand_side_denominator
        let right_hand_side = 1 + (rate / n)
        let Annual_Amount = left_hand_side * right_hand_side
                        
        let am = Mics.convertDoubleToCurrency(amount: Annual_Amount.rounded(toPlaces: 2))
        let ma = Mics.convertDoubleToCurrency(amount: mainAmount)
        
        self.amountLbl.text = "If you save \(ma) a week"
        self.liquidAmountLbl.text = "\(am)"
        
        //Tbil interest
        let tbilInterestRate: CGFloat = 0.12
        let tPMT: Double = Double(mainAmount)
        let trate: Double = Double(tbilInterestRate)
        let tn: Double = 52.0
        let ttime: Double = 1.0
        
        let tget_the_power = pow((1 + trate/n), tn*ttime)
        let tpower_less_one = tget_the_power - 1
        let tleft_hand_side_numberator = tPMT * tpower_less_one
        let tleft_hand_side_denominator = trate / tn
        let tleft_hand_side = tleft_hand_side_numberator / tleft_hand_side_denominator
        let tright_hand_side = 1 + (trate / tn)
        let tAnnual_Amount = tleft_hand_side * tright_hand_side
        
        let tam = Mics.convertDoubleToCurrency(amount: tAnnual_Amount.rounded(toPlaces: 2))
        self.TbilAmountLbl.text = "\(tam)"
        
        self.scrollView.addSubview(startToday)
        self.scrollView.addSubview(card)
        self.scrollView.addSubview(howMuch)
        card.addSubview(accBalance)
        card.addSubview(amountLbl)
        card.addSubview(amountSlider)
        card.addSubview(LiquidAvgTextView)
        
        LiquidAvgTextView.addSubview(liquidAvgLbl)
        LiquidAvgTextView.addSubview(liquidAmountLbl)
        LiquidAvgTextView.addSubview(liquidPerYearLbl)
        
        card.addSubview(TbilTextView)
        TbilTextView.addSubview(TbilAvgLbl)
        TbilTextView.addSubview(TbilAmountLbl)
        TbilTextView.addSubview(TbilPerYearLbl)
        
        self.scrollView.addSubview(partnershipImageView)
        self.scrollView.addSubview(icImageView)
        self.scrollView.addSubview(startImageView)
        
        startToday.anchor(self.scrollView.topAnchor, left: self.scrollView.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: self.view.frame.size.width / 2, heightConstant: 120)
        
        card.anchor(startToday.bottomAnchor, left: startToday.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 250)
        
        howMuch.topAnchor.constraint(equalTo: card.topAnchor, constant: -10).isActive = true
        howMuch.leftAnchor.constraint(equalTo: card.leftAnchor, constant: 16).isActive = true
        howMuch.rightAnchor.constraint(equalTo: card.rightAnchor, constant: -100).isActive = true
        howMuch.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        accBalance.anchor(howMuch.bottomAnchor, left: howMuch.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 60, leftConstant: 0, bottomConstant: 0, rightConstant: -16, widthConstant: 0, heightConstant: 20)
        
        amountLbl.anchor(accBalance.bottomAnchor, left: accBalance.leftAnchor, bottom: nil, right: accBalance.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 25)
        
        amountSlider.anchor(amountLbl.bottomAnchor, left: amountLbl.leftAnchor, bottom: nil, right: accBalance.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: 35)
        
        LiquidAvgTextView.anchor(amountSlider.bottomAnchor, left: amountSlider.leftAnchor, bottom: card.bottomAnchor, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 12, rightConstant: 0, widthConstant: 140, heightConstant: 0)
        LiquidAvgTextView.layer.cornerRadius = 10
        LiquidAvgTextView.layer.masksToBounds = true
        
        liquidAvgLbl.anchor(LiquidAvgTextView.topAnchor, left: LiquidAvgTextView.leftAnchor, bottom: nil, right: LiquidAvgTextView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        liquidAmountLbl.anchor(liquidAvgLbl.bottomAnchor, left: liquidPerYearLbl.leftAnchor, bottom: liquidPerYearLbl.topAnchor, right: liquidPerYearLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        liquidPerYearLbl.anchor(nil, left: LiquidAvgTextView.leftAnchor, bottom: LiquidAvgTextView.bottomAnchor, right: LiquidAvgTextView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        TbilTextView.anchor(LiquidAvgTextView.topAnchor, left: nil, bottom: LiquidAvgTextView.bottomAnchor, right: amountSlider.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 140, heightConstant: 0)
        TbilTextView.layer.cornerRadius = 10
        TbilTextView.layer.masksToBounds = true
        
        TbilAvgLbl.anchor(TbilTextView.topAnchor, left: TbilTextView.leftAnchor, bottom: nil, right: TbilTextView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        TbilAmountLbl.anchor(TbilAvgLbl.bottomAnchor, left: TbilPerYearLbl.leftAnchor, bottom: TbilPerYearLbl.topAnchor, right: TbilPerYearLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        TbilPerYearLbl.anchor(nil, left: TbilTextView.leftAnchor, bottom: TbilTextView.bottomAnchor, right: TbilTextView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        partnershipImageView.topAnchor.constraint(equalTo: card.bottomAnchor, constant: 8).isActive = true
        partnershipImageView.centerXAnchor.constraint(equalTo: self.scrollView.centerXAnchor).isActive = true
        partnershipImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        partnershipImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        icImageView.anchor(partnershipImageView.bottomAnchor, left: card.leftAnchor, bottom: nil, right: card.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        startImageView.anchor(icImageView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 24, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 60)
    }
    
    @objc func createNewGoal(){
        let destination = NewGoalController()
        destination.navigationItem.title = ""
        self.navigationController?.pushFade(destination)
    }
    
    @objc func changeValue(_ sender: UISlider) {
        print("value is" , Int(sender.value));
        
        let dynamicValue: CGFloat = CGFloat(sender.value)
        
        //liquid interest
        let LiquidInterestRate: CGFloat = 0.16
        let mainAmount = Double(sender.value)
        
        let PMT: Double = Double(dynamicValue).rounded(toPlaces: 2)
        let rate: Double = Double(LiquidInterestRate)
        let n: Double = 52.0
        let time: Double = 1.0
        
        let get_the_power = pow((1 + rate/n), n*time)
        let power_less_one = get_the_power - 1
        let left_hand_side_numberator = PMT * power_less_one
        let left_hand_side_denominator = rate / n
        let left_hand_side = left_hand_side_numberator / left_hand_side_denominator
        let right_hand_side = 1 + (rate / n)
        let Annual_Amount = left_hand_side * right_hand_side
        
        let am = Mics.convertDoubleToCurrency(amount: Annual_Amount.rounded(toPlaces: 2))
        let ma = Mics.convertDoubleToCurrency(amount: mainAmount)

        self.liquidAmountLbl.text = "\(am)"
        self.amountLbl.text = "If you save \(ma) a week"
        
        //Tbil interest
        let tbilInterestRate: CGFloat = 0.12
        let tPMT: Double = Double(dynamicValue).rounded(toPlaces: 2)
        let trate: Double = Double(tbilInterestRate)
        let tn: Double = 52.0
        let ttime: Double = 1.0
        
        let tget_the_power = pow((1 + trate/n), tn*ttime)
        let tpower_less_one = tget_the_power - 1
        let tleft_hand_side_numberator = tPMT * tpower_less_one
        let tleft_hand_side_denominator = trate / tn
        let tleft_hand_side = tleft_hand_side_numberator / tleft_hand_side_denominator
        let tright_hand_side = 1 + (trate / tn)
        let tAnnual_Amount = tleft_hand_side * tright_hand_side
        
        let tba = Mics.convertDoubleToCurrency(amount: tAnnual_Amount.rounded(toPlaces: 2))
        self.TbilAmountLbl.text = "\(tba)"
    }
    
    @objc func gotoIC(){
        let destination = LQWebViewController()
        self.navigationItem.title = ""
        destination.url = "https://www.icassetmanagers.com/"
        destination.title = "IC Asset Managers"
        self.navigationController?.setViewControllers([(self.navigationController?.viewControllers.first)!, destination], animated: true)
    }
}
