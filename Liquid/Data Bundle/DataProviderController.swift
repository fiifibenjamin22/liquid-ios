//
//  DataProviderController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 06/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class DataProviderController: ControllerWithBack {
    
    let collectionId = "collectionId"
    let cellSection = "cellHeading"
    let favouriteSection = "favouriteSection"
    var isNavigation = false
    var favourites = [FavouriteContact]()
    var favouriteCellSection: FavouriteView?
    
    var providers = [
        AirtimeProvider(name: "BUY", imageName: "SelfieProfile",type: .NONE),
        AirtimeProvider(name: "Busy 4G",imageName: "Busy",type: .BUSY4G_DATA),
        //AirtimeProvider(name: "MTN Post Paid",imageName: "NetworkVodafone",type: .MTN_POSTPAID_DATA),
        AirtimeProvider(name: "Surfline",imageName: "Surfline",type: .SURFLINE_DATA),
        //AirtimeProvider(name: "Surfline Plus",imageName: "Surfline",type: .SURFLINE_PLUS_DATA),
        AirtimeProvider(name: "Vodafone Broadband",imageName: "NetworkVodafone",type: .VODAFONE_BROADBAND_DATA)
    ]
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setUpNavigationBar(title: "Data Provider")
       
        self.showFavourite()
        self.setUpLayout()
    }
    
    func showFavourite()  {
        //MARK:- parse all provides
        self.favourites.removeAll()
        self.favourites.append(contentsOf: FavouriteContact.filterByTypeData())
        if !favourites.isEmpty {
            self.providers.insert(AirtimeProvider(name: "My Favourites", imageName: "", type: .ALL), at: 0)
        }
        self.collectionView.reloadData()
    }
    
    func setUpLayout()  {
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
        self.collectionView.register(ProviderCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(ProviderCellSection.self, forCellWithReuseIdentifier: cellSection)
        self.collectionView.register(FavouriteView.self, forCellWithReuseIdentifier: favouriteSection)
        
    }
    
}

extension DataProviderController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return providers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = providers[indexPath.row]
        if item.type == .NONE {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellSection, for: indexPath) as! ProviderCellSection
            cell.item = self.providers[indexPath.row].name
            return cell
        }
        
        if item.type == .ALL {
            favouriteCellSection =  collectionView.dequeueReusableCell(withReuseIdentifier: favouriteSection, for: indexPath) as? FavouriteView
            favouriteCellSection?.favourites = self.favourites
            favouriteCellSection?.delegate = self
            return favouriteCellSection!
        }
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! ProviderCell
        cell.item = self.providers[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = providers[indexPath.row]
        let itemWidth = collectionView.frame.width
        if item.type == .ALL {
            let itemHeight = CGFloat(120)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(provider: self.providers[indexPath.row])
    }
    
    //MARK:- clicked network provider
    func transactionClicked(provider:AirtimeProvider) {
        let destination = RecipientController()
        destination.isData = true
        destination.provider = provider.type
        destination.name = provider.name
        self.navigationController?.pushFade(destination)
    }
}

extension DataProviderController: FavouriteDelegate {
    
    //selected the favourite contact
    func didSelectContact(contact: FavouriteContact) {
        let destination = TransactionAmountController()
        destination.contact = contact
        
        destination.name = "\(Mics.targetIssuerToName(target: contact.network))"
        destination.isData = true
        self.navigationController?.pushFade(destination)
    }
    
    func didSelectAll() {
        let destination = FavouriteListController()
        destination.delegate = self
        self.navigationController?.pushFade(destination)
    }
    
    func removeContact(contact: FavouriteContact) {
        ViewControllerHelper.showRemoveFavouriteCotact(vc: self, item: contact) { (isDone) in
            if isDone {
                self.providers.remove(at: 0)
                self.showFavourite()
                self.favouriteCellSection?.favs = self.favourites
                if self.favourites.isEmpty {
                  self.providers.remove(at: 0)
                  self.collectionView.reloadData()
                }
            }
        }
    }
    
}
