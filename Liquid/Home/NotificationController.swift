//
//  NotificationController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import PopupDialog

class NotificationController: UIViewController {
    
    let collectionId = "collectionId"
    let sectionId = "sectionId"
    var notifications = [Any]()
    var loadedPages = [String]()
    let viewControllerHelper = ViewControllerHelper()
    var page = 1
    var nextUrl = ApiUrl().notifications()
    var popup: PopupDialog?
    
    lazy var collectionView: UITableView = {
        let collectionIn = UITableView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Notifications"
        
        self.view.addSubview(collectionView)
        self.collectionView.separatorStyle = .none
        
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.collectionView.register(NotificationSectionCell.self, forCellReuseIdentifier: sectionId)
        self.collectionView.register(NotificationCell.self, forCellReuseIdentifier: collectionId)
        
        self.setUpNotifcations()
        
    }
    
    func setUpNotifcations()  {
        self.loadLocalData()
        if self.notifications.isEmpty {
            self.viewControllerHelper.showActivityIndicator()
        }
        self.loadedPages.append(nextUrl)
        ApiService().getNotifications(url: nextUrl, pageNumber: page, isMore: false) { (status, notifications,currentPage, withMessage) in
            self.page = currentPage
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
    }
    
    func fetchMoreNotifcations()  {
        self.loadedPages.append(nextUrl)
        ApiService().getNotifications(url: nextUrl, pageNumber: self.page, isMore: true) { (status, notifications,currentPage, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                for item in notifications! {
                    self.notifications.append(item)
                }
                self.collectionView.reloadData()
            }
           
        }
    }
    
    func loadLocalData() {
        
        self.notifications.removeAll()
        self.notifications.append("TODAY")
        let all = Notification.all()
        all.forEach { (item) in
            let todayOnlysecond = item.dateTime.asDate.timeAgoDisplay().range(of: "second")
            let todayOnlymin = item.dateTime.asDate.timeAgoDisplay().range(of: "min")
            let todayOnlyhour = item.dateTime.asDate.timeAgoDisplay().range(of: "hour")
            if todayOnlysecond != nil || todayOnlymin != nil || todayOnlyhour != nil {
                self.notifications.append(item)
                print(":::: all notes ::::",item.dateTime.asDate.timeAgoDisplay())
            }else{
                self.notifications.removeAll()
            }
        }
        self.notifications.append("THIS WEEK")
        all.forEach { (item) in
            let todayOnly = item.dateTime.asDate.timeAgoDisplay().range(of: "day")
            if todayOnly != nil {
                self.notifications.append(item)
                print(":::: all notes ::::",item.dateTime.asDate.timeAgoDisplay())
            }else{
                self.notifications.removeAll()
            }
        }
        self.notifications.append("EARLIER")
        all.forEach { (item) in
            let todayOnlyweek = item.dateTime.asDate.timeAgoDisplay().range(of: "week")
            let todayOnlymonth = item.dateTime.asDate.timeAgoDisplay().range(of: "month")
            if todayOnlyweek != nil || todayOnlymonth != nil {
                self.notifications.append(item)
                print(":::: all notes ::::",item.dateTime.asDate.timeAgoDisplay())
            }else{
                self.notifications.removeAll()
            }
        }
        self.collectionView.reloadData()
    }
}

extension NotificationController: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.notifications[indexPath.row]
        if item is String {
            let cell = collectionView.dequeueReusableCell(withIdentifier: sectionId, for: indexPath) as! NotificationSectionCell
            cell.item = item as? String
            cell.hideSeparator = true
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withIdentifier: collectionId, for: indexPath) as! NotificationCell
        cell.item = self.notifications[indexPath.row] as? Notification
        cell.accessoryType = .disclosureIndicator
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.previewDetail(_:)))
        cell.messageTextView.isUserInteractionEnabled = true
        cell.messageTextView.tag = indexPath.row
        cell.messageTextView.addGestureRecognizer(tapped)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func deleteAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
            
            let confirm_vc = DeleteAlertController(nibName: "DeleteAlert", bundle: nil)
            confirm_vc.delegate = self
            confirm_vc.index = indexPath
            self.popup = PopupDialog(viewController: confirm_vc)
            self.present(self.popup!, animated: true, completion: nil)
        }
        action.image = UIImage(named: "delete-bin")
        action.backgroundColor = .red
        return action
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.notifications[indexPath.row]
        if item is String {
            let itemHeight = CGFloat(50)
            return itemHeight
        }
        
        let itemHeight = CGFloat(110)
        return itemHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.notifications[indexPath.row]
        if item is Notification {
           self.showNotificationDetail(notification: item as! Notification)
        }
    }
    
    //MARK: respond to the notification touch
    @objc func previewDetail(_ sender: UITapGestureRecognizer) {
        let item = self.notifications[sender.view!.tag] as! Notification
        self.showNotificationDetail(notification: item)
    }
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if !(self.nextUrl.isEmpty) && !self.loadedPages.contains(self.nextUrl) {
                self.fetchMoreNotifcations()
            }
        }
    }
}



extension NotificationController: deleteAlertDelegate {
    
    func deleteAction(i: IndexPath) {
        self.viewControllerHelper.showActivityIndicator()
        let noteId = self.notifications[i.row] as! Notification
        ApiService().notificationsDelete(id: noteId.id) { (status, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.notifications.remove(at: i.row)
                self.collectionView.deleteRows(at: [i], with: .fade)
                self.popup?.dismiss()
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
                self.popup?.dismiss()
            }
        }
    }
    
    func cancelAction() {
        self.popup?.dismiss()
    }
    
    func showNotificationDetail(notification:Notification)  {
        ViewControllerHelper.showPrompt(vc: self, message: "\(notification.type.uppercased())\n\n\(notification.message)") { (isDone) in
            if isDone {
            }
        }
        ApiService().markNotificationAsRead(id: notification.id) { (status, withMessage) in
            
        }
    }
}


extension String {
    var asDate: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: self) ?? Date()
    }
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
