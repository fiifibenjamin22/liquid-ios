//
//  AccountController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 05/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import RealmSwift
import BiometricAuthentication

class AccountController: NavConfigController {

    let collectionId = "collectionId"
    let cellSection = "cellHeading"
    let cellHeader = "cellHeader"
    
    let accountInfoView = AccountInfoView()
    let viewControllerHelper = ViewControllerHelper()
    let user = User.getUser()!
    var nextUrl = ApiUrl().notifications()
    var page = 1
  
    var settings = [
        SettingItem(title: "ACCOUNT",type: .NONE),
        //SettingItem(title: "Upgrade To A Full Account Today",type: .NAME),
        SettingItem(title: "Share Account Information",type: .SHARE),
        SettingItem(title: "Statement",type: .STATEMENT),
        SettingItem(title: "Transaction History",type: .HISTORY),
        //SettingItem(title: "Withdraw and Deposit Cash",type: .NAME),
        SettingItem(title: "REFERRAL",type: .NONE),
        SettingItem(title: "Invite Friends",type: .INVITE),
        //SettingItem(title: "Add Referral Code",type: .ADD_REFERRAL),
        SettingItem(title: "SETTINGS",type: .NONE),
        SettingItem(title: "Security",type: .SECURITY),
        SettingItem(title: "Payment Options",type: .PAYMENT),
        SettingItem(title: "Log Out",type: .LOGOUT),
        SettingItem(title: "LEGAL AND SUPPORT",type: .NONE),
        SettingItem(title: "Terms and Conditions",type: .TERMS),
        SettingItem(title: "Support",type: .SUPPORT),
        SettingItem(title: "FAQ",type: .FAQ)
    ]
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadMenuItems()
        self.setNavItem()
        self.checkAccountType()
    }
    
    func loadMenuItems(){
        self.settings = [
            SettingItem(title: "ACCOUNT",type: .NONE),
            //SettingItem(title: "Upgrade To A Full Account Today",type: .NAME),
            SettingItem(title: "Share Account Information",type: .SHARE),
            SettingItem(title: "Statement",type: .STATEMENT),
            SettingItem(title: "Transaction History",type: .HISTORY),
            //SettingItem(title: "Withdraw and Deposit Cash",type: .NAME),
            SettingItem(title: "REFERRAL",type: .NONE),
            SettingItem(title: "Invite Friends",type: .INVITE),
            SettingItem(title: "SETTINGS",type: .NONE),
            SettingItem(title: "Security",type: .SECURITY),
            SettingItem(title: "Payment Options",type: .PAYMENT),
            SettingItem(title: "Log Out",type: .LOGOUT),
            SettingItem(title: "LEGAL AND SUPPORT",type: .NONE),
            SettingItem(title: "Terms and Conditions",type: .TERMS),
            SettingItem(title: "Support",type: .SUPPORT),
            SettingItem(title: "FAQ",type: .FAQ)
        ]
        
        if !User.getUser()!.has_full_account {
            self.settings.insert(SettingItem(title: "Upgrade To A Full Account Today",type: .UPGRADE), at: 1)
            
//            if !User.getUser()!.has_used_invite_code {
//                self.settings.insert(SettingItem(title: "Add Referral Code",type: .ADD_REFERRAL), at: 7)
//            }
        } else {
//            if !User.getUser()!.has_used_invite_code {
//                self.settings.insert(SettingItem(title: "Add Referral Code",type: .ADD_REFERRAL), at: 6)
//            }
        }
        
        self.collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        setUpNavigationBar(title: "My Account")
        
        self.setUpViews()
        self.getallNotifications()
        //self.checkAccountType()
        
        let user = User.getUser()
        print("User ID: \(user!.id)")
    }
    
    func checkAccountType(){
        let user = User.getUser()!
        ApiService().checkAccountType(user_id: "\(user.cutomerId)") { (status, withMessage, item) in
            if status == ApiCallStatus.SUCCESS {
                
                print(":::::: user_account from server :::: ",item["has_full_account"])
                user.setHasFullAccount(state: item["has_full_account"].boolValue)
                self.loadMenuItems()
            }
        }
    }
    
    func setNavItem()  {

    }
    
    func setNavItem(badgeNumber: Int)  {
        
        let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        let image = UIImage(named: "NotificationBell")?.withRenderingMode(.alwaysOriginal)
        let menuNotification: UIBarButtonItem?
        if badgeNumber == 0 {
            menuNotification = UIBarButtonItem(badge: "", title: image!, target: self, action: #selector(handleNotification))
        }else{
            menuNotification = UIBarButtonItem(badge: "\(badgeNumber)", title: image!, target: self, action: #selector(handleNotification))
        }
        
        //let calculatorIcon = UIImage(named: "calculator")?.withRenderingMode(.alwaysOriginal)
        //let menuCalc = UIBarButtonItem(badge: "", title: calculatorIcon!, target: self, action: #selector(handleCalc))
        
        let faqIcon = UIImage(named: "NotificationFAQ")?.withRenderingMode(.alwaysTemplate)
        let menuFaq = UIBarButtonItem(image: faqIcon, style: .plain, target: self, action: #selector(gotoFAQs))
        
        if passcodeEnabled {
            let image = UIImage(named: "Lock")?.withRenderingMode(.alwaysOriginal)
            let menuNotificationLock = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleLockIcon))
            
            navigationItem.rightBarButtonItems = [menuNotification!]
            navigationItem.rightBarButtonItems = [menuNotification!,menuFaq,menuNotificationLock]
            
            let barImage = UIImage(named: Images.barBg)
            self.navigationController?.setBackgroundImage(barImage!)
        } else {
            navigationItem.rightBarButtonItems = [menuNotification!,menuFaq]
        }
    }
    
    @objc func handleNotification()  {
        let destination = NotificationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getallNotifications(){
        ApiService().getNotifications(url: nextUrl, pageNumber: self.page, isMore: true) { (status, notifications,currentPage, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                var allNotes = [Notification]()
                allNotes.removeAll()
                for i in notifications! {
                    if i.isRead == false {
                        allNotes.append(i)
                    }
                }
                self.setNavItem(badgeNumber: allNotes.count)
            }else{
                print("FAILED")
            }
        }
    }

    @objc func gotoFAQs()  {
        let destination = LQWebViewController()
        destination.url = "\(AppConstants.faqUrl)#security"
        destination.title = "FAQs"
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func handleCalc(){
        let destination = CalculatorController()
        destination.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    
    @objc func handleLockIcon()  {
        User.lock(locked: true)
        let destination = LockScreenController()
        destination.modalPresentationStyle = .fullScreen
        self.present(destination, animated: true, completion: nil)
    }

    
    @objc func addFund(){
        let destination = FundAccountController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func setUpViews()  {
      
        self.view.addSubview(collectionView)
        
        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -50, rightConstant: 0)
        self.collectionView.register(SettingCell.self, forCellWithReuseIdentifier: collectionId)
        self.collectionView.register(SettingSectionCell.self, forCellWithReuseIdentifier: cellSection)
        self.collectionView.register(AccountCellHeder.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellHeader)
    }
}

extension AccountController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = settings[indexPath.row]
        if item.type == .NONE {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellSection, for: indexPath) as! SettingSectionCell
            cell.item = item
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! SettingCell
        cell.item = item
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(180 + 90)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView  = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: cellHeader, for: indexPath) as! AccountCellHeder
        reusableView.item = ""
        reusableView.delagate = self
        reusableView.editIConImageView.isUserInteractionEnabled = true
        reusableView.editIConImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startEditing)))
        
        return reusableView
    }
    
    
    @objc func startEditing(sender: UITapGestureRecognizer){
        let passcodeEnabled = UserDefaults.standard.bool(forKey: Key.passcodeEnabled)
        let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
        
        if passcodeEnabled {
            let authController = AuthenticateController()
            authController.pinDelegate = self
            authController.isAuthOnly = true
            let nav = UINavigationController(rootViewController: authController)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)
        } else if fingerPrintEnabled {
            self.biometricAuth()
        } else {
            self.navigationController?.pushViewController(EditProfileController(), animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.settingClicked(item: settings[indexPath.row])
    }
    
    func biometricAuth(){
        // start authentication
        BioMetricAuthenticator.authenticateWithPasscode(reason: "Use your fingerprint to unlock", completion: { result in
            switch result {
            case .success( _):
                // authentication successful
                self.navigationController?.pushViewController(EditProfileController(), animated: true)
            case .failure(let error):
                // do nothing on canceled
                if error == .canceledByUser || error == .canceledBySystem {
                    return
                }
                    // device does not support biometric (face id or touch id) authentication
                else if error == .biometryNotAvailable {
                    self.showErrorAlert(message: error.message())
                }
                    // show alternatives on fallback button clicked
                else if error == .fallback {
                    // here we're entering username and password
                }
                    // Biometry is locked out now, because there were too many failed attempts.
                    // Need to enter device passcode to unlock.
                else if error == .biometryLockedout {
                    self.showErrorAlert(message: error.message())
                }
                    // show error on authentication failed
                else {
                    self.showErrorAlert(message: error.message())
                }
            }
            
            
        })
    }
    
    func showAlert(title: String, message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
    
    func showErrorAlert(message: String) {
        ViewControllerHelper.showPrompt(vc: self, message: message)
    }
}

extension AccountController {
    //MARK:- clicked here
    func settingClicked(item:SettingItem) {
        switch item.type {
        //upgrading account
        case .UPGRADE:
            // Clear skip flags
            User.setSkipUserSelfieStatus(status: false)
            let destination = UpgradeAccountController()
            self.navigationController?.pushViewController(destination, animated: true)
        //Share account
        case .SHARE:
            self.accountInfoView.delegate = self
            self.accountInfoView.showSection()
        //Statement
        case .STATEMENT:
            let destination = StatementSummaryController()
            self.navigationController?.pushViewController(destination, animated: true)
        //Transaction
        case .HISTORY:
            let destination = TransactionHistoryController()
            self.navigationController?.pushViewController(destination, animated: true)
        case .INVITE:
            let destination = InviteFriendsController()
            self.navigationController?.pushViewController(destination, animated: true)
        //security Options
        case .SECURITY:
            let destination = SecurityController()
            self.navigationController?.pushViewController(destination, animated: true)
        //Payment Options
        case .PAYMENT:
            let destination = PaymentOptionController()
            self.navigationController?.pushViewController(destination, animated: true)
        //Log out
        case .LOGOUT:
            self.showLogOutPrompt()
        case .TERMS:
            let destination = LQWebViewController()
            destination.url = AppConstants.termsUrl
            destination.title = "Terms and Conditions"
            self.navigationController?.pushViewController(destination, animated: true)
        case .SUPPORT:
            let destination = SupportController()
            self.navigationController?.pushViewController(destination, animated: true)
        case .FAQ:
            let destination = LQWebViewController()
            destination.url = AppConstants.faqUrl
            destination.title = "FAQs"
            self.navigationController?.pushViewController(destination, animated: true)
//        case .ADD_REFERRAL:
//            let destination = AddReferralViewController()
//            destination.title = "Add Referral code"
//            self.navigationController?.pushViewController(destination, animated: true)
            
        default:
            print("Nothing")
        }
    }
    
    func showLogOutPrompt()  {
        let message = "Are you sure you want to log out of your account?"
        
        let alertAction = UIAlertAction(title: "Proceed", style: .default, handler: {(Alert:UIAlertAction!) -> Void in
            self.viewControllerHelper.showActivityIndicator()
            ApiService().logOut(completion: { (status, withMessage) in
                self.viewControllerHelper.hideActivityIndicator()
                
                if status == ApiCallStatus.SUCCESS {
                    guard let user = User.getUser() else { return }
                    let fingerPrintEnabled = UserDefaults.standard.bool(forKey: Key.fingerPrintEnabled)
                    
                    var phone = ""
                    var password = ""
                    if fingerPrintEnabled {
                        phone = user.phone
                        password = User.getPassword()
                        print("Saving stuff before logout \(phone) \(password)")
                    }
                    
                    Mics.setOnboardTheme()
                    User.delete()
                    Goal.delete()
                    TransactionHistory.delete()
                    
                    let realm = try! Realm()
                    try! realm.write {
                        realm.deleteAll()
                    }
                    
                    // Clear user defaults
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    
                    if fingerPrintEnabled {
                        let aes = SHA256()
                        UserDefaults.standard.set(phone, forKey: "phone")
                        UserDefaults.standard.set(aes.encryptString(string: password), forKey: "password")
                        
                        print("Checking after logout \(phone) \(password)")
                        let savedPhone = UserDefaults.standard.string(forKey: "phone")
                        let savedPassword = UserDefaults.standard.string(forKey: "password")
                        
                        print("Saved Phone \(savedPhone ?? "")")
                        print("Finger print status \(savedPassword ?? "")")
                        
                        UserDefaults.standard.synchronize()
                    }
                    
                    let vc = OnboardViewController()
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }   else {
                    ViewControllerHelper.showPrompt(vc: self, message: withMessage)
                }
            })
        })
        
        let alertCancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(Alert: UIAlertAction!) -> Void in
            
        })
        
        let alertView = UIAlertController(title: Key.APP_NAME, message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.hex(hex: Key.primaryHexCode)
        
        alertView.addAction(alertCancelAction)
        alertView.addAction(alertAction)
        
        alertView.preferredAction = alertAction
        
        self.present(alertView, animated: true, completion: nil)
    }
}

extension AccountController: PinDelegate, accountHeaderDelegate {
    
    func isHeaderSelected() {
        print("header selected")
        let cc = CoachController()
        self.navigationItem.title = ""
        self.navigationController?.pushFade(cc)
    }
    
    func didSetPin() {
        self.navigationController?.pushViewController(EditProfileController(), animated: true)
    }
    
    func didFailed() {
        
    }
    
    func didCanceled() {
        
    }
}


//MARK: the share delagate section
extension AccountController: ShareDelegate {
    
    func didSelect(message: String) {
         self.accountInfoView.closePicker()
         ViewControllerHelper.presentSharer(targetVC: self, message: message)
    }
}
