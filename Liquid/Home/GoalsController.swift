//
//  GoalsController.swift
//  Liquid
//
//  Created by Benjmain Acquah on 05/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//


import UIKit
import AlamofireImage

class GoalsController: NavConfigController {
    let collectionId = "collectionId"
    let sectionCollectionId = "sectionCollectionId"
    var delegate: HomeDelegate?
    let viewControllerHelper = ViewControllerHelper()
    var goals = [Any]()
    let user = User.getUser()!

    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.layer.cornerRadius = 10
        return collectionIn
    }()

    lazy var createGoalButton: UIButton = {
        let button = ViewControllerHelper.baseRoudImageButton()
        button.setImage(UIImage(named: "goals"), for: .normal)
        button.setTitle("  Create goal", for: .normal)
        button.addTarget(self, action: #selector(createNewGoal), for: .touchUpInside)
        button.semanticContentAttribute = .forceLeftToRight
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: 10, right: -20)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return button
    }()


    lazy var emptyView: EmptyGoalView = {
        let view = EmptyGoalView()
        //view.delegate = self
        view.width = self.view.frame.size.width
        print(self.view.frame.size.width)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()


    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.hex(hex: "FFFFFF")
        setUpNavigationBar(title: "Goals")
        self.setUpUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        if  UserDefaults.standard.bool(forKey: Key.createGoal) {
            UserDefaults.standard.set(false, forKey: Key.createGoal)
            setUpGoals()
        }
    }

    //MARK:- setup main layouts
    func setUpUI()  {

        self.view.addSubview(collectionView)
        self.view.addSubview(createGoalButton)

        NotificationCenter.default.addObserver(self, selector: #selector(addParticipantListen(notification:)), name: NSNotification.Name(rawValue: "addParticipantListen"), object: nil)

        self.collectionView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.collectionView.register(GenericSectionCell.self, forCellWithReuseIdentifier: sectionCollectionId)
        self.collectionView.register(GoalTimelineCell.self, forCellWithReuseIdentifier: collectionId)

        self.createGoalButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.createGoalButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.createGoalButton.widthAnchor.constraint(equalToConstant: 150).isActive = true

        self.setUpGoals()

        self.setNavItem()

        // Add pull to refresh
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshGoals(_:)), for: .valueChanged)
    }

    @objc func addParticipantListen(notification: Notification){
        print("addParticipantListen method called")
        //self.fetchAllParticipants()
        //self.collectionView.reloadData()
        self.refreshCall()
    }

    func refreshCall(){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().goals { (status, goals, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
    }

    @objc func refreshGoals(_ sender: Any) {
        self.refreshControl.endRefreshing()

        self.viewControllerHelper.showActivityIndicator()
        ApiService().goals { (status, goals, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
    }


    func setUpGoals()  {
        self.loadLocalData()
        if self.goals.isEmpty {
           self.viewControllerHelper.showActivityIndicator()
        }
        ApiService().goals { (status, goals, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
        //Listen for new goal creation
        NotificationCenter.default.addObserver(self, selector: #selector(self.fundNewGoal(_:)), name: NSNotification.Name(rawValue: Key.createGoal), object: nil)
    }

    @objc func fundNewGoal(_ notification: Notification){

    }

    //MARK: load all the local goals
    func loadLocalData() {
        self.goals.removeAll()
        var allGoals = Goal.all()
        allGoals.reverse()
        if allGoals.isEmpty {
            self.setUpEmptyView()
        } else {
            self.createGoalButton.isHidden = false
            self.emptyView.removeFromSuperview()
            self.goals.append("ACTIVE")
            allGoals.forEach { (goal) in
                if goal.status == "active" {
                    self.goals.append(goal)
                    //self.goals.reverse()
                }
            }

            let completedCount = allGoals.filter({ $0.status == "completed" }).count
            if completedCount > 0 {
                self.goals.append("COMPLETED")
                allGoals.forEach { (goal) in
                    if goal.status == "completed" {
                        self.goals.append(goal)
                        //self.goals.reverse()
                    }
                }
            }
        }
        self.collectionView.reloadData()
    }

    func setUpEmptyView()  {
       self.view.addSubview(emptyView)
       self.emptyView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.emptyView.bgView.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
        self.createGoalButton.isHidden = true
    }

    @objc func createNewGoal(){
        let destination = NewGoalController()
        let nav = UINavigationController(rootViewController: destination)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }

    func setNavItem()  {
        let image = UIImage(named: "NotificationBell")?.withRenderingMode(.alwaysOriginal)
        let menuNotification = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleNotification))

        let imageFaq = UIImage(named: "NotificationFAQ")?.withRenderingMode(.alwaysOriginal)
        let menuFaq = UIBarButtonItem(image: imageFaq, style: .plain, target: self, action: #selector(handleFAQ))
        navigationItem.rightBarButtonItems = [menuNotification,menuFaq]

        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
    }


    @objc func handleNotification()  {
        let destination = NotificationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }

    @objc func handleFAQ()  {
        let destination = LQWebViewController()
        destination.url = AppConstants.faqUrl
        destination.title = "FAQs"
        self.navigationController?.pushViewController(destination, animated: true)
    }
}

extension GoalsController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.goals.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.goals[indexPath.row]
        if item is Goal {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! GoalTimelineCell
            cell.item = item as? Goal
            return cell
        }

        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: sectionCollectionId, for: indexPath) as! GenericSectionCell
        cell.item = self.goals[indexPath.row] as? String
        return cell

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var itemWidth = collectionView.frame.width - 16
        var itemHeight = CGFloat(200)
        let item = self.goals[indexPath.row]
        if item is String {
           itemHeight = 40
           itemWidth = collectionView.frame.width
        }
        return CGSize(width: itemWidth, height: itemHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let goal = self.goals[indexPath.row]
        if goal is String {
            return
        }

        self.getRecuringSetting(theGoal: goal as? Goal)
    }

    func getRecuringSetting(theGoal: Goal?){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().getRecurringDebitSettings(goalId: theGoal!.id) { (status, response, message) in

            print("get recurring data: ",response as Any)
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let destination = GoalDetailController()
                destination.goal = theGoal
                destination.isGoalOwner = theGoal!.is_goal_owner
                destination.goalImage = theGoal!.image
                print("get recurring data: ",response?.payment_method as Any)
                if response?.payment_method != "" {
                    destination.isLinked = true
                    destination.isAutoDebit = true
                }
                self.navigationController?.pushViewController(destination, animated: true)
            }else if status == ApiCallStatus.DETAIL {
                let destination = GoalDetailController()
                destination.goal = theGoal
                destination.isGoalOwner = theGoal!.is_goal_owner
                destination.goalImage = theGoal!.image
                if response?.payment_method != "" {
                    destination.isLinked = true
                    destination.isAutoDebit = true
                }
                self.navigationController?.pushViewController(destination, animated: true)
            }
            else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
}

//extension GoalsController: HomeDelegate {
//
//
//    func clickedAddFund() {
//
//    }
//
//    func clickedNotification() {
//        let destination = NotificationController()
//        self.navigationController?.pushViewController(destination, animated: true)
//    }
//
//    func didSelectContact(contact: FavouriteContact) {
//
//    }
//
//    func didSelectGoal(item: Goal) {
//
//    }
//
//    func didStartInvite() {
//
//    }
//
//    func clickedBuyAirtime() {
//
//    }
//
//    func clickedManageGoal() {
//
//    }
//
//    func addNewGoal() {
//       self.createNewGoal()
//    }
//
//    func didGoalCategory(item: GoalCategory) {
//
//    }
//
//    func startTransactions(){
////        let vc = TransactionsController()
////        self.navigationController?.pushViewController(vc, animated: true)
//    }
//
//}
