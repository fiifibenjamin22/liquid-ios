//
//  GoalListView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalListViewCell: BaseCell {
    
    let collectionId = "collectionId"
    var goals = [GoalCategory]()
    var delegate: HomeDelegate?
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let createLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "CREATE A GOAL"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        return textView
    }()
    
    lazy var manageGoalButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("MANAGE GOALS ", for: .normal)
        button.setImage(UIImage(named: "NextActionBlack"), for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 12)!)
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5,left: 15,bottom: 5,right: 0)
        button.addTarget(self, action: #selector(manageGoal), for: .touchUpInside)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    func updateList()  {
        
    }

    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(splitLine)
        addSubview(collectionView)
        addSubview(createLabel)
        addSubview(manageGoalButton)
        
        self.splitLine.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.splitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        self.createLabel.anchorWithConstantsToTop(top: splitLine.bottomAnchor, left: leadingAnchor, bottom: nil, right: manageGoalButton.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        self.createLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.manageGoalButton.anchorWithConstantsToTop(top: splitLine.bottomAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: -16)
        self.manageGoalButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.manageGoalButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        self.collectionView.anchorWithConstantsToTop(top: createLabel.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: -10, rightConstant: 0)
        self.collectionView.register(GoalCell.self, forCellWithReuseIdentifier: collectionId)
        
        self.goals.append(contentsOf: GoalCategory.all())
        self.collectionView.reloadData()
        
    }
    
    @objc func manageGoal(){
       self.delegate?.clickedManageGoal()
    }
}

extension GoalListViewCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return goals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = goals[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! GoalCell
        cell.itemCart = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = CGFloat(150)
        return CGSize(width: itemWidth, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.clicked(item: self.goals[indexPath.row])
    }
    
    //MARK:- clicked here
    func clicked(item:GoalCategory) {
        self.delegate?.didGoalCategory(item: item)
    }
    
}

