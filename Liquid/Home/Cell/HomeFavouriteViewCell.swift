//
//  HomeFavouriteView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//


import UIKit

class HomeFavouriteViewCell: BaseCell {
    
    let collectionId = "collectionId"
    var favourites = [FavouriteContact]()
    var delegate: HomeDelegate?
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    let favouriteLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "FAVOURITES"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 14)!)
        return textView
    }()
    
    lazy var buyAirtimeButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("BUY AIRTIME ", for: .normal)
        button.setImage(UIImage(named: "NextActionBlack"), for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 12)!)
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5,left: 15,bottom: 5,right: 0)
        button.addTarget(self, action: #selector(buyAirtime), for: .touchUpInside)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        addSubview(splitLine)
        addSubview(collectionView)
        addSubview(favouriteLabel)
        addSubview(buyAirtimeButton)
        
        self.splitLine.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.splitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        self.favouriteLabel.anchorWithConstantsToTop(top: splitLine.bottomAnchor, left: leadingAnchor, bottom: nil, right: buyAirtimeButton.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        self.favouriteLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.buyAirtimeButton.anchorWithConstantsToTop(top: splitLine.bottomAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: -16)
        self.buyAirtimeButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.buyAirtimeButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        self.collectionView.anchorWithConstantsToTop(top: favouriteLabel.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
        self.collectionView.register(HomeFavouriteCell.self, forCellWithReuseIdentifier: collectionId)
        
    
        let someFavourites = FavouriteContact.all()
        self.favourites.append(contentsOf: someFavourites)
        self.collectionView.reloadData()
        
    }
    
    @objc func buyAirtime(){
        self.delegate?.clickedBuyAirtime()
    }
}

extension HomeFavouriteViewCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favourites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = favourites[indexPath.row]
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! HomeFavouriteCell
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = CGFloat(100)
        return CGSize(width: item, height: item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(item: self.favourites[indexPath.row])
    }
    
    //MARK:- clicked here
    func transactionClicked(item:FavouriteContact) {
        self.delegate?.didSelectContact(contact: item)
    }
    
}


class HomeFavouriteCell: BaseCell {
    
    var item: FavouriteContact?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.nameLabel.text = unwrapedItem.name
            self.letterImageView.image = Mics.imageWith(name: unwrapedItem.name)
            self.iconImageView.image = Mics.networkIcon(name: unwrapedItem.network)
        }
    }
    
    let iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let letterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Thin", size: 12)!)
        return textView
    }()
    
    let bgView: CardView = {
        let view = CardView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(bgView)
        bgView.addSubview(letterImageView)
        bgView.addSubview(iconImageView)
        bgView.addSubview(nameLabel)
        
        self.bgView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: -8, rightConstant: -8)
        
        let height = frame.height - 16 - 20 - 16
        self.letterImageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.letterImageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.letterImageView.centerXAnchor.constraint(equalTo: bgView.centerXAnchor, constant: 0).isActive = true
        self.letterImageView.bottomAnchor.constraint(equalTo: nameLabel.topAnchor, constant: -4).isActive = true
        self.letterImageView.layer.cornerRadius = height/2
        
        self.iconImageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        self.iconImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        self.iconImageView.topAnchor.constraint(equalTo: letterImageView.topAnchor, constant: 8).isActive = true
        self.iconImageView.trailingAnchor.constraint(equalTo: letterImageView.trailingAnchor, constant: 8).isActive = true
        
        self.nameLabel.anchorWithConstantsToTop(top: nil, left: bgView.leadingAnchor, bottom: bgView.bottomAnchor, right: bgView.trailingAnchor, topConstant: 0, leftConstant: 8, bottomConstant: -8, rightConstant: -8)
        self.nameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.letterImageView.image = nil
        self.iconImageView.image = nil
        self.nameLabel.text = nil
    }
}

