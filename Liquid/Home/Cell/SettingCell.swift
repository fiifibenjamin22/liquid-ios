//
//  SettingCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class SettingCell: BaseCell {
    
    var item: SettingItem?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = unwrapedItem.title
            if unwrapedItem.title.contains("Upgrade") {
               self.messageTextView.textColor = UIColor.hex(hex: Key.primaryHexCode)
            } else {
               self.messageTextView.textColor = UIColor.darkText
            }
        }
    }
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Light", size: 18)!)
        return textView
    }()
    
    let arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "Next")
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(messageTextView)
        addSubview(arrowImageView)
        addSubview(splitLine)
        
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: splitLine.topAnchor, right: arrowImageView.leadingAnchor, topConstant: 4, leftConstant: 24, bottomConstant: -4, rightConstant: -16)
        
        self.arrowImageView.anchorWithConstantsToTop(top: nil, left: nil, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: -24)
        self.arrowImageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        self.arrowImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        self.arrowImageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        
        self.splitLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        self.splitLine.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
    }
}

