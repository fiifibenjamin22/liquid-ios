//
//  GoalTimelineCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import Alamofire

class GoalTimelineCell: BaseCell {
    
    let placeholder = UIImage(named: placeholderImage)
    
    var item: Goal? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.contentTextView.text = unwrapedItem.name
            
            let currencyFormatter = NumberFormatter()
            currencyFormatter.usesGroupingSeparator = true
            currencyFormatter.numberStyle = .currency
            currencyFormatter.locale = Locale(identifier: "ha_Latn_GH")
            let thebalance = currencyFormatter.string(from: NSNumber(value: unwrapedItem.balance.rounded(toPlaces: 2)))!.dropFirst(3)
            let theTargetAmount = currencyFormatter.string(from: NSNumber(value: unwrapedItem.targetAmount))!.dropFirst(3)
            
            let regularFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 16)!)
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
            
            let ofText = " of ".formatAsAttributed(font: regularFont, color: UIColor.white)
            let currency = "\(unwrapedItem.currency) ".formatAsAttributed(font: boldFont, color: UIColor.white)
            let balance = "\(thebalance)".formatAsAttributed(font: boldFont, color: UIColor.white)
            let targetAmount = "\(theTargetAmount)".formatAsAttributed(font: regularFont, color: UIColor.white)
           
            let resultingAmount = NSMutableAttributedString()
            resultingAmount.append(currency)
            resultingAmount.append(balance)
            
            self.amountTextView.attributedText = resultingAmount
            
            let targetString = NSMutableAttributedString()
            targetString.append(ofText)
            targetString.append(targetAmount)
            self.totalTextView.attributedText = targetString
            
            let percent = (unwrapedItem.balance/unwrapedItem.targetAmount)
            //self.progressPercentTextView.text = "\(percent*100)% complete"
            self.progressUIView.setProgress(Float(percent), animated: true)
            
            
            if !(unwrapedItem.image.isEmpty) {
                guard let imageUrl = URL(string: unwrapedItem.image) else { return }
                self.bgView.setImage(url: imageUrl, placeholder: placeholder)
                } else {
                self.bgView.image = Mics.userPlaceHolder()
            }
            
            //show participants logic
            if !(unwrapedItem.owner_picture.isEmpty) {
                guard let imageUrl = URL(string: unwrapedItem.owner_picture) else { return }
                self.ownerImage.setImage(url: imageUrl, placeholder: placeholder)
                } else {
                self.ownerImage.image = Mics.userPlaceHolder()
            }
            self.ownerName.text = unwrapedItem.owner_name
            
            //part one
            if !(unwrapedItem.image1.isEmpty) {
                guard let imageUrl = URL(string: unwrapedItem.image1) else { return }
                self.participantsOne.setImage(url: imageUrl, placeholder: placeholder)
                } else {
                self.participantsOne.image = Mics.userPlaceHolder()
            }
            
            //Part two
            if !(unwrapedItem.image2.isEmpty) {
                guard let imageUrl = URL(string: unwrapedItem.image2) else { return }
                self.participantsTwo.setImage(url: imageUrl, placeholder: placeholder)
                } else {
                self.participantsTwo.image = Mics.userPlaceHolder()
            }
            
            //part three
            if !(unwrapedItem.image3.isEmpty) {
                guard let imageUrl = URL(string: unwrapedItem.image3) else { return }
                self.participantsThree.setImage(url: imageUrl, placeholder: placeholder)
                } else {
                self.participantsThree.image = Mics.userPlaceHolder()
            }
            
            if unwrapedItem.is_shared {
                self.participantParentView.isHidden = false
                
                if unwrapedItem.is_goal_owner {
                    self.ownerName.isHidden = true
                    self.ownerStatus.isHidden = true
                    self.ownerImage.isHidden = false
                    
                    if unwrapedItem.total_participants == 1 {
                        self.parentViewWidthContraint?.constant = 55
                        self.ownerImage.isHidden = false
                        self.participantsOne.isHidden = false
                        
                        self.participantsTwo.isHidden = true
                        self.participantsThree.isHidden = true
                        self.plusOne.isHidden = true
                    }else if unwrapedItem.total_participants == 2 {
                        self.parentViewWidthContraint?.constant = 73
                        self.ownerImage.isHidden = false
                        self.participantsThree.isHidden = true
                        self.plusOne.isHidden = true
                    }else if unwrapedItem.total_participants == 3 {
                        self.parentViewWidthContraint?.constant = 80
                        self.ownerImage.isHidden = false
                        self.plusOne.isHidden = true
                    }else if unwrapedItem.total_participants > 3 {
                        self.ownerImage.isHidden = false
                        self.plusOne.isHidden = false
                    }
                }else{
                    self.ownerName.isHidden = false
                    self.ownerStatus.isHidden = false
                    
                    self.plusOne.isHidden = true
                    self.participantsOne.isHidden = true
                    self.participantsTwo.isHidden = true
                    self.participantsThree.isHidden = true
                }
            }else{
                self.participantParentView.isHidden = true
            }
            
        }
    }
    
    let participantParentView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    //owner Data
    let ownerImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let ownerName: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Mensah"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
        return lbl
    }()
    
    let ownerStatus: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "Owner"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        return lbl
    }()
    
    //Particitpants info
    let participantsOne: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderWidth = 1.0
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    let participantsTwo: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()

    let participantsThree: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "AvatarLady")
        img.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        img.contentMode = .scaleAspectFit
        return img
    }()

    let plusOne: UILabel = {
        let lbl = ViewControllerHelper.baseLabel()
        lbl.text = "+1"
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        return lbl
    }()
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Regular, size: 18)!)
        return textView
    }()
    
    let progressUIView: UIProgressView = {
        let progressBar = ViewControllerHelper.baseProgressBar()
        progressBar.tintColor = UIColor.hex(hex: "6DDCF1")
        progressBar.layer.borderColor = UIColor.clear.cgColor
        progressBar.progress = Float(0.5)
        progressBar.layer.cornerRadius = 1.5
        progressBar.backgroundColor = .white
        return progressBar
    }()
    
    let bgView: ImageCardView = {
        let view = ImageCardView()
        view.backgroundColor = UIColor.white
        view.cornerRadius = 5
        view.shadowOffsetHeight = 2
        view.translatesAutoresizingMaskIntoConstraints = true
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        
        let overlay: UIView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        overlay.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        view.addSubview(overlay)
        
        overlay.anchorWithConstantsToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        return view
    }()
    
    let amountTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.textAlignment = .left
        return textView
    }()
    
    let totalTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.textAlignment = .right
        return textView
    }()
    
    var parentViewWidthContraint: NSLayoutConstraint?
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(bgView)
        self.bgView.addSubview(contentTextView)
        
        self.contentTextView.addSubview(participantParentView)
        self.participantParentView.addSubview(ownerImage)
        self.participantParentView.addSubview(ownerName)
        self.participantParentView.addSubview(ownerStatus)

        self.participantParentView.addSubview(participantsOne)
        self.participantParentView.addSubview(participantsTwo)
        self.participantParentView.addSubview(participantsThree)
        self.participantParentView.addSubview(plusOne)
        
        self.bgView.addSubview(progressUIView)
        self.bgView.addSubview(amountTextView)
        self.bgView.addSubview(totalTextView)
        
        self.bgView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 10, leftConstant: 5, bottomConstant: 0, rightConstant: -5)
    
        self.contentTextView.anchorWithConstantsToTop(top: nil, left: progressUIView.leadingAnchor, bottom: progressUIView.topAnchor, right: bgView.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: -48, rightConstant: -16)
        
        self.participantParentView.layer.cornerRadius = 20
        self.participantParentView.layer.masksToBounds = true
        self.participantParentView.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 16).isActive = true
        self.participantParentView.rightAnchor.constraint(equalTo: contentTextView.rightAnchor, constant: 0).isActive = true
        self.participantParentView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.parentViewWidthContraint = participantParentView.widthAnchor.constraint(equalToConstant: 110)
        self.parentViewWidthContraint?.isActive = true

        self.ownerImage.layer.cornerRadius = 15
        self.ownerImage.layer.masksToBounds = true
        self.ownerImage.anchor(participantParentView.topAnchor, left: participantParentView.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 6, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)

        self.ownerName.anchor(ownerImage.topAnchor, left: ownerImage.rightAnchor, bottom: nil, right: participantParentView.rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)

        self.ownerStatus.anchor(ownerName.bottomAnchor, left: ownerName.leftAnchor, bottom: ownerImage.bottomAnchor, right: ownerName.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        self.participantsOne.layer.cornerRadius = 15
        self.participantsOne.layer.masksToBounds = true
        self.participantsOne.anchor(ownerImage.topAnchor, left: ownerImage.leftAnchor, bottom: ownerImage.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 0)

        self.participantsTwo.layer.cornerRadius = 15
        self.participantsTwo.layer.masksToBounds = true
        self.participantsTwo.anchor(participantsOne.topAnchor, left: participantsOne.leftAnchor, bottom: participantsOne.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 0)
        
        self.participantsThree.layer.cornerRadius = 15
        self.participantsThree.layer.masksToBounds = true
        self.participantsThree.anchor(participantsTwo.topAnchor, left: participantsTwo.leftAnchor, bottom: participantsTwo.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 0)

        self.plusOne.anchor(participantsThree.topAnchor, left: participantsThree.rightAnchor, bottom: participantsThree.bottomAnchor, right: nil, topConstant: 0, leftConstant: 3, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        //MARK:- progress section
        self.progressUIView.anchorWithConstantsToTop(top: nil, left: bgView.leadingAnchor, bottom: bottomAnchor, right: bgView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.progressUIView.heightAnchor.constraint(equalToConstant: 3).isActive = true
        
        //MARK:- amount section
        self.amountTextView.anchorWithConstantsToTop(top: nil, left: progressUIView.leadingAnchor, bottom: progressUIView.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.amountTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.amountTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.totalTextView.anchorWithConstantsToTop(top: nil, left: nil, bottom: progressUIView.topAnchor, right: progressUIView.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.totalTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        self.totalTextView.widthAnchor.constraint(equalToConstant: 100).isActive = true
    }
}
