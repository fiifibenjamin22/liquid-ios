//
//  CoachMainCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 09/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class CoachingMainCell: BaseCell {
    
    //var homeDelagate: HomeDelegate?
    let CellId = "CellId"
    var homeDelagate: HomeDelegate?
    //var coach = Coach()
    var categories = [CoachCategories]()
    var arrayColors = [[UIColor.hex(hex: "02A9E8"), UIColor.hex(hex: "00F6FF")],[UIColor.hex(hex: "FF2E00"), UIColor.hex(hex: "FF8A00")],[UIColor.hex(hex: "0500FF"), UIColor.hex(hex: "CC00ED")]]
    
    var coachObject: Coach? {
        didSet {
            
            guard let unwrapedItem = coachObject else { return }
            print("getting cat data from coach: ",unwrapedItem.CoachCategories.count)
            self.categories.removeAll()
            
            self.categories.append(contentsOf: unwrapedItem.CoachCategories)
            self.currentPointsLbl.text = "\(unwrapedItem.points)"
            self.pointsToNextRank.text = "\(unwrapedItem.points_to_next_rank) points to next rank"
            self.yourRank.text = "Your rank \n\(unwrapedItem.rank)"
            self.collectionView.reloadData()
        }
    }
    
    let pointsView: CardView = {
        let v = CardView()
        v.backgroundColor = .white
//        v.layer.shadowColor = UIColor.gray.cgColor
        v.layer.shadowOffset = CGSize.zero
        v.layer.cornerRadius = 8
        v.clipsToBounds = true
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let currentPointsLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont.boldSystemFont(ofSize: 26)
        lbl.textAlignment = .center
        lbl.textColor = .black
        lbl.layer.cornerRadius = 35
        lbl.layer.masksToBounds = true
        lbl.layer.borderWidth = 2
        lbl.layer.borderColor = UIColor.hex(hex: Key.primaryHexCode).cgColor
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let yourRank: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont.boldSystemFont(ofSize: 19)
        lbl.backgroundColor = .clear
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let pointsToNextRankView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    let pointsToNextRank: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont.systemFont(ofSize: 19)
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        lbl.layer.cornerRadius = 8
        lbl.layer.masksToBounds = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        pointsToNextRankView.roundCorners([.bottomLeft, .bottomRight], radius: 8)
    }
    
    var gradientLayer = CAGradientLayer()
    
    override func setUpView() {
        super.setUpView()
        
        self.collectionView.register(ThreeCells.self, forCellWithReuseIdentifier: CellId)
        
        gradientLayer.frame = self.frame
        gradientLayer.colors = [UIColor.magenta.cgColor, UIColor.cyan.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.95)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.05)
        
        
        addSubview(pointsView)
        addSubview(pointsToNextRankView)
        pointsToNextRankView.addSubview(pointsToNextRank)
        pointsView.addSubview(currentPointsLbl)
        pointsView.addSubview(yourRank)
        addSubview(collectionView)
        
        pointsView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        pointsView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        pointsView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        pointsView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        currentPointsLbl.centerYAnchor.constraint(equalTo: self.pointsView.centerYAnchor, constant: 0).isActive = true
        currentPointsLbl.leftAnchor.constraint(equalTo: self.pointsView.leftAnchor, constant: 8).isActive = true
        currentPointsLbl.widthAnchor.constraint(equalToConstant: 70).isActive = true
        currentPointsLbl.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        yourRank.leftAnchor.constraint(equalTo: self.currentPointsLbl.rightAnchor, constant: 8).isActive = true
        yourRank.topAnchor.constraint(equalTo: self.pointsView.topAnchor, constant: 8).isActive = true
        yourRank.rightAnchor.constraint(equalTo: self.pointsView.rightAnchor, constant: -8).isActive = true
        yourRank.bottomAnchor.constraint(equalTo: self.pointsView.bottomAnchor, constant: -8).isActive = true
        
        pointsToNextRankView.topAnchor.constraint(equalTo: self.pointsView.bottomAnchor, constant: 0).isActive = true
        pointsToNextRankView.leftAnchor.constraint(equalTo: self.pointsView.leftAnchor, constant: 16).isActive = true
        pointsToNextRankView.rightAnchor.constraint(equalTo: self.pointsView.rightAnchor, constant: -16).isActive = true
        pointsToNextRankView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        pointsToNextRank.topAnchor.constraint(equalTo: self.pointsToNextRankView.topAnchor, constant: 10).isActive = true
        pointsToNextRank.bottomAnchor.constraint(equalTo: self.pointsToNextRankView.bottomAnchor, constant: -5).isActive = true
        pointsToNextRank.leftAnchor.constraint(equalTo: self.pointsToNextRankView.leftAnchor, constant: 8).isActive = true
        pointsToNextRank.rightAnchor.constraint(equalTo: self.pointsToNextRankView.rightAnchor, constant: -8).isActive = true
        
        //collection view
        self.collectionView.topAnchor.constraint(equalTo: pointsToNextRankView.bottomAnchor, constant: 50).isActive = true
        self.collectionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.collectionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
}

extension CoachingMainCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //print("print list items: ",coach.CoachCategories.count)
        print("print list items: ",categories.count)
        
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: CellId, for: indexPath) as! ThreeCells
        cell.item = self.categories[indexPath.row]
        print("print list items: ",self.categories[indexPath.row].name)
        cell.collectionView.isScrollEnabled = false
        cell.mainCardView.colors = self.arrayColors[indexPath.row]
        cell.catCell = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = collectionView.frame.size.height
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
    
    func addGradientToView(view: UIView, gradColors: Array<CGColor>){
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradColors
        gradientLayer.locations = [0.0, 0.6, 0.8]
        gradientLayer.frame = view.bounds
        gradientLayer.cornerRadius = 10.0
        gradientLayer.masksToBounds = true
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
