//
//  TasksCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 10/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class TasksCell: BaseCell {
    
    var taskCell: CoachCell?
    var isCompleted = false
    
    var item: CoachTask? {
        didSet {
            guard let unwrapped = item else { return }
            coachDescription.text = unwrapped.taskDescription
            if unwrapped.taskCompleted == true {
                self.checkImage.image = UIImage(named: "coach_check")
                self.coachDescription.textColor = UIColor.white.withAlphaComponent(0.8)
                self.coachPoint.textColor = UIColor.white.withAlphaComponent(0.8)
            }
            coachPoint.text = "\(unwrapped.taskValue)pts"
        }
    }
    
    let coachDescription: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.numberOfLines = 2
        lbl.textColor = .white
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let checkImage: UIImageView = {
       let img = UIImageView()
        img.image = UIImage(named: "coach_uncheck")?.withRenderingMode(.alwaysOriginal)
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let coachPoint: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let seperatorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(coachPoint)
        addSubview(checkImage)
        addSubview(coachDescription)
        addSubview(seperatorView)
        
        coachPoint.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        coachPoint.centerYAnchor.constraint(equalTo: self.centerYAnchor,  constant: 5).isActive = true
        coachPoint.widthAnchor.constraint(equalToConstant: 50).isActive = true
        coachPoint.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        checkImage.rightAnchor.constraint(equalTo: self.coachPoint.leftAnchor, constant: 0).isActive = true
        checkImage.topAnchor.constraint(equalTo: self.coachPoint.topAnchor, constant: 0).isActive = true
        checkImage.bottomAnchor.constraint(equalTo: self.coachPoint.bottomAnchor, constant: 0).isActive = true
        checkImage.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        coachDescription.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        coachDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        coachDescription.rightAnchor.constraint(equalTo: checkImage.leftAnchor, constant: -8).isActive = true
        coachDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        seperatorView.topAnchor.constraint(equalTo: self.coachDescription.bottomAnchor, constant: 5).isActive = true
        seperatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        seperatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        seperatorView.heightAnchor.constraint(equalToConstant: 1.5).isActive = true
    }
}
