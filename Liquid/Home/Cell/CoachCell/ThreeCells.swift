//
//  ReadySetGoal.swift
//  Liquid
//
//  Created by Benjamin Acquah on 09/06/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit
import GradientView

class ThreeCells: BaseCell {
    
    var delagate: HomeDelegate?
    let CellId = "CellId"
    var catCell: CoachingMainCell?
    var tasks = [CoachTask]()
    
    var item: CoachCategories? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.coachTitle.text = unwrapedItem.name
            self.tasks.removeAll()
            self.tasks.append(contentsOf: unwrapedItem.CoachTask)
        }
    }
    
    lazy var mainCardView: GradientView = {
        let v = GradientView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 12
        v.layer.masksToBounds = true
        v.layer.shadowColor = UIColor.gray.cgColor
        v.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        v.layer.shadowRadius = 5.0
        v.layer.shadowOpacity = 0.24
        v.locations = [0.3, 1.0]
        v.direction = .vertical
        return v
    }()
    
    let coachTitle: UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textColor = .white
        lbl.font = UIFont.boldSystemFont(ofSize: 27)
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        collectionIn.backgroundColor = .clear
        return collectionIn
    }()
    
    override func setUpView() {
        super.setUpView()
        
        self.collectionView.register(TasksCell.self, forCellWithReuseIdentifier: CellId)
        
        addSubview(mainCardView)
        self.mainCardView.addSubview(coachTitle)
        self.mainCardView.addSubview(self.collectionView)
        
        mainCardView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        mainCardView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 24).isActive = true
        mainCardView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24).isActive = true
        mainCardView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
        
        coachTitle.topAnchor.constraint(equalTo: mainCardView.topAnchor, constant: 12).isActive = true
        coachTitle.leftAnchor.constraint(equalTo: mainCardView.leftAnchor, constant: 12).isActive = true
        coachTitle.rightAnchor.constraint(equalTo: mainCardView.rightAnchor, constant: -8).isActive = true
        coachTitle.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //collection view
        self.collectionView.topAnchor.constraint(equalTo: coachTitle.bottomAnchor, constant: 8).isActive = true
        self.collectionView.leftAnchor.constraint(equalTo: self.mainCardView.leftAnchor, constant: 8).isActive = true
        self.collectionView.rightAnchor.constraint(equalTo: self.mainCardView.rightAnchor, constant: -8).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.mainCardView.bottomAnchor, constant: -8).isActive = true
    }
}


extension ThreeCells: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellId, for: indexPath) as! TasksCell
        cell.item = self.tasks[indexPath.row]
        //cell.taskCell = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.mainCardView.frame.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

}
