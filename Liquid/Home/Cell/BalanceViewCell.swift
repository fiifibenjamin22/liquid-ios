//
//  BalanceView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class BalanceViewCell: BaseCell {
    
    let cellId = "cellId"
    var homeDelagate: HomeDelegate?
    var balances = [BalanceItem]()
    
    //MARK:- UI elements setup
    lazy var pageControl: UIPageControl = {
        let pc = ViewControllerHelper.basePageControl()
        pc.isHidden = true
        return pc
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    
    override func setUpView() {
        super.setUpView()
        
        //addSubview(pageControl)
        addSubview(collectionView)
        
        //self.pageControl.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -8, rightConstant: -16)
        //self.pageControl.heightAnchor.constraint(equalToConstant: 0).isActive = true
        
        self.collectionView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.collectionView.register(BalanceCell.self, forCellWithReuseIdentifier: cellId)
        
        self.addData()
    }
    
    func addData() {
        let user = User.getUser()!
        let goals = Goal.all()
        
        print("number of goals: \(goals.count)")
        print("number of goals: \(user.balance)")
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "ha_Latn_GH")
        let balance = currencyFormatter.string(from: NSNumber(value: user.balance))!.dropFirst(3)
        let goalBalance = currencyFormatter.string(from: NSNumber(value: user.goalBalance))!.dropFirst(3)
        
        if goals.count < 1 || user.balance == 0.00 {
            
            //self.balances.removeAll()
            print("user balance: \(user.currency)")
            print("user balance: \(balance)")
            //let balanceGoals = BalanceItem(title:"Goal Balance",currency:user.currency,amount:"\(goalBalance)",action:"Tap to view goals", type: BalanceType.AVAILABLE)
            let balanceEmptyGoals = BalanceItem(title:" \"Be a goal-getter, \nAdd funds and start today.\" ",currency:"",amount:"",action:"Tap to create goal",type: BalanceType.NONE)
            //balances.append(balanceEmptyGoals)
            
            //let balanceItem = BalanceItem(title:"Liquid Rewards / Refunds",currency:user.goalCurrency,amount:"\(balance)",action:"Tap to make a\n transaction",type: BalanceType.GOALS)
            
            balances.append(balanceEmptyGoals)
            //balances.append(balanceItem)
        }else{
            
            let balanceGoals = BalanceItem(title:"Goal Balance",currency:user.currency,amount:"\(goalBalance)",action:"Tap to view goals", type: BalanceType.AVAILABLE)
            
            let balanceItem = BalanceItem(title:"Liquid Rewards / Refunds",currency:user.goalCurrency,amount:"\(balance)",action:"Tap to make a\n transaction",type: BalanceType.GOALS)
            
            balances.append(balanceGoals)
            balances.append(balanceItem)
            
        }
        self.pageControl.numberOfPages = self.balances.count
        self.collectionView.reloadData()
    }
    
}

extension BalanceViewCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return balances.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BalanceCell
        cell.delagate = self.homeDelagate
        cell.item = balances[indexPath.row]
        
        cell.iconImageView.isHidden = false
        cell.iconImageView.isUserInteractionEnabled = true
        cell.iconImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapNotification)))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(Key.balanceSectionHeight - 50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = Int(targetContentOffset.pointee.x/frame.width)
        self.pageControl.currentPage = pageNumber
        if self.balances.isEmpty {
            return
        }
    }
    
    @objc func tapNotification(sender: UITapGestureRecognizer){
        self.homeDelagate?.clickedNotification()
    }
    
    @objc func tapTransaction(_ sender: UIButton) {
        //print("print \(String(describing: sender.titleLabel?.text?.contains("goals")))")
        if (sender.titleLabel?.text?.contains("goals"))! {
            self.homeDelagate?.clickedManageGoal()
        }else if (sender.titleLabel?.text?.contains("transaction"))! {
            self.homeDelagate?.startTransactions()
        }else{
            self.homeDelagate?.addNewGoal()
        }
    }
}
