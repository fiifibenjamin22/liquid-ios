//
//  BalanceCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class BalanceCell: BaseCell {
    
    var delagate: HomeDelegate?
    
    var item: BalanceItem? {
        didSet {
            guard let unwrapedItem = item else {return}
            let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
            let title = "\(unwrapedItem.title)\n\n".formatAsAttributed(font: boldFont, color: UIColor.white)
            
            let currency = "\(unwrapedItem.currency) ".formatAsAttributed(font: boldFont, color: UIColor.white)
            let amount = unwrapedItem.amount.formatAsAttributed(font: boldFont, color: UIColor.white)
            
            let result = NSMutableAttributedString()
            result.append(title)
            result.append(currency)
            result.append(amount)
            self.contentTextView.attributedText = result
            self.contentTextView.textAlignment = .center
            self.actionButton.setTitle(unwrapedItem.action, for: .normal)
            self.overlayButton.setTitle(unwrapedItem.action, for: .normal)
            
            self.iconImageView.isHidden = false
            switch unwrapedItem.type {
            case BalanceType.AVAILABLE:
                self.imageView.image = UIImage(named: "BalanceAccount")
                self.iconImageView.isHidden = false
            case BalanceType.GOALS:
                self.imageView.image = UIImage(named: "BalanceAccount")
                self.iconImageView.isHidden = false
            default:
                self.imageView.image = UIImage(named: "BalanceAccount")
                self.iconImageView.isHidden = true
                let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 16)!)
                let title = "\(unwrapedItem.title)".formatAsAttributed(font: boldFont, color: UIColor.white)
                self.contentTextView.attributedText = title
            }
        }
    }
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let iconImageView: UIImageView = {
        let imageView =  ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "Notification")
        imageView.isHidden = false
        return imageView
    }()
    
    //overlay button
    lazy var overlayButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.contentHorizontalAlignment = .center
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = .clear
        button.setTitleColor(.clear, for: .normal)
        button.addTarget(self, action: #selector(tapAction(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var actionButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.contentHorizontalAlignment = .center
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        button.titleLabel?.numberOfLines = 2
        button.addTarget(self, action: #selector(tapAction(_:)), for: .touchUpInside)
        return button
    }()
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.clear
        
        addSubview(imageView)
        imageView.addSubview(contentTextView)
        imageView.addSubview(actionButton)
        addSubview(overlayButton)
        addSubview(iconImageView)
        
        //let height = CGFloat(Key.balanceSectionHeight)
        let height = frame.height - 30
        self.imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.imageView.widthAnchor.constraint(equalToConstant: height).isActive = true
        
        self.contentTextView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        self.contentTextView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        self.contentTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 0, leftConstant: 64, bottomConstant: 0, rightConstant: -64)
        
        self.actionButton.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        self.actionButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        self.actionButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.actionButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: -32, rightConstant: 0)
        
        self.overlayButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.overlayButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.overlayButton.heightAnchor.constraint(equalToConstant: height).isActive = true
        self.overlayButton.widthAnchor.constraint(equalToConstant: height).isActive = true
        self.overlayButton.layer.cornerRadius = height / 2
        
        self.iconImageView.anchorWithConstantsToTop(top: imageView.topAnchor, left: nil, bottom: nil, right: imageView.trailingAnchor, topConstant: 16, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.iconImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.iconImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    
    @objc func tapAction(_ sender: UIButton){
        
        if (sender.titleLabel?.text?.contains("goals"))! {
            self.delagate?.clickedManageGoal()
        }else if (sender.titleLabel?.text?.contains("transaction"))! {
            self.delagate?.startTransactions()
        }else{
            self.delagate?.addNewGoal()
        }
        
        //        guard let unwrapedItem = item else {return}
        //        if unwrapedItem.type == .NONE {
        //          self.delagate?.addNewGoal()
        //        } else if unwrapedItem.type == .GOALS {
        //          self.delagate?.clickedManageGoal()
        //        } else {
        //          self.delagate?.addNewGoal()
        //        }
    }
}
