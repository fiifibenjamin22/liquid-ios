//
//  RewardView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 29/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class RewardViewCell: BaseCell {
    
    var delegate: HomeDelegate?
    let boldFont = Font.Roboto.Bold
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
    
    lazy var rewardLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "Rewards"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: boldFont, size: 14)!)
        return textView
    }()
    
    
    lazy var inviteButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("INVITE A FRIEND ", for: .normal)
        button.setImage(UIImage(named: "NextActionBlack"), for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: boldFont, size: 12)!)
        button.setTitleColor(UIColor.darkText, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5,left: 15,bottom: 5,right: 0)
        button.addTarget(self, action: #selector(starInvite), for: .touchUpInside)
        return button
    }()
    
    /*lazy var numberLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.text = "0 Friends joined"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: boldFont, size: 12)!)
        return textView
    }()*/
    
    lazy var progressUIView: UIProgressView = {
        let progressBar = ViewControllerHelper.baseProgressBar()
        progressBar.progress = Float(0.0)
        return progressBar
    }()
    
    lazy var messageLabel: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.text = "Get 10 more friends to unlock this reward"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: boldFont, size: 12)!)
        return textView
    }()
    
    var numberOfInvites: Int? {
        didSet {
            guard let friendsCount = numberOfInvites else { return}
            progressUIView.progress = Float(Double(friendsCount)/10.0)
            /*self.numberLabel.text = "\(friendsCount) Friends joined"
            self.messageLabel.text = "Get \(friendsCount)  more friends to unlock this reward"*/
        }
    }
    
    
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(splitLine)
        addSubview(rewardLabel)
        addSubview(inviteButton)
        
        //addSubview(numberLabel)
        
        addSubview(progressUIView)
        addSubview(messageLabel)
        
        self.splitLine.anchorToTop(top: topAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor)
        self.splitLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
    
        self.inviteButton.anchorWithConstantsToTop(top: splitLine.bottomAnchor, left: nil, bottom: nil, right: trailingAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: -16)
        self.inviteButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        self.inviteButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        self.rewardLabel.anchorWithConstantsToTop(top: splitLine.bottomAnchor, left: leadingAnchor, bottom: nil, right: inviteButton.leadingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -8)
        self.rewardLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        //self.numberLabel.anchorWithConstantsToTop(top: inviteButton.bottomAnchor, left: leadingAnchor, bottom: progressUIView.topAnchor, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        
        self.progressUIView.anchorWithConstantsToTop(top: self.rewardLabel.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -2, rightConstant: -16)
        self.progressUIView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        self.messageLabel.anchorWithConstantsToTop(top: progressUIView.bottomAnchor, left: leadingAnchor, bottom: nil, right: trailingAnchor, topConstant: 2, leftConstant: 16, bottomConstant: -16, rightConstant: -16)
        self.messageLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    @objc func starInvite(){
       delegate?.didStartInvite()
    }
    

}
