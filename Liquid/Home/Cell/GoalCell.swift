//
//  GoalCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class GoalCell: BaseCell {
    
    var item: Goal? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.contentTextView.text = unwrapedItem.name
            
            if  !(unwrapedItem.image.isEmpty) {
                self.imageView.af_setImage(
                    withURL: URL(string: (unwrapedItem.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.imageView.image = Mics.placeHolder()
            }
            
            
        }
    }
    
    var itemCart: GoalCategory? {
        didSet {
            guard let unwrapedItem = itemCart else {return}
            self.contentTextView.text = unwrapedItem.name.htmlToString
            
            if  !(unwrapedItem.image.isEmpty) {
                self.imageView.af_setImage(
                    withURL: URL(string: (unwrapedItem.image))!,
                    placeholderImage: Mics.placeHolder(),
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.imageView.image = Mics.placeHolder()
            }
        }
    }
    
    let contentTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        return textView
    }()
    
    let imageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    let coverImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        return imageView
    }()
    
    
    lazy var actionButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.contentHorizontalAlignment = .center
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 12)!)
        return button
    }()
    
    override func setUpView() {
        super.setUpView()
        
        addSubview(imageView)
        addSubview(coverImageView)
        addSubview(contentTextView)
        addSubview(actionButton)
        
        let textHeight  = frame.height
        self.imageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.coverImageView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        
        self.contentTextView.anchorWithConstantsToTop(top: nil, left: leadingAnchor, bottom: bottomAnchor, right: actionButton.leadingAnchor, topConstant: 0, leftConstant: 16, bottomConstant: -16, rightConstant: -8)
        self.contentTextView.heightAnchor.constraint(equalToConstant: textHeight/3).isActive = true
        
        self.actionButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: bottomAnchor, right: trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.actionButton.heightAnchor.constraint(equalToConstant: textHeight/3).isActive = true
        self.actionButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
    }
}
