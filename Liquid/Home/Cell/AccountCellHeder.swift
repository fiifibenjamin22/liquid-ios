//
//  AccountCellHeder.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/04/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

protocol accountHeaderDelegate: class {
    func isHeaderSelected()
}

class AccountCellHeder: BaseCellHeader {
    
    var delagate: accountHeaderDelegate?
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let profileCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: Images.Account.profileWave)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let editIConImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: Images.Account.edit)
        return imageView
    }()
    
    let profileNameTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .center
        textView.textColor = UIColor.white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return textView
    }()
    
    let headerView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: Images.Account.accountBg)
        return imageView
    }()
    
    //Coach
    let coachCard: UIView = {
        let card = UIView()
        card.backgroundColor = UIColor.white
        card.layer.cornerRadius = 5
        card.layer.borderWidth = 1
        card.layer.borderColor = UIColor.hex(hex: "#E8E8E8").cgColor
        card.translatesAutoresizingMaskIntoConstraints = false
        return card
    }()
    
    let savingsCoach: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.gray
        textView.text = "Your savings coach"
        textView.textAlignment = .center
        textView.backgroundColor = .white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let coachImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "coachicon")
        return imageView
    }()
    
    let coachTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .justified
        textView.isSelectable = false
        textView.isUserInteractionEnabled = true
        return textView
    }()
    
    let rightArrowImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "NextActionBlack")
        return imageView
    }()
    
    var item: String?{
        didSet{
            let user = User.getUser()!
            self.profileNameTextView.text = "\(user.firstName) \(user.lastName)"
            
            let placeholder = UIImage(named: placeholderImage)
            guard let imageUrl = URL(string: user.picture) else { return }
            
            if  !(user.picture.isEmpty) {
                self.profileImageView.setImage(url: imageUrl, placeholder: placeholder)
                } else {
                self.profileImageView.image = Mics.userPlaceHolder()
            }
            
        }
    }
    
    let user = User.getUser()!
    let coach = Coach.getCoach()!

    override func setUpViews() {
        super.setUpViews()
        
        //Coach
        let coachheaderfont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        let coachmiddlefont = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        let coachmessageHeader = "Rank: \(self.coach.rank)\n".formatAsAttributed(font: coachheaderfont, color: UIColor.darkText)
        //let coachmessageHeaderMid = "-\n".formatAsAttributed(font: coachheaderfont, color: UIColor.darkText)
        let coachmessageBody = "\(self.user.points) points to next rank".formatAsAttributed(font: coachmiddlefont, color: UIColor.darkText)
        let coachresult = NSMutableAttributedString()
        coachresult.append(coachmessageHeader)
        //coachresult.append(coachmessageHeaderMid)
        coachresult.append(coachmessageBody)
        self.coachTextView.attributedText = coachresult
        self.coachTextView.textAlignment = .left
        
        self.addSubview(headerView)
        self.addSubview(profileImageView)
        self.addSubview(profileCoverImageView)
        self.addSubview(editIConImageView)
        self.addSubview(profileNameTextView)
        
        addSubview(coachCard)
        addSubview(savingsCoach)
        coachCard.addSubview(coachImageView)
        coachCard.addSubview(coachTextView)
        coachCard.addSubview(rightArrowImageView)
        
        self.headerView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 160)

        self.profileImageView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        self.profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.profileImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        self.profileImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.profileImageView.layer.cornerRadius = 40
        
        self.profileCoverImageView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        self.profileCoverImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.profileCoverImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        self.profileCoverImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        self.profileCoverImageView.layer.cornerRadius = 40
        
        self.editIConImageView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        self.editIConImageView.trailingAnchor.constraint(equalTo: profileCoverImageView.trailingAnchor, constant: -8).isActive = true
        self.editIConImageView.widthAnchor.constraint(equalToConstant: 25).isActive = true
        self.editIConImageView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        self.profileNameTextView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        self.profileNameTextView.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 8).isActive = true
        self.profileNameTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        coachCard.anchor(headerView.bottomAnchor, left: headerView.leftAnchor, bottom: nil, right: headerView.rightAnchor, topConstant: 16, leftConstant: 24, bottomConstant: 0, rightConstant: 24, widthConstant: 0, heightConstant: 90)
        
        savingsCoach.anchor(coachCard.topAnchor, left: coachCard.leftAnchor, bottom: nil, right: coachCard.rightAnchor, topConstant: -10, leftConstant: 16, bottomConstant: 0, rightConstant: 150, widthConstant: 0, heightConstant: 20)
        
        coachImageView.centerYAnchor.constraint(equalTo: coachCard.centerYAnchor).isActive = true
        coachImageView.leftAnchor.constraint(equalTo: coachCard.leftAnchor, constant: 8).isActive = true
        coachImageView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        coachImageView.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
        
        coachTextView.anchor(savingsCoach.bottomAnchor, left: coachImageView.rightAnchor, bottom: coachCard.bottomAnchor, right: coachCard.rightAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 8, rightConstant: 16, widthConstant: 0, heightConstant: 0)
        
        rightArrowImageView.anchor(nil, left: nil, bottom: coachTextView.bottomAnchor, right: coachCard.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 8, widthConstant: 20, heightConstant: 20)
        
        self.coachCard.isUserInteractionEnabled = true
        self.coachCard.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoCoach)))
    }
    
    @objc func gotoCoach(){
        self.delagate?.isHeaderSelected()
    }
}
