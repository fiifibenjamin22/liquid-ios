//
//  EmptyGoalCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol EmptyGoalDelegate: class {
    func gotoicPage()
    func gotocreateGoal()
    func changeAmount(amountFigure: Double)
}

class EmptyGoalCell: BaseCell {
    
    var delegate: EmptyGoalDelegate?
    
    let startToday: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "start_today")
        return imageView
    }()
    
    let card: CardView = {
        let card = CardView()
        card.backgroundColor = UIColor.white
        card.cornerRadius = 5
        card.shadowOffsetHeight = 2
        card.layer.borderWidth = 1
        card.layer.borderColor = UIColor.gray.cgColor
        card.translatesAutoresizingMaskIntoConstraints = false
        return card
    }()
    
    let howMuch: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.gray
        textView.text = "See how much you could’ve made"
        textView.textAlignment = .center
        textView.backgroundColor = .white
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let accBalance: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkGray
        textView.text = "ACCOUNT BALANCE"
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        return textView
    }()
    
    let amountLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.text = "¢ 1000"
        textView.textAlignment = .left
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 18)!)
        return textView
    }()
    
    lazy var amountSlider: UISlider = {
        let amtSlider = UISlider()
        amtSlider.minimumTrackTintColor = UIColor.hex(hex: Key.primaryHexCode)
        amtSlider.maximumTrackTintColor = UIColor.hex(hex: Key.primaryHexCode).withAlphaComponent(0.5)
        amtSlider.thumbTintColor = UIColor.hex(hex: Key.primaryHexCode)
        amtSlider.maximumValue = 50000
        amtSlider.minimumValue = 1000
        amtSlider.setValue(5000, animated: true)
        amtSlider.addTarget(self, action: #selector(changeValue(_:)), for: .valueChanged)
        return amtSlider
    }()
    
    let LiquidAvgTextView: UIView = {
        let textView = UIView()
        textView.backgroundColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let liquidAvgLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.text = "LIQUID AVG(16%)"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let liquidAmountLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.text = "GH¢ 1,600"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        return textView
    }()
    
    let liquidPerYearLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.white
        textView.text = "Interest per year"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return textView
    }()
    
    let TbilTextView: UIView = {
        let textView = UIView()
        textView.backgroundColor = UIColor.hex(hex: "#D8D8D8")
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    let TbilAvgLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "T-BILL RATE(12%)"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 16)!)
        return textView
    }()
    
    let TbilAmountLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "GH¢1,200"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 14)!)
        return textView
    }()
    
    let TbilPerYearLbl: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.text = "Interest per year"
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Light, size: 14)!)
        return textView
    }()
    
    let partnershipImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "partnership")
        return imageView
    }()
    
    lazy var icImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "ic")
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoIC)))
        return imageView
    }()
    
    lazy var startImageView: UIImageView = {
        let imageView = ViewControllerHelper.baseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "start")
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoCreateGoal)))
        return imageView
    }()
    
    var messageBody:NSAttributedString?
    
    override func setUpView() {
        super.setUpView()
        
        let mainAmount: Double = Double(amountSlider.value).rounded(toPlaces: 2)
        let LiquidInterestRate: CGFloat = CGFloat(0.16 * amountSlider.value)
        let amount = Double(LiquidInterestRate).rounded(toPlaces: 2)
        
        let am = Mics.convertDoubleToCurrency(amount: amount)
        let ma = Mics.convertDoubleToCurrency(amount: mainAmount)
        
        self.amountLbl.text = "\(ma)"
        self.liquidAmountLbl.text = "\(am)"
        
        //Tbil interest
        let tbilInterestRate: CGFloat = CGFloat(0.12 * amountSlider.value)
        let TbilAmount = Double(tbilInterestRate).rounded(toPlaces: 2)
        self.TbilAmountLbl.text = "GH¢\(TbilAmount)"
        
        addSubview(startToday)
        addSubview(card)
        addSubview(howMuch)
        card.addSubview(accBalance)
        card.addSubview(amountLbl)
        card.addSubview(amountSlider)
        card.addSubview(LiquidAvgTextView)
        
        LiquidAvgTextView.addSubview(liquidAvgLbl)
        LiquidAvgTextView.addSubview(liquidAmountLbl)
        LiquidAvgTextView.addSubview(liquidPerYearLbl)
        
        card.addSubview(TbilTextView)
        
        TbilTextView.addSubview(TbilAvgLbl)
        TbilTextView.addSubview(TbilAmountLbl)
        TbilTextView.addSubview(TbilPerYearLbl)
        
        addSubview(partnershipImageView)
        addSubview(icImageView)
        addSubview(startImageView)
        
        startToday.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.size.width / 2, heightConstant: 120)
        
        card.anchor(startToday.bottomAnchor, left: startToday.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 250)
        
        howMuch.topAnchor.constraint(equalTo: card.topAnchor, constant: -10).isActive = true
        howMuch.leftAnchor.constraint(equalTo: card.leftAnchor, constant: 16).isActive = true
        howMuch.rightAnchor.constraint(equalTo: card.rightAnchor, constant: -100).isActive = true
        howMuch.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        accBalance.anchor(howMuch.bottomAnchor, left: howMuch.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 60, leftConstant: 0, bottomConstant: 0, rightConstant: -16, widthConstant: 0, heightConstant: 20)
        
        amountLbl.anchor(accBalance.bottomAnchor, left: accBalance.leftAnchor, bottom: nil, right: accBalance.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 25)
        
        amountSlider.anchor(amountLbl.bottomAnchor, left: amountLbl.leftAnchor, bottom: nil, right: accBalance.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: 35)
        
        LiquidAvgTextView.anchor(amountSlider.bottomAnchor, left: amountSlider.leftAnchor, bottom: card.bottomAnchor, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 12, rightConstant: 0, widthConstant: 140, heightConstant: 0)
        LiquidAvgTextView.layer.cornerRadius = 10
        LiquidAvgTextView.layer.masksToBounds = true
        
        liquidAvgLbl.anchor(LiquidAvgTextView.topAnchor, left: LiquidAvgTextView.leftAnchor, bottom: nil, right: LiquidAvgTextView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        liquidAmountLbl.anchor(liquidAvgLbl.bottomAnchor, left: liquidPerYearLbl.leftAnchor, bottom: liquidPerYearLbl.topAnchor, right: liquidPerYearLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        liquidPerYearLbl.anchor(nil, left: LiquidAvgTextView.leftAnchor, bottom: LiquidAvgTextView.bottomAnchor, right: LiquidAvgTextView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        TbilTextView.anchor(LiquidAvgTextView.topAnchor, left: nil, bottom: LiquidAvgTextView.bottomAnchor, right: amountSlider.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 140, heightConstant: 0)
        TbilTextView.layer.cornerRadius = 10
        TbilTextView.layer.masksToBounds = true
        
        TbilAvgLbl.anchor(TbilTextView.topAnchor, left: TbilTextView.leftAnchor, bottom: nil, right: TbilTextView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        TbilAmountLbl.anchor(TbilAvgLbl.bottomAnchor, left: TbilPerYearLbl.leftAnchor, bottom: TbilPerYearLbl.topAnchor, right: TbilPerYearLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        TbilPerYearLbl.anchor(nil, left: TbilTextView.leftAnchor, bottom: TbilTextView.bottomAnchor, right: TbilTextView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        partnershipImageView.topAnchor.constraint(equalTo: card.bottomAnchor, constant: 8).isActive = true
        partnershipImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        partnershipImageView.widthAnchor.constraint(equalToConstant: 90).isActive = true
        partnershipImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        icImageView.anchor(partnershipImageView.bottomAnchor, left: card.leftAnchor, bottom: nil, right: card.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 25)
        
        startImageView.anchor(icImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 24, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 60)
    }
        
    @objc func changeValue(_ sender: UISlider) {
        print("value is" , Int(sender.value));
        
        let dynamicValue: CGFloat = CGFloat(sender.value)
        //liquid interest
        let LiquidInterestRate: CGFloat = 0.16 * dynamicValue
        let amount = Double(LiquidInterestRate).rounded(toPlaces: 2)
        let mainAmount = Double(sender.value)
        
        let am = Mics.convertDoubleToCurrency(amount: amount)
        let ma = Mics.convertDoubleToCurrency(amount: mainAmount)

        self.liquidAmountLbl.text = "\(am)"
        self.amountLbl.text = "\(ma)"
        
        //Tbil interest
        let tbilInterestRate: CGFloat = 0.12 * dynamicValue
        let TbilAmount = Double(tbilInterestRate).rounded(toPlaces: 2)
        let tba = Mics.convertDoubleToCurrency(amount: TbilAmount)
        self.TbilAmountLbl.text = "\(tba)"
    }
    
    @objc func gotoIC(){
        self.delegate?.gotoicPage()
    }
    
    @objc func gotoCreateGoal(){
        self.delegate?.gotocreateGoal()
    }
}
