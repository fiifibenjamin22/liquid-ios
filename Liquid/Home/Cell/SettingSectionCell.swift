//
//  SettingSectionCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 22/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class SettingSectionCell: BaseCell {
    
    var item: SettingItem? {
        didSet {
            guard let unwrapedItem = item else {return}
            self.messageTextView.text = unwrapedItem.title
        }
    }
    
    var labelItem: String? {
        didSet {
            guard let unwrapedItem = labelItem else {return}
            self.messageTextView.text = unwrapedItem
        }
    }
    
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        return textView
    }()
    
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.hex(hex: Key.viewBackgroundColor)
        
        addSubview(messageTextView)
        self.messageTextView.anchorWithConstantsToTop(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, topConstant: 8, leftConstant: 8, bottomConstant: -8, rightConstant: -8)
        
    }
}
