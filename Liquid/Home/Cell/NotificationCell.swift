//
//  NotificationCell.swift
//  Liquid
//
//  Created by Benjamin Acquah on 31/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class NotificationCell: BaseTableCell {
    
    var item: Notification?{
        didSet {
            guard let unwrapedItem = item else {return}
            self.titleTextView.text = unwrapedItem.type
            self.timeTextView.text = unwrapedItem.dateTime.asDate.timeAgoDisplay()
            self.messageTextView.text = unwrapedItem.message
        }
    }
    
    
    let titleTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textAlignment = .left
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Bold, size: 16)!)
        textView.contentInset = UIEdgeInsets(top: 5, left: 16, bottom: 5, right: 16)
        return textView
    }()
    
    let messageTextView: UILabel = {
        let textView = ViewControllerHelper.baseLabel()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.numberOfLines = 2
        textView.lineBreakMode = .byTruncatingTail
        return textView
    }()
    
    let timeTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkText
        textView.textAlignment = .left
        textView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 16)
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: Font.Roboto.Thin, size: 12)!)
        return textView
    }()
    
    let splitLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.hex(hex: Key.backgroundColor)
        return view
    }()
        
    override func setUpView() {
        super.setUpView()
        backgroundColor = UIColor.white
        
        addSubview(titleTextView)
        addSubview(messageTextView)
        addSubview(timeTextView)
        addSubview(splitLine)
        
        self.titleTextView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: -5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        self.messageTextView.anchor(titleTextView.bottomAnchor, left: titleTextView.leftAnchor, bottom: timeTextView.topAnchor, right: rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 0, rightConstant: 50)
        
        self.timeTextView.anchor(nil, left: messageTextView.leftAnchor, bottom: splitLine.topAnchor, right: messageTextView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 24)
        
        self.splitLine.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 1)
    }
}
