//
//  TBillsController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 19/07/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class TBillsController: UIViewController {
    
    let backImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "tbill-back-img")
        return img
    }()
    
    let contentImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "tbills-Content")
        return img
    }()
    
    lazy var btnImage: UIImageView = {
        let img = ViewControllerHelper.baseImageView()
        img.image = UIImage(named: "tbills-Button")
        img.contentMode = .scaleAspectFill
        img.isUserInteractionEnabled = true
        img.layer.cornerRadius = 10
        img.layer.masksToBounds = true
        img.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handlegetstarted)))
        return img
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationItem.title = "T-Bills"
        self.setupViews()
    }
    
    func setupViews(){
        
        self.view.addSubview(backImage)
        self.view.addSubview(contentImage)
        self.view.addSubview(btnImage)
        
        backImage.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        backImage.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 16).isActive = true
        backImage.widthAnchor.constraint(equalToConstant: self.view.frame.width - 20).isActive = true
        backImage.heightAnchor.constraint(equalToConstant: (self.view.frame.height / 2) - 100).isActive = true
        
        btnImage.anchor(nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 8, rightConstant: 16, widthConstant: 0, heightConstant: 50)
        
        contentImage.anchor(nil, left: self.view.leftAnchor, bottom: self.btnImage.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 16, bottomConstant: 50, rightConstant: 16, widthConstant: 0, heightConstant: 400)
    }
    
    @objc func handlegetstarted(){
        let vc = LegalInfoController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
