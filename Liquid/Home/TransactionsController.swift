//
//  TransactionsController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 05/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class TransactionsController: NavConfigController {
    
   var transactions = [
        Transaction
    ]()
    
    //Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
    //Transaction(title: "Buy airtime",imageName: "airtime", type: .AIRTIME),
    //Transaction(title: "Buy data", imageName: "data", type: .DATA),
    //Transaction(title: "Pay for utilities", imageName: "utilitise", type: .UTILITY),
    //Transaction(title: "MOVE MONEY", imageName: "NetworkMtn", type: .NONE),
    //Transaction(title: "Send money", imageName: "sendMoney", type: .SENDING),
    //Transaction(title: "Request money",type: .RECEIVING),
    //Transaction(title: "Withdraw cash",type: .WITHDRAWAL)
    
    let collectionId = "collectionId"
    let cellSection = "transactionCellHeader"
    let cellHeading = "cellHeading"
    var features = [String]()
    let user = User.getUser()

    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        setUpNavigationBar(title: "Transact")
                
        self.toggleSetting()
        self.setUpLayout()
    }
    
    func toggleSetting(){
        ApiService().togglebetweenFeatures(customer_id: user!.cutomerId) { (status, item, message) in

            let feat = item["features"].arrayValue
            self.features.removeAll()
            
            for f in feat {
                self.features.append("\(f)")
                print("feature array: ",self.features)
            }
            
            //self.transactions.removeAll()
            
            if self.features.contains("utilities") && self.features.contains("send_money") && self.features.contains("data"){
                //show both
                print("utilities : send_money : data: ",self.transactions.count)
                
                self.transactions = [
                     Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Buy airtime",imageName: "airtime", type: .AIRTIME),
                     Transaction(title: "Buy data", imageName: "data", type: .DATA),
                     Transaction(title: "Pay for utilities", imageName: "utilitise", type: .UTILITY),
                     Transaction(title: "MOVE MONEY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Send money", imageName: "sendMoney", type: .SENDING)
                 ]
                self.collectionView.reloadData()
                print("utilities : send_money : data: ",self.transactions.count)
                
            }else if self.features.contains("utilities") && self.features.contains("send_money"){
                print("utilities : send_money")
                self.transactions = [
                     Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Pay for utilities", imageName: "utilitise", type: .UTILITY),
                     Transaction(title: "MOVE MONEY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Send money", imageName: "sendMoney", type: .SENDING)
                 ]
                self.collectionView.reloadData()
                
            }else if self.features.contains("send_money") && self.features.contains("data"){
                print("send_money : data")
                self.transactions = [
                     Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Buy airtime",imageName: "airtime", type: .AIRTIME),
                     Transaction(title: "Buy data", imageName: "data", type: .DATA),
                     Transaction(title: "MOVE MONEY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Send money", imageName: "sendMoney", type: .SENDING)
                 ]
                self.collectionView.reloadData()
                
            }else if self.features.contains("utilities") && self.features.contains("data"){
                print("utilities : data")
                self.transactions = [
                     Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Buy airtime",imageName: "airtime", type: .AIRTIME),
                     Transaction(title: "Buy data", imageName: "data", type: .DATA),
                     Transaction(title: "Pay for utilities", imageName: "utilitise", type: .UTILITY)
                 ]
                self.collectionView.reloadData()
                
            }else if self.features.contains("utilities"){
                //hide send money | show utitlties
                print("utilities")
                self.transactions = [
                     Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Pay for utilities", imageName: "utilitise", type: .UTILITY)
                 ]
                self.collectionView.reloadData()
                
            }else if self.features.contains("data"){
                print("data")
                self.transactions = [
                     Transaction(title: "PAY/BUY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Buy airtime",imageName: "airtime", type: .AIRTIME),
                     Transaction(title: "Buy data", imageName: "data", type: .DATA)
                 ]
                self.collectionView.reloadData()
                
            }else if self.features.contains("send_money"){
                //hid utilities | show send money
                print("send_money")
                self.transactions = [
                     Transaction(title: "MOVE MONEY", imageName: "NetworkMtn", type: .NONE),
                     Transaction(title: "Send money", imageName: "sendMoney", type: .SENDING)
                 ]
                self.collectionView.reloadData()
            }else{
                print("none")
                //remove both
                self.transactions.removeAll()
            }
        }
    }
    
    func setUpLayout() {
       self.view.addSubview(collectionView)
        
       self.collectionView.anchorToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor)
       self.collectionView.register(TransactionCell.self, forCellWithReuseIdentifier: collectionId)
       self.collectionView.register(TransactionCellSection.self, forCellWithReuseIdentifier: cellSection)
       
        self.setNavItem()
    }
    
    func setNavItem()  {
        let image = UIImage(named: "NotificationBell")?.withRenderingMode(.alwaysOriginal)
        let menuNotification = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleNotification))
        
        let imageFaq = UIImage(named: "NotificationFAQ")?.withRenderingMode(.alwaysOriginal)
        let menuFaq = UIBarButtonItem(image: imageFaq, style: .plain, target: self, action: #selector(handleFAQ))
        navigationItem.rightBarButtonItems = [menuNotification,menuFaq]
        
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
    }
    
    
    @objc func handleNotification()  {
        let destination = NotificationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func handleFAQ()  {
        let destination = LQWebViewController()
        destination.url = AppConstants.faqUrl
        destination.title = "FAQs"
        self.navigationController?.pushViewController(destination, animated: true)
    }
}


extension TransactionsController: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = transactions[indexPath.row]
        if item.type == .NONE {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellSection, for: indexPath) as! TransactionCellSection
            cell.item = self.transactions[indexPath.row].title
            return cell
        }
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: collectionId, for: indexPath) as! TransactionCell
        cell.item = self.transactions[indexPath.row]
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = collectionView.frame.width
        let itemHeight = CGFloat(50)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.transactionClicked(transaction: self.transactions[indexPath.row])
    }
    
    //MARK:- clicked here
    func transactionClicked(transaction:Transaction) {
        if transaction.type == TransactionType.AIRTIME {
            let destination = AirtimeProviderController()
            self.navigationController?.pushFade(destination)
        } else if transaction.type == TransactionType.SENDING {
            let destination = MoneyRecievingWalletController()
            self.navigationController?.pushFade(destination)
        } else if transaction.type == TransactionType.RECEIVING {
            let destination = DestinationWalletController()
            self.navigationController?.pushFade(destination)
        } else if transaction.type == TransactionType.WITHDRAWAL {
            let destination = DestinationWalletController()
            self.navigationController?.pushFade(destination)
        } else if transaction.type == TransactionType.DATA {
            let destination = DataProviderController()
            self.navigationController?.pushFade(destination)
        } else if transaction.type == TransactionType.UTILITY {
            let destination = UtilityProviderController()
            self.navigationController?.pushFade(destination)
        }
    
    }
    
}

