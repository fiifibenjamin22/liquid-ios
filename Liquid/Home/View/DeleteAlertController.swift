//
//  DeleteAlertController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 04/11/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

protocol deleteAlertDelegate {
    func deleteAction(i: IndexPath)
    func cancelAction()
}

class DeleteAlertController: UIViewController {
    
    @IBOutlet weak var iconIMage: UIImageView!
    var delegate: deleteAlertDelegate?
    var index: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.iconIMage.image = UIImage(named: "recycle-bin")?.withRenderingMode(.alwaysTemplate)
        self.iconIMage.contentMode = .scaleAspectFit
        self.iconIMage.tintColor = .gray
    }
    
    @IBAction func deleteBtn(_ sender: Any) {
        self.delegate?.deleteAction(i: index!)
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.delegate?.cancelAction()
    }
    
}
