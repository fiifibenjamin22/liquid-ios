//
//  HomeFeedBackView.swift
//  Liquid
//
//  Created by Benjamin Acquah on 30/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import Cosmos

class HomeFeedBackView: NSObject {
    
    var delegate: HomeDelegate?
    var ratingValue = 5.0
    var ratingMessage = ""
    
    
    lazy var blackView: UIView = {
        let blackView = UIView()
        blackView.isUserInteractionEnabled = true
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return blackView
    }()
    
    lazy var contentView: UIView = {
        let blackView = UIView()
        blackView.layer.cornerRadius = 10
        blackView.dropShadow()
        blackView.backgroundColor = UIColor.white
        return blackView
    }()
    
    let messageTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.hex(hex: Key.primaryHexCode)
        textView.textAlignment = .center
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
        return textView
    }()
    
    let titleTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkGray
        textView.textAlignment = .center
        textView.text = "Rate"
        textView.font = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
        return textView
    }()
    
    let rateValueTextView: UITextView = {
        let textView = ViewControllerHelper.baseTextView()
        textView.textColor = UIColor.darkGray
        textView.textAlignment = .center
        textView.text = "Like It!"
        return textView
    }()
    
    
    //MARK:- UI elements setup
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.currentPageIndicatorTintColor = UIColor.hex(hex: Key.primaryHexCode)
        pc.pageIndicatorTintColor = UIColor.darkGray
        pc.hidesForSinglePage = true
        pc.transform = CGAffineTransform(scaleX: 2, y: 2)
        pc.numberOfPages = 2
        pc.translatesAutoresizingMaskIntoConstraints = false
        return pc
    }()
    
    let ratingView: CosmosView = {
        let cosmosView = CosmosView()
        cosmosView.rating = 0
        cosmosView.settings.updateOnTouch = true
        cosmosView.settings.fillMode = .full
        cosmosView.settings.starSize = 40
        cosmosView.settings.starMargin = 5
        cosmosView.settings.filledColor = UIColor.orange
        cosmosView.settings.emptyBorderColor = UIColor.darkGray
        cosmosView.settings.filledBorderColor = UIColor.orange
        cosmosView.translatesAutoresizingMaskIntoConstraints = false
        return cosmosView
    }()
   
    lazy var ratingMessageTextField: UITextField = {
        let color = UIColor.darkGray
        let textField = ViewControllerHelper.baseField()
        textField.keyboardType = UIKeyboardType.alphabet
        textField.textColor = UIColor.darkText
        textField.attributedPlaceholder =  NSAttributedString(string: "Tell us what you think",
                                                              attributes: [NSAttributedString.Key.foregroundColor: color])
        textField.setBottomBorder(color: UIColor.hex(hex: Key.primaryHexCode).cgColor)
        return textField
    }()
    
    lazy var nextButton: UIButton = {
        let button = ViewControllerHelper.plainButton()
        button.setTitle("Next", for: .normal)
        button.contentHorizontalAlignment = .center
        button.setTitleColor(UIColor.hex(hex: Key.primaryHexCode), for: .normal)
        button.addTarget(self, action: #selector(showInput), for: .touchUpInside)
        return button
    }()
    
    
    override init() {
        super.init()
        
        let result = NSMutableAttributedString()
        let user = User.getUser()!
        
        let header = "Hi, \(user.firstName)\n\n".formatAsAttributed(fontSize: 20, color: UIColor.darkGray)
        let boldFont = UIFontMetrics.default.scaledFont(for: UIFont(name: "Roboto-Bold", size: 20)!)
        let point = "15 transactions".formatAsAttributed(font: boldFont, color: UIColor.darkText)
        let messageStart = "You have successfully made ".formatAsAttributed(fontSize: 20, color: UIColor.darkGray)
        let messageFinal = ", please help us improve by rating and giving us feedback on your experience.".formatAsAttributed(fontSize: 20, color: UIColor.darkGray)
        result.append(header)
        result.append(messageStart)
        result.append(point)
        result.append(messageFinal)
        self.messageTextView.attributedText = result
        self.messageTextView.textAlignment = .center
        
        self.ratingView.didFinishTouchingCosmos = { rating in
           print("Rate \(rating)")
           var message = "Like It!"
            if rating < 2.0 {
                message = "Not the best"
            }
           self.rateValueTextView.text = message
           self.showNextAction()
        }
        
    }
    
    func showMenu()  {
        if let window = UIApplication.shared.keyWindow {
            let height = CGFloat(400)
            let y = window.frame.height - height
            window.addSubview(blackView)
            window.addSubview(contentView)
            contentView.addSubview(messageTextView)
            contentView.addSubview(titleTextView)
            contentView.addSubview(pageControl)
            contentView.addSubview(ratingView)
            contentView.addSubview(rateValueTextView)
            contentView.addSubview(nextButton)
            contentView.addSubview(ratingMessageTextField)
            
            self.blackView.frame = window.frame
            self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            self.messageTextView.anchorWithConstantsToTop(top: contentView.topAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.messageTextView.heightAnchor.constraint(equalToConstant: 180).isActive = true
            
            self.titleTextView.anchorWithConstantsToTop(top: messageTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 8, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.titleTextView.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            self.ratingView.anchorWithConstantsToTop(top: titleTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
            self.ratingView.heightAnchor.constraint(equalToConstant: 40).isActive = true
            self.ratingView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            
            //MARK:- hidden first
            self.ratingMessageTextField.anchorWithConstantsToTop(top: titleTextView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 4, leftConstant: 16, bottomConstant: 0, rightConstant: -16)
            self.ratingMessageTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
            self.ratingMessageTextField.isHidden = true
            
            
            self.rateValueTextView.anchorWithConstantsToTop(top: ratingView.bottomAnchor, left: contentView.leadingAnchor, bottom: nil, right: contentView.trailingAnchor, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
            self.rateValueTextView.heightAnchor.constraint(equalToConstant: 30).isActive = true
            
            self.pageControl.anchorWithConstantsToTop(top: rateValueTextView.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: -44, rightConstant: 0)
            self.pageControl.heightAnchor.constraint(equalToConstant: 20).isActive = true
            self.pageControl.widthAnchor.constraint(equalToConstant: 100).isActive = true
            self.pageControl.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
            
            self.nextButton.anchorWithConstantsToTop(top: rateValueTextView.bottomAnchor, left: nil, bottom: nil, right: contentView.trailingAnchor, topConstant: 16, leftConstant: 16, bottomConstant: -44, rightConstant: -16)
            self.nextButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
            self.nextButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
            self.nextButton.isHidden = true
            
            UIView.animate(withDuration: 0.5, animations: {
                self.blackView.alpha = 1
                self.contentView.frame = CGRect(x: 0, y: y, width: window.frame.width, height: height)
            })
        }
    }
    
    
    @objc func handleClose() {
        self.closePicker()
    }
    
    
    @objc func showInput() {
        let title = self.nextButton.currentTitle!
        if title == "Next" {
           self.ratingView.isHidden = true
           self.rateValueTextView.isHidden = true
           self.nextButton.setTitle("Finish", for: .normal)
           self.titleTextView.text = "Write a short review"
           self.pageControl.currentPage = 2
           self.ratingMessageTextField.isHidden = false
        }  else {
           self.ratingValue = self.ratingView.rating
           self.ratingMessage = self.ratingMessageTextField.text!
           self.closePicker()
        }
    }
    

    @objc func showNextAction() {
      self.nextButton.isHidden = false
    }
    
    @objc func handleDismiss(sender: UITapGestureRecognizer) {
        self.closePicker()
    }
    
    func closePicker() {
        UIView.animate(withDuration: 0.5, animations: {
            self.blackView.removeFromSuperview()
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.contentView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
                self.contentView.removeFromSuperview()
            }
        })
    }
}

