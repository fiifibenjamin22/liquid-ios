//
//  HomeController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 28/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import PopupDialog

class HomeController: NavConfigController,AlertConfirmDelegate {
    
    let cellId = "CellId"
    let emptyCellId = "emptyCellId"
    let headerId = "headerCellId"
    
    let user = User.getUser()!
    var delegate: HomeDelegate?
    let viewControllerHelper = ViewControllerHelper()
    var goals = [Goal]()
    var isGoalAvailable = false
    var numberOfCompletedGoals = [Goal]()
    private let refreshControl = UIRefreshControl()
    var popup: PopupDialog?
    var loadedPages = [String]()
    var nextUrl = ApiUrl().notifications()
    var page = 1
    var coach = Coach()
    
    lazy var collectionView: UICollectionView = {
        let collectionIn = ViewControllerHelper.baseVerticalCollectionView()
        collectionIn.dataSource = self
        collectionIn.delegate = self
        return collectionIn
    }()
    
    lazy var createGoalButton: UIButton = {
        let button = ViewControllerHelper.baseRoudImageButton()
        button.setImage(UIImage(named: "goals"), for: .normal)
        button.setTitle("  New Goal", for: .normal)
        button.addTarget(self, action: #selector(createNewGoal), for: .touchUpInside)
        button.semanticContentAttribute = .forceLeftToRight
        button.isHidden = true
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: 10, right: -20)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return button
    }()
    
    lazy var emptyView: EmptyGoalView = {
        let view = EmptyGoalView()
        view.delegate = self
        view.width = self.view.frame.size.width
        print(self.view.frame.size.width)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                        
        let navView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 48))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 48))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "SplashLogo")?.withRenderingMode(.alwaysTemplate)
        imageView.image = image
        imageView.tintColor = .white
        navView.addSubview(imageView)
        navigationItem.titleView = navView
        
        DispatchQueue.main.async {
            self.setUpGoals()
            self.getallNotifications()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if  UserDefaults.standard.bool(forKey: Key.createGoal) {
            UserDefaults.standard.set(false, forKey: Key.createGoal)
            setUpGoals()
        }
        self.getCoachData()
        self.getallNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.fundNewGoal(_:)), name: NSNotification.Name(rawValue: Key.createGoal), object: nil)
    }
    
    func setupUI(){
        
        self.view.addSubview(collectionView)
        self.view.addSubview(createGoalButton)
        
        self.collectionView.register(HomeHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        self.collectionView.register(GoalTimelineCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        
        self.createGoalButton.anchorWithConstantsToTop(top: nil, left: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -16, rightConstant: -16)
        self.createGoalButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.createGoalButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        //self.setUpGoals()
        self.checkAccountType()
        
        // Add pull to refresh
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshGoals(_:)), for: .valueChanged)
    }
    
    //Get Coach Data
    func getCoachData(){
        //self.viewControllerHelper.showActivityIndicator()
        ApiService().getcoachData { (status, data, message) in
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                self.coach = data!
                User.updateUserCoach(r: data!.rank, p: data!.points_to_next_rank)
                self.collectionView.reloadData()
            }else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    func setUpEmptyView()  {
       self.view.addSubview(emptyView)
       self.emptyView.anchorWithConstantsToTop(top: view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
        self.emptyView.bgView.contentSize = CGSize(width: self.view.frame.size.width, height: 700)
        self.createGoalButton.isHidden = true
        self.collectionView.removeFromSuperview()
    }
    
    func checkAccountType(){
        let user = User.getUser()!
        ApiService().checkAccountType(user_id: "\(user.cutomerId)") { (status, withMessage, item) in
            if status == ApiCallStatus.SUCCESS {
                user.setHasFullAccount(state: item["has_full_account"].boolValue)
                if Wallet.all().count < 1 {
                    let alertConfirm = AlertConfirmViewController(nibName: "AlertConfirmView", bundle: nil)
                    alertConfirm.delegate = self
                    self.popup = PopupDialog(viewController: alertConfirm)
                    self.present(self.popup!, animated: true, completion: nil)
                }
            }
        }
    }
    
    func setNavItem(badgeNumber: Int)  {
        
        let image = UIImage(named: "NotificationBell")?.withRenderingMode(.alwaysOriginal)
        let menuNotification: UIBarButtonItem?
        if badgeNumber == 0 {
            menuNotification = UIBarButtonItem(badge: "", title: image!, target: self, action: #selector(handleNotification))
        }else{
            menuNotification = UIBarButtonItem(badge: "\(badgeNumber)", title: image!, target: self, action: #selector(handleNotification))
        }
        
        let calculatorIcon = UIImage(named: "calculator")?.withRenderingMode(.alwaysOriginal)
        let menuCalc = UIBarButtonItem(badge: "", title: calculatorIcon!, target: self, action: #selector(handleCalc))
        
        let faqIcon = UIImage(named: "NotificationFAQ")?.withRenderingMode(.alwaysTemplate)
        let menuFaq = UIBarButtonItem(image: faqIcon, style: .plain, target: self, action: #selector(gotoFAQs))
        
        navigationItem.rightBarButtonItems = [menuNotification!,menuFaq,menuCalc]
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addParticipantListen(notification:)), name: NSNotification.Name(rawValue: "addParticipantListen"), object: nil)
    }
    
    @objc func gotoFAQs()  {
        let destination = LQWebViewController()
        destination.url = "\(AppConstants.faqUrl)#security"
        destination.title = "FAQs"
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getallNotifications(){
        ApiService().getNotifications(url: nextUrl, pageNumber: self.page, isMore: true) { (status, notifications,currentPage, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                var allNotes = [Notification]()
                allNotes.removeAll()
                for i in notifications! {
                    if i.isRead == false {
                        allNotes.append(i)
                    }
                }
                self.setNavItem(badgeNumber: allNotes.count)
            }else{
                print("FAILED")
            }
        }
    }
    
    @objc func handleCalc(){
        let destination = CalculatorController()
        destination.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func handleNotification()  {
        let destination = NotificationController()
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func setUpGoals()  {
        self.loadLocalData()
        if self.goals.isEmpty {
            self.createGoalButton.isHidden = true
            self.viewControllerHelper.showActivityIndicator()
        }
        self.createGoalButton.isHidden = false
        ApiService().goals { (status, goals, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
    }
    
    func loadLocalData() {
        self.goals.removeAll()
        var allGoals = Goal.all()
        allGoals.reverse()
        if allGoals.isEmpty {
            self.setUpEmptyView()
            UserDefaults.standard.set(false, forKey: "isGoalAvailable")
            print("isGoalAvailable: ",UserDefaults.standard.bool(forKey: "isGoalAvailable"))
            self.isGoalAvailable = false
        } else {
            self.emptyView.removeFromSuperview()
            self.setupUI()
            UserDefaults.standard.set(true, forKey: "isGoalAvailable")
            self.isGoalAvailable = true
            
            var totalDepositArray = [Double]()
            var totalWithdrawalsArray = [Double]()
            var totalBalanceArray = [Double]()
            
            allGoals.forEach { (goal) in
                if goal.status == "active" {
                    if goal.is_goal_owner {
                        totalDepositArray.append(goal.total_deposits)
                        totalWithdrawalsArray.append(goal.total_withdrawals)
                        totalBalanceArray.append(goal.balance)
                    }
                    self.goals.append(goal)
                }
                
                
                if goal.status == "completed" {
                    numberOfCompletedGoals.removeAll()
                    numberOfCompletedGoals.append(goal)
                }
            }
            
            let totalDeposte = gettotals(totals: totalDepositArray)
            let totalWithdrawals = gettotals(totals: totalWithdrawalsArray)
            let totalBalance = gettotals(totals: totalBalanceArray)
            let totalEarnings: Double = (totalBalance + totalWithdrawals) - totalDeposte

            User.updateUserTotalAmount(d: totalDeposte, w: totalWithdrawals, b: totalBalance, e: totalEarnings,c: numberOfCompletedGoals.count)
            self.collectionView.reloadData()
        }
    }
    
    func gettotals(totals: Array<Double>) -> Double {
        var counter = 0
        var amount: Double = 0
        while counter < totals.count {
            let newValue = totals[counter]
            amount += newValue
              counter += 1
        }
        return amount
    }
    
    @objc func fundNewGoal(_ notification: Notification){
        self.refreshCall()
    }
    
    @objc func addParticipantListen(notification: Notification){
        self.refreshCall()
    }
    
    func refreshCall(){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().goals { (status, goals, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
    }

    @objc func refreshGoals(_ sender: Any) {
        self.refreshControl.endRefreshing()

        self.viewControllerHelper.showActivityIndicator()
        ApiService().goals { (status, goals, withMessage) in
            self.viewControllerHelper.hideActivityIndicator()
            self.loadLocalData()
        }
    }
    
    func getRecuringSetting(theGoal: Goal?){
        self.viewControllerHelper.showActivityIndicator()
        ApiService().getRecurringDebitSettings(goalId: theGoal!.id) { (status, response, message) in

            print("get recurring data: ",response as Any)
            self.viewControllerHelper.hideActivityIndicator()
            if status == ApiCallStatus.SUCCESS {
                let destination = GoalDetailController()
                destination.goal = theGoal
                destination.isGoalOwner = theGoal!.is_goal_owner
                destination.goalImage = theGoal!.image
                self.navigationItem.title = ""
                print("get recurring data: ",response?.payment_method as Any)
                if response?.payment_method != "" {
                    destination.isLinked = true
                    destination.isAutoDebit = true
                }
                self.navigationController?.pushViewController(destination, animated: true)
            }else if status == ApiCallStatus.DETAIL {
                let destination = GoalDetailController()
                destination.goal = theGoal
                destination.isGoalOwner = theGoal!.is_goal_owner
                destination.goalImage = theGoal!.image
                self.navigationItem.title = ""
                if response?.payment_method != "" {
                    destination.isLinked = true
                    destination.isAutoDebit = true
                }
                self.navigationController?.pushViewController(destination, animated: true)
            }
            else{
                ViewControllerHelper.showPrompt(vc: self, message: message)
            }
        }
    }
    
    @objc func createNewGoal(){
        let destination = NewGoalController()
        destination.navigationItem.title = ""
        self.navigationController?.pushFade(destination)
    }
}

extension HomeController: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.goals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! GoalTimelineCell
        let item = self.goals[indexPath.row]
        cell.item = item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let goal = self.goals[indexPath.row]
        self.getRecuringSetting(theGoal: goal)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let HomeHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId, for: indexPath) as! HomeHeaderCell
        HomeHeader.delagate = self
        HomeHeader.card.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(isHeaderSelected)))
        return HomeHeader
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if numberOfCompletedGoals.count > 0 {
            let width: CGFloat = self.view.frame.size.width - 16.0
            let height:CGFloat = 170.0
            
            return CGSize(width: width, height: height)
        }
        
        let width: CGFloat = self.view.frame.size.width - 16.0
        let height:CGFloat = 100.0
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width: CGFloat = self.view.frame.size.width - 16.0
        let height:CGFloat = 200.0
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func proceed() {
        self.popup?.dismiss()
        let paymentAddVc = NewPaymentOptionController()
        let nav = UINavigationController(rootViewController: paymentAddVc)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }

    func dismiss() {
        self.popup?.dismiss()
    }
}

extension HomeController: segmentSwitchDelegate, EmptyGoalDelegate {
    
    func gotoicPage() {
        let destination = LQWebViewController()
        self.navigationItem.title = ""
        destination.url = "https://www.icassetmanagers.com/"
        destination.title = "IC Asset Managers"
        self.navigationController?.setViewControllers([(self.navigationController?.viewControllers.first)!, destination], animated: true)
    }
    
    func gotocreateGoal() {
        self.createNewGoal()
    }
    
    func changeAmount(amountFigure: Double) {

    }
    
    func isActiveOnly() {
        self.goals.removeAll()
        let allGoals = Goal.all()
        allGoals.forEach { (goal) in
            if goal.status == "active" {
                self.goals.append(goal)
            }
        }
        self.collectionView.reloadData()
    }
    
    func isCompletedOnly() {
        self.goals.removeAll()
        let allGoals = Goal.all()
        allGoals.forEach { (goal) in
            if goal.status == "completed" {
                self.goals.append(goal)
            }
        }
        self.collectionView.reloadData()
    }
    
    @objc func isHeaderSelected() {
        //print("header selected")
        //let cc = CoachController()
        //self.navigationController?.pushFade(cc)
    }
}
