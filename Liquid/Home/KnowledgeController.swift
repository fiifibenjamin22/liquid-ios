//
//  KnowledgeController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 23/10/2019.
//  Copyright © 2019 Liquid Ltd. All rights reserved.
//

import UIKit

class KnowledgeController: NavConfigController, UIWebViewDelegate {
    
    var url: String?
    let utilViewController = ViewControllerHelper()
    var nextUrl = ApiUrl().notifications()
    var page = 1

    lazy var webView: UIWebView = {
        let webview = UIWebView()
        webview.backgroundColor = UIColor.clear
        webview.translatesAutoresizingMaskIntoConstraints = false
        return webview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(webView)
        webView.delegate = self
        
        setUpNavigationBar(title: "Knowledge")
        
        self.getallNotifications()
                
        let frame = self.view.frame.width
        self.webView.anchorWithConstantsToTop(top: view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 16, leftConstant: 0, bottomConstant: -16, rightConstant: 0)
        self.webView.widthAnchor.constraint(equalToConstant: frame).isActive = true
        
        self.webView.loadRequest(URLRequest(url: URL(string: self.url!)!))
    }
    
    func setNavItem(badgeNumber: Int)  {
        
        let image = UIImage(named: "NotificationBell")?.withRenderingMode(.alwaysOriginal)
        let menuNotification: UIBarButtonItem?
        if badgeNumber == 0 {
            menuNotification = UIBarButtonItem(badge: "", title: image!, target: self, action: #selector(handleNotification))
        }else{
            menuNotification = UIBarButtonItem(badge: "\(badgeNumber)", title: image!, target: self, action: #selector(handleNotification))
        }
        
        let calculatorIcon = UIImage(named: "calculator")?.withRenderingMode(.alwaysOriginal)
        let menuCalc = UIBarButtonItem(badge: "", title: calculatorIcon!, target: self, action: #selector(handleCalc))
        
        let faqIcon = UIImage(named: "NotificationFAQ")?.withRenderingMode(.alwaysTemplate)
        let menuFaq = UIBarButtonItem(image: faqIcon, style: .plain, target: self, action: #selector(gotoFAQs))
        
        navigationItem.rightBarButtonItems = [menuNotification!,menuFaq,menuCalc]
        let barImage = UIImage(named: Images.barBg)
        self.navigationController?.setBackgroundImage(barImage!)        
    }
    
    @objc func gotoFAQs()  {
        let destination = LQWebViewController()
        destination.url = "\(AppConstants.faqUrl)#security"
        destination.title = "FAQs"
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func handleCalc(){
        let destination = CalculatorController()
        destination.navigationItem.title = ""
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getallNotifications(){
        ApiService().getNotifications(url: nextUrl, pageNumber: self.page, isMore: true) { (status, notifications,currentPage, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                var allNotes = [Notification]()
                allNotes.removeAll()
                for i in notifications! {
                    if i.isRead == false {
                        allNotes.append(i)
                    }
                }
                self.setNavItem(badgeNumber: allNotes.count)
            }else{
                print("FAILED")
            }
        }
    }

    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.utilViewController.showActivityIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.utilViewController.hideActivityIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.utilViewController.hideActivityIndicator()
    }

    @objc func handleNotification()  {
        let destination = NotificationController()
        self.navigationController?.pushViewController(destination, animated: true)
    }
}

