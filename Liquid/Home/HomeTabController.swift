//
//  HomeTabController.swift
//  Liquid
//
//  Created by Benjamin Acquah on 05/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import UIKit

class HomeTabController: UITabBarController {
    let service = ApiService()
    let service2 = ApiService()
    let user = User.getUser()
    var features = [String]()
    
    override func viewDidLoad() {
        self.view.backgroundColor = UIColor.white
        super.viewDidLoad()

        self.hidesBottomBarWhenPushed = true

        self.updateLocalStorage()

        //MARK:- ended notification
        NotificationCenter.default.addObserver(self, selector: #selector(receivedEndedNotification(notification:)), name: NSNotification.Name(Key.manageGoal), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(recieveTransactionNotifiction(notification:)), name: NSNotification.Name(Key.transaction), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveUpdateNotification(notification:)), name: NSNotification.Name("loadWallets"), object: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name("loadToggle"), object: nil)
        
        ApiService().togglebetweenFeatures(customer_id: user!.cutomerId) { (status, item, message) in

            let feat = item["features"].arrayValue
            self.features.removeAll()
            
            for f in feat {
                self.features.append("\(f)")
                print("feature array: ",self.features)
            }
            
            self.setUpTabs()
        }
    }
    
    @objc func receivedEndedNotification(notification: Notification){
        self.selectedIndex = 1
    }
    
    @objc func recieveTransactionNotifiction(notification: Notification){
        self.selectedIndex = 2
    }
    
    func setUpTabs()  {
        
        // Create home tab
        let homeController = HomeController()
        let homeBarItem = UITabBarItem(title: "Home", image: UIImage(named: "liquid"), tag: 0)
        homeController.tabBarItem = homeBarItem
        
        //Knowledge tab
        let knowledgeController = KnowledgeController()
        knowledgeController.url = "https://medium.com/liquid-financial-services"
        let knowledgeBarItem = UITabBarItem(title: "Knowledge", image: UIImage(named: "know"), tag: 1)
        knowledgeController.tabBarItem = knowledgeBarItem
        
         // Create goals tab
        let goalsController = GoalsController()
        let goalBarItem = UITabBarItem(title: "Set Goal", image: UIImage(named: "TabGoal"), tag: 2)
        goalsController.tabBarItem = goalBarItem
        
        // Create transaction tab
        let transactController = TransactionsController()
        let transactionBarItem = UITabBarItem(title: "Transact", image: UIImage(named: "TabTransaction"),tag: 3)
        transactController.tabBarItem = transactionBarItem
        
        // Create airtime
        let airtimeController = AirtimeProviderController()
        airtimeController.isNavigation = true
        let airtimeBarItem = UITabBarItem(title: "Airtime", image: UIImage(named: "TabAirtime"), tag: 4)
        airtimeController.tabBarItem = airtimeBarItem
        
        //let tbillController = TBillsController()
        //let tbillsItems = UITabBarItem(title: "T-Bills", image: UIImage(named: "baseline-loyalty-24px"), tag: 3)
        //tbillController.tabBarItem = tbillsItems
        
        
        // Create account tab
        let accountController = AccountController()
        let accountBarItem = UITabBarItem(title: "My Account", image: UIImage(named: "user"), tag: 5)
        accountController.tabBarItem = accountBarItem
        
        

        //test
        //self.features.removeAll { $0 == "transact" }
        //self.features.removeAll { $0 == "airtime" }
        self.features.removeAll { $0 == "goals" }
        
        print("feature array: ",self.features)
        
        if self.features.contains("transact") && self.features.contains("airtime") && self.features.contains("goals") {
            print("transact", "airtime", "goals")
            let controllers = [homeController,goalsController, transactController,airtimeController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }else if self.features.contains("transact") && self.features.contains("airtime"){
            print("transact", "airtime")
            let controllers = [homeController,transactController,airtimeController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }else if self.features.contains("airtime") && self.features.contains("goals"){
            print("airtime", "goals")
            let controllers = [homeController,goalsController,airtimeController,knowledgeController,accountController]
            self.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
        }else if self.features.contains("transact") && self.features.contains("goals"){
            print("transact", "goals")
            let controllers = [homeController,goalsController, transactController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }else if self.features.contains("transact"){
            print("transact")
            let controllers = [homeController,transactController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }else if self.features.contains("airtime"){
            print("airtime")
            let controllers = [homeController,transactController,airtimeController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }else if self.features.contains("goals"){
            print("goals")
            let controllers = [homeController,goalsController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }else{
            print("none")
            let controllers = [homeController,knowledgeController,accountController]
            self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        }
        
        //let controllers = [homeController,goalsController, transactController,airtimeController,accountController]
        //self.viewControllers  = controllers.map { UINavigationController(rootViewController: $0)}
        
    }
    
    @objc func receiveUpdateNotification(notification: Notification){
        self.updateLocalStorage()
    }
    
    func updateLocalStorage() {
        
        ApiService().allPaymentMethods { (status, wallets, withMessage) in
            if status == ApiCallStatus.SUCCESS {
                print("WALLETS \(wallets!)")
            }
        }
        
        ApiService().getReferralCode { (status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
              //  print("CODE DONE")
            }
        }
        
        ApiService().getMissingSteps { (missingSteps,status, withMessage) in
            if status == ApiCallStatus.SUCCESS {
               // print("MISSING DONE")
            }
        }
        
        service.getEditOverview(customerNumber: User.getUser()!.cutomerId) { (status) in
            if status == ApiCallStatus.SUCCESS {
              //  print("INFO DONE")
            }
        }
        
        ApiService().allDestinations { (status, goals, withMessage) in
            if status == ApiCallStatus.SUCCESS {
              //  print("DESTINATIONS DONE")
            }
        }
        
        service2.allRelationships { (status) in
            if status == ApiCallStatus.SUCCESS {
              //  print("RELATIONS DONE")
            }
        }
        
        ApiService().checkPasscodeStatus{ (status,withMessage) in
            if status == ApiCallStatus.SUCCESS {
             //   print("STAUS CHECK \(withMessage)")
                UserDefaults.standard.set(true, forKey: Key.passcodeEnabled)
            }
        }
        Mics.trackUsage(eventName: "VISITED", title: "VISITED-HOME")
    }

}
