//
//  ApiService.swift
//  Liquid
//
//  Created by Benjamin Acquah on 14/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Alamofire
import SwiftyJSON
import RealmSwift
import FirebaseInstanceID


class ApiService {
    
    private let interntConnectionStatus = "Request failed. Check your internet connection and try again."
    private let failureStatus = "Your request failed, try again later."
    private let defaultStatus = "Unable to get requested data, try again later."
    private let duplicateDevice = "err.DuplicateDevice"
    private let invalidCode = "err.InvalidCode"
    private let unknownNumber = "err.unknownPhoneNumber"
    private let invalidPassword = "err.IncorrectPassword"
    private let duplicateEmail = "err.duplicateEmail"
    private let clientNotFound = "err.CustomerRecordNotFound"
    private let incorrectConfirmationCode = "err.IncorrectConfirmationCode"
    private let customerDateOfBirthMismatch = "err.CustomerDateOfBirthMismatch"
    private let customerIdMismatch = "err.IdNumberIncorrect"
    private let paymentTypeNotFound = "err.paymentTypeNotFound"
    private let invalidCardProvided = "err.invalidCardProvided"
    
    
    private typealias GenericCompletionHandler = (_ status: ApiCallStatus,_ withMessage:String) -> Void
    private typealias FileUploadCompletionHandler = (_ status: ApiCallStatus,_ url:String?,_ withMessage:String) -> Void
    private typealias GoalCompletionHandler = (_ status: ApiCallStatus, _ goal: Goal?, _ withMessage:String) -> Void
    
    let realm = try! Realm()
    
    var sessionManager: SessionManager?
    
    //MARK:- set dynamic headers
    private func headerAuth() -> [String: String]  {
        var headers: HTTPHeaders = ["Content-type": "application/json", "Authorization": "Bearer "]
        if let user = User.getUser() {
             headers.updateValue("Bearer \(user.token)", forKey: "Authorization")
        }
        return headers
    }
    
    //MARK:- set dynamic headers
    private func noBearerHeaderAuth() -> [String: String]  {
        var headers: HTTPHeaders = ["Content-type": "application/json", "Authorization": ""]
        if let user = User.getUser() {
            headers.updateValue("\(user.token)", forKey: "Authorization")
        }
        return headers
    }
    
    //MARK:- set dynamic headers
    private func multiMediaHeaderAuth() -> [String: String]  {
        var headers: HTTPHeaders = ["Content-type": "multipart/form-data", "Authorization": "Bearer "]
        if let user = User.getUser() {
            headers.updateValue("Bearer \(user.token)", forKey: "Authorization")
        }
        return headers
    }
    
    //MARK:- call encrypted url
    func callEncrypted(url: String, method: HTTPMethod, parameters: [String: Any], headers: [String: String], completion: @escaping (DataResponse<Any>, JSON) -> ()){
        let paramsJson = parameters.convertToJson()
        let key = Obsufucator().reveal(key: Encrypter().getHMACKey())
        let digest = paramsJson.digest(.sha512, key: key).uppercased()
        let aes = SHA256()
        let encrypted = aes.encryptString(string: paramsJson)
        
        let p = ["a": encrypted, "b": digest]
        
        print("print p: ",p)
        
        print("Params \(parameters) Headers \(headers)")
        
        let pathToCert = Bundle.main.path(forResource: "liquid", ofType: "cer")
        let localCertificate: NSData = NSData(contentsOfFile: pathToCert!)!
        
        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: [SecCertificateCreateWithData(nil, localCertificate)!],
            validateCertificateChain: true,
            validateHost: true
        )
        
        let serverTrustPolicies = [
            "api.liquid.com.gh": serverTrustPolicy
        ]
        
        self.sessionManager = SessionManager()
        
        //self.sessionManager?.session.invalidateAndCancel()
        
        self.sessionManager!.request(url, method: .post, parameters: p, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    //let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    //print(datastring)
                    print(status)
                    
                    if let item = try? JSON(data: response.data!) {
                        let json = aes.decryptString(string: item["a"].stringValue)
                        
                        let responseJSON = JSON(parseJSON: json)
                        if item["b"].stringValue == json.digest(.sha512, key: key).uppercased() {
                            //print(responseJSON)
                            completion(response, responseJSON)
                        } else {
                            //print(responseJSON)
                            //print(item)
                            //print(json.digest(.sha512, key: key).uppercased())
                            completion(response, JSON.null)
                        }
                    } else {
                        print("Logging out: ",response.response?.statusCode as Any)
                        // Logout user: To do
                        //print("Logging out: ",response)
                        print(completion(response, JSON.null))
                        completion(response, JSON.null)
                    }
                } else {
                    completion(response, JSON.null)
                }
        }
    }
    
    //MARK:- reset password
    func resetPassword(password: String, completion: @escaping (ApiCallStatus, String) -> ())  {
        let user = User.getUser()!
        let url = "\(ApiUrl().resetPassword())"
        let params = ["customer_id": user.cutomerId, "new_password": password] as [String : Any]
        print("PASSWORD \(url) \(params) \(headerAuth())")
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    if let error = item["error"].string {
                        let message = error == self.incorrectConfirmationCode ? "Invalid confirmation code" : "Password reset failed"
                        completion(.DETAIL, message)
                    } else {
                        completion(.SUCCESS, "Password reset successfully")
                    }
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                case 401...499:
                    completion(.DETAIL, self.failureStatus)
                default:
                    completion(.FAILED, self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- init forgor password
    func initForgotPassword(number:String,completion: @escaping (ApiCallStatus,String) -> ()) {
        let url = "\(ApiUrl().initPasswordReset())"
        let params = ["phone_number": number]
        print("INFO \(url) \(params)")
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    if let error = item["error"].string {
                        var message = "Password reset failed."
                        if error == self.customerDateOfBirthMismatch {
                            message = "Incorrect date of birth provided."
                        }
                        if error == self.customerIdMismatch {
                            message = "Incorrect ID information provided."
                        }
                        completion(.DETAIL, message)
                    } else {
                        let customerId = item["customer_id"].intValue
                        _ = item["confirmation_code"].stringValue
                        completion(.SUCCESS,"\(customerId)")
                    }
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                case 401...499:
                    completion(.DETAIL, self.failureStatus)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- init forgotten password
    func confirmForgottenCode(code: String,completion: @escaping (ApiCallStatus, String) -> ()) {
        let user = User.getUser()!
        let url = "\(ApiUrl().confirmForgottenPasswordPasscode())"
        let params = ["confirmation_code": code, "customer_id": user.cutomerId] as [String : Any]
        print("INFO \(url) \(params)")
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    if item["error"].string != nil {
                        let message = "Incorrect code provided."
                        completion(.DETAIL, message)
                    } else {
                        completion(.SUCCESS, "")
                    }
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                case 401...499:
                    completion(.DETAIL, self.failureStatus)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    //MARK:- trigger password reset
    func triggerReset(number:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().triggerPassword())"
        let params = ["phone_number":number]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- change password
    func changePassword(oldPassword: String, newPassword: String, completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().changePassword())"
        let params = ["old_password": oldPassword, "new_password": newPassword, "user_id": User.getUser()!.id] as [String : Any]
        print("CHANGE PASSWORD: \(url) \(params) \(headerAuth())")
        
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    let value = item["return_value"].boolValue
                    if value {
                        completion(.SUCCESS, "Password changed successfully")
                    } else {
                        completion(.DETAIL, "Failed to change password. Try later")
                    }
                case 301...399:
                    completion(.DETAIL, "Completed Successfully")
                default:
                    completion(.FAILED, self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- trigger email change
    func triggerEmailChange(email:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().triggerEmail())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"email":email,"changed": false] as [String : Any]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- save contact
    func saveContact(name:String,identifierType:String,identifier:String,network:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().saveContact())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"name":name,"identifier_type": identifierType,"identifier":identifier,"network":network] as [String : Any]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- remove contact
    func removeContact(contactId: Int, completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().removeContact())"
        let params = ["contact_id":contactId]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    

    //MARK:- change email
    func verifyEmailChange(pin:String, completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().changeEmail())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"pin":Int(pin)!] as [String : Any]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    func checkEmail(email: String, completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().checkEmail())"
        let params = ["email":email] as [String : Any]
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    if item["error"].stringValue == "" {
                        completion(.SUCCESS, "Completed Successfully")
                    } else {
                        let error = item["error"].stringValue
                        var errorMessage = "An unknown error has occured"
                        if error == "emailAlreadyExists" {
                            errorMessage = "The email address you have provided is already in use"
                        }
                        completion(.DETAIL, errorMessage)
                    }
                case 301...399:
                    completion(.DETAIL, self.failureStatus)
                case 401:
                    completion(.DETAIL, self.failureStatus)
                case 402...499:
                    completion(.DETAIL, self.failureStatus)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- accept terms
    func acceptTerms(completion: @escaping (ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId] as [String : Any]
        let url =  "\(ApiUrl().acceptTerms())"
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- send invite code
    func sendInviteCode(inviteCode:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"invite_code":inviteCode] as [String : Any]
        let url =  "\(ApiUrl().saveInviteCode())"
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- logging out user
    func logOut(completion: @escaping (ApiCallStatus,String) -> ())  {
       let url = "\(ApiUrl().logOut())"
       let user = User.getUser()!
       let params = ["customer_id":user.cutomerId]
       self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- change number step one
    func changeNumberStepOne(newNumber:String, password:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().changeNumberStepOne())"
        let user = User.getUser()!
        let params = ["old_phone_number":user.phone,"new_phone_number":newNumber,"password":password]
        
        print("chnage phone number url: \(url)")
        print("chnage phone number param: \(params)")
        
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- change number step two
    func changeNumberStepTwo(newNumber:String,confirmationCode:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().changeNumberStepTwo())"
        let params = ["confirmation_code":confirmationCode,"new_phone_number":newNumber]
        //self.basicGenericCall(url: url, params: params, completion: completion)
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                //print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                
                completion(.FAILED, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, "\(item)")
                    }  else {
                        //completion(.DETAIL, "An error has occured.")
                        
                        switch error {
                            case "err.InvalidConfirmationCode":
                                completion(.DETAIL, "The invitation code you have entered is invalid.")
                            default:
                                completion(.DETAIL, "try again")
                        }
                    }
                    
                case 301...499:
                    
                    let detail = item["error"].stringValue
                    var message = "An error has occured processing your request."
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(.DETAIL, message)
                default:
                    completion(.FAILED, self.failureStatus)
                }
            }
        }

    }
    
    //MARK:- register device
    func registerDevice(completion: @escaping (ApiCallStatus,String) -> ())  {
        var imei = ""
        var systemFingerprint = ""
        let version  = ProcessInfo().operatingSystemVersion.majorVersion
        var appVersion = "1.0"
        var gcmToken = ""
        if let device = UIDevice.current.identifierForVendor {
             imei = device.uuidString
             systemFingerprint = imei
        }
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        if let refreshedToken = InstanceID.instanceID().token() {
             gcmToken = refreshedToken
        }
        let params = ["channel":"iOS","operating_system":"iOS \(version)","app_version":"\(appVersion)","system_fingerprint":systemFingerprint,"imei":imei,"gcm_token":gcmToken]
        let url =  "\(ApiUrl().registerDevice())"
        print("URL \(url) \(params)")
        
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: [:], completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    
                    let deviceId = item["device_id"].intValue
                    DeviceInfo.delete()
                    let deviceInfo = DeviceInfo()
                    deviceInfo.appVersion = appVersion
                    deviceInfo.deviceId = deviceId
                    deviceInfo.uuid = imei
                    deviceInfo.appVersion = appVersion
                    deviceInfo.osVersion = "\(version)"
                    DeviceInfo.save(data: deviceInfo)
                    completion(.SUCCESS, "Device registered successfully")
                    
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    
    //MARK:- checking user phone number
    func checkNumber(number:String, justCheck: Bool ,completion: @escaping (ApiCallStatus, String, DataResponse<Any>?) -> ())  {
        let params = ["phone_number":number]
        let url =  "\(ApiUrl().checkNumber())"
        if !Mics.isInternetAvailable() {
            completion(.FAILED,self.interntConnectionStatus, nil)
            return
        }
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: [:], completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    // Clear users
                    if !justCheck {
                        User.delete()
                    }

                    print("DATA CHECK NUMBER \(item)")
                    let status = item["status"].stringValue
                    if status == UserStatus.ACTIVE.rawValue {
                        if justCheck {
                            print("Probably changing number")
                            completion(.SUCCESS, UserStatus.ACTIVE.rawValue, response)
                        } else {
                            print("create new account")
                            let user = User()
                            user.id = item["user_id"].intValue
                            user.cutomerId = item["customer_id"].intValue
                            //data operation
                            User.updateUser(data: user)
                            completion(.SUCCESS, UserStatus.ACTIVE.rawValue, response)
                        }
                    } else if item["status"] == "no_password" {
                        let user = User()
                        user.id = item["user_id"].intValue
                        User.updateUser(data: user)

                        completion(.WARNING, status, response)
                    } else {
                        completion(.WARNING, status, response)
                    }
                case 301...399:

                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail, nil)
                default:
                    completion(.FAILED,self.failureStatus, nil)
                }
            }
        })
    }
    
    //MARK:- check accountType
    func checkAccountType(user_id: String, completion: @escaping (ApiCallStatus, String, JSON) -> ())  {
        let params = ["customer_id": Int(user_id)]
        let url =  "\(ApiUrl().checkAccountType())"
        print(url)
        print(params)
        if !Mics.isInternetAvailable() {
            completion(.FAILED,self.interntConnectionStatus, JSON.null)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params as [String : Any], headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA CHECK ACCOUNT TYPE \(item)")
                        completion(.SUCCESS, "success", item)
                        
                    case 301...399:
                        
                        let detail = item["detail"].stringValue
                        completion(.DETAIL, detail, JSON.null)
                    default:
                        completion(.FAILED, self.failureStatus, JSON.null)
                    }
                }
        })
    }
    
    //MARK:- create customer
    func createCustomer(firstName:String,lastName:String,otherNames:String,email:String,DOB:String,password:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["user_id":user.id,"first_name":firstName,"last_name":lastName,"other_names":otherNames,"email":email,"birth_date":DOB,"password":password] as [String : Any]
        let url =  "\(ApiUrl().createCustomer())"
        print("URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: [:], completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    print("DATA \(item)")
                    if let error = item["error"].string {
                        if error == self.duplicateEmail {
                            completion(.DETAIL,"The email you are trying to use already exists.")
                        }  else {
                            completion(.DETAIL,"Unable to create account")
                        }
                    } else {
                        let customerId = item["customer_id"].intValue
                        User.updateUser(customer:customerId,firstName: firstName, lastName: lastName, otherName: otherNames, email: email, DOB: DOB)
                        completion(.SUCCESS,"Customer created successfully")
                    }
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- save residence
    func saveResidence(latitude:Double,longitude:Double,streetAddress:String,city:String,country:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"latitude":latitude,"longitude":longitude,"street_address":streetAddress,"digital_address":"",
                      "city":city,"country":country] as [String : Any]
        let url =  "\(ApiUrl().saveUserAddress())"
        print("URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    
                    print("DATA \(item)")
                    User.updateUser(customerId: user.cutomerId, lat: latitude, lon: longitude, address: streetAddress, digAddress: "",city:city,country:country)
                    completion(.SUCCESS,"Saved residence successfully")
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                case 400...499:
                    completion(.DETAIL, self.failureStatus)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- register phone number
    func registerPhone(number:String,completion: @escaping (ApiCallStatus, String) -> ())  {
        var deviceId: Int = 123456789
        if let device = DeviceInfo.getDevice() {
            deviceId = device.deviceId
        }
        
        let params = ["phone_number":number,"device_id": deviceId ] as [String : Any]
        let url =  "\(ApiUrl().registerPhone())"
        print("URL \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED,self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    print("REGISTER \(item)")
                    let userId = item["user_id"].intValue
                    if userId > 0 {
                        //saving user
                        if let user = User.getUser() {
                            try! self.realm.write({
                                user.phone = number
                                user.accountStatus = UserStatus.NOT_VERIFIED.rawValue
                            })
                            
                            User.save(data: user)
                        } else {
                            let user = User()
                            user.id = userId
                            user.phone = number
                            user.accountStatus = UserStatus.NOT_VERIFIED.rawValue
                            User.save(data: user)
                        }
                        
                        //saving here
                        completion(.SUCCESS,"Confirmation SMS sent to: \(number)")
                    } else {
                        completion(.WARNING,"Failed, unable to send SMS.")
                    }
                case 301...499:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL, detail)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- get id types
    func getIdTypes(completion: @escaping (ApiCallStatus,[LiquidIdType]?,String) -> ())  {
        let url =  "\(ApiUrl().documentTypes())"
        let params = ["nothing":""]
        print("URL \(url)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if response.error != nil {
                completion(.FAILED,nil,self.interntConnectionStatus)
                return
            }
            if let status = response.response?.statusCode {
                print("Status IDS \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    LiquidIdType.delete()
                    var ids = [LiquidIdType]()
                    for itemIn in item["id_types"].enumerated() {
                        let liquidIdType = LiquidIdType()
                        liquidIdType.id = itemIn.element.1["id"].intValue
                        liquidIdType.name = itemIn.element.1["name"].stringValue
                        LiquidIdType.save(data: liquidIdType)
                        ids.append(liquidIdType)
                    }
                    completion(.SUCCESS,ids,"Data Retrieved Succussfully")
                case 301...499:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL,nil, detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        })
    }
    
    
    //MARK:- goal category
    func goalCategories(completion: @escaping (ApiCallStatus,[GoalCategory]?,String) -> ())  {
        let url =  "\(ApiUrl().goalCategory())"
        let params = ["liquid":""]
        print("URL \(url)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        GoalCategory.delete()
                        var categories = [GoalCategory]()
                        for itemIn in item["categories"].enumerated() {
                            let category = self.parseGoalCategory(item: itemIn.element.1)
                            GoalCategory.save(data: category)
                            categories.append(category)
                        }
                        completion(.SUCCESS,categories,"Data Retrieved Succussfully")
                    case 301...499:
                        
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil, detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- all contacts
    func allContacts(completion: @escaping (ApiCallStatus,[FavouriteContact]?,String) -> ())  {
        let url =  "\(ApiUrl().contacts())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"identifier_type":"phone","limit":100,"page_number": 1] as [String : Any]
        print("CONTACT \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...300:
                        //parse success
                        //FavouriteContact.delete()
                        print("CONTACT \(item)")
                        var contacts = [FavouriteContact]()
                        //data contact
                        for itemIn in item["data"].enumerated() {
                            let item = self.parseContact(item: itemIn.element.1)
                            FavouriteContact.save(data: item)
                            contacts.append(item)
                        }
                        //artime contact
                        for itemIn in item["airtime"].enumerated() {
                            let item = self.parseContact(item: itemIn.element.1)
                            FavouriteContact.save(data: item)
                            contacts.append(item)
                        }
                        //momo contact section
                        for itemIn in item["momo"].enumerated() {
                            let item = self.parseContact(item: itemIn.element.1)
                            FavouriteContact.save(data: item)
                            contacts.append(item)
                        }
                        
                        //utilities contact section
                        for itemIn in item["utilities"].enumerated() {
                            let item = self.parseContact(item: itemIn.element.1)
                            FavouriteContact.save(data: item)
                            contacts.append(item)
                        }
                        completion(.SUCCESS,contacts,"Data Retrieved Succussfully")
                    case 301...499:
                        
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil, detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- all goal destinations
    func allDestinations(completion: @escaping (ApiCallStatus,[GoalDestination]?,String) -> ())  {
        let url =  "\(ApiUrl().listWithdrawalDestination())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId]
        print("URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        GoalDestination.delete()
                        print("ITEM \(item)")
                        var destinations = [GoalDestination]()
                        for itemIn in item["destinations"].enumerated() {
                            let itemParsed = self.parseGoalDestination(item: itemIn.element.1)
                            GoalDestination.save(data: itemParsed)
                            destinations.append(itemParsed)
                        }
                        completion(.SUCCESS, destinations,"Data Retrieved Succussfully")
                    case 301...499:
                        //
                       // let detail = item["detail"].stringValue
                        completion(.DETAIL,nil, "Failed to get destination data")
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- list payment methods
    func allPaymentMethods(completion: @escaping (ApiCallStatus,[Wallet]?,String) -> ())  {
        let url =  "\(ApiUrl().listPaymentOptions())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"page_number":1,"limit":50]
        print("URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        Wallet.delete()
                        print("ALL PAYMENT METHOD ITEMS \(item)")
                        var wallets = [Wallet]()
                        for itemIn in item["details"].enumerated() {
                            let itemParsed = self.parseWallet(item: itemIn.element.1)
                            Wallet.save(data: itemParsed)
                            wallets.append(itemParsed)
                        }
                        completion(.SUCCESS,wallets,"Data Retrieved Succussfully")
                    case 301...499:
                        
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil, detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- remove wallet
    func removePaymentMethod(walletId:Int,paymentType:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url =  "\(ApiUrl().removeWallet())"
        let params = ["payment_wallet_id":walletId,"payment_type": paymentType] as [String : Any]
        print("URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("ITEM \(item)")
                        Wallet.deleteWallet(walletId: walletId)
                        completion(.SUCCESS, "Wallet deleted successfuly")
                    case 301...499:
                        print("ITEM \(item)")
                        let detail = item["error"].stringValue
                        switch detail {
                        case "err.paymentWalletUsedForRecurringDebits":
                            completion(.DETAIL,"Payment wallet is currently used for recurring debits")
                        default:
                            completion(.DETAIL,detail)
                        }
                        
                    default:
                        completion(.FAILED,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- all goals
    func goals(completion: @escaping (ApiCallStatus,[Goal]?,String) -> ())  {
        let url =  "\(ApiUrl().goals())"
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId]
        print("URL \(url)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("ITEM \(item)")
                        Goal.delete()
                        var goals = [Goal]()
                        for itemIn in item["goals"].enumerated() {
                            let goalIn = self.parseGoal(item: itemIn.element.1)
                            Goal.save(data: goalIn)
                            goals.append(goalIn)
                        }
                        completion(.SUCCESS,goals,"Data Retrieved Succussfully")
                    case 301...499:
                        
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil, detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get missing steps
    func getMissingSteps(completion: @escaping (MissingSteps?,ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId,"include_skipped_steps":true] as [String : Any]
        let url =  "\(ApiUrl().getAccoutMissingSteps())"
        print("MISSING URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(nil,.FAILED,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        print("MISSING STEPS \(item)")
                        
                        var hasAcceptedTermsOfAgreement = false
                        var hasSelfie = false
                        var hasAddress = false
                        var hasExtraKyc = false
                        var hasIdDocument = false
                        
                        if (item["has_accepted_terms_of_agreement"].intValue == 1){
                            hasAcceptedTermsOfAgreement = true
                        }
                        
                        if (item["has_selfie"].intValue == 1){
                            hasSelfie = true
                        }
                        
                        if (item["has_address"].intValue == 1){
                            hasAddress = true
                        }
                        
                        if (item["has_extra_kyc"].intValue == 1){
                            hasExtraKyc = true
                        }
                        
                        if (item["has_id_document"].intValue == 1){
                            hasIdDocument = true
                        }
                        
                        print("MISSING STEPS: ", hasAcceptedTermsOfAgreement)
                        print("MISSING STEPS: ", hasAddress)
                        print("MISSING STEPS: ", hasExtraKyc)
                        print("MISSING STEPS: ", hasIdDocument)
        
                        let missingSteps = MissingSteps(hasIdDocument: hasIdDocument,hasAcceptedTermsOfAgreement: hasAcceptedTermsOfAgreement,hasAddress: hasAddress,hasSelfie: hasSelfie,hasExtraKyc: hasExtraKyc)
                        User.saveMissingSteps(missingSteps: missingSteps)
                        completion(missingSteps,.SUCCESS,"Data retrived successfully")
                    case 301...499:
                        
                        let detail = item["detail"].stringValue
                        completion(nil,.DETAIL, detail)
                    default:
                        completion(nil,.FAILED,self.failureStatus)
                    }
                }
        })
    }
    
    
    //MARK:- pause goal
    func pauseGoal(goal:Goal,completion: @escaping (ApiCallStatus,Goal?,String) -> ())  {
        let url =  "\(ApiUrl().terminateGoal())"
        let params = ["goal_id":goal.id] as [String : Any]
        print("URL \(url) \(params)")
        self.basicGoalCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- get goal
    func getGoal(goal:Goal,completion: @escaping (ApiCallStatus,Goal?,String) -> ())  {
        let url =  "\(ApiUrl().getAGoal())"
        let params = ["goal_id":goal.id] as [String : Any]
        print("URL \(url) \(params)")
        self.basicGoalCall(url: url, params: params, completion: completion)
    }
    
    //MARK: - create new goal destination
    func newGoalDestinationCall(item: WithdrawalDestination, completion: @escaping (ApiCallStatus,Int?,String) -> ()){
        let url =  "\(ApiUrl().goalWithdrawalDestination())"
        let params = ["customer_id": item.customerId,"issuer":item.issuer,"identifier":item.identifier,"type":item.type, "name": item.name] as [String : Any]
        
        print("URL \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED,nil,self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, itemIn) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        let id = itemIn["id"].intValue
                        print("DATA \(item)")
                        let goalDestination = GoalDestination()
                        goalDestination.id = id
                        goalDestination.type = item.type
                        goalDestination.issuer = item.issuer
                        goalDestination.identifier = item.identifier
                        GoalDestination.save(data: goalDestination)
                        completion(.SUCCESS,id,"Completed Successfully")
                    case 301...400:
                        let detail = "Unable to save goal withdrawal destination."
                        completion(.DETAIL, nil, detail)
                    case 401...499:
                        completion(.DETAIL,nil,self.failureStatus)
                    default:
                        completion(.FAILED, nil, "Failed to add the payout option. Please check that you don't already have a payout option with the same number.")
                    }
                }
        })
    }
    
    func getGoalWithdrawalDestinationBanks(completion: @escaping (ApiCallStatus,Int?,String) -> ()){
        let url =  "\(ApiUrl().goalWithdrawalDestinationBanks())"
        let params = [String:Any]()
        
        print("URL \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED, nil, self.interntConnectionStatus)
            return
        }
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    
                    switch(status){
                    case 200...300:
                        //parse success
                        print(item)
                        
                        for bank in item["banks"].arrayValue {
                            let b = Bank()
                            b.name = bank["name"].stringValue
                            b.id = bank["id"].intValue
                            
                            Bank.updateBank(data: b)
                        }
                        
                        completion(.SUCCESS, 0, "Completed Successfully")
                    case 301...400:
                        let detail = "Unable to get destination banks"
                        completion(.DETAIL, nil, detail)
                    case 401...499:
                        completion(.DETAIL, nil, self.failureStatus)
                    default:
                        completion(.FAILED, nil, "Uknown error occured.")
                    }
                }
        })
    }
    
    //MARK:- update goal assist info
    func updateGoalAssist(goalAssist:GoalAssist,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url =  "\(ApiUrl().newGoalAssist())"
        let parameters = ["goal_id":goalAssist.goalId,"period":goalAssist.period,"amount":goalAssist.amount,"currency":"GHS"] as [String : Any]
        self.basicGenericCall(url: url, params: parameters, completion: completion)
    }
    
    //MARK: - get the goal assist information
    func getGoalAssistInfo(goalId:Int, completion: @escaping (ApiCallStatus,GoalAssist?,String) -> ()){
        let url =  "\(ApiUrl().goalAssist())"
        let params = ["goal_id": goalId] as [String : Any]
        print("URL \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED,nil,self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let error  = item["error"].stringValue
                        if !error.isEmpty {
                            completion(.DETAIL,nil,"GoalAssist settings not found.")
                            return
                        }
                        let goalId = item["goal_id"].intValue
                        let amount = item["amount"].doubleValue
                        let period = item["period"].stringValue
                        let currency = item["currency"].stringValue
                        let goalAssist = GoalAssist(goalId: goalId, amount: amount, period: period, currency: currency)
                        completion(.SUCCESS,goalAssist,"Completed Successfully")
                    case 301...399:
                        let detail = "Failed to get goal assist information"
                        completion(.DETAIL,nil,detail)
                    case 401...499:
                        completion(.DETAIL,nil,self.failureStatus)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- create goal
    func newGoal(goal:Goal,image:UIImage?,completion: @escaping (ApiCallStatus,String?,String) -> ())  {
        let url =  "\(ApiUrl().newGoal())"
        let user = User.getUser()!
        let partial = goal.assistModeEnabled ? 1 : 0
        
        let nextOfKin = ["email":goal.nextKinEmail,"phone":goal.nextKinContact,"name":goal.nextOfKinName,"relationship":goal.nextOfKinrelationship]
       
        let paramRisKyc = ["investment_knowledge":goal.investmentKnowledge,"risk_tolerance":goal.riskLevel,"next_of_kin":nextOfKin].prettyPrintedJSON
        
        let goalAssistParams = ["currency":goal.currency,"period":goal.goalAssistPeriod,"amount": goal.goalAssistAmount].prettyPrintedJSON
      
        var parameters  =  ["name":goal.name,"category_id":goal.categoryId,"customer_id":user.cutomerId,"target_amount":goal.targetAmount,"due_date":goal.dueDate,"partial_withdrawal_enabled":partial,
                          "ccy":"GHS",
                          "initial_deposit":goal.initialAmountDeposited,
                          "goal_kyc": paramRisKyc ?? "",
                          "goal_assist_enabled": goal.assistModeEnabled,
                          "goal_withdrawal_destination": goal.withdrawalDestinationId] as [String : Any]
        
        if goal.assistModeEnabled {
            parameters["goal_assist"] = goalAssistParams ?? ""
        }
        
        self.baseDocUploadGoal(type: "GOAL", url: url, image: image, fileUploadKey: "cover_image", parameters: parameters as Dictionary<String, Any>, completion: completion)
    }
    
    
    //MARK:- edit goal
    func editGoal(goal:Goal,image:UIImage?,completion: @escaping (ApiCallStatus,String?,String) -> ())  {
        let url =  "\(ApiUrl().editGoal())"
        let parameters = ["goal_assist_enabled": goal.assistModeEnabled, "goal_id":goal.id, "name":goal.name, "target_amount":goal.targetAmount, "due_date":goal.dueDate] as [String : Any]
        self.baseDocUploadGoal(type: "GOAL", url: url, image: image,fileUploadKey: "cover_image",parameters: parameters, completion: completion)
    }
    
    //MARK:- update goal next of kin
    func updateGoalNextOfKin(name:String,mobile:String,relationship:String,goal:Goal,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url =  "\(ApiUrl().updateGoalNextOfKin())"
        let parameters = ["goal_id":goal.id,"name":name,"phone":mobile,"email":goal.nextKinEmail,"relationship":relationship] as [String : Any]
        self.basicGenericCall(url: url, params: parameters, completion: completion)
    }
    
    //MARK: - generic goal call
    private func basicGoalCall(url:String, params: [String:Any], completion: @escaping GoalCompletionHandler){
        print("URL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...300:
                        //parse success
                        print("Goal \(item)")
                        let goal = self.parseGoal(item: item)
                        completion(.SUCCESS,goal,"Completed Successfully")
                    case 301...399:
                       completion(.DETAIL,nil,"Completed Successfully")
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
  
    //MARK:- get occupation
    func occupations(completion: @escaping (ApiCallStatus,[Occupation]?,String) -> ())  {
        let url =  "\(ApiUrl().occupationList())"
        let params = ["":""]
        print("URL \(url)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: self.headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("\(url) Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    Occupation.delete()
                    var occupations = [Occupation]()
                    for itemIn in item["occupations"].enumerated() {
                        let occupation = Occupation()
                        occupation.id = itemIn.element.1["id"].intValue
                        occupation.name = itemIn.element.1["name"].stringValue
                        Occupation.save(data: occupation)
                        occupations.append(occupation)
                    }
                    completion(.SUCCESS,occupations,"Data Retrieved Succussfully")
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL,nil, detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- get next of kin relationships
    func allRelationships(completion: @escaping (ApiCallStatus) -> ())  {
        let url =  "\(ApiUrl().nextOfKinRelationships())"
        let params = ["nothing":""]
        print("URL \(url)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status Occupation \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        GoalNextKin.delete()
                        var allRelationship = [GoalNextKin]()
                        
                        for itemIn in item["next_of_kin_relationships"].enumerated() {
                            let nextOfKins = GoalNextKin()
                            nextOfKins.id = itemIn.element.1.stringValue
                            nextOfKins.name = itemIn.element.1.stringValue
                            GoalNextKin.save(data: nextOfKins)
                            allRelationship.append(nextOfKins)
                        }
                        completion(.SUCCESS)
                    case 301...399:
                        
                        let _ = item["detail"].stringValue
                        completion(.DETAIL)
                    default:
                        completion(.FAILED)
                    }
                }
        })
    }
    
    //MARK:- verify phone number
    func verifyPhone(number:String,code:String,completion: @escaping (ApiCallStatus,User?,String) -> ())  {
        let user = User.getUser()!
        let params = ["phone_number":number,"confirmation_code":code,"user_id":user.id] as [String : Any]
        let url =  "\(ApiUrl().confirmNumber())"
        print("URL \(url) Param \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: [:], completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    if let error = item["error"].string {
                        if error == self.invalidCode || error == self.unknownNumber  {
                            completion(.DETAIL,nil, "Invalid verification code provided")
                        }  else {
                            completion(.DETAIL,nil, error)
                        }
                    } else {
                        let user = self.parseToken(item: item,number: number)
                        completion(.SUCCESS,user,"Account verified successfully")
                    }
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL,nil, detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- login
    func loginUser(number:String,password:String,isFingerprint:Bool,completion: @escaping (ApiCallStatus,User?,String) -> ())  {
        let params = ["phone_number":number,"password":password]
        let url =  "\(ApiUrl().login())"
        print("URL \(url) Param \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: [:], completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    print("DATA \(item)")
                    if let error = item["error"].string {
                        if error == self.invalidPassword {
                            print("\n\n\n", "\(error)", " \n\n\n")
                            if isFingerprint == true {
                                let message = "Invalid fingerprint provided, This account does not belong to you"
                                completion(.DETAIL,nil, message)
                            }else{
                                let message = "Incorrect authentication provided"
                                completion(.DETAIL,nil, message)
                            }

                        } else {
                            completion(.DETAIL,nil, "Unable to login with the information")
                        }
                    } else {
                        let user = self.parseToken(item: item, number: number)
                        Mics.updateLoginTime()
                        completion(.SUCCESS,user,"Login successfully")
                    }
                case 301...399:
                    
                    let detail = item["detail"].stringValue
                    
                    print("detail json after login: ",detail)
                    
                    completion(.DETAIL,nil, detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- check password
    func checkPassword(number:String,password:String,completion: @escaping (ApiCallStatus,String) -> ())  {
        let params = ["phone_number":number,"password":password]
        let url =  "\(ApiUrl().login())"
        print("URL \(url) Param \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        print("DATA \(item)")
                        if let error = item["error"].string {
                            if error == self.invalidPassword {
                                let message = "The password you have provided is invalid."
                                completion(.DETAIL, message)
                            } else {
                                completion(.DETAIL, "Unable to verify password. Try again")
                            }
                        } else {
                            let user = self.parseToken(item: item, number: number)
                            Mics.updateLoginTime()
                            completion(.SUCCESS,"Valid password provided")
                        }
                    case 301...399:
                        
                        let detail = item["detail"].stringValue
                        completion(.DETAIL, detail)
                    default:
                        completion(.FAILED,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- user profile
    func getProfile(customerId:Int,completion: @escaping (ApiCallStatus,User?,String) -> ())  {
        let params = ["customer_id":customerId]
        let url =  "\(ApiUrl().getCustomerOverview())"
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if !Mics.isInternetAvailable() {
                completion(.FAILED,nil,self.interntConnectionStatus)
                return
            }
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let user = self.parseCustomerOverview(item: item)
                    completion(.SUCCESS,user,"Login successfully")
                case 301...400:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL,nil, detail)
                case 401...499:
                    completion(.FAILED,nil,self.failureStatus)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- update user balance
    func getUpdateBalance(customerId:Int,completion: @escaping (ApiCallStatus) -> ())  {
        let params = ["customer_id":customerId]
        let url =  "\(ApiUrl().getCustomerOverview())"
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if !Mics.isInternetAvailable() {
                completion(.FAILED)
                return
            }
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA USER \(item)")
                    self.parseBalanceUpdate(item: item)
                    completion(.SUCCESS)
                case 301...400:
                    completion(.DETAIL)
                case 401...499:
                    completion(.FAILED)
                default:
                    completion(.FAILED)
                }
            }
        })
    }
    
    
    //MARK:- save user information
    func saveKYCinformation(gender: String, citizenship: String, occupation:String, completion: @escaping (ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id": user.cutomerId, "gender": gender, "country_citizenship": citizenship, "occupation": occupation] as [String : Any]
        let url =  "\(ApiUrl().saveKYCInformation())"
        print("URL \(url) Param \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if !Mics.isInternetAvailable() {
                completion(.FAILED,self.interntConnectionStatus)
                return
            }
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    print("DATA \(item)")
                    User.updateUserKYC(gender: gender, citezenShip: citizenship, occupation: occupation)
                    print("USER \(User.getUser()!)")
                    completion(.SUCCESS,"Saved successfully")
                case 301...499:
                    if let item = try? JSON(data: response.data!){
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,detail)
                    } else {
                        completion(.DETAIL, "An unknown error has occured")
                    }
                    
                default:
                    completion(.FAILED, self.failureStatus)
                }
            }
        })
    }
    //MARK:- update device gcm
    func updateDeviceGcm(completion: @escaping (ApiCallStatus,String) -> ())  {
        if let device = DeviceInfo.getDevice() {
            var gcmToken = ""
            if let refreshedToken = InstanceID.instanceID().token() {
                gcmToken = refreshedToken
            }
            let params = ["device_id":device.deviceId,"gcm_token":gcmToken] as [String : Any]
            let url =  "\(ApiUrl().updateDeviceGcm())"
            self.basicGenericCall(url: url, params: params, completion: completion)
        } else {
            var gcmToken = ""
            if let refreshedToken = InstanceID.instanceID().token() {
                gcmToken = refreshedToken
            }
            let params = ["device_id": (123456789 as! Int),"gcm_token": gcmToken] as [String : Any]
            let url =  "\(ApiUrl().updateDeviceGcm())"
            self.basicGenericCall(url: url, params: params, completion: completion)
        }
    }
    
    
    //MARK:- export goal statement
    func exportStatement(accountId: Int, startDate: String, endDate: String, completion: @escaping (ApiCallStatus, String) -> ())  {
        let parameters = ["goal_id": accountId, "start_date": startDate, "end_date": endDate, "page_number":1, "limit": 500] as [String : Any]
        let url =  "\(ApiUrl().exportStatement())"
        print("EXPORT STATEMENT \(url) \(parameters)")
        
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0];
            let fileURL = documentsURL.appendingPathComponent("\(startDate) - \(endDate) Statement.pdf")
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        let paramsJson = parameters.convertToJson()
        let key = Obsufucator().reveal(key: Encrypter().getHMACKey())
        let digest = paramsJson.digest(.sha512, key: key).uppercased()
        let aes = SHA256()
        let encrypted = aes.encryptString(string: paramsJson)
        
        let p = ["a": encrypted, "b": digest]
        
        Alamofire.download(url, method: .post, parameters: p, encoding: JSONEncoding.default, headers: headerAuth(), to: destination)
            .response { result in
                if let error = result.error {
                    print("Statement export failure")
                    completion(.FAILED, error.localizedDescription)
                } else {
                    print("Statement export success")
                    completion(.SUCCESS, result.destinationURL!.absoluteString)
                }
        }
    }
    

    //MARK:- refresh token
    func refreshToken(completion: @escaping (ApiCallStatus,User?,String) -> ())  {
        let user = User.getUser()!
        let params = ["phone_number":user.phone,"password":""]
        let url =  "\(ApiUrl().refreshToken())"
        print("URL \(url) Param \(params)")
        print("REFRESH \(url) \(params)")
    }
    
    //MARK:- get customer
    func getCustomer(customerNumber:Int,completion: @escaping (ApiCallStatus,User?,String) -> ())  {
        let params = ["customer_id":customerNumber]
        let url =  "\(ApiUrl().getCustomer())"
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let user = self.parseCustomer(item: item)
                    completion(.SUCCESS,user,"Profile retrieved successfully")
                case 301...399:
                    let detail = item["detail"].stringValue
                    completion(.DETAIL,nil, detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- get overview
    func getAccountOverview(customerNumber:Int,completion: @escaping (ApiCallStatus,User?,String) -> ())  {
        let params = ["customer_id":customerNumber]
        let url =  "\(ApiUrl().getCustomerOverview())"
        print("OVERVIEW \(url) \(params) \(headerAuth())")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("\(url) Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let user = self.parseCustomerOverview(item: item)
                        completion(.SUCCESS, user, "Profile retrieved successfully")
                    case 301...399:
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil, detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get edit overview
    func getEditOverview(customerNumber:Int,completion: @escaping (ApiCallStatus) -> ())  {
        let params = ["customer_id":customerNumber]
        let url =  "\(ApiUrl().getEditOverview())"
        print("OVERVIEW \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        let emailStatus = item["email_verification_status"].stringValue
                        let address = item["street_address"].stringValue
                        let _ = item["phone_verification_status"].stringValue
                        let latitude = item["latitude"].doubleValue
                        let longitude = item["longitude"].doubleValue
                        User.updateExtraInfo(lat: latitude, lon: longitude, emailStatus: emailStatus, address: address)
                        print("DATA \(item)")
                        completion(.SUCCESS)
                    case 301...399:
                        completion(.DETAIL)
                    default:
                        completion(.FAILED)
                    }
                }
        })
    }
    
    
    //MARK:- get overview
    func getAccountStatementOverview(completion: @escaping (ApiCallStatus,String?,[Goal]?,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId]
        let url =  "\(ApiUrl().getAccountStatementOverview())"
        print("STATEMENT \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let balance = item["total_balance"].doubleValue
                        let balanceCurrency = item["currency"].stringValue
                        var goals = [Goal]()
                        for itemIn in item["goals"].enumerated() {
                            let goal = self.parseAccountGoal(item: itemIn.element.1)
                            Goal.save(data: goal)
                            goals.append(goal)
                        }
                        completion(.SUCCESS,"\(balanceCurrency) \(balance)",goals,self.interntConnectionStatus)
                    case 301...499:
                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil,nil,detail)
                    default:
                        completion(.FAILED,nil,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get account statement
    func getAccountStatement(accountId:Int,startDate:String,endDate:String,page:Int,completion: @escaping (ApiCallStatus,String?,Goal?,[TransactionHistory]?,String) -> ())  {
        let params = ["goal_id":accountId,"start_date":startDate,"end_date":endDate,"page_number":page,"limit":100] as [String : Any]
        let url =  "\(ApiUrl().goalAccountStatement())"
        print("ACCOUNT STATEMENT \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,nil,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let goal = self.parseGoalWithInOut(item: item, id: accountId)
                        var histories = [TransactionHistory]()
                        for itemIn in item["statements"].enumerated() {
                            let itemParsed = self.parseGoalTransaction(item: itemIn.element.1)
                            histories.append(itemParsed)
                        }
                        completion(.SUCCESS,"Goal statement retrived successfuly",goal,histories,self.interntConnectionStatus)
                    case 301...499:

                        let detail = item["detail"].stringValue
                        completion(.DETAIL,nil,nil,nil,detail)
                    default:
                        completion(.FAILED,nil,nil,nil,self.failureStatus)
                    }
                }
        })
    }
    //MARK:- get referral code
    func getReferralCode(completion: @escaping (ApiCallStatus,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId]
        let url =  "\(ApiUrl().getReferralCode())"
        print("REFERRAL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let code = item["referral_code"].stringValue
                        User.updateReferralCode(code: code)
                        completion(.SUCCESS,"Code Saves")
                    case 301...499:
                        let detail = item["error"].stringValue
                        completion(.DETAIL, detail)
                    default:
                        completion(.FAILED,self.failureStatus)
                    }
                }
        })
    }
    
    
    //MARK:- get referral code
    func getReferrals(completion: @escaping (ApiCallStatus,[Friend]?,String) -> ())  {
        let user = User.getUser()!
        let params = ["customer_id":user.cutomerId]
        let url =  "\(ApiUrl().getReferrals())"
        print("FRIENDS \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        var friends = [Friend]()
                        print("DATA \(item)")
                        for itemIn in item["referrals"].enumerated() {
                            let friend = self.parseFriend(item: itemIn.element.1)
                            friends.append(friend)
                        }
                        completion(.SUCCESS,friends,"Data retrived")
                    case 301...499:
                        let detail = item["error"].stringValue
                        completion(.DETAIL,nil,detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get transaction
    func getGoalTransaction(url:String,params:[String:Any],completion: @escaping (ApiCallStatus,[GoalActivity]?,String) -> ())  {
        print("GOAL TRNASACTIONS \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        var transactions = [GoalActivity]()
                        print("DATA \(item)")
                        for itemIn in item["transactions"].enumerated() {
                            let transaction = self.parseGoalActivity(item: itemIn.element.1)
                            transactions.append(transaction)
                        }
                        completion(.SUCCESS,transactions,"Data retrived")
                    case 301...499:
                        let detail = item["error"].stringValue
                        completion(.DETAIL,nil,detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get transaction
    func getTransaction(url:String,params:[String:Any],completion: @escaping (ApiCallStatus,[TransactionHistory]?,String) -> ())  {
        print("TRNASACTIONS \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        var transactions = [TransactionHistory]()
                        print("DATA \(item)")
                        for itemIn in item["overview"].enumerated() {
                            let transaction = self.parseStatementTransaction(item: itemIn.element.1)
                            transactions.append(transaction)
                        }
                        completion(.SUCCESS,transactions,"Data retrived")
                    case 301...499:
                        let detail = item["error"].stringValue
                        completion(.DETAIL,nil,detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get transaction detail
    func transactionDetail(id:Int,completion: @escaping (ApiCallStatus,TransactionHistory?,String) -> ())  {
        let url = "\(ApiUrl().transactionDetail())"
        let params = ["trans_id":id]
        print("TRNASACTIONS \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("ITEM \(item)")
                        let transaction = self.parseTransaction(item: item)
                        completion(.SUCCESS,transaction,"Data retrived")
                    case 301...499:
                        let detail = item["error"].stringValue
                        completion(.DETAIL,nil,detail)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- mark notification as read
    func markNotificationAsRead(id:Int,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().markNotificationsAsRead())"
        let params = ["notification_id":id]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    //MARK:- notification delete
    func notificationsDelete(id: Int,completion: @escaping (ApiCallStatus,String) -> ()) {
        let url = "\(ApiUrl().notificationsDelete())"
        let params = ["notificationId":id]
        self.basicGenericCall(url: url, params: params, completion: completion)
    }
    
    
    //MARK:- get notifications
    func getNotifications(url:String,pageNumber:Int,isMore:Bool,completion: @escaping (ApiCallStatus,[Notification]?,Int,String) -> ())  {
        let user = User.getUser()!
        let params = ["account_id":user.accountId,"page_number":pageNumber,"limit":50] as [String : Any]
        print("NOTIFICATION \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(.FAILED,nil,0,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        let page = item["page_number"].intValue
                        var messages = [Notification]()
                        print("DATA \(item)")
                        for itemIn in item["notifications"].enumerated() {
                            let itemParsed = self.parseNotification(item: itemIn.element.1)
                            if !isMore {
                               Notification.save(data: itemParsed)
                            }
                            messages.append(itemParsed)
                        }
                        completion(.SUCCESS,messages,page,"Data retrived")
                    case 301...499:
                        let detail = item["error"].stringValue
                        completion(.DETAIL,nil,0,detail)
                    default:
                        completion(.FAILED,nil,0,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- get notifications
    func getBillDetails(issuer: String, identifier: String, completion: @escaping (ApiCallStatus,JSON,String) -> ())  {
        let params = ["issuer": issuer, "identifier": identifier] as [String : Any]
        let url = ApiUrl().billDetail()
        print("BILL DETAILS \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if response.error != nil {
                completion(.FAILED,JSON.null,self.interntConnectionStatus)
                return
            }
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    completion(.SUCCESS, item, "Data retrived")
                case 301...499:
                    let detail = item["error"].stringValue
                    completion(.DETAIL,JSON.null,detail)
                default:
                    completion(.FAILED,JSON.null,self.failureStatus)
                }
            }
        })
    }
    
    
    //MARK:- add payment option
    func addPaymentOption(params: [String:Any],completion: @escaping (ApiCallStatus,Wallet?,String) -> ())  {
        let url =  "\(ApiUrl().addPaymentOption())"
        print("ADDING WALLET \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED,nil,self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring)
                
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        if item["error"].stringValue != "" {
                            let error = item["error"].stringValue
                            var m = "An error occured, please try again later."
                            if error == "err.cardAlreadyAdded" {
                                m = "The card has already been added, please try another card"
                            }
                            completion(.FAILED, nil, m)
                            return
                        }
                        
                        print("WALLET \(item)")
                        let walletId = item["payment_wallet_id"].intValue
                        
                        let wallet = Wallet()
                        wallet.amount = ""
                        wallet.id = walletId
                        let type = params["payment_type"] as! String
                        
                        if type == PaymentOptionType.card.rawValue {
                            wallet.name = params["card_name"] as! String
                            wallet.icon = "VISA"
                            wallet.type = PaymentOptionType.card.rawValue
                            wallet.paymentType = PaymentOptionType.card.rawValue
                            let lastFourDigits = (params["card_number"] as! String).suffix(4)
                            
                            wallet.number =  "XXXX-XXXX-XXXX-\(lastFourDigits)"
                        } else {
                            wallet.name = params["payment_name"] as! String
                            wallet.icon = params["payment_network"] as! String
                            wallet.type = PaymentOptionType.momo.rawValue
                            wallet.paymentType = PaymentOptionType.momo.rawValue
                            wallet.number = params["payment_number"] as! String
                        }
                        Wallet.save(data: wallet)
                        
                        completion(.SUCCESS, wallet, "Wallet added successfully")
                    case 301...499:
                        let detail = item["error"].stringValue
                        var message = ""
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        if detail == "err.invalidCardProvided" {
                            message = "Your card is invalid. Please use a valid card"
                        }
                        completion(.DETAIL,nil, message)
                    default:
                        completion(.FAILED, nil, self.failureStatus)
                    }
                }
        })
    }
    
    
    func microDebitCard(params: [String:Any],completion: @escaping (ApiCallStatus,Int, String) -> ())  {
        let url =  "\(ApiUrl().microCardDebit())"
        print("Debiting card for verification \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED, -1, self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print("Data response")
                print(datastring)
                
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let cardId = params["card_id"] as! Int
                        if item["transaction_status"] == "SUCCESS" {
                            completion(.SUCCESS, cardId, "Card debited successfully")
                        } else if item["transaction_status"] == "FAILED" {
                            completion(.FAILED, -1, "Failed to debit card")
                        } else if item["transaction_status"] == "PENDING" {
                            let weburl = item["web_prompt"].string
                            completion(.DETAIL, -3, weburl!)
                        }
                    case 301...499:
                        print("Bad response")
                        
                        let detail = item["error"].stringValue
                        var message = "Unknown error occured"
                        if detail == self.paymentTypeNotFound {
                            message = "The payment method type your adding wasn't found"
                        } else if detail == self.invalidCardProvided {
                            message = "Invalid card provided"
                        }
                        completion(.DETAIL, -1, message)
                    default:
                        completion(.FAILED, -1, self.failureStatus)
                    }
                }
        })
    }
    
    func verifyDebitCard(params: [String:Any],completion: @escaping (ApiCallStatus,Int, String) -> ())  {
        let url =  "\(ApiUrl().verifyDebitAmount())"
        print("Verifying card debit amount \(url) \(params)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED, -1, self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring)
                
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let cardId = item["card_id"].intValue
                        
                        completion(.SUCCESS, cardId, "Card verified successfully")
                    case 301...499:
                        
                        let detail = item["error"].stringValue
                        var message = ""
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        completion(.DETAIL, -1, message)
                    default:
                        completion(.FAILED, -1, self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- initiate transaction
    func initTransaction(amount: Double, methodType: String, reference: String, sourceWallet: Wallet, issuerToken: String, targetType: String, targetAccount: String, theGoal: Goal?, completion: @escaping (ApiCallStatus, ConfirmationItem?, String) -> ())  {
        let user = User.getUser()!
        var issuerType = "LIQUID_ACCOUNT"
        var issuerAccountNumber = "\(user.accountId)"
        
        if sourceWallet.paymentType == "card" {
            issuerType = "DEBIT_CARD"
            issuerAccountNumber = "\(sourceWallet.id)"
        } else if sourceWallet.paymentType == "momo" {
            issuerType = Mics.sourceWalletType(wallet: sourceWallet)
            issuerAccountNumber = "\(sourceWallet.number)"
        }
        
        if sourceWallet.icon == "bankTransfers" {
            issuerType = "BANK_TRANSFER"
            issuerAccountNumber = "\(theGoal?.id ?? 0)"
        }

        if methodType == PaymentMethodTypes.WITHDRAW_CASH.rawValue {
            issuerType = "LIQUID_GOAL_ACCOUNT"
            issuerAccountNumber = sourceWallet.number
        }
        
        let paramsSource = ["issuer": issuerType, "account": issuerAccountNumber, "token": issuerToken]
        
        let paramsTarget = ["issuer": targetType, "account": targetAccount]
        let paramsInner = ["source": paramsSource, "target":paramsTarget] as [String : Any]
        let params = [ "account_id": user.accountId, "amount": amount, "currency": "GHS", "method_type": methodType, "payload": paramsInner] as [String : Any]
        
        
        let url =  "\(ApiUrl().initTransaction())"
        print("INIT TRANSACTION \(url) \(params) \(headerAuth())")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    print(response.error ?? "")
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    print(datastring ?? "")
                    
                    completion(.FAILED,nil,self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(datastring ?? "")
                        
                        print("DATA HERE::::: \(item)")
                        let error = item["error"].stringValue
                        if error.isEmpty {
                            let reference = item["reference"].stringValue
                            let currency = item["currency"].stringValue
                            let amount = item["amount"].doubleValue
                            let fees = item["fees"].doubleValue
                            let total = item["total"].doubleValue
                            let voucher = item["transfer_voucher"].stringValue
                            let confirmationItem = ConfirmationItem(reference: reference, amount: amount, fees: fees, total: total, currency: currency, transferVoucher: voucher)
                            completion(.SUCCESS,confirmationItem,"Transaction initiated successfully")
                        }  else {
                            completion(.DETAIL,nil,error)
                        }
                    case 301...499:
                        
                        print("ERROR \(item)")
                        let detail = item["error"].stringValue
                        var message = ""
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        completion(.DETAIL,nil, message)
                    default:
                        completion(.FAILED,nil,self.failureStatus)
                    }
                } else {
                    completion(.FAILED, nil, self.failureStatus)
                }
        })
    }
    
    //MARK:- transfer Voucher
    func getTransferVouch(amount: Double, methodType: String, reference: String, issuerToken: String, targetType: String, targetAccount: String, theGoal: Goal, completion: @escaping(JSON, ApiCallStatus, String) -> ()){
        let user = User.getUser()!
        
        let issuerType = "BANK_TRANSFER"
        //let sourceAccount = "\(user.accountId)"
        //let transactionId = "\(user.accountId)"
        
        let sourceParams = ["issuer":issuerType, "account":"\(theGoal.id)", "token": issuerToken] as [String: Any]
        let targetParams = ["issuer":targetType, "account": "\(theGoal.id)"] as [String: Any]
        let source = ["source":sourceParams, "target": targetParams] as [String: Any]
        let params = ["account_id": user.accountId, "amount": amount, "currency": "GHS", "method_type": methodType, "payload": source, "reference": reference] as [String: Any]
        
        let url = "\(ApiUrl().bankVoucherURL())"
        print("BANK TRANSACTION \(url) \(params) \(headerAuth())")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
            if response.error != nil {
                guard let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) else { return }
                print(datastring)
                
                completion(JSON.null, .FAILED, self.interntConnectionStatus)
                return
            }
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    
                    print("BANK DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        let reference = item["voucher"].stringValue
                        print("bank transfer voucher: \(reference)")
                        completion(item, .SUCCESS, "successful")
                    }  else {
                        completion(JSON.null, .DETAIL, error)
                    }
                case 301...499:
                    
                    print("ERROR \(item)")
                    let detail = item["error"].stringValue
                    var message = ""
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(JSON.null, .DETAIL, message)
                default:
                    completion(JSON.null, .DETAIL, self.failureStatus)
                }
            }
        })
    }
    
    //MARK:- share voucher
    func share_voucher_via(destination_values: String, voucher: String, completion: @escaping (JSON, ApiCallStatus, String) -> ()) {
        let url = "\(ApiUrl().shareTransferVoucherUrl())"
        let params = ["transfer_voucher": voucher,"share_via": destination_values] as [String: Any]
        
        print("share params:\(url) \(params)")
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            if response.error != nil {
                guard let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue) else { return }
                print(datastring)
                
                completion(JSON.null, .FAILED, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    
                    print("SHARE DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(item, .SUCCESS, "successful")
                    }  else {
                        completion(JSON.null, .DETAIL, error)
                    }
                case 301...499:
                    
                    print("ERROR \(item)")
                    let detail = item["error"].stringValue
                    var message = ""
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(JSON.null, .DETAIL, message)
                default:
                    completion(JSON.null, .DETAIL, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- confirm transaction
    func confirmTransaction(amount: Double, methodType: String, reference: String, sourceWallet: Wallet, cvv: String, issuerToken: String, targetType: String, targetAccount: String, completion: @escaping (JSON, ApiCallStatus, String) -> ())  {
        let user = User.getUser()!
        
        var issuerType = "LIQUID_ACCOUNT"
        var issuerAccountNumber = "\(user.accountId)"
        if sourceWallet.paymentType == "card" {
            issuerType = "DEBIT_CARD"
            issuerAccountNumber = "\(sourceWallet.id)"
        } else if sourceWallet.paymentType == "momo" {
            issuerType = Mics.sourceWalletType(wallet: sourceWallet)
            issuerAccountNumber = "\(sourceWallet.number)"
        }
        
        var paramsSource = ["issuer": issuerType, "account":issuerAccountNumber, "token": issuerToken]
        if sourceWallet.paymentType == "card" {
            paramsSource["cvv"] = cvv
        }
        
        let paramsTarget = ["issuer": targetType, "account": targetAccount]
        let paramsInner = ["source":paramsSource,"target":paramsTarget] as [String : Any]
        let params = ["account_id": user.accountId, "amount":amount, "currency": "GHS", "method_type": methodType, "payload": paramsInner, "reference": reference] as [String : Any]
        let url =  "\(ApiUrl().confirmTransaction())"
        let initUrl =  "\(ApiUrl().initTransaction())"
        
        print("INLINE INIT TRANSACTION \(initUrl) \(params) \(headerAuth())")
        self.callEncrypted(url: initUrl, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    print(response.error ?? " ")
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    print(datastring ?? " ")
                    
                    completion(JSON.null, .FAILED, self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        //let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        //print(datastring)
                        
                        print("INLINE DATA \(item)")
                        let error = item["error"].stringValue
                        if error.isEmpty {
                            let reference = item["reference"].stringValue
                            
                            
                            let params = ["account_id": user.accountId, "amount":amount, "currency": "GHS", "method_type": methodType, "payload": paramsInner, "reference": reference] as [String : Any]
                            
                            print("INLINE CONFIRM \(url) \(params)")
                            self.callEncrypted(url: url, method: .post, parameters: params, headers: self.headerAuth(), completion: { (response, item) in
                                    if response.error != nil {
                                        completion(JSON.null, .FAILED, self.interntConnectionStatus)
                                        return
                                    }
                                    if let status = response.response?.statusCode {
                                        print("Status \(status)")
                                        switch(status){
                                        case 200...300:
                                            //parse success
                                            print("DATA \(item)")
                                            let error = item["error"].stringValue
                                            if error.isEmpty {
                                                let _ = item["currency"].stringValue
                                                let _ = item["transaction_status"].stringValue
                                                let _ = item["transaction_id"].intValue
                                                completion(item, .SUCCESS, "Your transaction is being processed. We will notify you when it is completed")
                                            }  else {
                                                switch error {
                                                    case "err.InsufficientBalance":
                                                        completion(item, .DETAIL, "The amount you have entered, currently exceeds the amount you have saved towards your goal.")
                                                    default:
                                                        completion(item, .DETAIL, "An error has occured processing your transaction.")
                                                }
                                            }
                                            
                                        case 301...499:
                                            
                                            let detail = item["error"].stringValue
                                            var message = "An error has occured processing your transaction."
                                            if detail == self.paymentTypeNotFound {
                                                message = ""
                                            }
                                            completion(JSON.null, .DETAIL, message)
                                        default:
                                            completion(JSON.null, .FAILED, self.failureStatus)
                                        }
                                    }
                            })
                        }  else {
                            completion(JSON.null, .DETAIL, error)
                        }
                    case 301...499:
                        
                        print("ERROR \(item)")
                        let detail = item["error"].stringValue
                        var message = ""
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        completion(JSON.null, .DETAIL, message)
                    default:
                        completion(JSON.null, .DETAIL, self.failureStatus)
                    }
                }
        })
    }
    
    
    //MARK:- confirm transaction
    func withdrawFromGoal(amount: Double, goal: Goal, reference: String, completion: @escaping (JSON, ApiCallStatus, String) -> ())  {
        let user = User.getUser()!
        
        let params = ["account_id": user.accountId, "amount":amount, "currency": "GHS", "method_type": "WITHDRAW_CASH", "payload": [
            "source" : [
                "account" : "\(goal.id)",
                "issuer" : "LIQUID_GOAL_ACCOUNT"
            ],
            "target" : [
                "account" : "-1",
                "issuer" : "CASH_WITHDRAWAL"
            ]
            ],
        "reference": reference
        ] as [String : Any]
        let url =  "\(ApiUrl().confirmTransaction())"
        
        
        print("CONFIRM \(url) \(params.prettyPrintedJSON!)")
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    completion(JSON.null, .FAILED, self.interntConnectionStatus)
                    return
                }
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let error = item["error"].stringValue
                        if error.isEmpty {
                            completion(item, .SUCCESS, "Your transaction is being processed. We will notify you when it is completed")
                        }  else {
                            switch error {
                                case "err.InsufficientBalance":
                                    completion(item, .DETAIL, "The amount you have entered, currently exceeds the amount you have saved towards your goal.")
                                default:
                                    completion(item, .DETAIL, "An error has occured processing your transaction.")
                            }
                            //completion(item, .DETAIL, "An error has occured processing your transaction.")
                        }
                        
                    case 301...499:
                        
                        let detail = item["error"].stringValue
                        var message = "An error has occured processing your transaction."
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        completion(JSON.null, .DETAIL, message)
                    default:
                        completion(JSON.null, .FAILED, self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- add Referral
    func addReferralCode(code: String, completion: @escaping (JSON, ApiCallStatus, String) -> ())  {
        let user = User.getUser()!
        let params = ["invite_code": code, "customer_id": user.cutomerId] as [String : Any]
        let url =  "\(ApiUrl().addReferral())"
        
        
        print("ADD REFERRAL \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    //print(response.error?.localizedDescription ?? "")
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    print(datastring ?? "")
                    
                    completion(JSON.null, .FAILED, self.interntConnectionStatus)
                    return
                }
                
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let error = item["error"].stringValue
                        if error.isEmpty {
                            completion(item, .SUCCESS, "You have successfully added a referral code")
                        }  else {
                            switch error {
                                case "err.inviteCodeNotFound":
                                    completion(item, .DETAIL, "The invitation code you have entered is invalid.")
                                case "inviteCodeAlreadyUsed":
                                    completion(item, .DETAIL, "You have already used the invite code.")
                                default:
                                    completion(item, .DETAIL, "An error has occured adding your referral.")
                            }
                        }
                        
                    case 301...499:
                        
                        let detail = item["error"].stringValue
                        var message = "An error has occured processing your transaction."
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        completion(JSON.null, .DETAIL, message)
                    default:
                        completion(JSON.null, .FAILED, self.failureStatus)
                    }
                }
        })
    }
    
    //MARK:- verify referral
    func verifyReferral(code: String, completion: @escaping (JSON, ApiCallStatus, String) -> ()){
        let params = ["invite_code": code] as [String: Any]
        let url = "\(ApiUrl().checkInvitaionCode())"
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            if response.error != nil {
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                
                completion(JSON.null, .FAILED, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(item, .SUCCESS, "referral code is valid")
                    }  else {
                        switch error {
                        case "err.inviteCodeNotFound":
                            completion(item, .DETAIL, "The invitation code you have entered is invalid.")
                        case "inviteCodeAlreadyUsed":
                            completion(item, .DETAIL, "You have already used the invite code.")
                        default:
                            completion(item, .DETAIL, "An error has occured adding your referral.")
                        }
                    }
                    
                case 301...499:
                    
                    let detail = item["error"].stringValue
                    var message = "An error has occured processing your transaction."
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(JSON.null, .DETAIL, message)
                default:
                    completion(JSON.null, .FAILED, self.failureStatus)
                }
            }

        }
    }
    
    //MARK: enable paccsode section
    func enablePasscode(passcode:Int,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().enablePasscode())"
        let params = ["customer_id":User.getUser()!.cutomerId,"passcode":passcode]
        print("URL \(url) \(params)")
        self.basicPasscodeCall(url: url,parameters: params, completion: completion)
    }
    
    //MARK: disble passcode section
    func disablePasscode(passcode:Int,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().disablePasscode())"
        let params = ["customer_id":User.getUser()!.cutomerId,"passcode":passcode]
        print("URL \(url)")
        self.basicPasscodeCall(url: url,parameters: params, completion: completion)
    }
    
    //MARK: check passcode status
    func checkPasscodeStatus(completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().checkPasscodeStatus())"
        let params = ["customer_id":User.getUser()!.cutomerId]
        print("URL \(url)")
        self.basicPasscodeCall(url: url,parameters: params, completion: completion)
    }
    
    //MARK: verify passcode
    func verifyPasscode(passcode:Int,completion: @escaping (ApiCallStatus,String) -> ())  {
        let url = "\(ApiUrl().checkPasscodeStatus())"
        let params = ["customer_id":User.getUser()!.cutomerId,"passcode":passcode]
        print("URL \(url)")
        self.basicPasscodeCall(url: url,parameters: params, completion: completion)
    }
    
    //MARK:- update passcode
    func updatePasscode(oldPasscode: Int, newPasscode: Int, completion: @escaping (JSON, ApiCallStatus, String) -> ())  {
        let user = User.getUser()!
        let params = ["oldpasscode": oldPasscode, "newpasscode": newPasscode, "customer_id": user.cutomerId] as [String : Any]
        let url =  "\(ApiUrl().updatePasscode())"
        
        
        print("UPDATE PASSCODE \(url) \(params)")
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth(), completion: { (response, item) in
                if response.error != nil {
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    
                    completion(JSON.null, .FAILED, self.interntConnectionStatus)
                    return
                }
                
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...300:
                        //parse success
                        print("DATA \(item)")
                        let error = item["error"].stringValue
                        if error.isEmpty {
                            completion(item, .SUCCESS, "You have successfully changed your passcode")
                        }  else {
                            completion(item, .DETAIL, "An error has occured while changing your passcode.")
                        }
                        
                    case 301...499:
                        
                        let detail = item["error"].stringValue
                        var message = "An error has occured processing your transaction."
                        if detail == self.paymentTypeNotFound {
                            message = ""
                        }
                        completion(JSON.null, .DETAIL, message)
                    default:
                        completion(JSON.null, .FAILED, self.failureStatus)
                    }
                }
        })
    }
    
    //MARK: - Fetch Coach Data
    func getcoachData(completion: @escaping (ApiCallStatus, Coach?, String) -> ()){
        let user = User.getUser()!
        let params = ["account_id": user.accountId] as [String : Any]
        let url =  "\(ApiUrl().coachDataUrl())"
        
        print("COACH: \(url) \(params)")
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            if response.error != nil {
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring!)
                completion(.FAILED, nil,self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("coach data in::  \(item)")
                    
                    //var coachItems = [Coach]()
                    Coach.delete()
                    let parseData = self.parseCoachData(item: item)
                    completion(.SUCCESS,parseData,"Data retrived")
                    
                case 301...499:
                    
                    let detail = item["error"].stringValue
                    var message = "An error has occured."
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(.DETAIL,nil,detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        }
    }
    
    //MARK: - generic passcode call
    private func basicPasscodeCall(url:String,parameters:[String:Any],completion: @escaping GenericCompletionHandler){
        if !Mics.isInternetAvailable() {
            completion(.FAILED, self.interntConnectionStatus)
            return
        }
        self.callEncrypted(url: url, method: .post, parameters: parameters, headers: headerAuth(), completion: { (response, item) in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status) {
                    case 200...300:
                        //parse success
                        print("PASSCODE DATA \(item)")
                        
                        //MARK: enable passcode
                        if let done = item["passcode_enabled"].bool {
                            if done {
                               completion(.SUCCESS,"Passcode enabled successfully.")
                            }  else {
                               completion(.DETAIL,"Failed to enable passcode.")
                            }
                            return
                        }
                        
                        //MARK: verify code
                        if let done = item["passcode_verified"].bool {
                            if done {
                                completion(.SUCCESS,"Passcode verified successfully.")
                            }  else {
                                completion(.DETAIL,"Failed to verify passcode.")
                            }
                            return
                        }
                        
                        //MARK: disable passcode
                        if let done = item["passcode_disabled"].bool {
                            if done {
                               completion(.SUCCESS,"Passcode disabled successfully.")
                            }  else {
                               completion(.DETAIL,"Failed to disable passcode.")
                            }
                            return
                        }
                        
                        completion(.DETAIL,"Unable to enable passcode.")
                    case 301...399:
                        let detail = self.failureStatus
                        completion(.DETAIL, detail)
                    case 401...499:
                        completion(.DETAIL, self.failureStatus)
                    default:
                        completion(.FAILED,self.failureStatus)
                    }
                }
        })
    }
    
    //MARK: - generic call
    private func basicGenericCall(url:String,method: HTTPMethod = .post, params: [String:Any], completion: @escaping GenericCompletionHandler){
        let header = headerAuth()
        print("URL \(url) \(params) \(header)")
        if !Mics.isInternetAvailable() {
            completion(.FAILED,self.interntConnectionStatus)
            return
        }
        
        self.callEncrypted(url: url, method: method, parameters: params, headers: header, completion: { (response, item) in
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA FROM ENCRYPTED: \(item)")
                    completion(.SUCCESS, "\(item)")
                case 301...399:
                    do {
                        let item = try JSON(data: response.data!)
                        let detail = item["detail"].stringValue
                        completion(.DETAIL, detail)
                    } catch {
                        let detail = "Unable to locate the right key in request"
                        completion(.DETAIL, detail)
                    }
                case 401:
                    if url.contains("logout"){
                        completion(.SUCCESS,"Completed Successfully")
                    } else {
                        completion(.DETAIL, self.failureStatus)
                    }
                case 402...499:
                    completion(.DETAIL, self.failureStatus)
                default:
                    completion(.FAILED,self.failureStatus)
                }
            }
        })
    }
    
    //MARK: - update with file
    func saveSelfieProfile(image:UIImage,completion: @escaping (ApiCallStatus,String?,String) -> ())  {
        let user = User.getUser()!
        let parameters = ["customer_id":user.cutomerId] as [String : Any]
        self.baseDocUpload(type: "PIC", url: "\(ApiUrl().updateProfileImage())", image: image,fileUploadKey: "picture",parameters: parameters, completion: completion)
    }
    
    //MARK: - save with file
    func saveDocumant(image:UIImage,idType:String,idNumber:String,completion: @escaping (ApiCallStatus,String?,String) -> ())  {
        let user = User.getUser()!
        let parameters = ["customer_id":user.cutomerId,"id_type":idType,"id_number":idNumber] as [String : Any]
        self.baseDocUpload(type: "DOC", url: "\(ApiUrl().saveDocument())", image: image, fileUploadKey: "picture", parameters: parameters, completion: completion)
    }
    
    //MARK:- base doc uploading function
    private func baseDocUpload(type:String,url:String,image:UIImage?,fileUploadKey:String,parameters:[String:Any], completion: @escaping FileUploadCompletionHandler)  {
        let imageName = NSUUID().uuidString
        var imageData: Data?
        if let imageIn = image {
            imageData = imageIn.jpegData(compressionQuality: 0.5) //UIImageJPEGRepresentation(imageIn,0.5)
        }
        let user = User.getUser()!
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(user.token)",
            "Content-type": "multipart/form-data"
        ]
        
        print("URL \(url) PARAMS \(parameters)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data, withName: fileUploadKey, fileName: "\(imageName).jpeg", mimeType: "image/jpeg")
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                    print(datastring ?? "")
                    print("Succesfully uploaded: ", imageName)

                    if let err = response.error {
                        print("ERROR \(err.localizedDescription)")
                        completion(.FAILED,nil,err.localizedDescription)
                        return
                    }
                    var url = ""
                    let item = try! JSON(data: response.data!)
                    if type == "PIC" {
                        
                        url = item["selfie_url"].stringValue
                        print("Succesfully uploaded: ",item)
                        User.updateUserPic(url: url)
                    }
                    if type == "GOAL" {
                        print("DATA \(item)")
                        //let goalId = item["goal_id"].intValue
                    }
                    completion(.SUCCESS,url, "Updated succussfully")
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completion(.FAILED,nil,error.localizedDescription)
            }
        }
    }
    
    //MARK:- base doc uploading function
    private func baseDocUploadGoal(type:String, url:String, image:UIImage?, fileUploadKey:String, parameters: Dictionary<String, Any>, completion: @escaping FileUploadCompletionHandler)  {
        let imageName = NSUUID().uuidString
        var imageData: Data?
        if let imageIn = image {
            print("image found")
            imageData = imageIn.jpegData(compressionQuality: 0.5)//UIImageJPEGRepresentation(imageIn,0.5)
        } else {
            print("No image found")
        }
        print("URL \(url) PARAMS \(parameters)")
        print("HEADERS ", multiMediaHeaderAuth())
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData {
                multipartFormData.append(data, withName: fileUploadKey, fileName: "\(imageName).jpeg", mimeType: "image/jpeg")
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: multiMediaHeaderAuth()) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseString { response in
                    print("Succesfully uploaded")
                    if let err = response.error {
                        print("ERROR \(err.localizedDescription)")
                        completion(.FAILED,nil,err.localizedDescription)
                        return
                    }
                    if let data = response.result.value {
                        if type == "GOAL" {
                            
                            print("DATA \(data)")
                            let item = try! JSON(data: response.data!)
                            
                            //let paramsJson = parameters.convertToJson()
                            let key = Obsufucator().reveal(key: Encrypter().getHMACKey())
                            //let digest = paramsJson.digest(.sha512, key: key).uppercased()
                            let aes = SHA256()
                            //let encrypted = aes.encryptString(string: paramsJson)
                            
                            let json = aes.decryptString(string: item["a"].stringValue)
                            
                            let responseJSON = JSON(parseJSON: json)
                            if item["b"].stringValue == json.digest(.sha512, key: key).uppercased() {
                                print(responseJSON)
                                
                                if responseJSON["error"].stringValue == "" {
                                    let goalId = responseJSON["goal_id"].intValue
                                    completion(.SUCCESS,"\(goalId)", "Completed succussfully")
                                    return
                                } else {
                                    completion(.FAILED, nil, self.errorToHumanString(error: item["error"].stringValue))
                                    return
                                }
                                
                            } else {
                                print(responseJSON)
                                print(item)
                                print(json.digest(.sha512, key: key).uppercased())
                                completion(.FAILED,nil, "unsuccessful")
                            }
                        }
                    }
                    completion(.SUCCESS,"", "Completed succussfully")
                }
            case .failure(let error):
                completion(.FAILED,nil,error.localizedDescription)
            }
        }
    }
    
    
    //MARK:- setup Recurring Debit
    func setupRecurringDebit(goalId: Int, frequency: String, amount: Double, paymentMethodId: Int, paymentType: String, completion: @escaping (JSON, ApiCallStatus, String) -> ()){
        
        let url = "\(ApiUrl().setuprecurringdebit())"
        let params = ["goal_id": goalId, "frequency": frequency, "amount": amount, "payment_method_type": paymentType, "payment_method_id": paymentMethodId] as [String: Any]
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            if response.error != nil {
                //print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                
                completion(JSON.null, .FAILED, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(item, .SUCCESS, "You have successfully added a wallet")
                    }  else {
                        switch error {
                        case "err.recurringDebitAlreadyExistsForGoal":
                            completion(item, .DETAIL, "recurring debit alreasy exists for the goal.")
                        case "err.goalNotFound":
                            completion(item, .DETAIL, "Goal not found")
                        case "err.amountTooLowForRecurringDebit":
                            completion(item, .DETAIL, "amount is lower than the goal funding minimum")
                        case "err.amountTooHighForRecurringDebit":
                            completion(item, .DETAIL, "amount provided is higher than the goal funding maximum")
                        default:
                            completion(item, .DETAIL, "An error has occured adding your referral.")
                        }
                    }
                    
                case 301...499:
                    
                    let error = item["error"].stringValue
                    var message = "An error has occured processing your transaction."
                    if error == self.paymentTypeNotFound {
                        message = ""
                    }
                    
                    switch error {
                    case "err.recurringDebitAlreadyExistsForGoal":
                        completion(item, .DETAIL, "recurring debit alreasy exists for the goal.")
                    case "err.goalNotFound":
                        completion(item, .DETAIL, "Goal not found")
                    case "err.amountTooLowForRecurringDebit":
                        completion(item, .DETAIL, "amount is lower than the goal funding minimum")
                    case "err.amountTooHighForRecurringDebit":
                        completion(item, .DETAIL, "amount provided is higher than the goal funding maximum")
                    default:
                        //completion(item, .DETAIL, "An error has occured adding your referral.")
                        completion(JSON.null, .DETAIL, message)
                    }
                    
                    
                default:
                    completion(JSON.null, .FAILED, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- get recurring debit data
    func getRecurringDebitSettings(goalId: Int, completion: @escaping(ApiCallStatus, RecurringDebit?,String) -> ()){
        let urls = "\(ApiUrl().getrecurringdebitsettings())"
        let params = ["goal_id": goalId]
        
        print("recurring settings: ",urls)
        print("recurring settings: ",params)
        
        self.callEncrypted(url: urls, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
             print("recurring settings: ",response)
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(.FAILED, nil, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        let parseData = self.parseRecurringDebitData(item: item)
                        completion(.SUCCESS, parseData, "successful")
                    }  else {
                        switch error {
                        case "err.recurringDebitSettingsNotFound":
                            completion(.DETAIL, nil, "recurring debit not set for the goal.")
                        case "err.goalNotFound":
                            completion(.DETAIL, nil, "Goal not found")
                        default:
                            completion(.DETAIL, nil, "An error has occured adding your referral.")
                        }
                    }
                    
                case 301...499:
                    
                    let error = item["error"].stringValue
                    switch error {
                    case "err.recurringDebitSettingsNotFound":
                        completion(.DETAIL, nil, "recurring debit not set for the goal.")
                    case "err.goalNotFound":
                        completion(.DETAIL, nil, "Goal not found")
                    default:
                        completion(.DETAIL, nil, "An error has occured adding your referral.")
                    }
                    
                default:
                    completion(.FAILED, nil, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- disable recurring debit
    func disableRecurringDebit(goalId: Int, completion: @escaping(ApiCallStatus, JSON, String) -> ()){
        let urls = "\(ApiUrl().disablerecurringdebit())"
        let params = ["goal_id": goalId]
        
        print("recurring settings: ",urls)
        print("recurring settings: ",params)

        self.callEncrypted(url: urls, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(.FAILED, JSON.null, self.interntConnectionStatus)
                return
            }

            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, item, "successful")
                    }  else {
                        switch error {
                        case "err.recurringDebitMustBeSetupFirst":
                            completion(.DETAIL, JSON.null, "A recurring debit has not previously been set up.")
                        case "err.goalNotFound":
                            completion(.DETAIL, JSON.null, "Goal not found")
                        case "err.generalError":
                            completion(.DETAIL, JSON.null, "An error has occured.")
                        default:
                            completion(.DETAIL, JSON.null, "An error has occured.")
                        }
                    }
                    
                case 301...499:
                    
                    let detail = item["error"].stringValue
                    var message = "An error has occured processing your request."
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(.DETAIL, JSON.null, message)
                default:
                    completion(.FAILED, JSON.null, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- enable recurring debit
    func enableRecurringDebit(goalId: Int, completion: @escaping(ApiCallStatus, JSON, String) -> ()){
        let urls = "\(ApiUrl().enablerecurringdebit())"
        let params = ["goal_id": goalId]
        
        print("recurring settings: ",urls)
        print("recurring settings: ",params)
        
        self.callEncrypted(url: urls, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(.FAILED, JSON.null, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, item, "successful")
                    }  else {
                        switch error {
                        case "err.recurringDebitMustBeSetupFirst":
                            completion(.DETAIL, JSON.null, "A recurring debit has not previously been set up.")
                        case "err.goalNotFound":
                            completion(.DETAIL, JSON.null, "Goal not found")
                        case "err.generalError":
                            completion(.DETAIL, JSON.null, "An error has occured.")
                        default:
                            completion(.DETAIL, JSON.null, "An error has occured.")
                        }
                    }
                    
                case 301...499:
                    
                    let detail = item["error"].stringValue
                    var message = "An error has occured processing your request."
                    if detail == self.paymentTypeNotFound {
                        message = ""
                    }
                    completion(.DETAIL, JSON.null, message)
                default:
                    completion(.FAILED, JSON.null, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- update recurring debit data
    func updateRecurringDebitSettings(goalId: Int, frequency: String, amount: Double, paymentMethodId: Int, paymentType: String, completion: @escaping(ApiCallStatus, RecurringDebit?,String) -> ()){
        let urls = "\(ApiUrl().updaterecurringdebit())"
        let params = ["goal_id": goalId, "frequency": frequency, "amount": amount, "payment_method_type": paymentType, "payment_method_id": paymentMethodId] as [String: Any]
        
        print("recurring settings: ",urls)
        print("recurring settings: ",params)
        
        self.callEncrypted(url: urls, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            print("recurring settings: ",response)
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(.FAILED, nil, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        let parseData = self.parseRecurringDebitData(item: item)
                        completion(.SUCCESS, parseData, "successful")
                    }  else {
                        switch error {
                        case "err.recurringDebitSettingsNotFound":
                            completion(.DETAIL, nil, "recurring debit not found for the goal.")
                        case "err.goalNotFound":
                            completion(.DETAIL, nil, "Goal not found")
                        default:
                            completion(.DETAIL, nil, "An error has occured adding your referral.")
                        }
                    }
                    
                case 301...499:
                    
                    let error = item["error"].stringValue
                    switch error {
                    case "err.recurringDebitSettingsNotFound":
                        completion(.DETAIL, nil, "recurring debit not found for the goal.")
                    case "err.goalNotFound":
                        completion(.DETAIL, nil, "Goal not found")
                    default:
                        completion(.DETAIL, nil, "An error has occured adding your referral.")
                    }
                    
                default:
                    completion(.FAILED, nil, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- Force update call
    func forceupdate(versionCode: String, completion: @escaping(JSON, ApiCallStatus, String) -> ()){
        let url = "\(ApiUrl().forceupdate())"
        
        let versionNumber = Double(versionCode)
        let params = ["channel": "iOS", "versionCode": versionNumber!] as [String: Any]
        
        print("force update data: ",url)
        print("force update data: ",versionNumber!)
        print("force update data: ",params)
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerAuth()).responseJSON { (response) in
            
            if response.error != nil {
                //print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                
                completion(JSON.null, .FAILED, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    do {
                        let item = try JSON(data: response.data!)
                        print("success: \(item)")
                        completion(item, .SUCCESS, "successful")
                    }catch let error as NSError {
                        completion(JSON.null, .DETAIL, "\(error.localizedDescription)")
                    }
                    
                case 301...499:
                    
                    do {
                        let item = try JSON(data: response.data!)
                        print("success: \(item)")
                        let detail = item["error"].stringValue
                        let message = "An error has occured processing your request. \(detail)"
                        completion(JSON.null, .DETAIL, message)
                       
                    }catch let error as NSError {
                        completion(JSON.null, .DETAIL, "\(error.localizedDescription)")
                    }
                default:
                    completion(JSON.null, .FAILED, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- Add Participant
    func addParticipant(goalId: Int,customerId: Int, custom_message: String,phone: String,completion: @escaping(JSON,ApiCallStatus,String) -> ()){
        let urls = "\(ApiUrl().addParticipant())"
        let params = ["goal_id": goalId, "customer_id": customerId, "phone": phone,"message":custom_message] as [String : Any]
        
        print("recurring settings: ",urls)
        print("recurring settings: ",params)
        
        self.callEncrypted(url: urls, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            print("recurring settings: ",response)
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(JSON.null, .FAILED, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(item, .SUCCESS, "successful")
                    }  else {
                        completion(JSON.null, .DETAIL, error)
                    }
                    
                case 301...499:
                    
                    let error = item["error"].stringValue
                    completion(JSON.null, .DETAIL, error)
                    
                default:
                    completion(JSON.null, .FAILED, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- All Goal Participants
    func getAllParticipants(goalId: Int, completion: @escaping(ApiCallStatus,[Participants]?,String) -> ()){
        let url = "\(ApiUrl().allGoalParticipant())"
        let params = ["goal_id": goalId]
        
        print("url: ",url)
        print("params: ",params)
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                completion(.FAILED,nil,self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                
                case 200...300:
                    
                    var participant = [Participants]()
                    for itemIn in item["participants"].enumerated() {
                        let partsIn = self.parseParticipants(item: itemIn.element.1)
                        //Participants.save(data: partsIn)
                        participant.append(partsIn)
                    }
                    completion(.SUCCESS,participant,"Data Retrieved Succussfully")
                    
                case 301...499:
                    
                    let detail = item["detail"].stringValue
                    completion(.DETAIL,nil, detail)
                default:
                    completion(.FAILED,nil,self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- delete Participant
    func removeParticipant(goal_id: Int, customer_id: Int, completion: @escaping(ApiCallStatus, JSON, String) -> ()){
        let url = "\(ApiUrl().deleteParticipants())"
        let params = ["goal_id": goal_id, "customer_id":customer_id]
        
        print("url: ",url)
        print("params: ",params)
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                //print("recurring settings: ",datastring!)
                completion(.FAILED, JSON.null, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, item, "successful")
                    }  else {
                        switch error {
                        case "error":
                            completion(.DETAIL, JSON.null, "There was an error")
                        default:
                            completion(.DETAIL, JSON.null, "An error has occured.")
                        }
                    }
                    
                case 301...499:
                    
                    let detail = item["error"].stringValue
                    completion(.DETAIL, JSON.null, detail)
                default:
                    completion(.FAILED, JSON.null, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- Find participant
    func findParticipant(mobileNumber: String, goalId: Int, completion: @escaping(ApiCallStatus, JSON, String) -> ()){
        let url = "\(ApiUrl().findParticipant())"
        let params = ["phone": mobileNumber, "goal_id": goalId] as [String : Any]
        
        print("url: ",url)
        print("params: ",params)
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(.FAILED, JSON.null, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, item, "successful")
                    }  else {
                        
                        switch error {
                        case "errInvalidPhoneNumber":
                            completion(.DETAIL, JSON.null, "The phone number you have provided is invalid.")
                        default:
                            completion(.DETAIL, JSON.null, "An error has occured.")
                        }
                    }
                    
                case 301...499:
                    
                    let error = item["error"].stringValue
                    completion(.DETAIL, JSON.null, error)
                    
                default:
                    completion(.FAILED, JSON.null, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- toggle sharing
    func toggleSharing(goal_id: Int,customer_id: Int,sharing_enabled: Bool, completion: @escaping(ApiCallStatus, JSON, String) -> ()){
        let url = "\(ApiUrl().toggleSharingUrl())"
        let params = ["goal_id": goal_id, "customer_id": customer_id,"sharing_enabled":sharing_enabled] as [String : Any]
        
        print("url: ",url)
        print("params: ",params)
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("recurring settings: ",datastring!)
                completion(.FAILED, JSON.null, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("Status \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("DATA \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, item, "successful")
                    }  else {
                        completion(.DETAIL, JSON.null, "An error has occured.")
                    }
                    
                case 301...499:
                    
                    let error = item["error"].stringValue
                    completion(.DETAIL, JSON.null, error)
                    
                default:
                    completion(.FAILED, JSON.null, self.failureStatus)
                }
            }
        }

    }
    
    func togglebetweenFeatures(customer_id: Int, completion: @escaping(ApiCallStatus, JSON, String) -> ()) {
        let url = "\(ApiUrl().featuresettings())"
        let params = ["customer_id": customer_id]
        
        print("url: ",url)
        print("params: ",params)
        
        self.callEncrypted(url: url, method: .post, parameters: params, headers: headerAuth()) { (response, item) in
            
            if response.error != nil {
                print(response.error?.localizedDescription ?? "")
                let datastring = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                print(datastring ?? "")
                print("toggle settings: ",datastring!)
                completion(.FAILED, JSON.null, self.interntConnectionStatus)
                return
            }
            
            if let status = response.response?.statusCode {
                print("toggle settings: \(status)")
                switch(status){
                case 200...300:
                    //parse success
                    print("toggle settings: \(item)")
                    let error = item["error"].stringValue
                    if error.isEmpty {
                        completion(.SUCCESS, item, "successful")
                    }  else {
                        completion(.DETAIL, JSON.null, "An error has occured.")
                    }
                    
                case 301...499:
                    let error = item["error"].stringValue
                    completion(.DETAIL, JSON.null, error)
                    
                default:
                    completion(.FAILED, JSON.null, self.failureStatus)
                }
            }
        }
    }
    
    //MARK:- parse Participants data
    func parseParticipants(item: JSON) -> Participants {
        
        let participant = Participants()
        participant.id = item["customer_id"].intValue
        participant.name = item["customer_name"].stringValue
        participant.total_contribution = item["totalContributions"].doubleValue
        participant.status = item["status"].stringValue
        participant.phone = item["customer_phone"].stringValue
        participant.date_joined = item["date_invited"].stringValue
        participant.picture = item["picture"].stringValue
        return participant
    }
    
    //MARK:- Parse recurring debit data
    func parseRecurringDebitData(item: JSON) -> RecurringDebit {
        
        let recurring = RecurringDebit()
        
        print("recurring settings: ",item)

        recurring.amount = item["amount"].doubleValue
        recurring.frequency = item["frequency"].stringValue
        recurring.payment_method = item["payment_method"].stringValue
        recurring.id = item["payment_id"].intValue
        recurring.goal_id = item["goal_id"].intValue
        
        RecurringDebit.delete()
        RecurringDebit.save(data: recurring)
        return recurring
    }
    
    //MARK: - parse coach data
    func parseCoachData(item: JSON) -> Coach {
        
        let coacher = Coach()
        
        print(item)
        let rank = item["rank"].stringValue
        let points = item["points"].intValue
        let points_to_next_rank = item["points_to_next_rank"].intValue
        
        let categories = item["categories"].arrayValue
        
        let coachCategory = List<CoachCategories>()
        for coachCat in  categories {
            let cat = self.parseCoachCategories(items: coachCat)
            coachCategory.append(cat)
        }
        coacher.CoachCategories = coachCategory
        
        coacher.points = points
        coacher.points_to_next_rank = points_to_next_rank
        coacher.rank = rank
        
        Coach.save(data: coacher)

        return coacher
    }
    
    //MARK:- parse coach categories
    func parseCoachCategories(items: JSON) -> CoachCategories {
        let coachCategory = CoachCategories()
        var categoryName = ""
        
        print("inside cate: ",items["name"].stringValue)
        categoryName = items["name"].stringValue
        
        let tasks = items["tasks"].arrayValue
        
        let coachTask = List<CoachTask>()
        for coachT in tasks {
            coachTask.append(self.parseCoachTasks(items: coachT))
        }
        coachCategory.CoachTask = coachTask
        
        coachCategory.name = categoryName
        return coachCategory
    }
    
    //MARK:- parse coach tasks
    func parseCoachTasks(items: JSON) -> CoachTask {
        let coachTask = CoachTask()
        
        var taskCode = ""
        var taskDescription = ""
        var taskCompleted = false
        var taskValue = 0
        
        print("task items: ",items)
        
        print("inside task: ", items["description"].stringValue)
        
        taskCode = items["code"].stringValue
        taskValue = items["value"].intValue
        taskDescription = items["description"].stringValue
        taskCompleted = items["completed"].boolValue
        
        coachTask.id = taskCode
        coachTask.taskValue = taskValue
        coachTask.taskDescription = taskDescription
        coachTask.taskCompleted = taskCompleted
        
        return coachTask
    }
    
    //MARK:- parse token
    func parseToken(item: JSON, number:String) -> User {
        
        print("Parsing token")
        print(item)
        let customerId = item["customer_id"].intValue
        let token = item["token"].stringValue
        _ = item["token_expiry_date"].stringValue
        _ = item["refresh_token"].stringValue
        
        //check for documents availiability
        let hasSelfie = item["has_selfie"].boolValue
        let hasExtraKyc = item["has_extra_kyc"].boolValue
        let hasAddress = item["has_address"].boolValue
        let hasDocuments = item["has_id_document"].boolValue
        
        print("user selfie: ",hasSelfie)
        
        let user = User.getUser()!
        try! self.realm.write({
            user.token = token
            user.cutomerId = customerId
            
            //save document availiability status
            user.hasSelfie = hasSelfie
            user.hasExtraKyc = hasExtraKyc
            user.hasAddress = hasAddress
            user.hasIdDocument = hasDocuments
            
            if !number.isEmpty {
                user.phone = number
            }
        })
        User.save(data: user)
        return user
    }
    
    //MARK:- parse slim customer
    func parseSlimCustomer(item: JSON) -> User {
        let firstName = item["first_name"].stringValue
        let lastName = item["last_name"].stringValue
        let otherNames = item["other_names"].stringValue
        let birthDate = item["birth_date"].stringValue
        let email = item["email"].stringValue
        //data operation
        User.updateUser(firstName: firstName, lastName: lastName, otherName: otherNames, email: email, DOB: birthDate)
        return User.getUser()!
    }
    
    
    //MARK:- parse contact
    func parseContact(item: JSON) -> FavouriteContact {
        let id = item["id"].intValue
        let name = item["name"].stringValue
        let identifier = item["identifier"].stringValue
        let type = item["network"].stringValue
        let contact = FavouriteContact()
        contact.id = id
        contact.name = name
        contact.number = identifier
        contact.network = type
        return contact
    }
    
    //MARK:- parse goal category
    func parseGoalWithInOut(item: JSON,id: Int) -> Goal {
        let totalCredit = item["total_credit"].doubleValue
        let targetAmount = item["target_amount"].doubleValue
        let balance = item["balance"].doubleValue
        let currency = item["currency"].stringValue
        let image = item["cover_image"].stringValue
        let name = item["goal_name"].stringValue
        let totalDebit = item["total_debit"].doubleValue
        
        let goal = Goal()
        goal.id = id
        goal.totalCredit = totalCredit
        goal.totalDebit = totalDebit
        goal.image = image
        goal.targetAmount = targetAmount
        goal.name = name
        goal.balance = balance
        goal.currency = currency
        
        return goal
    }
    
    
    //MARK:- parse goal category
    func parseGoalCategory(item: JSON) -> GoalCategory {
        let id = item["category_id"].intValue
        let name = item["name"].stringValue
        let image = item["cover_image"].stringValue
      
        let category = GoalCategory()
        category.categoryId = id
        category.name = name
        category.image = image
        return category
    }
    
    //MARK:- parse goal category
    func parseGoal(item: JSON) -> Goal {
        let id = item["goal_id"].intValue
        let name = item["name"].stringValue
        let image = item["cover_image"].stringValue
        let balance = item["balance"].doubleValue
        let target = item["target_amount"].doubleValue
        let assistEnabled = item["goalAssist_enabled"].boolValue
        let _ = item["goalPlus_enabled"].boolValue
        let currency = item["currency"].stringValue
        let dueDate = item["due_date"].stringValue
        let status = item["status"].stringValue
        let awaiting_invesment = item["awaiting_invesment"].doubleValue
        let invested = item["invested"].doubleValue
       
        let total_withdrawals = item["total_withdrawals"].doubleValue
        let total_deposits = item["total_deposits"].doubleValue
        
        let is_shared = item["is_shared"].boolValue
        let is_goal_owner = item["is_goal_owner"].boolValue
        let total_participants = item["total_participants"].intValue
        let owner_picture = item["owner_picture"].stringValue
        let owner_name = item["owner_name"].stringValue
        var imagesArrayBox = [String]()
        var imageOne = ""
        var imageTwo = ""
        var imageThree = ""
        
        let participantImages = item["participants_pictures"].arrayValue
        for image in participantImages {
            imagesArrayBox.append("\(image)")
        }
        
        if imagesArrayBox.count > 0 && imagesArrayBox.count < 2 {
            imageOne = imagesArrayBox[0]
        }else if imagesArrayBox.count == 2 {
            imageOne = imagesArrayBox[0]
            imageTwo = imagesArrayBox[1]
        }else if imagesArrayBox.count == 3 {
            imageOne = imagesArrayBox[0]
            imageTwo = imagesArrayBox[1]
            imageThree = imagesArrayBox[2]
        }else {
            print("nothing in array to print")
        }
        
        print("print participant images: ",imageOne)
        print("print participant images: ",imageTwo)
        print("print participant images: ",imageThree)
        
        let goal = Goal()
        goal.id = id
        goal.dueDate = dueDate
        goal.image = image
        goal.assistModeEnabled = assistEnabled
        goal.targetAmount = target
        goal.name = name
        goal.balance = balance
        goal.status = status
        goal.currency = currency
        goal.awaiting_invesment = awaiting_invesment
        goal.invested = invested
        goal.total_deposits = total_deposits
        goal.total_withdrawals = total_withdrawals
        
        goal.is_shared = is_shared
        goal.total_participants = total_participants
        goal.is_goal_owner = is_goal_owner
        
        goal.owner_picture = owner_picture
        goal.owner_name = owner_name
        goal.image1 = imageOne
        goal.image2 = imageTwo
        goal.image3 = imageThree
        
        if let nextItem = item["next_of_kin"].dictionary {
            if let contact = nextItem["phone"]?.stringValue {
                goal.nextKinContact = contact
            }
            if let nextOfKinName = nextItem["name"]?.stringValue {
                goal.nextOfKinName = nextOfKinName
            }
            
            if let nextOfKinEmail = nextItem["email"]?.stringValue {
                goal.nextKinEmail = nextOfKinEmail
            }
            if let relationship = nextItem["relationship"]?.stringValue {
                goal.nextOfKinrelationship = relationship
            }
        }
    
        if let destination = item["withdrawal_destination"].dictionary {
            if let withdrawalDestinationIdentifier = destination["identifier"]?.stringValue {
                goal.withdrawalDestinationIdentifier = withdrawalDestinationIdentifier
            }
            
            if let withdrawalDestinationIssuer = destination["issuer"]?.stringValue {
                goal.withdrawalDestinationIssuer = withdrawalDestinationIssuer
            }
        }
        
        return goal
    }
    
    //MARK:- parse account goal category
    func parseAccountGoal(item: JSON) -> Goal {
        let id = item["goal_id"].intValue
        let name = item["goal_name"].stringValue
        let image = item["cover_image"].stringValue
        let balance = item["goal_balance"].doubleValue
        let target = item["target_amount"].doubleValue
        let assistEnabled = item["goalAssist_enabled"].boolValue
        let _ = item["goalPlus_enabled"].boolValue
        let currency = item["currency"].stringValue
        let dueDate = item["due_date"].stringValue
        let status = item["status"].stringValue
        
        let goal = Goal()
        goal.id = id
        goal.dueDate = dueDate
        goal.image = image
        goal.assistModeEnabled = assistEnabled
        goal.targetAmount = target
        goal.name = name
        
        goal.balance = balance
        goal.status = status
        goal.currency = currency
        
        return goal
    }
    
    
    //MARK:- update user balance
    func updateBalance(item: JSON) -> User {
        let currency = item["currency"].stringValue
        let balance = item["balance"].doubleValue
        let user = User.getUser()!
        try! self.realm.write({
            user.currency = currency
            user.balance = balance
        })
        //data operation
        User.updateUser(data: user)
        
        return user
    }
    
    
    //MARK:- parse destinations
    func parseGoalDestination(item: JSON) -> GoalDestination {
        let issuer = item["issuer"].stringValue
        let id = item["id"].intValue
        let identifier = item["identifier"].stringValue
        let type = item["type"].stringValue
        
        
        let destination = GoalDestination()
        destination.id = id
        destination.issuer = issuer
        destination.identifier = identifier
        destination.type = type
        return destination
    }
    
    //MARK:- parse wallet
    func parseWallet(item: JSON) -> Wallet {
        let name = item["payment_name"].stringValue
        let id = item["payment_wallet_id"].intValue
        let number = item["payment_number"].stringValue
        let bank = item["payment_bank"].stringValue
        let paymentType = item["payment_type"].stringValue
        let expiry = item["payment_expiry"].stringValue
        let paymentNetwork = item["payment_network"].stringValue
        let verified = item["payment_verified"].stringValue
        
        let wallet = Wallet()
        wallet.id = id
        wallet.name = name
        wallet.number = number
        wallet.bank = bank
        wallet.expiry = expiry
        if paymentNetwork == "" {
            wallet.icon = "VISA"
            wallet.type = "card"
        } else {
            wallet.type = paymentNetwork
            wallet.icon = paymentNetwork
        }
        
        wallet.paymentType = paymentType
        wallet.verification_status = verified
        
        if wallet.type == PaymentOptionType.card.rawValue {
            if verified == "VERIFIED"  {
                wallet.payment_verified = true
            }
        } else {
            wallet.payment_verified = true
        }
        
        return wallet
    }
    
    //MARK:- parse friend
    func parseFriend(item: JSON) -> Friend {
        let friend = Friend()
        friend.inviteeName = item["invitee_name"].stringValue
        friend.inviteeContact = item["invitee_contact"].stringValue
        friend.inviteeSelfieUrl = item["invitee_selfie_url"].stringValue
        return friend
    }
    
    //MARK:- parse transaction
    func parseTransaction(item: JSON) -> TransactionHistory {
        let transactionHistory = TransactionHistory()
        let datetime = item["date"].stringValue
        let amount = item["amount"].doubleValue
        let status = item["status"].stringValue
        let type = item["target_issuer"].stringValue
        let targetAccount = item["target_account"].stringValue
        let sourceAccount = item["source_account"].stringValue
        let targetImage = item["target_image"].stringValue
        
        let reference = item["reference"].stringValue
        let fees = item["fees"].doubleValue
        let sourceImage = item["source_image"].stringValue
        
        let totalTransactionAmount = item["total_transaction_amount"].doubleValue
        let sourceAccountType = item["source_issuer"].stringValue
        
        transactionHistory.amount = amount
        transactionHistory.datePerformed = datetime
        transactionHistory.type = type
        transactionHistory.sourceAccount = type
        transactionHistory.status = status
        transactionHistory.senderProfile = sourceImage
        transactionHistory.senderName = sourceAccount
        transactionHistory.recipientName = targetAccount
        transactionHistory.recipientNetwork = targetImage
        transactionHistory.recipientProfile = targetImage
        transactionHistory.sourceAccount = sourceAccountType
        transactionHistory.fees = fees
        transactionHistory.message = reference
        transactionHistory.totalAmount = totalTransactionAmount
        return transactionHistory
    }
    
    //MARK:- parse transaction
    func parseStatementTransaction(item: JSON) -> TransactionHistory {
        let transactionHistory = TransactionHistory()
        let id = item["transactionId"].intValue
        let datetime = item["transaction_date"].stringValue
        let amount = item["total_amount"].doubleValue
        let status = item["status"].stringValue
        let type = item["target_issuer"].stringValue
        let targetAccount = item["target_account"].stringValue
        let targetImage = item["target_image"].stringValue
        
        transactionHistory.amount = amount
        transactionHistory.datePerformed = datetime
        transactionHistory.type = type
        transactionHistory.sourceAccount = type
        transactionHistory.status = status
        transactionHistory.id = id
        transactionHistory.recipientProfile = targetImage
        transactionHistory.recipientName = targetAccount
        return transactionHistory
    }
    
    //MARK:- parse goal transaction
    func parseGoalTransaction(item: JSON) -> TransactionHistory {
        let transactionHistory = TransactionHistory()
        let id = item["ref_number"].intValue
        let datetime = item["posting_time"].stringValue
        let amount = item["amount"].doubleValue
        let totalTransactionAmount = item["total_transaction_amount"].doubleValue
        let message = item["details"].stringValue
        let fees = item["fees"].doubleValue
        let direction = item["direction"].stringValue
        
        
        transactionHistory.amount = amount
        transactionHistory.fees = fees
        transactionHistory.totalAmount = totalTransactionAmount
        transactionHistory.datePerformed = datetime
        transactionHistory.type = direction
        transactionHistory.sourceAccount = message
        transactionHistory.message = message
        transactionHistory.id = id
        return transactionHistory
    }
    
    //MARK:- parse goal actiivity
    func parseGoalActivity(item: JSON) -> GoalActivity {
        let activity = GoalActivity()
        let currency = item["currency"].stringValue
        let datetime = item["date"].stringValue
        let amount = item["amount"].doubleValue
        let entryIssuer = item["entry_issuer"].stringValue
        let entryType = item["entry_type"].stringValue

        activity.amount = amount
        activity.date = datetime
        activity.name = entryType
        activity.issuer = entryIssuer
        activity.currency = currency
        activity.issuer = entryIssuer
        return activity
    }
    
    //MARK:- parse notification
    func parseNotification(item: JSON) -> Notification {
        let notification = Notification()
        let datetime = item["datetime"].stringValue.formateNotificationDate()
        let date = item["datetime"].stringValue
        let title = item["title"].stringValue
        let body = item["description"].stringValue
        let status = item["status"].stringValue
        let id = item["id"].intValue
        
        notification.id = id
        notification.time = datetime
        notification.dateTime = date
        notification.isRead = status == "R"
        notification.message = body
        notification.type = title
        return notification
    }
    
    //MARK:- parse customer
    func parseCustomer(item: JSON) -> User {
        let firstName = item["first_name"].stringValue
        let lastName = item["last_name"].stringValue
        let otherNames = item["other_names"].stringValue
        let occupation = item["occupation"].stringValue
        let birthDate = item["birth_date"].stringValue
        let sex = item["sex"].stringValue
        let pictureUrl = item["picture_url"].stringValue
        let email = item["email"].stringValue
        let referraleCode = item["referral_code"].stringValue
        let acccountNumber = item["accounts"].arrayValue[0]["account_number"].stringValue
        let statusCode = item["accounts"].arrayValue[0]["status_code"].stringValue
        let accountId = item["accounts"].arrayValue[0]["id"].intValue
        
        let user = User.getUser()!
        
        try! self.realm.write({
            user.firstName = firstName
            user.lastName = lastName
            user.otherName = otherNames
            user.occupation = occupation
            user.birthDate = birthDate
            user.email = email
            user.sex = sex
            user.picture = pictureUrl
            user.referralCode = referraleCode
            user.accountId = accountId
            user.accountNumber = acccountNumber
            user.accountStatus = statusCode
        })
        
        //data operation
        User.updateUser(data: user)
        
        return user
    }
    
    func parseBalanceUpdate(item: JSON)  {
        let balance = item["account"]["balance"].doubleValue
        let _ = item["account"]["account_id"].intValue
        let _ = item["account"]["currency"].stringValue
        
        let _ = item["goals"]["goal_count"].intValue
        let goalBalance = item["goals"]["total_balance"].doubleValue
        let _ = item["goals"]["currency"].stringValue
        let hasNotifications = item["has_notifications"].boolValue
    
        User.updateUserBalance(goalBalance: goalBalance, accountBalance: balance,status: hasNotifications)
    }
    //MARK:- parse customer
    func parseCustomerOverview(item: JSON) -> User {
        
        print("USER IN \(item)")
        
        let firstName = item["first_name"].stringValue
        let lastName = item["last_name"].stringValue
        let otherNames = item["other_names"].stringValue
        let birthDate = item["birth_date"].stringValue
        let sex = item["sex"].stringValue
        let pictureUrl = item["selfie_url"].stringValue
        let email = item["email"].stringValue
        let phone = item["phone"].stringValue
        
        let acccountNumber = item["account"]["account_number"].stringValue
        let balance = item["account"]["balance"].doubleValue
        let accountId = item["account"]["account_id"].intValue
        let currency = item["account"]["currency"].stringValue
        
        let invite_code = item["invite_code"].stringValue
        
        let goalsNumber = item["goals"]["goal_count"].intValue
        let goalBalance = item["goals"]["total_balance"].doubleValue
        let goalCurrency = item["goals"]["currency"].stringValue
        
        let user = User.getUser()!
        try! self.realm.write({
            user.firstName = firstName
            user.lastName = lastName
            user.otherName = otherNames
            user.birthDate = birthDate
            user.email = email
            user.sex = sex
            user.picture = pictureUrl
            user.accountId = accountId
            user.accountNumber = acccountNumber
            user.goalCurrency = goalCurrency
            user.totalGoals = goalsNumber
            user.goalBalance = goalBalance
            user.currency = currency
            user.balance = balance
            user.phone = phone
            
            if invite_code != "" {
                user.has_used_invite_code = true
            }
        })
        
        //data operation
        User.updateUser(data: user)
        
        return user
    }
    
    private func errorToHumanString(error: String) -> String {
        let errors = ["nextOfKinPhoneInvalid": "The phone number for the next of kin is invalid"]
        
        if let message = errors[error] {
            return message
        }
        return error
    }
}
