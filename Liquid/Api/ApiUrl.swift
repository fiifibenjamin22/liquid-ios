//
//  ApiUrl.swift
//  Liquid
//
//  Created by Benjamin Acquah on 14/03/2018.
//  Copyright © 2018 Liquid Ltd. All rights reserved.
//

import Foundation


class ApiUrl {
   
//    #if DEBUG
//    let baseUrl  = "http://34.204.39.191:9000/v1/"
//    #else
//    let baseUrl = "http://api.liquid.com.gh/v1/"
//    #endif
    //MARK:- base url
    //let baseUrl = "http://api.liquid.com.gh/v1/"
    let baseUrl = "https://api.liquid.com.gh/v1/"
    let baseUrlSafe = "https://api.liquid.com.gh/v1/"
    let baseUrlHome = "https://api.liquid.com.gh/v1/"
    
    func registerDevice() -> String {
        return "\(baseUrl)device/register"
    }
    
    func checkNumber() -> String {
        return "\(baseUrl)signup/check-phone"
    }
    
    func checkAccountType() -> String {
        return "\(baseUrlSafe)account/account-type"
    }
    
    func registerPhone() -> String {
        return "\(baseUrl)signup/register-phone"
    }
    
    func confirmNumber() -> String {
        return "\(baseUrl)signup/verify-phone"
    }
    
    func refreshToken() -> String {
        return "\(baseUrl)auth/refresh-token"
    }
    
    func updateDeviceGcm() -> String {
        return "\(baseUrl)device/update-gcm-token"
    }
    
    func logOut() -> String {
        return "\(baseUrl)customer/logout"
    }
    
    func triggerPassword() -> String {
        return "\(baseUrl)customer/trigger-password-reset"
    }
    
    func updateProfileImage() -> String {
        return "\(baseUrl)customer/update-selfie"
    }
    
    func changePassword() -> String {
        return "\(baseUrl)customer/change-password"
    }
    
    func initPasswordReset() -> String {
        return "\(baseUrl)customer/init-forgot-password"
    }
    
    func confirmForgottenPasswordPasscode() -> String {
        return "\(baseUrl)customer/confirm-forgot-password-code"
    }

    func triggerEmail() -> String {
        return "\(baseUrlSafe)account/update-email"
    }
    
    func changeEmail() -> String {
        return "\(baseUrlSafe)account/verify-email"
    }
    
    func checkEmail() -> String {
        return "\(baseUrlSafe)signup/check-email"
    }
    
    func resetPassword() -> String {
        return "\(baseUrl)customer/reset-forgot-password"
    }
    
    func login() -> String {
        return "\(baseUrl)auth/login"
    }
    
    func createCustomer() -> String {
        return "\(baseUrl)customer/new"
    }
    
    func saveUserAddress() -> String {
        return "\(baseUrl)customer/save-address"
    }
    
    func saveSelfie() -> String {
        return "\(baseUrl)customer/save-selfie"
    }
    
    func saveDocument() -> String {
        return "\(baseUrl)customer/save-id-doc"
    }
    
    func saveKYCInformation() -> String {
        return "\(baseUrl)customer/save-kyc-info"
    }
    
    func saveInviteCode() -> String {
        return "\(baseUrl)customer/save-invite-code"
    }
    
    func acceptTerms() -> String {
        return "\(baseUrl)customer/accept_tc"
    }
    
    func getCustomer() -> String {
        return "\(baseUrl)customer/profile"
    }
    
    func addReferral() -> String {
        return "\(baseUrl)customer/save-invite-code"
    }
    
    func getCustomerOverview() -> String {
        return "\(baseUrlSafe)account/overview"
    }
    
    func getEditOverview() -> String {
        return "\(baseUrlSafe)account/edit-user-overview"
    }
    
    func getAccountBalance() -> String {
        return "\(baseUrlSafe)account/details"
    }
    
    func documentTypes() -> String {
        return "\(baseUrl)static/id-types"
    }
    
    func occupationList() -> String {
        return "\(baseUrl)static/occupation"
    }
    
    func goalCategory() -> String {
        return "\(baseUrlSafe)savings/categories"
    }

    func goals() -> String {
        return "\(baseUrlSafe)savings/goals"
    }
    
    func newGoal() -> String {
        return "\(baseUrlSafe)savings/new-goal"
    }
    
    func goalWithdrawalDestination() -> String {
        return "\(baseUrlSafe)savings/add-withdraw-destination"
    }
    
    func goalWithdrawalDestinationBanks() -> String {
        return "\(baseUrlSafe)savings/list-banks"
    }
    
    func listWithdrawalDestination() -> String {
        return "\(baseUrlSafe)savings/list-withdraw-destinations"
    }

    func nextOfKinRelationships() -> String {
        return "\(baseUrlSafe)savings/list-next-of-kin-relationships"
    }
    
    func terminateGoal() -> String {
        return "\(baseUrlSafe)savings/terminate-goal"
    }
    
    func editGoal() -> String {
        return "\(baseUrlSafe)savings/edit-goal"
    }
    
    func updateGoalNextOfKin() -> String {
        return "\(baseUrlSafe)savings/update-next-of-kin"
    }
    
    func getAGoal() -> String {
        return "\(baseUrlSafe)savings/fetch-goal"
    }
    
    func getGoalTransactions() -> String {
        return "\(baseUrlSafe)savings/list-goal-transactions"
    }
    
    func getTransacitons() -> String {
        return "\(baseUrlSafe)account/transaction-history-overview"
    }
    
    func goalAssist() -> String {
        return "\(baseUrlSafe)savings/goal-assist-settings"
    }
    
    func newGoalAssist() -> String {
        return "\(baseUrlSafe)savings/update-goal-assist-settings"
    }
    
    func transactionDetail() -> String {
        return "\(baseUrlSafe)account/transaction-history-detail"
    }
    
    func listPaymentOptions() -> String {
        return "\(baseUrlSafe)account/payment-methods/list"
    }
    
    func saveContact() -> String {
        return "\(baseUrlSafe)transaction/contacts/create"
    }
    
    func removeContact() -> String {
        return "\(baseUrlSafe)transaction/contacts/delete"
    }
    
    func contacts() -> String {
        return "\(baseUrlSafe)transaction/contacts/list"
    }
    
    func addPaymentOption() -> String {
        return "\(baseUrlSafe)account/payment-methods/add-new"
    }
    
    func initCardVerification() -> String {
        return "\(baseUrlSafe)account/initiate-card-verification"
    }
    
    func verifyCard() -> String {
        return "\(baseUrlSafe)account/confirm-card-verification"
    }
    
    func microCardDebit() -> String {
        return "\(baseUrlSafe)transaction/micro/debit"
    }
    
    func verifyDebitAmount() -> String {
        return "\(baseUrlSafe)transaction/micro/verify"
    }
    
    func getReferralCode() -> String {
        return "\(baseUrlSafe)account/referral-code"
    }
    
    func getReferrals() -> String {
        return "\(baseUrlSafe)account/referrals"
    }
    
    func fundAccount() -> String {
        return "\(baseUrlSafe)account/nt-add-funds"
    }
    
    func notifications() -> String {
        return "\(baseUrlSafe)notifications/list"
    }

    func markNotificationsAsRead() -> String {
        return "\(baseUrlSafe)notifications/read"
    }
    
    func notificationsDelete() -> String {
        return "\(baseUrlSafe)notifications/delete"
    }
    
    func initTransaction() -> String {
        return "\(baseUrlHome)transaction/payment/request-fees"
    }
    
    //MARK: confirm transactions
    func confirmTransaction() -> String {
        return "\(baseUrlHome)transaction/confirm"
    }
    
    //MARK: confirm transactions
    func billDetail() -> String {
        return "\(baseUrlHome)transaction/payment/query-bill-details"
    }
    
    //MARK: get the missing steps
    func getAccoutMissingSteps() -> String {
        return "\(baseUrlHome)account/missing-steps"
    }
    
    func getAccountStatementOverview() -> String {
        return "\(baseUrlHome)account/statement-overview"
    }

    //MARK: account statment
    func getAccountStatement() -> String {
        return "\(baseUrlSafe)account/statement"
    }
    
    //MARK: goal statment
    func goalAccountStatement() -> String {
        return "\(baseUrlSafe)account/goal-statement"
    }
    
    //MARK: export the statment
    func exportStatement() -> String {
        return "\(baseUrlSafe)account/export-goal"
    }
    
    //MARK: remove the payment method
    func removeWallet() -> String {
        return "\(baseUrlSafe)account/payment-methods/delete"
    }
    
    //MARK: change number step one, init number changing
    func changeNumberStepOne() -> String {
        return "\(baseUrl)signup/update-phonenumber"
    }
    
    //MARK: change number step two, confirm the number change with code
    func changeNumberStepTwo() -> String {
        return "\(baseUrl)signup/confirm-phonenumber-update"
    }
    
    //MARK: save passcode
    func enablePasscode() -> String {
        return "\(baseUrlSafe)account/enable-passcode"
    }
    
    //MARK: update passcode
    func updatePasscode() -> String {
        return "\(baseUrlSafe)account/update-passcode"
    }
    
    //MARK: check passcode status
    func checkPasscodeStatus() -> String {
        return "\(baseUrlSafe)account/check-passcode-status"
    }
    
    //MARK: verify passcode
    func verifyPasscode() -> String {
        return "\(baseUrlSafe)account/verify-passcode"
    }
    
    //MARK: disable passcode
    func disablePasscode() -> String {
        return "\(baseUrlSafe)account/disable-passcode"
    }
    
    //MARK: get coach data
    func coachDataUrl() -> String {
        return "\(baseUrlSafe)account/coach"
    }
    
    //MARK:- check invitation code
    func checkInvitaionCode() -> String {
        return "\(baseUrlSafe)customer/check-invite-code"
    }
    
    //MARK:- bank transaction Voucher
    func bankVoucherURL() -> String {
        return "\(baseUrlSafe)transaction/bank-transfer/save-voucher"
    }
    
    //MARK:- share transfer voucher
    func shareTransferVoucherUrl() -> String {
        return "\(baseUrlSafe)transaction/bank-transfer/share-voucher"
    }
    
    //MARK:- setup recurring debit
    func setuprecurringdebit() -> String {
        return "\(baseUrlSafe)savings/set-recurring-debit"
    }
    
    //MARK:- update recurring debit setup
    func updaterecurringdebit() -> String {
        return "\(baseUrlSafe)savings/update-recurring-debit"
    }
    
    //MARK:- Disable Recurring Debits
    func disablerecurringdebit() -> String {
        return "\(baseUrlSafe)savings/disable-recurring-debit"
    }
    
    //MARK:- Enable Recurring Debits
    func enablerecurringdebit() -> String {
        return "\(baseUrlSafe)savings/enable-recurring-debit"
    }
    
    //MARK:- Get settings
    func getrecurringdebitsettings() -> String {
        return "\(baseUrlSafe)savings/get-recurring-debit"
    }
    
    //MARK:- (Manually) Set Next Debit Date
    func setnextdebitdate() -> String {
        return "\(baseUrlSafe)savings/set-next-debit-date"
    }
    
    //MARK:- force update
    func forceupdate() -> String {
        return "\(baseUrlSafe)device/app-version-supported"
    }
    
    //MARK:- Add Goal Participant
    func addParticipant() -> String {
        return "\(baseUrlSafe)savings/add-participant"
    }
    
    //MARK:- All Goal Participants
    func allGoalParticipant() -> String {
        return "\(baseUrlSafe)savings/list-participants"
    }
    
    //MARK:- find participant by phone number
    func findParticipant() -> String {
        return "\(baseUrlSafe)savings/get-customer-by-phone"
    }
    
    //MARK:- delete participant
    func deleteParticipants() -> String {
        return "\(baseUrlSafe)savings/delete-participant"
    }
    
    //MARK:- toggle sharing for a participant
    func toggleSharingUrl() -> String {
        return "\(baseUrlSafe)savings/toggle-sharing"
    }
    
    //feature settings
    func featuresettings() -> String {
        return "\(baseUrlSafe)account/features"
    }
}
